package com.matchstixapp;

public class AppConfig {
	public static final boolean DEV_BUILD = false;
	public static final boolean ENABLE_FILE_LOGS = false;
	public static final boolean ENABLE_LOGS = true;
	public static final String PUSH_APP_NAME = DEV_BUILD ? "matchstix_dev" : "matchstix_prod";
	public static final String PUSH_APP_KEY = DEV_BUILD ? "492444c703e4d30e9940156da340ba9e"
			: "492444c703e4d30e9940156da340ba9e";
	//public static final String PUSH_APP_NUMBER = DEV_BUILD ? "720783729572"	: "720783729572";
	public static final String PUSH_APP_NUMBER = DEV_BUILD ? "308163470825"	: "308163470825";
}
