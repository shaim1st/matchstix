package com.matchstixapp.pojo;

import com.craterzone.cz_commons_lib.EncodingDecodingUtil;
import com.matchstixapp.utils.Constants;

public class FacebookGraphUser {
	
	private String id;
	private String birthday;
	private String gender;
	private String email;
	private String name;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getBirthday() {
		return birthday;
	}
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		if (gender.equalsIgnoreCase("male")) {
			this.gender = Constants.MALE;
		} else {
			this.gender = Constants.FEMALE;
		}
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getName() {
		return EncodingDecodingUtil.decodeString(name);
	}
	public void setName(String name) {
		this.name = EncodingDecodingUtil.encodeString(name);
	}

}
