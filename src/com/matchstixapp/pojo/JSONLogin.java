package com.matchstixapp.pojo;

public class JSONLogin {
	
	private String token;
	private long userId;
	private JSONSettings discoverySettings;
	private String socialType;
	private String password;
	private String about;
	private boolean imageSync;
	
	
	public String getAbout() {
		return about;
	}

	public void setAbout(String about) {
		this.about = about;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public JSONSettings getDiscoverySettings() {
		return discoverySettings;
	}

	public void setDiscoverySettings(JSONSettings discoverySettings) {
		this.discoverySettings = discoverySettings;
	}

	public String getSocialType() {
		return socialType;
	}

	public void setSocialType(String socialType) {
		this.socialType = socialType;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isImageSync() {
		return imageSync;
	}

	public void setImageSync(boolean imageSync) {
		this.imageSync = imageSync;
	}
	
	

}
