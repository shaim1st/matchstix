package com.matchstixapp.pojo;

import java.util.ArrayList;

public class JSONSignUp {
	private String socialURL;
	private String emailId;
	private ArrayList<String> friends = new ArrayList<String>();
	private ArrayList<JSONInterestList> interestList = new ArrayList<JSONInterestList>();
	private String socialType;
	private JSONDeviceInfo deviceInfo;
	private String socialId;
	private JSONUserProfile profile;
	
	public JSONSignUp() {
	}

	public JSONSignUp(String socialURL, String emailId, String socialType,
			JSONDeviceInfo deviceInfo, String socialId, JSONUserProfile profile) {
		this.socialURL = socialURL;
		this.emailId = emailId;
		this.socialType = socialType;
		this.deviceInfo = deviceInfo;
		this.socialId = socialId;
		this.profile = profile;
	}

	public String getSocialURL() {
		return socialURL;
	}

	public void setSocialURL(String socialURL) {
		this.socialURL = socialURL;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public ArrayList<String> getFriends() {
		return friends;
	}

	public void setFriends(ArrayList<String> friends) {
		this.friends = friends;
	}

	public ArrayList<JSONInterestList> getInterestList() {
		return interestList;
	}

	public void setInterestList(ArrayList<JSONInterestList> interestList) {
		this.interestList = interestList;
	}

	public String getSocialType() {
		return socialType;
	}

	public void setSocialType(String socialType) {
		this.socialType = socialType;
	}

	public JSONDeviceInfo getDeviceInfo() {
		return deviceInfo;
	}

	public void setDeviceInfo(JSONDeviceInfo deviceInfo) {
		this.deviceInfo = deviceInfo;
	}

	public String getSocialId() {
		return socialId;
	}

	public void setSocialId(String socialId) {
		this.socialId = socialId;
	}

	public JSONUserProfile getProfile() {
		return profile;
	}

	public void setProfile(JSONUserProfile profile) {
		this.profile = profile;
	}

}
