package com.matchstixapp.pojo;

import com.craterzone.cz_commons_lib.EncodingDecodingUtil;

public class JSONUserProfile {
	private String lastName;
	private int age;
	private String aboutUser;
	private String gender;
	private double lng;
	private String firstName;
	private double lat;

	public JSONUserProfile() {
	}

	public JSONUserProfile( String firstName, String lastName,
			int age, String gender, String aboutUser) {
		this.lastName = lastName;
		this.age = age;
		this.aboutUser = aboutUser;
		this.gender = gender;
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getAboutUser() {
		return aboutUser;
	}

	public void setAboutUser(String aboutUser) {
		this.aboutUser = aboutUser;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public double getLng() {
		return lng;
	}

	public void setLng(double lng) {
		this.lng = lng;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

}
