package com.matchstixapp.pojo;

public class JSONUpdateAbout {
	private String message;

	public JSONUpdateAbout(String message) {
		super();
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

}
