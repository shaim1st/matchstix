package com.matchstixapp.pojo;

public class JSONInterestList {
    private String interestId;
    private String interestName;
	public String getInterestId() {
		return interestId;
	}
	public void setInterestId(String interestId) {
		this.interestId = interestId;
	}
	public JSONInterestList() {
		super();
		// TODO Auto-generated constructor stub
	}
	public JSONInterestList(String interestId, String interestName) {
		super();
		this.interestId = interestId;
		this.interestName = interestName;
	}
	public String getInterestName() {
		return interestName;
	}
	public void setInterestName(String interestName) {
		this.interestName = interestName;
	}
    
    


}
