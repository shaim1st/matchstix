package com.matchstixapp.pojo;

public class JSONDeviceInfo {
	private String deviceType;
	private String deviceToken;
	private boolean prod;
	private String xmppResource;
	
	public JSONDeviceInfo() {
		super();
	}

	public JSONDeviceInfo(String deviceType, String deviceToken,boolean prod, String xmppResource) {
		super();
		this.deviceType = deviceType;
		this.deviceToken = deviceToken;
		this.prod = prod;
		this.xmppResource = xmppResource;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	public String getDeviceToken() {
		return deviceToken;
	}

	public void setDeviceToken(String deviceToken) {
		this.deviceToken = deviceToken;
	}

	public boolean isProd() {
		return prod;
	}

	public void setProd(boolean prod) {
		this.prod = prod;
	}

	public String getXmppResource() {
		return xmppResource;
	}

	public void setXmppResource(String xmppResource) {
		this.xmppResource = xmppResource;
	}
}
