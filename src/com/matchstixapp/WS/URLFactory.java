package com.matchstixapp.WS;

import com.matchstixapp.utils.Constants;

class URLFactory implements URLConstants {

		
	static String createSignupURL(){
		StringBuilder sb = new StringBuilder(URL_API_USER);
		return sb.toString();
	}
	
	static String registerPushTokenURL(long userId){
		StringBuilder sb = new StringBuilder(URL_API_USER);
		sb.append(userId);
		sb.append("/device");
		return sb.toString();
	}
	
	static String createAddOrUpdateImageURL(long userId){
		StringBuilder sb = new StringBuilder(URL_API_USER);
		sb.append(userId);
		sb.append(PATH_IMAGES);
		return sb.toString();
	}
	static String createGetSuggestionsURL(long userId,int count){
		StringBuilder sb = new StringBuilder(URL_API_USER);
		sb.append(userId);
		sb.append(PATH_SUGGESTIONS);
		sb.append('?');
		sb.append("count=");
		sb.append(count);
		return sb.toString();
	}
	static String createLikeUserURL(long userId){
		StringBuilder sb = new StringBuilder(URL_API_USER);
		sb.append(userId);
		sb.append(PATH_LIKE);
		return sb.toString();
	}
	static String createDislikeUserURL(long userId){
		StringBuilder sb = new StringBuilder(URL_API_USER);
		sb.append(userId);
		sb.append(PATH_DISLIKE);
		return sb.toString();
	}
	static String creatediscoverysettingsURL(long userId){
		StringBuilder sb = new StringBuilder(URL_API_USER);
		sb.append(userId);
		sb.append(PATH_DISCOVERY_SETTING);
		return sb.toString();
	}
	static String createUpdateUserAboutMeURL(long userId){
		StringBuilder sb = new StringBuilder(URL_API_USER);
		sb.append(userId);
		sb.append(PATH_ABOUT_ME);
		return sb.toString();
	}
	static String createUpdateLocationURL(long userId){
		StringBuilder sb = new StringBuilder(URL_API_USER);
		sb.append(userId);
		sb.append(PATH_UPDATE_LOCATION);
		return sb.toString();
	}
	static String createReportUserURL(long userId){
		StringBuilder sb = new StringBuilder(URL_API_USER);
		sb.append(userId);
		sb.append(PATH_REPORT_USER);
		return sb.toString();
	}
	static String createPassUserURL(long userId){
		StringBuilder sb = new StringBuilder(URL_API_USER);
		sb.append(userId);
		sb.append(PATH_PASS);
		return sb.toString();
	}
	static String createFriendsURL(long userId){
		StringBuilder sb = new StringBuilder(URL_API_USER);
		sb.append(userId);
		sb.append(PATH_FRIENDS);
		return sb.toString();
	}

	
	static String createLogoutURL(long userId,String deviceToken){
		StringBuilder sb = new StringBuilder(URL_API_USER);
		sb.append(userId);
		sb.append("/device/");
		sb.append(deviceToken);
		sb.append(PATH_LOGOUT);
		return sb.toString();
	}	
		
	
	static String createProfileURL(long userId,long friendId){
		StringBuilder sb = new StringBuilder(URL_API_USER);
		sb.append(userId);
		sb.append('/');
		sb.append(friendId);
		sb.append(PATH_PROFILE);
		return sb.toString();
	}
	static String createNumberVarificationStatusURL(long userId){
		StringBuilder sb = new StringBuilder(URL_API_USER);
		sb.append(userId);
		sb.append(PATH_PHONE);
		sb.append("/status");
		return sb.toString();
	}
	static String createSendVarificationCodeURL(long userId){
		StringBuilder sb = new StringBuilder(URL_API_USER);
		sb.append(userId);
		sb.append(PATH_PHONE);
		sb.append("/code");
		return sb.toString();
	}
	static String createValidateCodeURL(long userId){
		StringBuilder sb = new StringBuilder(URL_API_USER);
		sb.append(userId);
		sb.append(PATH_PHONE);
		return sb.toString();
	}
	
	static String createForceUpgradeURL(int majorVersion, int minorVersion, int pointVersion){
		StringBuilder sb = new StringBuilder(URL_API_APP);
		sb.append(UPGRADE);
		sb.append(Constants.DEVICE_TYPE);
		sb.append(MAJOR_VERSION);
		sb.append(majorVersion);
		sb.append(MINOR_VERSION);
		sb.append(minorVersion);
		sb.append(POINT_VERSION);
		sb.append(pointVersion);
		return sb.toString();
		
	}
}
