package com.matchstixapp.WS;

import com.matchstixapp.AppConfig;

public interface URLConstants {
	String DEV_SERVER_IP ="http://119.9.108.24:";//"http://119.9.92.156:";
	//String PROD_SERVER_IP = "http://prdmastapp01.matchstixapp.com:";
	String PROD_SERVER_IP = "http://119.9.108.24:";
	String XMPP_DEV_SERVER_IP ="http://119.9.108.24:";//"http://119.9.92.156:";// 
	//String XMPP_PROD_SERVER_IP = "http://prdmastapp01.matchstixapp.com:";
	String XMPP_PROD_SERVER_IP = "http://119.9.108.24:";
	String URL_API_PRE = "8080/matchstix/api/v1/";
	String LAST_SEEN_URL_PRE = "9090/plugins/lastseen/activity/last?username=";
	String URL_API = AppConfig.DEV_BUILD ? DEV_SERVER_IP + URL_API_PRE : PROD_SERVER_IP + URL_API_PRE;
	String LAST_SEEN_URL = AppConfig.DEV_BUILD ? XMPP_DEV_SERVER_IP + LAST_SEEN_URL_PRE : XMPP_PROD_SERVER_IP + LAST_SEEN_URL_PRE;
	String URL_API_USER = URL_API + "user/";
	String URL_API_APP = URL_API + "app/";
	String PATH_IMAGES = "/images";
	String PATH_SUGGESTIONS = "/suggestion";
	String PATH_LIKE = "/like";
	String PATH_DISLIKE = "/unmatch";
	String PATH_DISCOVERY_SETTING = "/discoverysettings";
	String PATH_ABOUT_ME = "/status";
	String PATH_UPDATE_LOCATION = "/location";
	String PATH_REPORT_USER = "/report";
	String PATH_FRIENDS = "/friends";
	String PATH_LOGOUT="/logout";
	String PATH_PROFILE = "/profile";
	String PATH_PHONE = "/phone";
	String UPGRADE = "upgrade/";
	String MAJOR_VERSION = "?majorVersion=";
	String MINOR_VERSION = "&minorVersion=";
	String POINT_VERSION = "&pointVersion=";
	
	String PATH_PASS = "/pass";
	
}
