package com.matchstixapp.WS;

import java.util.ArrayList;

import android.util.Log;

import com.craterzone.cz_commons_lib.JsonUtil;
import com.craterzone.httpclient.HttpUrlConnectionUtil;
import com.craterzone.httpclient.model.CZResponse;
import com.craterzone.httpclient.model.CustomHttpParams;
import com.matchstixapp.MatchstixApplication;
import com.matchstixapp.db.DataController;
import com.matchstixapp.db.MatchstixPreferences;
import com.matchstixapp.modal.JSONUpdateImages;
import com.matchstixapp.pojo.JSONNumber;
import com.matchstixapp.pojo.JSONPhone;
import com.matchstixapp.pojo.JSONReportUser;
import com.matchstixapp.pojo.JSONSettings;
import com.matchstixapp.pojo.JSONSignUp;
import com.matchstixapp.pojo.JSONUpdateAbout;
import com.matchstixapp.pojo.JSONUpdateLocation;
import com.matchstixapp.utils.Utils;

public class HTTPOperationController {
	private static final String HEADER_USER_AGENT = "user-agent";
	private final String TAG = HTTPOperationController.class.getSimpleName();
	private final String CONTENT_TYPE = "application/json";
	private final String ACCEPT = "application/json";
	private final String HEADER_TOKEN = "token";
	public static int likeCounter = 0;

	private static HTTPOperationController instance = new HTTPOperationController();

	private HTTPOperationController() {
	}

	public static synchronized HTTPOperationController getInstance() {
		return instance;
	}

	/*
	 * 
	 * */
	public CZResponse requestSignup(JSONSignUp signup) {
		String url = URLFactory.createSignupURL();
		CZResponse czResponse = null;
		try {
			Log.d(TAG, "requestSignup() url: " + url);
			String body = JsonUtil.toJson(signup);
			Log.d(TAG, "requestSignup() body: " + body);
			CustomHttpParams param = new CustomHttpParams(HEADER_USER_AGENT,
					MatchstixApplication.getInstance().getUserAgent());
			ArrayList<CustomHttpParams> httpParams = new ArrayList<CustomHttpParams>();
			httpParams.add(param);
			czResponse = HttpUrlConnectionUtil.put(url, body, CONTENT_TYPE,
					ACCEPT, httpParams);
		} catch (Exception e) {
			Log.e(TAG, "ERROR: requestSignup()", e);
		}
		return czResponse;
	}

	/**
	 * Register push token
	 * 
	 * @param signup
	 * @return
	 */
	public CZResponse registerPushToken(String json) {
		String url = URLFactory.registerPushTokenURL(DataController
				.getInstance().getUser().getId());
		CZResponse czResponse = null;
		try {
			CustomHttpParams param = new CustomHttpParams(HEADER_USER_AGENT,
					MatchstixApplication.getInstance().getUserAgent());
			ArrayList<CustomHttpParams> httpParams = new ArrayList<CustomHttpParams>();
			CustomHttpParams agent = new CustomHttpParams(HEADER_TOKEN,
					DataController.getInstance().getUser().getAccessToken());
			httpParams.add(param);
			httpParams.add(agent);
			czResponse = HttpUrlConnectionUtil.post(url, json, CONTENT_TYPE,
					ACCEPT, httpParams);
		} catch (Exception e) {
			Log.e(TAG, "ERROR: requestPush Token", e);
		}
		return czResponse;
	}

	public CZResponse requestAddOrUpdateImage(long userId, String token,
			JSONUpdateImages mProfilePhotos) {
		String url = URLFactory.createAddOrUpdateImageURL(userId);
		CZResponse czResponse = null;
		try {
			Log.d(TAG, "requestAddOrUpdateImage() url: " + url);
			CustomHttpParams params = new CustomHttpParams(HEADER_TOKEN, token);
			CustomHttpParams userAgent = new CustomHttpParams(
					HEADER_USER_AGENT, MatchstixApplication.getInstance()
							.getUserAgent());
			ArrayList<CustomHttpParams> httpParams = new ArrayList<CustomHttpParams>();
			httpParams.add(params);
			httpParams.add(userAgent);
			String body = JsonUtil.toJson(mProfilePhotos);
			Log.d(TAG, "requestAddOrUpdateImage() body: " + body);
			czResponse = HttpUrlConnectionUtil.post(url, body, CONTENT_TYPE,
					ACCEPT, httpParams);
		} catch (Exception e) {
			Log.e(TAG, "ERROR: requestAddOrUpdateImage()- ", e);
		}
		return czResponse;
	}

	public CZResponse requestGetSuggestions(long userId, String token, int count) {
		String url = URLFactory.createGetSuggestionsURL(userId, count);
		CZResponse czResponse = null;
		try {
			Log.d(TAG, "requestGetSuggestions() URL: " + url);
			czResponse = Utils.get(url, ACCEPT, token);
		} catch (Exception e) {
			Log.e(TAG, "ERROR: requestGetSuggestions()", e);
		}
		return czResponse;
	}

	public CZResponse requestLikeUser(String token, String body) {
		String url = URLFactory.createLikeUserURL(DataController.getInstance()
				.getUser().getId());
		CZResponse czResponse = null;
		CustomHttpParams param = new CustomHttpParams(HEADER_TOKEN, token);
		CustomHttpParams userAgent = new CustomHttpParams(HEADER_USER_AGENT,
				MatchstixApplication.getInstance().getUserAgent());
		ArrayList<CustomHttpParams> params = new ArrayList<CustomHttpParams>();
		params.add(param);
		params.add(userAgent);
		try {
			Log.d(TAG, "requestLikeUser(): " + url);
			Log.d(TAG, "requestLikeUser() BODY: " + body);
			czResponse = HttpUrlConnectionUtil.post(url, body, CONTENT_TYPE,
					ACCEPT, params);
		} catch (Exception e) {
			Log.e(TAG, "ERROR: requestLikeUser()" + e);
		}
		return czResponse;
	}

	public CZResponse requestDislikeUser(String token, String body) {
		String url = URLFactory.createDislikeUserURL(DataController
				.getInstance().getUser().getId());
		CZResponse czResponse = null;
		CustomHttpParams param = new CustomHttpParams(HEADER_TOKEN, token);
		CustomHttpParams userAgent = new CustomHttpParams(HEADER_USER_AGENT,
				MatchstixApplication.getInstance().getUserAgent());
		ArrayList<CustomHttpParams> params = new ArrayList<CustomHttpParams>();
		params.add(param);
		params.add(userAgent);
		try {
			Log.d(TAG, "requestDislikeUser(): " + url);
			Log.d(TAG, "requestDislikeUser() body: " + body);
			czResponse = HttpUrlConnectionUtil.post(url, body, CONTENT_TYPE,
					ACCEPT, params);
		} catch (Exception e) {
			Log.e(TAG, "ERROR: requestDislikeUser()" + e);
		}
		return czResponse;
	}

	public CZResponse requestdiscoverysettings(long userId, String token,
			JSONSettings settings) {
		String url = URLFactory.creatediscoverysettingsURL(userId);
		CZResponse czResponse = null;
		CustomHttpParams param = new CustomHttpParams(HEADER_TOKEN, token);
		CustomHttpParams userAgent = new CustomHttpParams(HEADER_USER_AGENT,
				MatchstixApplication.getInstance().getUserAgent());
		ArrayList<CustomHttpParams> params = new ArrayList<CustomHttpParams>();
		params.add(param);
		params.add(userAgent);
		try {
			Log.d(TAG, "requestdiscoverysettings() url: " + url);
			String body = JsonUtil.toJson(settings);
			Log.d(TAG, "requestdiscoverysettings() body:" + body);
			czResponse = HttpUrlConnectionUtil.post(url, body, CONTENT_TYPE,
					ACCEPT, params);
		} catch (Exception e) {
			Log.e(TAG, "ERROR: requestdiscoverysettings()", e);
		}
		return czResponse;
	}

	public CZResponse requestUpdateUserAboutMe(long userId, String token,
			JSONUpdateAbout aboutMe) {
		String url = URLFactory.createUpdateUserAboutMeURL(userId);
		CZResponse czResponse = null;
		CustomHttpParams param = new CustomHttpParams(HEADER_TOKEN, token);
		CustomHttpParams userAgent = new CustomHttpParams(HEADER_USER_AGENT,
				MatchstixApplication.getInstance().getUserAgent());
		ArrayList<CustomHttpParams> params = new ArrayList<CustomHttpParams>();
		params.add(param);
		params.add(userAgent);
		try {
			Log.d(TAG, "requestUpdateUserAboutMe() url: " + url);
			String body = JsonUtil.toJson(aboutMe);
			Log.d(TAG, "requestUpdateUserAboutMe() body:" + body);
			czResponse = HttpUrlConnectionUtil.post(url, body, CONTENT_TYPE,
					ACCEPT, params);
		} catch (Exception e) {
			Log.e(TAG, "ERROR: requestUpdateUserAboutMe()", e);
		}
		return czResponse;
	}

	public CZResponse requestUpdateUserLatLng(long userId, String token,
			JSONUpdateLocation latLng) {
		String url = URLFactory.createUpdateLocationURL(userId);
		CZResponse czResponse = null;
		CustomHttpParams param = new CustomHttpParams(HEADER_TOKEN, token);
		CustomHttpParams userAgent = new CustomHttpParams(HEADER_USER_AGENT,
				MatchstixApplication.getInstance().getUserAgent());
		ArrayList<CustomHttpParams> params = new ArrayList<CustomHttpParams>();
		params.add(param);
		params.add(userAgent);
		try {
			Log.d(TAG, "requestUpdateUserLatLng() url: " + url);
			String body = JsonUtil.toJson(latLng);
			Log.d(TAG, "requestUpdateUserLatLng() body:" + body);
			czResponse = HttpUrlConnectionUtil.post(url, body, CONTENT_TYPE,
					ACCEPT, params);
		} catch (Exception e) {
			Log.e(TAG, "ERROR: requestUpdateUserLatLng()", e);
		}
		return czResponse;
	}

	public CZResponse requestReportUser(String token, JSONReportUser reportUser) {
		String url = URLFactory.createReportUserURL(DataController
				.getInstance().getUser().getId());
		CZResponse czResponse = null;
		CustomHttpParams param = new CustomHttpParams(HEADER_TOKEN, token);
		CustomHttpParams userAgent = new CustomHttpParams(HEADER_USER_AGENT,
				MatchstixApplication.getInstance().getUserAgent());
		ArrayList<CustomHttpParams> params = new ArrayList<CustomHttpParams>();
		params.add(param);
		params.add(userAgent);
		try {
			Log.d(TAG, "requestReportUser() url: " + url);
			String body = JsonUtil.toJson(reportUser);
			Log.d(TAG, "requestReportUser()- " + body);
			czResponse = HttpUrlConnectionUtil.post(url, body, CONTENT_TYPE,
					ACCEPT, params);
		} catch (Exception e) {
			Log.e(TAG, "ERROR requestReportUser()- ", e);
		}
		return czResponse;
	}

	public CZResponse requestPassUser(String token, String body) {
		String url = URLFactory.createPassUserURL(DataController.getInstance()
				.getUser().getId());
		CZResponse czResponse = null;
		CustomHttpParams param = new CustomHttpParams(HEADER_TOKEN, token);
		CustomHttpParams userAgent = new CustomHttpParams(HEADER_USER_AGENT,
				MatchstixApplication.getInstance().getUserAgent());
		ArrayList<CustomHttpParams> params = new ArrayList<CustomHttpParams>();
		params.add(param);
		params.add(userAgent);
		try {
			Log.d(TAG, "requestPassUser(): " + url);
			Log.d(TAG, "requestPassUser() body: " + body);
			czResponse = HttpUrlConnectionUtil.post(url, body, CONTENT_TYPE,
					ACCEPT, params);
		} catch (Exception e) {
			Log.e(TAG, "ERROR: requestPassUser()" + e);
		}
		return czResponse;
	}

	public CZResponse requestFriends(long userId, String token) {
		String url = URLFactory.createFriendsURL(userId);
		CZResponse czResponse = null;
		CustomHttpParams param = new CustomHttpParams(HEADER_TOKEN, token);
		CustomHttpParams userAgent = new CustomHttpParams(HEADER_USER_AGENT,
				MatchstixApplication.getInstance().getUserAgent());
		ArrayList<CustomHttpParams> params = new ArrayList<CustomHttpParams>();
		params.add(param);
		params.add(userAgent);
		try {
			Log.d(TAG, "requestFriends() url: " + url);
			czResponse = HttpUrlConnectionUtil.get(url, ACCEPT, CONTENT_TYPE,
					params);
		} catch (Exception e) {
			Log.e(TAG, "ERROR: requestFriends() ", e);
		}
		return czResponse;
	}

	public CZResponse requestLogout(long userId, String token,
			String deviceToken) {
		try {
			MatchstixPreferences.getInstance().setJsonHashCode(0);
			MatchstixPreferences.getInstance().setJsonHashCodeFlag(0);
		} catch (Exception e) {
		}
		String url = URLFactory.createLogoutURL(userId, deviceToken);
		CZResponse czResponse = null;
		CustomHttpParams param1 = new CustomHttpParams(HEADER_TOKEN, token);
		CustomHttpParams param = new CustomHttpParams(HEADER_USER_AGENT,
				MatchstixApplication.getInstance().getUserAgent());

		ArrayList<CustomHttpParams> params = new ArrayList<CustomHttpParams>();
		params.add(param1);
		params.add(param);

		try {

			Log.d(TAG, "requestLogout() URL: " + url);
			Log.d(TAG, "requestLogout() USER-AGENT: "
					+ MatchstixApplication.getInstance().getUserAgent());

			czResponse = HttpUrlConnectionUtil.post(url, null, CONTENT_TYPE,
					ACCEPT, params);
		} catch (Exception e) {
			Log.e(TAG, "ERROR: requestLogout() ", e);
		}

		return czResponse;
	}

	public CZResponse requestProfile(long userId, long friendId, String token) {
		String url = URLFactory.createProfileURL(userId, friendId);
		CZResponse czResponse = null;
		CustomHttpParams param = new CustomHttpParams(HEADER_TOKEN, token);
		CustomHttpParams userAgent = new CustomHttpParams(HEADER_USER_AGENT,
				MatchstixApplication.getInstance().getUserAgent());
		ArrayList<CustomHttpParams> params = new ArrayList<CustomHttpParams>();
		params.add(param);
		params.add(userAgent);
		try {

			Log.d(TAG, "requestProfile() url: " + url);
			czResponse = HttpUrlConnectionUtil.get(url, CONTENT_TYPE, ACCEPT,
					params);
		} catch (Exception e) {
			Log.e(TAG, "ERROR: requestProfile() ", e);
		}

		return czResponse;
	}

	public CZResponse requestVarifyCode(long userId, String token,
			JSONPhone request) {
		String url = URLFactory.createValidateCodeURL(userId);
		CZResponse czResponse = null;
		CustomHttpParams param = new CustomHttpParams(HEADER_TOKEN, token);
		CustomHttpParams userAgent = new CustomHttpParams(HEADER_USER_AGENT,
				MatchstixApplication.getInstance().getUserAgent());
		ArrayList<CustomHttpParams> params = new ArrayList<CustomHttpParams>();
		params.add(param);
		params.add(userAgent);
		try {
			Log.d(TAG, "requestVarifyCode() URL: " + url);
			String body = JsonUtil.toJson(request);
			Log.d(TAG, "requestVarifyCode() BODY:" + body);
			czResponse = HttpUrlConnectionUtil.put(url, body, CONTENT_TYPE,
					ACCEPT, params);
		} catch (Exception e) {
			Log.e(TAG, "ERROR: requestVarifyCode()", e);
		}
		return czResponse;
	}

	public CZResponse requestSendValidationCode(long userId, String token,
			JSONNumber request) {
		String url = URLFactory.createSendVarificationCodeURL(userId);
		CZResponse czResponse = null;
		CustomHttpParams param = new CustomHttpParams(HEADER_TOKEN, token);
		CustomHttpParams userAgent = new CustomHttpParams(HEADER_USER_AGENT,
				MatchstixApplication.getInstance().getUserAgent());
		ArrayList<CustomHttpParams> params = new ArrayList<CustomHttpParams>();
		params.add(param);
		params.add(userAgent);
		try {
			Log.d(TAG, "requestSendValidationCode() URL: " + url);
			String body = JsonUtil.toJson(request);
			Log.d(TAG, "requestSendValidationCode() BODY:" + body);
			czResponse = HttpUrlConnectionUtil.post(url, body, CONTENT_TYPE,
					ACCEPT, params);
		} catch (Exception e) {
			Log.e(TAG, "ERROR: requestSendValidationCode()", e);
		}
		return czResponse;
	}

	public CZResponse requestCheckVarificationStatus(long userId, String token) {
		String url = URLFactory.createNumberVarificationStatusURL(userId);
		CZResponse czResponse = null;
		CustomHttpParams param = new CustomHttpParams(HEADER_TOKEN, token);
		CustomHttpParams userAgent = new CustomHttpParams(HEADER_USER_AGENT,
				MatchstixApplication.getInstance().getUserAgent());
		ArrayList<CustomHttpParams> params = new ArrayList<CustomHttpParams>();
		params.add(param);
		params.add(userAgent);
		try {
			Log.d(TAG, "requestCheckVarificationStatus() URL: " + url);
			czResponse = HttpUrlConnectionUtil.get(url, ACCEPT, CONTENT_TYPE,
					params);
		} catch (Exception e) {
			Log.e(TAG, "ERROR: requestCheckVarificationStatus()", e);
		}
		return czResponse;
	}

	/*
	 * Check for new version available or not
	 */
	public CZResponse checkForceUpgrade(int majorVersion, int minorVersion,
			int pointVersion) {
		String url = URLFactory.createForceUpgradeURL(majorVersion,
				minorVersion, pointVersion);
		return HttpUrlConnectionUtil.get(url, ACCEPT);
	}

}