package com.matchstixapp.db;

import java.util.ArrayList;
import java.util.HashMap;

import com.matchstixapp.MatchstixApplication;
import com.matchstixapp.PNV.LocationDAO;
import com.matchstixapp.modal.Album;
import com.matchstixapp.modal.AlbumPhoto;
import com.matchstixapp.modal.DiscoverySettings;
import com.matchstixapp.modal.FBAlbumPhotos;
import com.matchstixapp.modal.FBAlbums;
import com.matchstixapp.modal.Friend;
import com.matchstixapp.modal.Roster;
import com.matchstixapp.modal.User;
import com.matchstixapp.utils.Constants;

public class DataController {

	private static DataController instance;
	private MatchstixPreferences preferences;
	private User user;
	private DiscoverySettings settings;
	private StorageController storageController;
	//private Actions suggestionActions;
	private FBAlbums albums;
	private FBAlbums myFBalbums;
	private HashMap<String, FBAlbumPhotos> photos;
	private ArrayList<AlbumPhoto> userProfilePics;
	private ArrayList<Long> newFriendList;

	private String fullJid;
	private DataController() {
		
		init();
	}

	private void init() {
		preferences = MatchstixPreferences.getInstance();
		user = preferences.getUser();
		settings = preferences.getSettings();
		storageController = StorageController.getInstance(MatchstixApplication
				.getInstance());
		photos = new HashMap<String, FBAlbumPhotos>();
		userProfilePics = preferences.getAlbum();
		newFriendList = new ArrayList<Long>();
		//loadSuggestionActions();
		loadFriends();
		loadCountryFlags();
	}

	

	public String getFullJid() {
		if(fullJid == null ) {
			fullJid = preferences.getFULLJID();
		}
		return fullJid;
	}

	public void setFullJid(String fullJid) {
		preferences.saveFULLJID(fullJid);
		this.fullJid = fullJid;
	}

	public static DataController getInstance() {
		if (instance == null) {
			instance = new DataController();
		}
		return instance;
	}

	public MatchstixPreferences getPreferences() {
		return preferences;
	}

	/*public Actions getSuggestionActions() {
		return suggestionActions;
	}*/

	/**
	 * This Method clears data from shared preferences and DB.
	 */
	public void clear() {
		preferences.clear();
		init();
	}

	public User getUser() {
		return user;
	}

	public DiscoverySettings getSettings() {
		return settings;
	}

	public void saveUser() {
		preferences.setUser(getUser());
	}

	public void saveSettings() {
		preferences.setSettings(getSettings());
	}

	public void updateSuggestionAction(SuggestionAction action) {
		storageController.updateSuggestionAction(action);
	}

	public void deleteSuggestionAction(SuggestionAction action) {
		storageController.deleteSuggestionAction(action);
	}

	/*private void loadSuggestionActions() {
		suggestionActions = storageController.loadSuggestionActions();
	}*/

	public String getAlbumIDByAlbumName(String albumName) {
		String id = null;
		if (albums != null) {
			for (int i = 0; i < albums.getData().size(); i++) {
				Album fbAlbum = albums.getData().get(i);
				if (fbAlbum != null
						&& fbAlbum.getName().equalsIgnoreCase(albumName)) {
					id = fbAlbum.getId();
					return id;
				}
			}
		}
		return null;
	}

	public String getProfileAlbumId() {
		return getAlbumIDByAlbumName(Constants.PROFILE_PIC_ALBUM_NAME);
	}

	public boolean hasFBAlbumPhotosDetails(String albumId) {
		return photos != null && photos.containsKey(albumId);
	}

	public FBAlbumPhotos getFBAlbumPhotos(String albumId) {
		return photos == null ? null : photos.get(albumId);
	}

	public AlbumPhoto getFBAlbumPhoto(String albumId, String photoId) {
		FBAlbumPhotos details = getFBAlbumPhotos(albumId);
		ArrayList<AlbumPhoto> photos = details.getData();
		for (int i = 0; i < photos.size(); i++) {
			AlbumPhoto photo = photos.get(i);
			if (photo.getId().equals(photoId)) {
				return photo;
			}
		}
		return null;
	}

	public Album getFBAlbum(String albumId) {
		if (albums == null || albums.getData() == null) {
			return null;
		}
		ArrayList<Album> albums = this.albums.getData();
		for (int i = 0; i < albums.size(); i++) {
			Album album = albums.get(i);
			if (album.getId().equals(albumId)) {
				return album;
			}
		}
		return null;
	}

	public FBAlbums getMyFBAlbums() {
		return myFBalbums;
	}

	public void setMyFBAlbums(FBAlbums mAlbums) {
		this.myFBalbums = mAlbums;
	}
	
	public FBAlbums getFBAlbums() {
		return albums;
	}

	public void updateFBAlbums(FBAlbums mAlbums) {
		this.albums = mAlbums;
	}

	public boolean hasFBAlbums() {
		return getFBAlbums() != null;
	}

	public void refreshFBAlbumPhotos(String id, FBAlbumPhotos details) {
		photos.put(id, details);
	}

	public ArrayList<AlbumPhoto> getUserProfilePics() {
		return userProfilePics;
	}

	public void setUserProfilePics(ArrayList<AlbumPhoto> pics, boolean isFromFacebook) {
		if (pics==null) {
			return;
		}
		this.userProfilePics = (ArrayList<AlbumPhoto>) pics.clone();
		//Trim profile pics
		if (userProfilePics.size() > 4 && isFromFacebook) {
			while (userProfilePics.size()>4) {
				userProfilePics.remove(userProfilePics.size()-1);
			}
		}
		saveUserPics();
	}

	public AlbumPhoto findPhotoInUserPics(String photoId) {
		for (int i = 0; i < userProfilePics.size(); i++) {
			if (userProfilePics.get(i).getId().equalsIgnoreCase(photoId)) {
				return userProfilePics.get(i);
			}
		}
		return null;
	}

	public void saveUserPics() {
		preferences.saveAlbum(userProfilePics);
	}

	public void updateFriend(Friend friend) {
		storageController.updateFriend(friend);
	}

	public void deleteFriend(Friend friend) {
		storageController.deleteFriend(friend);
	}

	private void loadFriends() {
		Roster roster = storageController.loadFriends();
		Roster.init(roster);
	}

	private void loadCountryFlags() {
		if(!LocationDAO.getInstance(MatchstixApplication.getInstance()).isCountryListExist()){
			LocationDAO.getInstance(MatchstixApplication.getInstance()).insertCountryDataInDBFromXML();
		}
	}
	
	public void setNewFriendList(long friendId){
		newFriendList.add(friendId);
	}
	
	public ArrayList<Long> getNewFriendList(){
		return newFriendList;
	}
	
	public void removeNewFriend(long friendId){
		if(newFriendList.contains(friendId)){
		newFriendList.remove(friendId);}
	}
}
