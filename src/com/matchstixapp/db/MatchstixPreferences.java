package com.matchstixapp.db;

import java.util.ArrayList;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.text.TextUtils;
import android.util.Log;

import com.craterzone.cz_commons_lib.JsonUtil;
import com.google.gson.reflect.TypeToken;
import com.matchstixapp.MatchstixApplication;
import com.matchstixapp.modal.AlbumPhoto;
import com.matchstixapp.modal.DiscoverySettings;
import com.matchstixapp.modal.User;
import com.xabber.xmpp.archive.Retrieve;

public class MatchstixPreferences {

	private static final String TAG = MatchstixPreferences.class.getName();
	private static final String NAME = "pref_matchstix";
	private static final MatchstixPreferences _instance = new MatchstixPreferences();

	private interface Keys {
		public static final String USER = "user";
		public static final String GCM_REG_ID = "gcm_reg_id";
		public static final String LATTITUDE = "lat";
		public static final String LONGITUDE = "lng";
		/*public static final String USER_PICS = "user_pics";*/
		public static final String FULL_JID = "full_jid";
		public static final String SETTINGS = "settings";
		public static final String ALBUM = "album";
		public static final String GCM_TOKEN_REGISTERED = "gcm_token_registered";
		public static final String IS_UPGRADE = "is_upgrade";
		public static final String JSON_DATA = "json_hashcode";
		public static final String JSON_FLAG = "flags";
		public static final String LIKE_COUNTER_START = "start";
		public static final String LIKE_COUNTER_END = "end";
	}

	private SharedPreferences preferences;

	private MatchstixPreferences() {
		preferences = MatchstixApplication.getInstance().getSharedPreferences(NAME, Context.MODE_PRIVATE);
	}

	public static MatchstixPreferences getInstance() {
		return _instance;
	}
	
	
	public void setLocation(String latitude, String longitude) {
		setString(Keys.LATTITUDE, latitude);
		setString(Keys.LONGITUDE, longitude);
	}
	
	public void setLikeStart(long code) {
		setLong(Keys.LIKE_COUNTER_START, code);
	}
	public long getLikeStart() {
		return (preferences.getLong(Keys.LIKE_COUNTER_START, 0));
	}
	public void setLikeEnd(long code) {
		setLong(Keys.LIKE_COUNTER_END, code);
	}
	public long getLikeEnd() {
		return (preferences.getLong(Keys.LIKE_COUNTER_END, 0));
	}
	
	public void setJsonHashCode(int code) {
		setInt(Keys.JSON_DATA, code);
	}
	public int getJsonHashCode() {
		return (preferences.getInt(Keys.JSON_DATA, 0));
	}
	public void setJsonHashCodeFlag(int code) {
		setInt(Keys.JSON_FLAG, code);
	}
	public int getJsonHashCodeFlag() {
		return (preferences.getInt(Keys.JSON_FLAG, 0));
	}
	public double getLatitude() {
		return Double.parseDouble(preferences.getString(Keys.LATTITUDE, "0.0"));
	}
	
	public double getLongitude() {
		return Double.parseDouble(preferences.getString(Keys.LONGITUDE, "0.0"));
	}
	
	
	public void setGCmTokenRegistered(boolean registered) {
		setBoolean(Keys.GCM_TOKEN_REGISTERED, registered);
	}
	
	public boolean isGCMTokenRegistered() {
		return getBoolean(Keys.GCM_TOKEN_REGISTERED, false);
	}
	
	public User getUser() {
		String data = getString(Keys.USER, "");
		if (TextUtils.isEmpty(data)) {
			return new User();
		}
		return (User) JsonUtil.toModel(data, User.class);
	}

	public void setUser(User user) {
		setString(Keys.USER, JsonUtil.toJson(user));
	}
	
	public DiscoverySettings getSettings() {
		String data = getString(Keys.SETTINGS, "");
		if (TextUtils.isEmpty(data)) {
			return new DiscoverySettings();
		}
		return (DiscoverySettings) JsonUtil.toModel(data, DiscoverySettings.class);
	}

	void setSettings(DiscoverySettings settings) {
		setString(Keys.SETTINGS, JsonUtil.toJson(settings));
	}

	public void saveGCMRegId(String regId) {
		setString(Keys.GCM_REG_ID, regId);
	}
	public String getGCMRegId() {
		return getString(Keys.GCM_REG_ID, "");
	}
	
	public void saveFULLJID(String fullJid) {
		setString(Keys.FULL_JID, fullJid);
	}
	public String getFULLJID() {
		return getString(Keys.FULL_JID, "");
	}
	
	public void saveAlbum(ArrayList<AlbumPhoto> pics) {
		setString(Keys.ALBUM, JsonUtil.toJson(pics));
	}
	
	public boolean isUpgradeAvailable(){
		return getBoolean(Keys.IS_UPGRADE, false);
	}
	
	public void setUpgradeAvailable(boolean value){
		setBoolean(Keys.IS_UPGRADE, value);
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<AlbumPhoto> getAlbum() {
		String data = getString(Keys.ALBUM, "");
		if (TextUtils.isEmpty(data)) {
			return new ArrayList<AlbumPhoto>();
		}
		return (ArrayList<AlbumPhoto>) JsonUtil.toModel(data,
				new TypeToken<ArrayList<AlbumPhoto>>() {}.getType());
	}
	
	/**
	 * This Method Clear shared preference.
	 */
	protected void clear() {
		Editor editor = preferences.edit();
		editor.clear();
		editor.commit();
	}

	protected void commit() {
		preferences.edit().commit();
	}

	private void setString(String key, String value) {
		if (key != null && value != null) {
			try {
				if (preferences != null) {
					Editor editor = preferences.edit();
					editor.putString(key, value);
					editor.commit();
				}
			} catch (Exception e) {
				Log.e(TAG, "Unable to set " + key + "= " + value
						+ "in shared preference", e);
			}
		}
	}

	private void setLong(String key, long value) {
		if (key != null) {
			try {
				if (preferences != null) {
					Editor editor = preferences.edit();
					editor.putLong(key, value);
					editor.commit();
				}
			} catch (Exception e) {
				Log.e(TAG, "Unable to set " + key + "= " + value
						+ "in shared preference", e);
			}
		}
	}

	private void setInt(String key, int value) {
		if (key != null) {
			try {
				if (preferences != null) {
					Editor editor = preferences.edit();
					editor.putInt(key, value);
					editor.commit();
				}
			} catch (Exception e) {
				Log.e(TAG, "Unable to set " + key + "= " + value
						+ "in shared preference", e);
			}
		}
	}

	private void setDouble(String key, double value) {
		if (key != null) {
			try {
				if (preferences != null) {
					Editor editor = preferences.edit();
					editor.putLong(key, Double.doubleToRawLongBits(value));
					editor.commit();
				}
			} catch (Exception e) {
				Log.e(TAG, "Unable to set " + key + "= " + value
						+ "in shared preference", e);
			}
		}
	}

	private void setBoolean(String key, boolean value) {
		if (key != null) {
			try {
				if (preferences != null) {
					Editor editor = preferences.edit();
					editor.putBoolean(key, value);
					editor.commit();
				}
			} catch (Exception e) {
				Log.e(TAG, "Unable to set " + key + "= " + value
						+ "in shared preference", e);
			}
		}
	}

	private int getInt(String key, int defaultValue) {
		if (preferences != null && key != null && preferences.contains(key)) {
			return preferences.getInt(key, defaultValue);
		}
		return defaultValue;
	}

	private long getLong(String key, long defaultValue) {
		if (preferences != null && key != null && preferences.contains(key)) {
			return preferences.getLong(key, defaultValue);
		}
		return defaultValue;
	}

	private boolean getBoolean(String key, boolean defaultValue) {
		if (preferences != null && key != null && preferences.contains(key)) {
			return preferences.getBoolean(key, defaultValue);
		}
		return defaultValue;
	}

	private String getString(String key, String defaultValue) {
		if (preferences != null && key != null && preferences.contains(key)) {
			return preferences.getString(key, defaultValue);
		}
		return defaultValue;
	}

	private double getDouble(String key, double defaultValue) {
		if (preferences != null && key != null && preferences.contains(key)) {
			return preferences.getFloat(key, (float) defaultValue);
		}
		return defaultValue;
	}

	private void putBoolean(String key, boolean value) {
		try {
			if (preferences != null) {
				Editor editor = preferences.edit();
				editor.putBoolean(key, value);
				editor.commit();
			}
		} catch (Exception e) {
			Log.e(TAG, "Unable Put Boolean in Shared preference", e);
		}
	}

}