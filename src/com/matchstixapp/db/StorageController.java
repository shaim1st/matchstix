/*
 *  Copyright @ CraterZone 2015 
 *
 */
package com.matchstixapp.db;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;

import com.matchstixapp.MatchstixApplication;
import com.matchstixapp.enums.Tables;
import com.matchstixapp.modal.Friend;
import com.matchstixapp.modal.Roster;

/**
 * This Class handle All thind Related to Database : 1. Create DB, Upgrade,
 * Create tablbe , Upgrade table, Open and Close Db
 * 
 * 
 */

public class StorageController extends SQLiteOpenHelper {
	public static final String TAG = StorageController.class.getName();
	private static final int DATABASE_VERSION = 1;
	private static final String DATABASE_NAME = "db_matchstix";
	private static StorageController _instance = null;
	private int openedConnections = 0;

	private StorageController(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	/**
	 * Getting instance of Sqlite class
	 * 
	 * @param context
	 * @return instance Sqlite Helper
	 */
	public static StorageController getInstance(Context context) {
		if (_instance == null) {
			synchronized (StorageController.class) {
				if (_instance == null) {
					_instance = new StorageController(context);
				}
			}
		}
		return _instance;
	}

	/**
	 * Count total no. instance of Readable DataBase created
	 */
	public synchronized SQLiteDatabase getReadableDatabase() {
		openedConnections++;
		return super.getReadableDatabase();
	}

	public synchronized void close() {
		openedConnections--;
		if (openedConnections == 0) {
			close();
		}
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		try {
			db.execSQL(Tables.Suggestions.createTableQuery());
			db.execSQL(Tables.SuggestionActions.createTableQuery());
			db.execSQL(Tables.Friends.createTableQuery());

			db.execSQL(Tables.Location.createTableQuery());
			Log.i(TAG, " Tables created");
		} catch (SQLException e) {
			Log.e(TAG, " Unable to create tables", e);
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

	}

	public static void close(Cursor cursor) {
		try {
			if (cursor != null) {
				cursor.close();
			}
		} catch (Exception e) {
			Log.e(TAG, " Unable to close cursor- ", e);
		}
	}

	void updateSuggestionAction(SuggestionAction action) {

		SuggestionActionsDAO.getInstance(MatchstixApplication.getInstance())
				.insertMember(action);

	}

	void deleteSuggestionAction(SuggestionAction action) {

		SuggestionActionsDAO.getInstance(MatchstixApplication.getInstance())
				.delete(action);
	}

	Actions loadSuggestionActions() {
		return SuggestionActionsDAO.getInstance(
				MatchstixApplication.getInstance()).loadActions();
	}

	void updateFriend(Friend friend) {
		FriendsDAO.getInstance(MatchstixApplication.getInstance()).update(
				friend);
	}

	void deleteFriend(Friend friend) {
		FriendsDAO.getInstance(MatchstixApplication.getInstance()).delete(
				friend);
	}

	Roster loadFriends() {
		return FriendsDAO.getInstance(MatchstixApplication.getInstance())
				.loadFriends();
	}
	
	
}
