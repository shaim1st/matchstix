package com.matchstixapp.db;

import java.util.Date;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import com.craterzone.cz_commons_lib.Logger;
import com.matchstixapp.enums.Tables;
import com.matchstixapp.enums.Tables.Friends;
import com.matchstixapp.modal.Friend;
import com.matchstixapp.modal.Roster;

public class FriendsDAO {
	private static final String TAG = FriendsDAO.class.getSimpleName();
	private static FriendsDAO instance;
	private SQLiteDatabase database;
	private Context context;
	private StorageController controller;

	public static FriendsDAO getInstance(Context context) {
		if (instance == null) {
			instance = new FriendsDAO(context);
		}
		return instance;
	}

	private FriendsDAO(Context context) {
		if (controller == null) {
			this.context = context;
			controller = StorageController.getInstance(context);
		}

	}

	public void open() throws SQLException {
		if (controller == null) {
			controller = StorageController.getInstance(context);
		} else {
			database = controller.getWritableDatabase();
		}
	}

	public boolean isOpen() {
		if (database != null) {
			return database.isOpen();
		}
		return false;
	}

	public void close() {
		if (controller != null) {
			controller.close();
		}
	}

	public void openDBIfClosed() {
		if (!isOpen()) {
			open();
		}
	}

	public void update(Friend friend) {

		openDBIfClosed();
		try {
			if (database != null) {
				StringBuilder sb =new StringBuilder();
				sb.append("INSERT OR REPLACE INTO ");
				sb.append(Tables.FRIENDS.getLabel() );
				sb.append("(");
				sb.append(Friends.COL_USER_ID.getLabel()+",");
				sb.append(Friends.COL_FIRST_NAME.getLabel()+",");
				sb.append(Friends.COL_LAST_NAME.getLabel()+",");
				sb.append(Friends.COL_PIC_URL.getLabel()+",");
				sb.append(Friends.COL_MATCHED_DATE.getLabel()+",");
				sb.append(Friends.COL_CHAT_DATE.getLabel());
				sb.append(")");
				sb.append("VALUES(");
				sb.append(friend.getUserId()+",");
				sb.append("'"+friend.getFirstName()+"',");
				sb.append("'"+friend.getLastName()+"',");
				sb.append("'"+friend.getProfilePicURL()+"',");
				sb.append("'"+friend.getMatchDate()+"',");
				sb.append(new Date().getTime());
				sb.append(")");
				String query=sb.toString();
				Logger.d(TAG, "update()- QUERY: "+query);
				database.execSQL(query);
			}
		} catch (Exception e) {
			Logger.e(TAG, "ER: update()- ", e);
		}
	}

	/**
	 * Get member List From DB
	 * 
	 * @return member List and if error or not found then null
	 */
	Roster loadFriends() {
		/**
		 * Check if DB is open : if not open then open
		 */
		openDBIfClosed();
		Cursor cursor = null;
		try {
			if (database != null) {
				String query = "SELECT * FROM " + Tables.FRIENDS.getLabel();
				cursor = database.rawQuery(query, null);
				if (cursor != null) {
					cursor.moveToFirst();
					while (cursor.moveToNext()) {

						long userId = cursor.getLong(cursor
								.getColumnIndex(Tables.Friends.COL_USER_ID
										.getLabel()));
						String firstName = cursor.getString(cursor
								.getColumnIndex(Tables.Friends.COL_FIRST_NAME
										.getLabel()));
						String lastName = cursor.getString(cursor
								.getColumnIndex(Tables.Friends.COL_LAST_NAME
										.getLabel()));
						String profilePicURL = cursor.getString(cursor
								.getColumnIndex(Tables.Friends.COL_PIC_URL
										.getLabel()));
						long matchDate = cursor.getLong(cursor
								.getColumnIndex(Tables.Friends.COL_MATCHED_DATE
										.getLabel()));
						long chatDate = cursor.getLong(cursor
								.getColumnIndex(Tables.Friends.COL_CHAT_DATE
										.getLabel()));
						Roster.getInstance().addFriend(userId, firstName, lastName, profilePicURL, matchDate,chatDate);
					}
				}
			}
		} catch (Exception e) {
			Logger.e(TAG, "ER: loadFriends()-", e);
		} finally {
			controller.close(cursor);
		}
		return Roster.getInstance();
	}

	boolean delete(Friend friend) {

		openDBIfClosed();
		try {
			if (database != null) {
				String whereClause = Tables.Suggestions.COL_USER_ID.getLabel() + "=?";
				String[] whereArgs = new String[] { String.valueOf(friend.getUserId()) };

				return database.delete(Tables.FRIENDS.getLabel(), whereClause,
						whereArgs) > 0;
			}
		} catch (Exception e) {
			Logger.e(TAG, "ER: update()- ", e);
		}
		return false;

		/*String whereClause = Tables.Suggestions.COL_USER_ID.getLabel() + "=?";
		String[] whereArgs = new String[] { String.valueOf(friend.getUserId()) };

		return database.delete(Tables.FRIENDS.getLabel(), whereClause,
				whereArgs) > 0;*/
	}
}
