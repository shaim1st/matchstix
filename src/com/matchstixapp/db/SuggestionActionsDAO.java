package com.matchstixapp.db;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import com.craterzone.cz_commons_lib.Logger;
import com.matchstixapp.enums.Tables;

public class SuggestionActionsDAO {
	private static final String TAG = SuggestionActionsDAO.class
			.getSimpleName();
	private static SuggestionActionsDAO instance;
	private SQLiteDatabase database;
	private Context context;
	private StorageController controller;

	public static SuggestionActionsDAO getInstance(Context context) {
		if (instance == null) {
			instance = new SuggestionActionsDAO(context);
		}
		return instance;
	}

	private SuggestionActionsDAO(Context context) {
		if (controller == null) {
			this.context = context;
			controller = StorageController.getInstance(context);
		}

	}

	public void open() throws SQLException {
		if (controller == null) {
			controller = StorageController.getInstance(context);
		} else {
			database = controller.getWritableDatabase();
		}
	}

	public boolean isOpen() {
		if (database != null) {
			return database.isOpen();
		}
		return false;
	}

	public void close() {
		if (controller != null) {
			controller.close();
		}
	}

	void openDBIfClosed() {
		if (!isOpen()) {
			open();
		}
	}

	void insertMember(SuggestionAction action) {

		openDBIfClosed();
		try {
			if (database != null) {
				String sql = "INSERT OR REPLACE INTO "
						+ Tables.SUGGESTION_ACTIONS.getLabel()
						+ " VALUES (?,?);";
				SQLiteStatement statement = database.compileStatement(sql);
				database.beginTransaction();

				statement.bindLong(1, action.getUserId());
				statement.bindString(2, String.valueOf(action.isLiked()));

				statement.execute();
				database.setTransactionSuccessful();
				database.endTransaction();
			}
		} catch (Exception e) {
			Logger.e(TAG, "Enable to Add Patient in Database :", e);
		}
	}

	/**
	 * Get member List From DB
	 * 
	 * @return member List and if error or not found then null
	 */
	Actions loadActions() {
		/**
		 * Check if DB is open : if not open then open
		 */
		openDBIfClosed();
		Actions actions = Actions.newInstance();
		Cursor cursor = null;
		try {
			if (database != null) {
				String query = "SELECT * FROM "
						+ Tables.SUGGESTION_ACTIONS.getLabel();
				cursor = database.rawQuery(query, null);
				if (cursor != null) {
					while (cursor.moveToNext()) {

						long userId = cursor
								.getLong(cursor
										.getColumnIndex(Tables.SuggestionActions.COL_USER_ID
												.getLabel()));
						boolean liked = Boolean
								.parseBoolean(cursor.getString(cursor
										.getColumnIndex(Tables.SuggestionActions.COL_LIKE
												.getLabel())));
						actions.add(userId, liked);
					}
				}
			}
		} catch (Exception e) {
			Logger.e(TAG, "Unable to get data from database", e);
		} finally {
			controller.close(cursor);
		}
		return actions;
	}

	boolean delete(SuggestionAction action) {

		String whereClause = Tables.Suggestions.COL_USER_ID.getLabel() + "=?";
		String[] whereArgs = new String[] { String.valueOf(action.getUserId()) };

		return database.delete(Tables.SUGGESTION_ACTIONS.getLabel(),
				whereClause, whereArgs) > 0;
	}

}
