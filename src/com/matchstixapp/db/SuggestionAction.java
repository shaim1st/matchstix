package com.matchstixapp.db;

public class SuggestionAction {
	private long userId;
	private boolean liked;

	public SuggestionAction(long userId, boolean liked) {
		this.userId = userId;
		this.liked = liked;
	}

	public long getUserId() {
		return userId;
	}

	public boolean isLiked() {
		return liked;
	}

	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		} else if (!(o instanceof SuggestionAction)) {
			return false;
		}
		SuggestionAction action = (SuggestionAction) o;
		return action.userId == this.userId;
	}

	@Override
	public int hashCode() {
		return String.valueOf(userId).hashCode();
	}
}
