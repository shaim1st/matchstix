package com.matchstixapp.db;

import java.util.ArrayList;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;
import com.craterzone.cz_commons_lib.Logger;
import com.matchstixapp.enums.Tables;
import com.matchstixapp.modal.SuggestionDetail;


public class SuggestionsDAO {
	private static final String TAG =SuggestionsDAO.class.getName();
	private static SuggestionsDAO instance;
	private SQLiteDatabase database;
	private Context context;
	private StorageController controller;
	
	public static SuggestionsDAO getInstance(Context context) {
		if (instance == null) {
			instance = new SuggestionsDAO(context);
		}
		return instance;
	}

	private SuggestionsDAO(Context context) {
		if (controller == null) {
			this.context = context;
            controller = StorageController.getInstance(context);
		}
		
	}
	
	public void open() throws SQLException {
		if (controller == null) {
			controller = StorageController.getInstance(context);
		} else {
			database = controller.getWritableDatabase();
		}
	}
	
	public boolean isOpen() {
		if (database != null) {
			return database.isOpen();
		}
		return false;
	}
	
	public void close() {
		if (controller != null) {
			controller.close();
		}
	}
	
	public void openDBIfClosed() {
		if (!isOpen()) {
			open();
		}
	}
	
	
	public ArrayList<SuggestionDetail> getSuggestions() {
		/**
		 * Check if DB is open : if not open then open
		 */
		
	    database =controller.getReadableDatabase();
		Cursor cursor = null;
		
		ArrayList<SuggestionDetail> suggestionsList = new ArrayList<SuggestionDetail>();
		try {
			if (database != null) {
				
				String query = "SELECT * FROM " + Tables.SUGGESTIONS.getLabel();
				
				cursor = database.rawQuery(query, null);
				Log.d("cursor is received", "now check");
				
				if (cursor != null) {
					while (cursor.moveToNext()) {
					SuggestionDetail suggestion = new SuggestionDetail();
					suggestion.setUserId(cursor.getLong(0));
					suggestion.setFirstName(cursor.getString(1));
					suggestion.setLastName(cursor.getString(2));
					suggestion.setAge(cursor.getInt(3));
					suggestion.setNoOfCommonFriend(cursor.getInt(4));
					suggestion.setNoOfCommonInterest(cursor.getInt(5));
					
					suggestionsList.add(suggestion);
				  }
				}
			}
		} catch (Exception e) {
			Logger.e(TAG, "Unable to get data from database", e);
		} finally {
			controller.close(cursor);
		}
		return suggestionsList;
	}
	
	
	
	public void insertMember(SuggestionDetail  suggestionDetail) {

		
		openDBIfClosed();
		
		try {
			if (database != null) {
				String sql = "INSERT OR REPLACE INTO " + Tables.SUGGESTIONS.getLabel()
						+ " VALUES (?,?,?,?,?,?);";
				SQLiteStatement statement = database.compileStatement(sql);
				database.beginTransaction();
				statement.clearBindings();
				
				statement.bindLong(1, suggestionDetail.getUserId());
				statement.bindString(2, suggestionDetail.getFirstName());
				statement.bindString(3, suggestionDetail.getLastName());
				statement.bindLong(4, suggestionDetail.getAge());
				statement.bindLong(5, suggestionDetail.getNoOfCommonFriend());
				statement.bindLong(6, suggestionDetail.getNoOfCommonInterest());
				
				
				statement.execute();
				database.setTransactionSuccessful();
				database.endTransaction();
			}
		} catch (Exception e) {
			Logger.e(TAG, "Enable to Add Patient in Database :", e);
		}
	}

	
	
	public boolean delete(SuggestionDetail suggestion) {
		
		String whereClause = Tables.Suggestions.COL_USER_ID.getLabel() + "=?";
		String[] whereArgs = new String[] { String.valueOf(suggestion.getUserId())};
		
		return database.delete(Tables.SUGGESTIONS.getLabel(), whereClause, whereArgs) > 0;
	}
	
}
