package com.matchstixapp.db;

import java.util.ArrayList;

public class Actions {

	private ArrayList<SuggestionAction> actions;

	private Actions() {
		actions = new ArrayList<SuggestionAction>();
	}

	public SuggestionAction add(long userId, boolean liked) {
		SuggestionAction action = new SuggestionAction(userId, liked);
		if (!actions.contains(action)) {
			actions.add(action);
			return action;
		}
		return null;
	}
	
	public boolean contains(long userId) {
		SuggestionAction action = null;
		for (int i = actions.size()-1; i >= 0; i--) {
			action = actions.get(i);
			if (action.getUserId() == userId) {
				return true;
			}
		}
		return false;
	}
	public SuggestionAction delete(long userId) {
		SuggestionAction action = null;
		for (int i = 0; i < actions.size(); i++) {
			action = actions.get(i);
			if (action.getUserId() == userId) {
				actions.remove(i);
				break;
			}
		}
		return action;
	}

	public SuggestionAction delete(SuggestionAction action) {
		return delete(action.getUserId());
	}

	public SuggestionAction[] getAllActions() {
		SuggestionAction[] list = new SuggestionAction[actions.size()];
		return actions.toArray(list);

	}

	static Actions newInstance() {
		return new Actions();
	}

	public SuggestionAction get(int index) {
		return actions.get(0);
	}

	public SuggestionAction delete(int index) {
		SuggestionAction action=get(0);
		actions.remove(0);
		return action;
	}

	public int size() {
		return actions.size();
	}

	public boolean isEmpty() {
		return size() <= 0;
	}

}
