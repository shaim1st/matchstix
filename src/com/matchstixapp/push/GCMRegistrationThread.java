package com.matchstixapp.push;

import android.content.Context;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.matchstixapp.AppConfig;
import com.matchstixapp.db.DataController;
import com.matchstixapp.logger.Logger;

public class GCMRegistrationThread implements Runnable {
	

	private static final String TAG = GCMRegistrationThread.class.getSimpleName();
	
	private Context mContext;
	
	public GCMRegistrationThread(Context context) {
		mContext = context;
	}
	
	@Override
	public void run() {
		try {
			GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(mContext);
			String regId = gcm.register(AppConfig.PUSH_APP_NUMBER);
			DataController.getInstance().getPreferences().saveGCMRegId(regId);
			Logger.d(TAG, "PUSH Registered: REG_ID= "+regId);
			
		} catch (Exception ex) {
			Log.e(TAG, "Error in registraing on GCM ", ex);
		}
	}

}
