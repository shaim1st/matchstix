package com.matchstixapp.push;

import android.annotation.SuppressLint;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.matchstixapp.R;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.matchstixapp.MatchstixApplication;
import com.matchstixapp.listener.Keys;
import com.matchstixapp.requester.FriendsDownloadRequester;
import com.matchstixapp.ui.activity.LetsTalkActivity;
import com.matchstixapp.ui.activity.SplashActivity;
import com.matchstixapp.utils.BackgroundExecutor;
import com.xabber.android.service.XabberService;

@SuppressLint("NewApi")
public class GCMNotificationIntentService extends IntentService {

	public int NOTIFICATION_ID = 1;

	public GCMNotificationIntentService() {
		super("GCMNotificationIntentService");
	}

	public static final String TAG = "GCMNotificationIntentService";

	@Override
	protected void onHandleIntent(Intent intent) {
		Bundle extras = intent.getExtras();
		GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
		Log.d(TAG, "PUSH_MESSAGE: " + extras.toString());

		String messageType = gcm.getMessageType(intent);
		if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
			
			//parsing fromUserId to long
			long fromUserId = 0;
			try {
				fromUserId = Long.parseLong(extras.getString("fromUserId"));
			} catch(NumberFormatException e) {
				Log.e("NumberFormatException", "Invalid fromUserId in push notification chat");
			} catch(Exception e) {
				Log.e("NumberFormatException", "Invalid fromUserId in push notification chat");
			}
			
			//Categorizing notification
			String notificationType = extras.getString("type");
			if(notificationType == null ) {
				Log.i("Unknown push notification", "Unknown push notification with type=?");
			} else if(notificationType.equalsIgnoreCase("1")) {
				FriendsDownloadRequester requester  = new FriendsDownloadRequester();
				if( !requester.isPending() ) {
					BackgroundExecutor.getInstance()
					.execute(requester);
					
				}
				if(fromUserId != 0 ) {
					showLikeNotification(fromUserId, extras.getString("message"));
				}
			} else if(extras.getString("type").equalsIgnoreCase("0")) {
				startService(XabberService.createIntent(getApplicationContext()));
				// Need to start xmpp Service only here
			} else {
				Log.i("Unknown push notification", "Unknown push notification with type=?");
			}
			
		}
		GcmBroadcastReceiver.completeWakefulIntent(intent);
	}

	private void showLikeNotification(long friendId, String message) {
		// Prepare intent which is triggered if the
		// notification is selected
		Intent intent = new Intent(getApplicationContext(),
				LetsTalkActivity.class);
		// Sets the Activity to start in a new, empty task
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
				| Intent.FLAG_ACTIVITY_CLEAR_TASK);
		intent.putExtra(Keys.KEY_FRIEND_ID, friendId);

		if(MatchstixApplication.getInstance().isAppInBackGround()) {
			// Creates the PendingIntent
			PendingIntent pIntent = PendingIntent.getActivity(this, 0, intent,
					PendingIntent.FLAG_UPDATE_CURRENT);

			// Build notification
			// Actions are just fake
			NotificationCompat.Builder noti = new NotificationCompat.Builder(this)
					.setContentTitle("Someone wants to be friends with you.")
					.setContentText(message)
					.setSmallIcon(R.drawable.ic_launcher)
					.setContentIntent(pIntent)
					.setOngoing(true)
					.setAutoCancel(true)
					.setSound(
							RingtoneManager
									.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
					.setOngoing(true).setAutoCancel(true)
					.setPriority(NotificationCompat.PRIORITY_MAX)
					.setWhen(System.currentTimeMillis());

			NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
			// hide the notification after its selected
			Notification notification = noti.build();
			notification.flags = Notification.FLAG_AUTO_CANCEL;
			manager.notify(NOTIFICATION_ID++, notification);
		} else {
			startActivity(intent);
			
		}
	}

	/*private void showMsgNotification(long friendId, String message) {
		// Prepare intent which is triggered if the
		// notification is selected
		Intent intent = new Intent(getApplicationContext(),
				ChatViewer.class);
		Intent intent = ChatViewer.createIntent(this, DataController.getInstance().getFullJid(), String.valueOf(message));
		//intent.putExtra(Keys.KEY_FRIEND_ID, friendId);
		
		 Create a back stack based on the Intent that starts the Activity 
		TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
		// Adds the back stack
		stackBuilder.addParentStack(ChatViewer.class);
		// Adds the Intent to the top of the stack
		stackBuilder.addNextIntent(intent);
		// Gets a PendingIntent containing the entire back stack
		PendingIntent pIntent = stackBuilder.getPendingIntent(0,
				PendingIntent.FLAG_UPDATE_CURRENT);

		// Build notification
		// Actions are just fake
		NotificationCompat.Builder noti = new NotificationCompat.Builder(this)
				.setContentTitle("Someone messaged you.")
				.setContentText(message)
				.setSmallIcon(R.drawable.ic_launcher)
				.setContentIntent(pIntent)
				.setOngoing(true)
				.setAutoCancel(true)
				.setSound(
						RingtoneManager
								.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
				.setOngoing(true).setAutoCancel(true)
				.setPriority(NotificationCompat.PRIORITY_MAX)
				.setWhen(System.currentTimeMillis());

		NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		// hide the notification after its selected
		Notification notification = noti.build();
		notification.flags = Notification.FLAG_AUTO_CANCEL;
		manager.notify(NOTIFICATION_ID++, notification);
	}*/

	/* not yet called! */
	private void showNotification(String data) {
		Intent notiIntent = new Intent(getApplicationContext(),
				SplashActivity.class);
		notiIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
				| Intent.FLAG_ACTIVITY_NEW_TASK);
		notiIntent.putExtra("notification", true);
		PendingIntent pIntent = PendingIntent.getActivity(this, 0, notiIntent,
				PendingIntent.FLAG_CANCEL_CURRENT);

		NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(
				this)
				.setContentTitle("Push Message")
				.setContentText(data)
				// .setContentInfo("Data: " + data)
				.setSmallIcon(R.drawable.ic_launcher)
				.setContentIntent(pIntent)
				.setOngoing(true)
				.setAutoCancel(true)
				.setSound(
						RingtoneManager
								.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
				.setOngoing(true).setAutoCancel(true)
				.setPriority(NotificationCompat.PRIORITY_MAX)
				.setWhen(System.currentTimeMillis());
		NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		Notification notification = notificationBuilder.build();
		notification.flags = Notification.FLAG_AUTO_CANCEL;
		notificationManager.notify(NOTIFICATION_ID++, notification);

	}

}
