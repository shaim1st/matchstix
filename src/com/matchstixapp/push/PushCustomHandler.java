package com.matchstixapp.push;

import java.util.HashMap;

import com.craterzone.cz_commons_lib.Logger;
import com.matchstixapp.MatchstixApplication;
import com.matchstixapp.R;
import com.matchstixapp.listener.Keys;
import com.matchstixapp.requester.FriendsDownloadRequester;
import com.matchstixapp.ui.activity.LetsTalkActivity;
import com.matchstixapp.ui.activity.SplashActivity;
import com.matchstixapp.utils.BackgroundExecutor;
import com.pushbots.push.PBNotificationIntent;
import com.pushbots.push.Pushbots;
import com.pushbots.push.utils.PBConstants;
import com.xabber.android.service.XabberService;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

public class PushCustomHandler extends BroadcastReceiver{
	
	private static final String TAG = "customHandler";
	public int NOTIFICATION_ID = 1;
	
    @Override
    public void onReceive(Context context, Intent intent)
    {
        String action = intent.getAction();
        Log.d(TAG, "action=" + action);
        // Handle Push Message when opened
        if (action.equals(PBConstants.EVENT_MSG_OPEN)) {
			//Check for Pushbots Instance
			Pushbots pushInstance = Pushbots.sharedInstance();
            if(!pushInstance.isInitialized()){
                Log.d(TAG, "Initializing Pushbots.");
                Pushbots.sharedInstance().init(context.getApplicationContext());
            }
			
			//Clear Notification array
            if(PBNotificationIntent.notificationsArray != null){
                PBNotificationIntent.notificationsArray = null;
            }
			
            HashMap<?, ?> PushdataOpen = (HashMap<?, ?>) intent.getExtras().get(PBConstants.EVENT_MSG_OPEN);
            Log.w(TAG, "User clicked notification with Message: " + PushdataOpen.get("message"));
			
			//Report Opened Push Notification to Pushbots
            if(Pushbots.sharedInstance().isAnalyticsEnabled()){
                Pushbots.sharedInstance().reportPushOpened( (String) PushdataOpen.get("PUSHANALYTICS"));
            }
            String packageName = context.getPackageName();
            Intent resultIntent = new Intent(context.getPackageManager().getLaunchIntentForPackage(packageName));
            resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_CLEAR_TASK);
            long fromUserId = Long.parseLong((String) intent.getStringExtra("fromUserId"));
            Logger.d("Push", ""+fromUserId);
            Logger.d("Pushdata",""+ intent.getBundleExtra("pushData"));
            if(fromUserId != 0){
            	intent.putExtra(Keys.KEY_FRIEND_ID, fromUserId);
        	}
		    //resultIntent.putExtras(intent.getBundleExtra("pushData"));
			Pushbots.sharedInstance().startActivity(resultIntent);
			
            // Handle Push Message when received
        }else if(action.equals(PBConstants.EVENT_MSG_RECEIVE)){
            HashMap<?, ?> PushdataOpen = (HashMap<?, ?>) intent.getExtras().get(PBConstants.EVENT_MSG_RECEIVE);
            Log.w(TAG, "User Received notification with Message: " + PushdataOpen.get("message"));
          //parsing fromUserId to long
			long fromUserId = 0;
			try {
				fromUserId = Long.parseLong((String) PushdataOpen.get("fromUserId"));
			} catch(NumberFormatException e) {
				Log.e("NumberFormatException", "Invalid fromUserId in push notification chat");
				return;
			} catch(Exception e) {
				Log.e("NumberFormatException", "Invalid fromUserId in push notification chat");
			}
            String notificationType = (String) PushdataOpen.get("type");
			if(notificationType == null ) {
				Log.i(TAG, "Unknown push notification with type=?");
				showNotification(context, (String)PushdataOpen.get("message"));
			} else if(notificationType.equalsIgnoreCase("1")) {
				FriendsDownloadRequester requester  = new FriendsDownloadRequester();
				if( !requester.isPending() ) {
					BackgroundExecutor.getInstance().execute(requester);
				}
				if(fromUserId != 0 ) {
					showLikeNotification(context, fromUserId, (String)PushdataOpen.get("message"));
				}
			} else if(((String)PushdataOpen.get("type")).equalsIgnoreCase("0")) {
				context.startService(XabberService.createIntent(context));
			} 
        }
    }
    
    private void showLikeNotification(Context context, long friendId, String message) {
		// Prepare intent which is triggered if notification is selected
		Intent intent = new Intent(context,LetsTalkActivity.class);
		// Sets the Activity to start in a new, empty task
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
		intent.putExtra(Keys.KEY_FRIEND_ID, friendId);

		if(MatchstixApplication.getInstance().isAppInBackGround()) {
			// Creates the PendingIntent
			PendingIntent pIntent = PendingIntent.getActivity(context, 0, intent,
					PendingIntent.FLAG_UPDATE_CURRENT);
			Log.w(TAG, "App is in background ");

			// Build notification
			NotificationCompat.Builder noti = new NotificationCompat.Builder(context)
					.setContentTitle(context.getString(R.string.matchstix))
					.setContentText(message)
					.setSmallIcon(R.drawable.ic_notification_logo)
					.setContentIntent(pIntent)
					.setOngoing(true)
					.setAutoCancel(true)
					.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
					.setOngoing(true).setAutoCancel(true)
					.setPriority(NotificationCompat.PRIORITY_MAX)
					.setWhen(System.currentTimeMillis());

			NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
			// hide the notification after its selected
			Notification notification = noti.build();
			notification.flags = Notification.FLAG_AUTO_CANCEL;
			manager.notify(NOTIFICATION_ID++, notification);
		} else {
			Log.w(TAG, "App is in foreground ");
			context.startActivity(intent);
			
		}
	}
    
    private void showNotification(Context context, String message){
    	Intent intent = new Intent(context,
				SplashActivity.class);
		// Sets the Activity to start in a new, empty task
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
				| Intent.FLAG_ACTIVITY_CLEAR_TASK);

			// Creates the PendingIntent
			PendingIntent pIntent = PendingIntent.getActivity(context, 0, intent,
					PendingIntent.FLAG_UPDATE_CURRENT);

			// Build notification
			// Actions are just fake
			NotificationCompat.Builder noti = new NotificationCompat.Builder(context)
					.setContentTitle(context.getString(R.string.matchstix))
					.setContentText(message)
					.setSmallIcon(R.drawable.ic_notification_logo)
					.setContentIntent(pIntent)
					.setOngoing(true)
					.setAutoCancel(true)
					.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
					.setOngoing(true).setAutoCancel(true)
					.setPriority(NotificationCompat.PRIORITY_MAX)
					.setWhen(System.currentTimeMillis());

			NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
			// hide the notification after its selected
			Notification notification = noti.build();
			notification.flags = Notification.FLAG_AUTO_CANCEL;
			manager.notify(NOTIFICATION_ID++, notification);
    }
    
   
}
