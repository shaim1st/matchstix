package com.matchstixapp.utils;


import com.matchstixapp.R;
import com.matchstixapp.ui.fragments.SettingsFragment;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.EditText;

/*
 * class to give custom typeface TextView extends to TextView.
 */
public class TypeFaceEditView extends EditText {
	private static final String TAG = TypeFaceEditView.class.getName();
	
	private Context context;

	public TypeFaceEditView(Context context) {
		super(context);
		this.context = context;
		init();
	}

	public TypeFaceEditView(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
		init();
	}

	public TypeFaceEditView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		this.context = context;
		TypedArray a = null;
		try {
			a = context.obtainStyledAttributes(attrs, R.styleable.TypeFaceTextView, 0, 0);
		} 
		catch (Exception e) {
			Log.d(TAG, "Exception", e);
		} 
		finally {
			if (a != null) 
			{
				a.recycle();
			}
		}
		
		init();
	}
	

	private void init() {
		if (!isInEditMode()) {
			Typeface currentTypeFace = this.getTypeface();
			setTypeface(Typeface.createFromAsset(getContext().getAssets(),Utils.getFontPath(currentTypeFace)));
		}
	}
}
