package com.matchstixapp.utils;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.app.Activity;

import com.matchstixapp.R;
import com.matchstixapp.db.MatchstixPreferences;
import com.matchstixapp.listener.LocationUpdatedBroadcastReceiver;
import com.matchstixapp.listener.MatchstixDialogListener;

public class LocationTracker implements LocationListener {
	private final Context mContext;
	// flag for GPS Status
	boolean isGPSEnabled = false;

	// flag for network status
	boolean isNetworkEnabled = false;

	boolean canGetLocation = false;

	Location location;
	double latitude;
	double longitude;

	// The minimum distance to change updates in metters
	private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 1000; // 10
																	// metters

	// The minimum time beetwen updates in milliseconds
	private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1; // 1 minute

	// Declaring a Location Manager
	protected LocationManager locationManager;

	public LocationTracker(Context context) {
		this.mContext = context;
		getLocation();
	}

	public Location getLocation() {
		try {
			locationManager = (LocationManager) mContext
					.getSystemService(Context.LOCATION_SERVICE);

			// getting GPS status
			isGPSEnabled = locationManager
					.isProviderEnabled(LocationManager.GPS_PROVIDER);

			// getting network status
			isNetworkEnabled = locationManager
					.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

			if (!isGPSEnabled && !isNetworkEnabled) {
				// no network provider is enabled
			} else {
				this.canGetLocation = true;

				// First get location from Network Provider
				if (isNetworkEnabled) {
					locationManager.requestLocationUpdates(
							LocationManager.NETWORK_PROVIDER,
							MIN_TIME_BW_UPDATES,
							MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

					Log.d("Network", "Network");

					if (locationManager != null) {
						location = locationManager
								.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
						updateGPSCoordinates();
					}
				}

				// if GPS Enabled get lat/long using GPS Services
				if (isGPSEnabled) {
					if (location == null) {
						locationManager.requestLocationUpdates(
								LocationManager.GPS_PROVIDER,
								MIN_TIME_BW_UPDATES,
								MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

						Log.d("GPS Enabled", "GPS Enabled");

						if (locationManager != null) {
							location = locationManager
									.getLastKnownLocation(LocationManager.GPS_PROVIDER);
							updateGPSCoordinates();
						}
					}
				}
			}
		} catch (Exception e) {
			// e.printStackTrace();
			Log.e("Error : Location",
					"Impossible to connect to LocationManager", e);
		}

		return location;
	}

	public void updateGPSCoordinates() {
		if (location != null) {
			latitude = location.getLatitude();
			longitude = location.getLongitude();
		}
	}

	/**
	 * Stop using GPS listener Calling this function will stop using GPS in your
	 * app
	 */

	public void stopUsingGPS() {
		if (locationManager != null) {
			locationManager.removeUpdates(LocationTracker.this);
		}
	}

	/**
	 * Function to get latitude
	 */
	public double getLatitude() {
		if (location != null) {
			latitude = location.getLatitude();
		}

		return latitude;
	}

	/**
	 * Function to get longitude
	 */
	public double getLongitude() {
		if (location != null) {
			longitude = location.getLongitude();
		}

		return longitude;
	}

	/**
	 * Function to check GPS/wifi enabled
	 */
	public boolean canGetLocation() {
		return this.canGetLocation;
	}

	/**
	 * Function to show settings alert dialog
	 */

	public void showSettingsAlert() {

		String title = mContext.getResources().getString(R.string.location);
		String message = mContext.getResources().getString(
				R.string.enable_loc_setting);
		String btn1Text = mContext.getResources().getString(R.string.settings);
		String btn2Text = mContext.getResources().getString(R.string.cancel);
		Utils.showDialog(mContext, title, message, btn1Text, btn2Text, false,
				false, new MatchstixDialogListener() {

					@Override
					public void onButtonTwoClick() {
						/*if (!isGPSEnabled()) {
							showSettingsAlert();
						} else {
							if (mContext instanceof SplashActivity) {
								SplashActivity activity = (SplashActivity) mContext;
								activity.delayNextActivityLaunch(false);
							}
						}*/
						((Activity) mContext).finish();
					}

					@Override
					public void onButtonOneClick() {
						Intent intent = new Intent(
								Settings.ACTION_LOCATION_SOURCE_SETTINGS);
						mContext.startActivity(intent);
					}
				});
	}

	/**
	 * Get list of address by latitude and longitude
	 * 
	 * @return null or List<Address>
	 */
	public List<Address> getGeocoderAddress(Context context) {
		if (location != null) {
			Geocoder geocoder = new Geocoder(context, Locale.ENGLISH);
			try {
				List<Address> addresses = geocoder.getFromLocation(latitude,
						longitude, 1);
				return addresses;
			} catch (IOException e) {
				// e.printStackTrace();
				Log.e("Error : Geocoder", "Impossible to connect to Geocoder",
						e);
			}
		}

		return null;
	}

	/**
	 * Try to get AddressLine
	 * 
	 * @return null or addressLine
	 */
	public String getAddressLine(Context context) {
		List<Address> addresses = getGeocoderAddress(context);
		if (addresses != null && addresses.size() > 0) {
			Address address = addresses.get(0);
			String addressLine = address.getAddressLine(0);

			return addressLine;
		} else {
			return null;
		}
	}

	/**
	 * Try to get Locality
	 * 
	 * @return null or locality
	 */
	public String getLocality(Context context) {
		List<Address> addresses = getGeocoderAddress(context);
		if (addresses != null && addresses.size() > 0) {
			Address address = addresses.get(0);
			String locality = address.getLocality();

			return locality;
		} else {
			return null;
		}
	}

	/**
	 * Try to get Postal Code
	 * 
	 * @return null or postalCode
	 */
	public String getPostalCode(Context context) {
		List<Address> addresses = getGeocoderAddress(context);
		if (addresses != null && addresses.size() > 0) {
			Address address = addresses.get(0);
			String postalCode = address.getPostalCode();

			return postalCode;
		} else {
			return null;
		}
	}

	/**
	 * Try to get CountryName
	 * 
	 * @return null or postalCode
	 */
	public String getCountryName(Context context) {
		List<Address> addresses = getGeocoderAddress(context);
		if (addresses != null && addresses.size() > 0) {
			Address address = addresses.get(0);
			String countryName = address.getCountryName();

			return countryName;
		} else {
			return null;
		}
	}

	@Override
	public void onLocationChanged(Location location) {

		if (mContext != null && location != null) {
			// updating location on Shared Preference
			MatchstixPreferences.getInstance().setLocation(
					""+location.getLatitude(), ""+location.getLongitude());
			mContext.sendBroadcast(new Intent(mContext,
					LocationUpdatedBroadcastReceiver.class));
		}
	}

	/**
	 * Try to get StateName
	 * 
	 * @return null or postalCode
	 */

	public String getStateName() {
		if (location != null) {
			String state;
			try {
				Geocoder gcd = new Geocoder(mContext, Locale.getDefault());
				List<Address> addresses = gcd.getFromLocation(
						location.getLatitude(), location.getLongitude(), 1);// 1
																			// for
																			// single
																			// recode
																			// from
																			// multiple
																			// for
																			// this
																			// location
				if (addresses.size() > 0) {
					state = addresses.get(0).getAdminArea();
					return state;
				}
			} catch (Exception e) {
				Log.e("Location Activity", "Error in getting Address");
			}
		}
		return "";
	}

	/**
	 * Try to get CityName
	 * 
	 * @return null or postalCode
	 */
	public String getCityName() {
		if (location != null) {
			String city;
			try {
				Geocoder gcd = new Geocoder(mContext, Locale.getDefault());
				List<Address> addresses = gcd.getFromLocation(
						location.getLatitude(), location.getLongitude(), 1);// 1
																			// for
																			// single
																			// recode
																			// from
																			// multiple
																			// for
																			// this
																			// location
				if (addresses.size() > 0) {
					city = addresses.get(0).getLocality();
					return city;
				}
			} catch (Exception e) {
				Log.e("Location Activity", "Error in getting Address");
			}
		}
		return "";
	}

	@Override
	public void onProviderDisabled(String provider) {
	}

	@Override
	public void onProviderEnabled(String provider) {
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
	}

	public static String getUDID(Context c) {
		return String.valueOf(Settings.Secure.getString(c.getContentResolver(),
				Settings.Secure.ANDROID_ID));
	}

	public boolean isGPSEnabled() {
		return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
	}

}
