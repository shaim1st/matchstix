package com.matchstixapp.utils;

import android.content.Context;
import android.content.Intent;
import com.matchstixapp.R;
import com.matchstixapp.listener.MatchstixDialogListener;

public class BlockedPopup {
	private static BlockedPopup _instance;
	static int isOTPDialog=0;
	private BlockedPopup() {
	}

	public static BlockedPopup getInstance() {
		if (_instance == null)
			_instance = new BlockedPopup();
		return _instance;
	}
	public Intent openEmail(Context context, String[] emailTo, String[] emailCC,String subject, String emailText) {
		final Intent emailIntent = new Intent(Intent.ACTION_SEND_MULTIPLE);
		emailIntent.setClassName("com.google.android.gm","com.google.android.gm.ComposeActivityGmail");
		emailIntent.setType("text/plain ");
		emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL,emailTo );
		emailIntent.putExtra(android.content.Intent.EXTRA_CC,emailCC);
		emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
		emailIntent.putExtra(Intent.EXTRA_TEXT, emailText);
		return emailIntent;
	}
	public void showBlockedUserDialog(final Context context) {
		if(isOTPDialog==0)
		Utils.showDialogTwoButton(context,
				context.getString(R.string.blockeduser_msg),
				context.getString(R.string.contact),
				context.getString(R.string.cancel),
				new MatchstixDialogListener() {

					@Override
					public void onButtonTwoClick() {
						isOTPDialog=0;
					}

					@Override
					public void onButtonOneClick() {
						isOTPDialog=0;
						context.startActivity(openEmail(context,
								new String[] { "matchstixadmin@gmail.com" },
								null, null, null));
						/*
						 * AccountManager.getInstance().disableAccount(
						 * DataController.getInstance().getFullJid());
						 * DataController
						 * .getInstance().getUser().setLoggedIn(false);
						 * DataController.getInstance().saveUser();
						 * FacebookSdk.sdkInitialize(context);
						 * LoginManager.getInstance().logOut(); Intent
						 * mStartActivity = new
						 * Intent(context,LanguageSelectionDefault.class);
						 * ((Activity)context).startActivity(mStartActivity);
						 * ((Activity)context).finish();
						 */
					}
				});
		isOTPDialog++;
	}
	public void showonDrawerBlockedUserDialog(final Context context) {
		Utils.showDialogTwoButton(context,
				context.getString(R.string.blockeduser_msg),
				context.getString(R.string.contact),
				context.getString(R.string.cancel),
				new MatchstixDialogListener() {

					@Override
					public void onButtonTwoClick() {
						isOTPDialog=0;
					}

					@Override
					public void onButtonOneClick() {
						isOTPDialog=0;
						context.startActivity(openEmail(context,
								new String[] { "matchstixadmin@gmail.com" },
								null, null, null));
						/*
						 * AccountManager.getInstance().disableAccount(
						 * DataController.getInstance().getFullJid());
						 * DataController
						 * .getInstance().getUser().setLoggedIn(false);
						 * DataController.getInstance().saveUser();
						 * FacebookSdk.sdkInitialize(context);
						 * LoginManager.getInstance().logOut(); Intent
						 * mStartActivity = new
						 * Intent(context,LanguageSelectionDefault.class);
						 * ((Activity)context).startActivity(mStartActivity);
						 * ((Activity)context).finish();
						 */
					}
				});
	}
}
