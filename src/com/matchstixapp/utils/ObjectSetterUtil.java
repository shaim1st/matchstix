package com.matchstixapp.utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import android.content.Context;
import android.util.Log;

import com.matchstixapp.R;
import com.matchstixapp.modal.Friend;
import com.matchstixapp.modal.Roster;
import com.xabber.android.data.message.MessageManager;
import com.xabber.android.data.message.type.AbstractMessageItem;

public class ObjectSetterUtil {

	public static Context mContext;
	private ArrayList<Friend> mFriends;
	private String account;
	private static ObjectSetterUtil _instance;
	
	private ObjectSetterUtil() {
	}

	public void setObjectSetter(Context mcontext, ArrayList<Friend> mFriends,
			String account) {
		mContext = mcontext;
		this.mFriends = mFriends;
		this.account = account;
	}

	public static ObjectSetterUtil getInstance() {
		if(_instance==null)
			_instance=new ObjectSetterUtil();
		return _instance;
	}

	public static void setmContext(Context mContext) {
		ObjectSetterUtil.mContext = mContext;
	}

	public ArrayList<Friend> getmFriends() {
		return mFriends;
	}

	public void setmFriends(ArrayList<Friend> mFriends) {
		this.mFriends = mFriends;
	}

	public void setLastChatTimestamp() {
		for (int i=0; i < mFriends.size(); i++) {
			Friend friend = mFriends.get(i);
			List<AbstractMessageItem> messageItemsList = new ArrayList<AbstractMessageItem>(
					MessageManager.getInstance().getMessages(account,
							getUser(friend.getUserId())));
			String statusText = "";
			if (messageItemsList.size() == 0) {
				statusText = formatDate(friend.getMatchDate());
			} else {
				statusText = String.valueOf(messageItemsList.get(
						messageItemsList.size() - 1).getText());
				// Roster.getInstance().setFriendChatTime(i,messageItemsList.get(messageItemsList.size()-1).getTimestamp());
				try {
					Roster.getInstance().setFriendChatTime(
							mFriends.get(i),
							messageItemsList.get(messageItemsList.size() - 1)
									.getTimestamp().getTime());
				} catch (Exception e) {
					Log.d("DrawerRightListAdapter", e.toString());
				}
			}
			if (messageItemsList.size() > 0
					&& !messageItemsList.get(messageItemsList.size() - 1)
							.isRead()
					&& messageItemsList.get(messageItemsList.size() - 1)
							.isIncoming()) {
				try {
					Roster.getInstance().setFriendChatTime(
							mFriends.get(i),
							messageItemsList.get(messageItemsList.size() - 1)
									.getTimestamp().getTime());
				} catch (Exception e) {
					Log.d("DrawerRightListAdapter", e.toString());
				}
			}
		}
	}
	public static String getUser(long userId){
		return userId + "@" + mContext.getResources().getString(R.string.chat_server);
	}
	private String formatDate(long matchDate) {
		StringBuilder sb = new StringBuilder(mContext.getResources().getString(
				R.string.matched_on));
		sb.append(' ');
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
		Date date = new Date(matchDate);
		sb.append(sdf.format(date));
		return sb.toString();
	}
}
