package com.matchstixapp.utils;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.net.ssl.HttpsURLConnection;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnKeyListener;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.graphics.Typeface;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import com.craterzone.cz_commons_lib.Logger;
import com.craterzone.httpclient.model.CZResponse;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.google.android.gms.nearby.connection.Connections.StartAdvertisingResult;
import com.matchstixapp.MatchstixApplication;
import com.matchstixapp.R;
import com.matchstixapp.db.DataController;
import com.matchstixapp.language.LanguageSelectionDefault;
import com.matchstixapp.listener.MatchstixDialogListener;
import com.matchstixapp.ui.activity.SplashActivity;
import com.matchstixapp.ui.fragments.SettingsFragment;
import com.xabber.android.data.account.AccountManager;

public class Utils {

	public static final String HEADER_ACCEPT = "Accept";
	public static final String CONTENT_TYPE = "Content-type";
	private static final String HEADER_USER_AGENT = "user-agent";
	public static final String PUT = "PUT";
	public static final String POST = "POST";
	public static final String GET = "GET";
	public static final int TIME_OUT = 2 * 60 * 1000;
	private static final int _4KB = 4 * 1024;
	public static final String USER_AGENT = "user-agent";

	private static final String TAG = Utils.class.getName();

	public static void getKeyHash(Context context) {
		PackageInfo info = null;
		try {
			info = context.getPackageManager().getPackageInfo(
					"com.matchstixapp", PackageManager.GET_SIGNATURES);
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		for (Signature signature : info.signatures) {
			MessageDigest md = null;
			try {
				md = MessageDigest.getInstance("SHA");
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			md.update(signature.toByteArray());
			Log.d("KeyHash:",
					Base64.encodeToString(md.digest(), Base64.DEFAULT));
		}

	}

	/**
	 * Request to server using GET method
	 * 
	 * @param urlString
	 * @param acceptType
	 * @return
	 */
	public static CZResponse get(String urlString, String acceptType,
			String token) {
		HttpURLConnection conn = null;
		try {
			URL url = new URL(urlString);
			if (checkHTTPS(urlString)) {
				conn = (HttpsURLConnection) url.openConnection();
			} else {
				conn = (HttpURLConnection) url.openConnection();
			}
			conn.setReadTimeout(TIME_OUT);
			conn.setConnectTimeout(TIME_OUT);
			conn.setRequestProperty(HEADER_ACCEPT, acceptType);
			conn.setRequestProperty("token", token);
			conn.setRequestProperty(HEADER_USER_AGENT, MatchstixApplication
					.getInstance().getUserAgent());
			conn.connect();
			int statusCode = conn.getResponseCode();
			Logger.d(TAG, "Status is" + statusCode);
			switch (statusCode) {
			case 200:
				String response = new String(readFullyBytes(
						conn.getInputStream(), 2 * _4KB));
				Logger.d(TAG, "Response String is : " + response);
				return new CZResponse(statusCode, response);
			default:
				return new CZResponse(statusCode, "");
			}
		} catch (Exception e) {
			Logger.e(TAG, "Error in getting response", e);
		} finally {
			if (conn != null)
				conn.disconnect();
		}
		return null;
	}

	public static String convertStreamToString(InputStream is) {
		StringBuilder sb = new StringBuilder();
		String line = null;
		try {
			BufferedReader reader = new BufferedReader(
					new InputStreamReader(is));
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
		} catch (IOException e) {
			Logger.e(TAG, "Error in converting stream to string", e);
			return null;
		} catch (IllegalStateException e) {
			Logger.e(TAG, "Error in converting stream to string", e);
			return null;
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				Logger.e(TAG, "Error in closing Input Stream", e);
			}
		}
		return sb.toString();
	}

	public static boolean checkHTTPS(String url) {
		if (url.contains("https")) {
			return true;
		}
		return false;

	}

	/**
	 * Read bytes from InputStream efficiently. All data will be read from
	 * stream. This method return the bytes or null. This method will not close
	 * the stream.
	 */
	public static byte[] readFullyBytes(InputStream is, int blockSize) {
		byte[] bytes = null;
		if (is != null) {
			try {
				int readed = 0;
				byte[] buffer = new byte[blockSize];
				ByteArrayOutputStream bos = new ByteArrayOutputStream();
				while ((readed = is.read(buffer)) >= 0) {
					bos.write(buffer, 0, readed);
				}
				bos.flush();
				bytes = bos.toByteArray();
			} catch (IOException e) {
				Logger.e(TAG, " : readFullyBytes: ", e);
			}
		}
		return bytes;
	}

	public static int convertInKilloMeters(int meters) {
		int km = (meters / 1000);
		if (km <= 0) {
			return (Constants.MIN_DISTANCE / 1000);
		}
		return km;
	}

	public static int convertInMeters(int km) {
		int meters = (km * 1000);
		if (meters <= 0) {
			return Constants.MIN_DISTANCE;
		}
		return meters;
	}

	public static void showDialog(Context context, String title,
			String message, String btn1Text, String btn2Text,
			final MatchstixDialogListener listener) {
		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.delete_image_dialog);
		((TypeFaceTextView) dialog.findViewById(R.id.id_dialog_title))
				.setText(title);
		((TypeFaceTextView) dialog.findViewById(R.id.id_dialog_msg))
				.setText(message);
		TypeFaceTextView btn1 = (TypeFaceTextView) dialog
				.findViewById(R.id.id_delete_txt_view);
		TypeFaceTextView btn2 = (TypeFaceTextView) dialog
				.findViewById(R.id.id_cancel_txt_view);
		if (title.equals("")) {
			dialog.findViewById(R.id.id_dialog_title).setVisibility(View.GONE);
		}
		btn1.setText(btn1Text);
		btn2.setText(btn2Text);
		btn1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
				listener.onButtonOneClick();
			}
		});

		btn2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
				listener.onButtonTwoClick();
			}
		});
		dialog.show();

	}

	public static void showDialogCancel(Context context, String title,
			String message, String btn1Text, String btn2Text,
			final MatchstixDialogListener listener) {
		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.delete_imagewithcancel);
		((TypeFaceTextView) dialog.findViewById(R.id.id_dialog_title))
				.setText(title);
		((TypeFaceTextView) dialog.findViewById(R.id.id_dialog_msg))
				.setText(message);
		TypeFaceTextView btn1 = (TypeFaceTextView) dialog
				.findViewById(R.id.id_delete_txt_view);
		TypeFaceTextView btn2 = (TypeFaceTextView) dialog
				.findViewById(R.id.id_cancel_txt_view);
		TypeFaceTextView btn3 = (TypeFaceTextView) dialog
				.findViewById(R.id.id_dialog_cancel);
		if (title.equals("")) {
			dialog.findViewById(R.id.id_dialog_title).setVisibility(View.GONE);
		}
		btn1.setText(btn1Text);
		btn2.setText(btn2Text);
		btn1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
				listener.onButtonOneClick();
			}
		});

		btn2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
				listener.onButtonTwoClick();
			}
		});
		btn3.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		dialog.show();

	}

	public static void showDialog(Context context, String title,
			String message, String btn1Text, String btn2Text,
			boolean cancelable, boolean canceledOnTouchOutside,
			final MatchstixDialogListener listener) {
		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.delete_image_dialog);
		((TypeFaceTextView) dialog.findViewById(R.id.id_dialog_title))
				.setText(title);
		((TypeFaceTextView) dialog.findViewById(R.id.id_dialog_msg))
				.setText(message);
		TypeFaceTextView btn1 = (TypeFaceTextView) dialog
				.findViewById(R.id.id_delete_txt_view);
		TypeFaceTextView btn2 = (TypeFaceTextView) dialog
				.findViewById(R.id.id_cancel_txt_view);

		btn1.setText(btn1Text);
		btn2.setText(btn2Text);
		btn1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
				listener.onButtonOneClick();
			}
		});

		btn2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				dialog.dismiss();
				listener.onButtonTwoClick();
			}
		});
		dialog.show();
		dialog.setCancelable(cancelable);
		dialog.setCanceledOnTouchOutside(canceledOnTouchOutside);

	}

	public static void showInviteOptions(Context context) {
		Intent sendIntent = new Intent();
		sendIntent.setAction(Intent.ACTION_SEND);
		sendIntent.putExtra(Intent.EXTRA_TEXT, context.getResources()
				.getString(R.string.invite_friends_msg));
		sendIntent.setType("text/plain");
		context.startActivity(Intent.createChooser(sendIntent, context
				.getResources().getString(R.string.app_name)));
	}

	public static String formatTime(Context context, long seconds) {
		int hr = (int) (seconds / 3600);
		int min = (int) (seconds / 60);
		int time = 0;
		String postFix = "";
		if (hr == 1) {
			time = hr;
			postFix = context.getResources().getString(R.string.hour_ago);
		} else if (hr > 1) {
			time = hr;
			postFix = context.getResources().getString(R.string.hours_ago);
		} else if (min > 0) {
			time = min;
			postFix = context.getResources().getString(R.string.minuts_ago);
		} else {
			return context.getResources().getString(
					R.string.last_active_just_now);
		}

		return context.getResources().getString(R.string.last_active_string,
				time, postFix);
	}

	public static String formatDistance(Context context, int meters) {
		int distance = 1;
		float km = (meters / 1000);
		int k = (int) km;
		if (meters < 1000) {
			distance = 1;
		} else if (km > (float) k) {
			distance = k + 1;
		} else {
			distance = k;
		}
		return context.getResources().getString(R.string.near_by, distance);
	}

	public static Intent openEmail(Context context, String[] emailTo,
			String[] emailCC, String subject, String emailText) {
		final Intent emailIntent = new Intent(Intent.ACTION_SEND_MULTIPLE);
		emailIntent.setClassName("com.google.android.gm",
				"com.google.android.gm.ComposeActivityGmail");
		emailIntent.setType("text/plain ");
		emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, emailTo);
		emailIntent.putExtra(android.content.Intent.EXTRA_CC, emailCC);
		emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
		emailIntent.putExtra(Intent.EXTRA_TEXT, emailText);
		return emailIntent;
	}

	public static void showFadeinAnimation(Context context, View view) {
		Animation myFadeInAnimation = AnimationUtils.loadAnimation(context,
				R.anim.fadein);
		view.startAnimation(myFadeInAnimation); // Set animation to your
												// ImageView
	}

	public static void showUpgradeDialog(final Context context,
			final MatchstixDialogListener listener) {
		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.upgrade_app_dialog_layout);
		TypeFaceTextView btn1 = (TypeFaceTextView) dialog
				.findViewById(R.id.id_ok_txt_view);
		btn1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// dialog.dismiss();
				listener.onButtonOneClick();
			}
		});
		dialog.show();
		dialog.setCancelable(false);
		dialog.setCanceledOnTouchOutside(false);
		dialog.setOnKeyListener(new OnKeyListener() {

			@Override
			public boolean onKey(DialogInterface dialog, int keyCode,
					KeyEvent event) {
				((Activity) context).finish();
				return true;
			}
		});

	}

	public static void showUnAuthorizedDialog(final Context context) {
		Utils.showDialog(context, context.getString(R.string.unauthorized_msg),
				context.getString(R.string.logout),
				new MatchstixDialogListener() {

					@Override
					public void onButtonTwoClick() {

					}

					@Override
					public void onButtonOneClick() {
						AccountManager.getInstance().disableAccount(
								DataController.getInstance().getFullJid());
						DataController.getInstance().getUser()
								.setLoggedIn(false);
						DataController.getInstance().saveUser();
						FacebookSdk.sdkInitialize(context);
						LoginManager.getInstance().logOut();
						Intent mStartActivity = new Intent(context,
								SplashActivity.class);
						((Activity) context).startActivity(mStartActivity);
						((Activity) context).finish();
					}
				});
	}

	public static void showBlockedUserDialog(final Context context) {
		Utils.showDialogTwoButton(context,
				context.getString(R.string.blockeduser_msg),
				context.getString(R.string.contact),
				context.getString(R.string.cancel),
				new MatchstixDialogListener() {

					@Override
					public void onButtonTwoClick() {
					}

					@Override
					public void onButtonOneClick() {
						context.startActivity(openEmail(context,
								new String[] { "matchstixadmin@gmail.com" },
								null, null, null));
						/*
						 * AccountManager.getInstance().disableAccount(
						 * DataController.getInstance().getFullJid());
						 * DataController
						 * .getInstance().getUser().setLoggedIn(false);
						 * DataController.getInstance().saveUser();
						 * FacebookSdk.sdkInitialize(context);
						 * LoginManager.getInstance().logOut(); Intent
						 * mStartActivity = new
						 * Intent(context,LanguageSelectionDefault.class);
						 * ((Activity)context).startActivity(mStartActivity);
						 * ((Activity)context).finish();
						 */
					}
				});
	}

	public static void showDialog(Context context, String message,
			String btn1Text, final MatchstixDialogListener listener) {
		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.unauthorize_dialog_layout);
		((TypeFaceTextView) dialog.findViewById(R.id.id_dialog_msg))
				.setText(message);
		TypeFaceTextView btn1 = (TypeFaceTextView) dialog
				.findViewById(R.id.id_delete_txt_view);
		btn1.setText(btn1Text);
		btn1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
				listener.onButtonOneClick();
			}
		});
		dialog.show();

	}

	public static void showDialogTwoButton(Context context, String message,
			String btn1Text, String btn2Text,
			final MatchstixDialogListener listener) {
		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.delete_image_dialog);
		((TypeFaceTextView) dialog.findViewById(R.id.id_dialog_msg))
				.setText(message);
		TypeFaceTextView btn1 = (TypeFaceTextView) dialog
				.findViewById(R.id.id_delete_txt_view);
		TypeFaceTextView btn2 = (TypeFaceTextView) dialog
				.findViewById(R.id.id_cancel_txt_view);
		btn1.setText(btn1Text);
		btn2.setText(btn2Text);
		btn1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
				listener.onButtonOneClick();
			}
		});
		btn2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
				listener.onButtonTwoClick();
			}
		});
		dialog.show();

	}
	public static String getFontPath(Typeface currentTypeFace){
		String fontPath = Constants.DEFAULT_FONT;
		switch (SettingsFragment.defaultLanguage) {
		case "English":
			if (currentTypeFace != null && currentTypeFace.getStyle() == Typeface.BOLD) {
				fontPath = Constants.ENGLISH_FONT_BOLD;
			} else {
				fontPath = Constants.ENGLISH_DEFAULT_FONT;
			}
			break;
		case "ខ្មែរ":
			if (currentTypeFace != null && currentTypeFace.getStyle() == Typeface.BOLD) {
				fontPath = Constants.KHMER_FONT_BOLD;
			} else {
				fontPath = Constants.KHMER_DEFAULT_FONT;
			}
			break;
		case "မြန်မာ":
			if (currentTypeFace != null && currentTypeFace.getStyle() == Typeface.BOLD) {
				fontPath = Constants.MYANMAR_FONT_BOLD;
			} else {
				fontPath = Constants.MYANMAR_FONT_BOLD;
			}
			break;
		case "বাংলা":
			if (currentTypeFace != null && currentTypeFace.getStyle() == Typeface.BOLD) {
				fontPath = Constants.BANGLA_FONT_BOLD;
			} else {
				fontPath = Constants.BANGLA_DEFAULT_FONT;
			}
			break;
		default:
			fontPath=Constants.DEFAULT_FONT;
			break;
		}
		return fontPath;
	}

}
