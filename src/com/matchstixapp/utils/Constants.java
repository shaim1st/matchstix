package com.matchstixapp.utils;

public class Constants {
	public static int CIRCULAR_IMG_VIEW_HEIGH = 120;
	public static int CIRCULAR_IMG_VIEW_WIDTH = 120;
	public static final String PROFILE_PIC_ALBUM_NAME = "Profile Pictures";
	public static final int REQUEST_CODE_ALBUMLIST = 1;
	public static final int REQUEST_CODE_ALBUM_PICS_GRID = 2;
	public static final String MALE="MALE";
	public static final String FEMALE="FEMALE";
	public static final String[] SEARCH_GENDERS={FEMALE,MALE};
	
	public static final long DELAY_SPLASH = 1500;//ms
	public static final int MIN_DISTANCE = 100000; //meters
	public static final int MIN_AGE = 18; //meters
	public static final int MAX_AGE = 60; //meters
	public static final String DEVICE_TYPE = "ANDROID";
	public static final String PREFIX_NAME_CROPED_FILE = "croped_";
	public static final long WAIT_LOAD_TIME = 2000;
	
	public static final String EMAIL_RECIPIENTS = "info@matchstixapp.com";
	public static final String EMAIL_SUBJECT = "SUBJECT";
	public static final String COUNTRY = "country";
	public static final String RECEIVER_COUNTRY_SELECTION = "country_selection";
	public static final String DATA_COUNTRY_SELECTION = "data_cntry_selection";

	public static final long MAX_LOCATION_UPDATE_TIME = 5*60*1000;//one minute
	public static final int MIN_AGE_FOR_SIGNUP = 18;
	
	public static final String KEY_INTENT_UNMATCH_REPORT = "unmatch_report";
	public static final int REQ_CODE_INTENT_FRND_PROFILE = 102;
	public static final String PLAYSTORE_URL = "http://play.google.com/store/apps/details?id=";
	
	public static final int REQ_LANGUAGE_SELECTION = 111;
	public static final int RES_LANGUAGE_SELECTION = 112;
	public static final String KEY_LANGUAGE = "LANGUAGE";
	public static final int BLOCKED_CODE = 423;
	public static final int LIKE_LIMIT = 50;
	/*
	 * Handling .TTF file According to language selection
	 */
	public static final String DEFAULT_FONT="fonts/Gotham-Rounded-Light_21020.ttf";
	
	public static final String ENGLISH_DEFAULT_FONT="fonts/Gotham-Rounded-Light_21020.ttf";
	public static final String ENGLISH_FONT_BOLD="fonts/Gotham-Rounded-Bold_21016.ttf";
	
	public static final String KHMER_DEFAULT_FONT="fonts/KhmerOSNew-Bold.ttf";
	public static final String KHMER_FONT_BOLD="fonts/KhmerOSNew-Bold.ttf";
	
	public static final String MYANMAR_DEFAULT_FONT="fonts/Padauk_V2.8.ttf";
	public static final String MYANMAR_FONT_BOLD="fonts/Padauk-bold_V2.8.ttf";
	
	public static final String BANGLA_DEFAULT_FONT="fonts/Gotham-Rounded-Light_21020.ttf";
	public static final String BANGLA_FONT_BOLD="fonts/Gotham-Rounded-Bold_21016.ttf";
	
}
