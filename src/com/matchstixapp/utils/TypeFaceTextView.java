package com.matchstixapp.utils;


import com.matchstixapp.R;
import com.matchstixapp.ui.fragments.SettingsFragment;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.TextView;

/*
 * class to give custom typeface TextView extends to TextView.
 */
public class TypeFaceTextView extends TextView {
	private static final String TAG = TypeFaceTextView.class.getName();
	
	private Context context;
	private String typeFace;

	public TypeFaceTextView(Context context) {
		super(context);
		this.context = context;
		init();
	}

	public TypeFaceTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
		init();
	}

	public TypeFaceTextView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		this.context = context;
		TypedArray a = null;
		try {
			a = context.obtainStyledAttributes(attrs, R.styleable.TypeFaceTextView, 0, 0);
			typeFace = a.getString(R.styleable.TypeFaceTextView_customFonts);
		} 
		catch (Exception e) {
			Log.d(TAG, "Exception", e);
		} 
		finally {
			if (a != null) 
			{
				a.recycle();
			}
		}
		
		init();
	}
	

	private void init() {
		if (!isInEditMode()) {
			Typeface currentTypeFace = this.getTypeface();
			setTypeface(Typeface.createFromAsset(getContext().getAssets(),Utils.getFontPath(currentTypeFace)));
		}
	}
}
