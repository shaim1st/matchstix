package com.matchstixapp.utils;

import java.util.regex.Pattern;

public final class Validator {

	private static String REGEX_PATTERN_NUMBER = "^[0-9]*$";
	
	private Validator() {}
	
	public static boolean isValidNumber(String number) {
		return Pattern.compile(REGEX_PATTERN_NUMBER).matcher(number).matches();
	}
}
