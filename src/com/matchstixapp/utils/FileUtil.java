package com.matchstixapp.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

public class FileUtil {
	ImageView imageView = null;

	public interface ImageDownloadListener {
		public void onDownloadingFinished(File file, String Url);
	}

	public static void download(Context context, String url) {
		PhotoDownloader downloader = new PhotoDownloader(context, url);
		downloader.execute(url);

	}

	private static class PhotoDownloader extends
			AsyncTask<String, String, 	File> {

		ImageView image;
		Context context;
		String url;

		public PhotoDownloader(Context context, String url) {
			this.context = context;
			this.url = url;
		}

		@Override
		protected 	File doInBackground(String... urls) {
			return download_Image(urls[0]);
		}

		@Override
		protected void onPostExecute(File result) {
				((ImageDownloadListener) context).onDownloadingFinished(result,url);
		}

		private File download_Image(String url) {
			try {
				URL aURL = new URL(url);
				URLConnection conn = aURL.openConnection();
				conn.connect();
				InputStream is = conn.getInputStream();
				File file=FileCache.getInstance(
						context).getFile(url);
				OutputStream os = new FileOutputStream(file);
				byte[] buffer = new byte[8*1024];
				int len = -1;
				while (((len = is.read(buffer)) != -1)) {
					os.write(buffer, 0, len);
				}
				os.close();
				is.close();
				return file;

			} catch (IOException e) {
				Log.e("CZ", "Error getting the image from server : "
						+ e.getMessage().toString());
			}
			return null;
		}

	}
}
