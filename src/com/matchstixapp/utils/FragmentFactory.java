package com.matchstixapp.utils;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;


/**
 * This Class Handle the Fragment transaction
 * 
 */

public class FragmentFactory {
	
	/**
	 * This method replace fragment on fragment transaction
	 * @param fragment
	 * @param id
	 * @param context
	 */
	public static void replaceFragment(Fragment fragment, int id, Context context) {
		FragmentTransaction fragmentTransaction = ((FragmentActivity) context).getSupportFragmentManager().beginTransaction();
		fragmentTransaction.replace(id, fragment).commit();

	}

	/**
	 * This method add fragment 
	 * @param fragment
	 * @param id
	 * @param context
	 */
	public static void addFragmentWithTag(Fragment fragment, int id,
			Context context, String tag) {
		FragmentManager fragmentManager=((FragmentActivity) context).getSupportFragmentManager();
		fragmentManager.beginTransaction().add(id, fragment, tag).commit();

	}
	
	/**
	 * This method replace fragment on fragment transaction
	 * @param fragment
	 * @param tag
	 * @param context
	 */
	public static void replaceFragment(Fragment fragment, int id, String tag,Context context) {
		FragmentTransaction fragmentTransaction = ((FragmentActivity) context).getSupportFragmentManager().beginTransaction();
		fragmentTransaction.replace(id, fragment).addToBackStack(tag).commit();

	}
	
	/**
	 * This method replace fragment and clear backStack on fragment transaction
	 * @param fragment
	 * @param tag
	 * @param currentTag
	 * @param context
	 */
	public static void replaceFragmentClearBackStack(Fragment fragment,int id, String tag,String currentTag, Context context) {
		FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
		fragmentManager.popBackStack(null,FragmentManager.POP_BACK_STACK_INCLUSIVE);
		FragmentTransaction fragmentTransaction = ((FragmentActivity) context).getSupportFragmentManager().beginTransaction();
		fragmentTransaction.replace(id, fragment, tag).addToBackStack(currentTag).commit();

	}

	


}
