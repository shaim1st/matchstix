package com.matchstixapp.utils;

import com.matchstixapp.ui.fragments.SettingsFragment;

import android.content.Context;
import android.graphics.Typeface;
import android.location.SettingInjectorService;
import android.util.AttributeSet;
import android.widget.TextView;

/*
 * class to give custom typeface TextView extends to TextView.
 */
public class HeaderTitleTextView extends TextView {
	
	public HeaderTitleTextView(Context context) {
		super(context);
		init();
	}

	public HeaderTitleTextView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public HeaderTitleTextView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}
	

	private void init() {
		if (!isInEditMode()) {
			Typeface currentTypeFace = this.getTypeface();
			setTypeface(Typeface.createFromAsset(getContext().getAssets(),Utils.getFontPath(currentTypeFace)));
		}
	}
}
