package com.matchstixapp.utils;


import com.matchstixapp.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;


	/**
	 * Utility class to show android Toasts with custotm ui.
	 * 
	 */
	public class CustomToast {

	    private static final String TAG = CustomToast.class.getSimpleName();
	    private static final int MINIMUM_TOAST_FONT_SIZE = 8;

	    private static void showToast(Context context, String message, int duration, Type type, int size) {

	        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	        ViewGroup container = null;

	        container = (ViewGroup) inflater.inflate(R.layout.custom_toast_layout, null);

	        /*
	         * Uncomment the below lines in case you want to make some differentiation
	         * among good toast messages and bad toast messages.
	         */
	        //if (type == Type.WTF) {
	        //    Do here anything for dirty messages display.
	        //} else {
	        //    Do here for happy messages display.
	        //}

	        TextView tvMessage = (TextView) container.findViewById(R.id.message);
	        tvMessage.setText(message);

	        // Lets keep minimum size as 12 for text font
	        if(size > MINIMUM_TOAST_FONT_SIZE) {
	            tvMessage.setTextSize(size);
	        }
	        android.widget.Toast toast = new android.widget.Toast(context);
	        toast.setDuration(duration);
	        toast.setView(container);
	        toast.show();
	    }

	    public static void showLargeToast(Context context, String message) {
	        showToast(context, message, android.widget.Toast.LENGTH_LONG, Type.HAPPY, 20);
	    }

	    public static void showLargeToast(Context context, int resId) {
	        showToast(context, context.getString(resId),
	                  android.widget.Toast.LENGTH_LONG, Type.HAPPY, 20);
	    }

	    public static void showLongToast(Context context, String message, Type type) {
	        showToast(context, message, android.widget.Toast.LENGTH_LONG, type, 0);
	    }

	    public static void showShortToast(Context context, String message, Type type) {
	        showToast(context, message, android.widget.Toast.LENGTH_SHORT, type, 0);
	    }

	    public static void showLongToast(Context context, int resId, Type type) {
	        showToast(context, context.getString(resId),
	                  android.widget.Toast.LENGTH_LONG, type, 0);
	    }

	    public static void showShortToast(Context context, int resId, Type type) {
	        showToast(context, context.getString(resId),
	                  android.widget.Toast.LENGTH_SHORT, type, 0);
	    }

	    public static void showShortToast(Context context, String message) {
	        showToast(context, message,android.widget.Toast.LENGTH_SHORT, Type.HAPPY, 0);
	    }

	    public static void showShortToast(Context context, int resId) {
	        showToast(context, context.getString(resId),
	                  android.widget.Toast.LENGTH_SHORT, Type.HAPPY, 0);
	    }

	    public static void showLongToast(Context context, int resId) {
	        showToast(context, context.getString(resId),
	                  android.widget.Toast.LENGTH_LONG, Type.HAPPY, 0);
	    }

	    public static void showLongToast(Context context, String message) {
	        showToast(context, message, android.widget.Toast.LENGTH_LONG, Type.HAPPY, 0);
	    }

	    public static void showErrorToast(Context context, String message) {
	        showToast(context, message, android.widget.Toast.LENGTH_LONG, Type.WTF, 0);
	    }

	    public static void showErrorToast(Context context, int resId) {
	        showToast(context, context.getString(resId),
	                  android.widget.Toast.LENGTH_LONG, Type.WTF, 0);
	    }

	    public enum Type {
	        HAPPY,
	        WTF
	    }

	}

