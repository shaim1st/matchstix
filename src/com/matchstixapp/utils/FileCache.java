package com.matchstixapp.utils;

import java.io.File;

import android.content.Context;

public class FileCache {
    
	private static final String FOLDER_NAME=".Matchstix";
    private File roortDir;
    private static FileCache fileCache;
    public static FileCache getInstance(Context context){
    	if(fileCache==null)
    		fileCache = new FileCache(context);
    	return fileCache;
    		
    }
    
	private FileCache(Context context) {

		// Find the dir at SDCARD to save cached images
		if (android.os.Environment.getExternalStorageState().equals(
				android.os.Environment.MEDIA_MOUNTED)) {

			roortDir = new File(
					android.os.Environment.getExternalStorageDirectory(),
					FOLDER_NAME);
		} else {
			// if checking on simulator the create cache dir in your application
			// context
			roortDir = context.getCacheDir();
		}

		if (!roortDir.exists()) {
			// create cache dir in your application context
			roortDir.mkdirs();
		}
	}
    
    public File getFile(String url){
        //Identify images by hashcode or encode by URLEncoder.encode.
        String filename=getFileName(url);        
        File f = new File(roortDir, filename);
        return f;        
    }
    
    public String getFileName(String url){
    	String name = url.substring(url.lastIndexOf('/')+1);
    	int index=name.indexOf('?');
		if (index>=0) {
			name = name.substring(0, index);
		}
        return name;
        
    }
    
    
    
    public File createFile(String filename){
        File f = new File(roortDir, filename);
        return f;        
    }
    
    public void clear(){
    	// list all files inside cache directory
        File[] files=roortDir.listFiles();
        if(files==null)
            return;
        //delete all cache directory files
        for(File f:files)
            f.delete();
    }

}