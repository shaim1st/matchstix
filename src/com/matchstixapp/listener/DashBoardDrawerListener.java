package com.matchstixapp.listener;

import android.view.View;

public interface DashBoardDrawerListener {

	public void onDrawerOpened(View view);
}
