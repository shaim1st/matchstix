package com.matchstixapp.listener;


public interface WSListener {

	public void onWSSucess(Object suggestions);
	public void onWSError(String error);

}
