package com.matchstixapp.listener;

public interface MatchstixDialogListener {
	public void onButtonOneClick();
	public void onButtonTwoClick();
}
