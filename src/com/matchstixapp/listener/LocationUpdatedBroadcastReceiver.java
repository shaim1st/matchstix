package com.matchstixapp.listener;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.matchstixapp.ConnectivityController;
import com.matchstixapp.db.DataController;
import com.matchstixapp.logger.Logger;
import com.matchstixapp.pojo.JSONUpdateLocation;
import com.matchstixapp.requester.UserLocationUpdater;
import com.matchstixapp.utils.Constants;

public class LocationUpdatedBroadcastReceiver extends BroadcastReceiver {

	private static final String TAG = LocationUpdatedBroadcastReceiver.class
			.getSimpleName();

	private long lastUpdatedTime;
	private boolean updating;
	private double latitude;
	private double longtitude;

	@Override
	public void onReceive(Context context, Intent intent) {

		Logger.d(TAG, "******** onReceive()");
		DataController dataController = DataController.getInstance();
		double lat = dataController.getPreferences().getLatitude();
		double lng = dataController.getPreferences().getLongitude();
		if (updating
				|| (lat == latitude && lng == longtitude)
				|| !dataController.getUser().isRegistered()
				|| (System.currentTimeMillis() - lastUpdatedTime < Constants.MAX_LOCATION_UPDATE_TIME) || !ConnectivityController.isNetworkAvailable(context)) {
			Logger.d(TAG, "******** NOT updating location");
			Logger.d(TAG, "lat/lng refreshed: "+(!(lat == latitude && lng == longtitude)));
			return;
		}
		updating = true;
		latitude = lat;
		longtitude = lng;
		JSONUpdateLocation request = new JSONUpdateLocation();
		request.setLat(lat);
		request.setLng(lng);
		UserLocationUpdater updater = new UserLocationUpdater(request,
				new OperationListener() {

					@Override
					public void onOperationFinished(Operation operation) {
						updating = false;
						if (operation.getState() == Operation.STATE_SUCCESS) {
							lastUpdatedTime = System.currentTimeMillis();
						}
					}

					@Override
					public void onBlockedUser() {
						// TODO Auto-generated method stub
						
					}
				});
		updater.start();
	}

}
