package com.matchstixapp.listener;

import com.matchstixapp.pojo.JSONLogin;
import com.xabber.android.data.BaseUIListener;

public interface HTTPLogInListener extends BaseUIListener {
	public void onHTTPLogInSuccess(JSONLogin responseLogin);
	public void onHTTPLogInFailed(String msg);
}
