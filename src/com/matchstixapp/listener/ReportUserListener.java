package com.matchstixapp.listener;

import com.xabber.android.data.BaseUIListener;

public interface ReportUserListener extends BaseUIListener {

	public void onReportSucceess();
	public void onReportFailed();
	public void onBlockedUser();
}
