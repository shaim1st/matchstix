package com.matchstixapp.listener;

import com.xabber.android.data.BaseUIListener;

public interface UpdateUserProfileDetailsListener extends BaseUIListener {

	public void onUserProfileDetailsUpdated(boolean userpic, boolean aboutme);
	public void onUserProfileDetailsUpdationFailed();
	public void onBlockedUser();

}
