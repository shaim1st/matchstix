package com.matchstixapp.listener;

import com.xabber.android.data.BaseUIListener;

public interface FriendPassListener extends BaseUIListener {
	
	public void onFriendPassSucceess(long friendId);
	public void onFriendPassFailed();
}
