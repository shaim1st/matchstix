package com.matchstixapp.listener;

import com.matchstixapp.pojo.JSONNumber;
import com.xabber.android.data.BaseUIListener;

public interface PINSendListener extends BaseUIListener {
	public void onPINSendSuccess(JSONNumber request);
	public void onPINSendFailed(int code,String msg);
	public void onPINSendError(int code);
}
