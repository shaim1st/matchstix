package com.matchstixapp.listener;

import com.matchstixapp.pojo.JSONPhone;
import com.xabber.android.data.BaseUIListener;

public interface PINValidateListener extends BaseUIListener {
	public void onPINValidationSuccess(JSONPhone request);

	public void onPINValidationFailed(int code,String msg);
}
