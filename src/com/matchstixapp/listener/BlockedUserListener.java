package com.matchstixapp.listener;

import com.xabber.android.data.BaseUIListener;

public interface BlockedUserListener extends BaseUIListener{
	
	public abstract void onUserBlocked();
}
