package com.matchstixapp.listener;

import com.xabber.android.data.BaseUIListener;

public interface FriendsDownloadListener extends BaseUIListener {
	public void onFriendsDownloadSucceess();
	public void onFriendsDownloadFailed(String msg);
	public void onBlockedUser();
}
