package com.matchstixapp.listener;

import com.xabber.android.data.BaseUIListener;

public interface UnmatchUserListener extends BaseUIListener {
	
	public void onUnmatchSucceess(long friendId);
	public void onUnmatchFailed();
	public void onBlockedUser();
}
