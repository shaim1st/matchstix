package com.matchstixapp.listener;

import com.matchstixapp.modal.SuggestionDetail;
import com.matchstixapp.modal.SuggestionProfile;
import com.xabber.android.data.BaseUIListener;

public interface UserProfileListener extends BaseUIListener {
	
	public void onUserProfileDownloadSuccess(SuggestionProfile profile,
			SuggestionDetail detail);
	public void onUserProfileDownloadError();

}
