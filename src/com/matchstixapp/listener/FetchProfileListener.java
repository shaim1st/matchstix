package com.matchstixapp.listener;

import com.matchstixapp.modal.SuggestionProfile;
import com.xabber.android.data.BaseUIListener;

public interface FetchProfileListener extends BaseUIListener {

	public void onInternetConnectionNotFound();
	public void onFetchProfileRequestSuccess(final SuggestionProfile profile);
	public void onFetchProfileRequestFailed();
	public void onBlockedUser();
}
