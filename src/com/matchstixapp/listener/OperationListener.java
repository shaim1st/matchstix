package com.matchstixapp.listener;

public interface OperationListener
{
	public void onOperationFinished(Operation operation);
	public void onBlockedUser();
}
