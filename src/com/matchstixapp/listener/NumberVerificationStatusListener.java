package com.matchstixapp.listener;

import com.matchstixapp.pojo.JSONNumber;
import com.xabber.android.data.BaseUIListener;

public interface NumberVerificationStatusListener extends BaseUIListener {

	public void onNumberVerificationSucceess(JSONNumber number);
	public void onNumberVerificationFailed(int code,String msg);
	public void onNumberVerificationError(String reason);
	
}
