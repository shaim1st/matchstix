package com.matchstixapp.listener;

import com.xabber.android.data.BaseUIListener;

public interface LikeDislikeListner extends BaseUIListener{
	
	public abstract void onLikeListner(long friendId);
	public abstract void onDisLikeListner(long friendId);

}
