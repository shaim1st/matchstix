package com.matchstixapp.listener;

import java.util.HashMap;

public class DataBundle extends HashMap<String, Object>
{
	public void putInt(String key, int value)
	{
		put(key, new Integer(value));
	}

	public int getInt(String key)
	{
		return ((Integer) get(key)).intValue();
	}

	public void putString(String key, String value)
	{
		put(key, value);
	}

	public String getString(String key)
	{
		return ((String) get(key));
	}

	public void putBoolean(String key, boolean updated) {
		put(key, updated);
	}
	public boolean getBoolean(String key) {
		if (containsKey(key)) {
			return ((Boolean)get(key));
		}
		return false;
	}
}
