package com.matchstixapp.listener;

import com.matchstixapp.modal.FBAlbums;
import com.xabber.android.data.BaseUIListener;

public interface FBAlbumsDownloadListener extends BaseUIListener {
	public void onFBAlbumsDownloadSucceess(FBAlbums albums);
	public void onFBAlbumsDownloadFailed(String msg);
	
}
