package com.matchstixapp.listener;

import com.xabber.android.data.BaseUIListener;

public interface HTTPLogOutListener extends BaseUIListener {
	public void onHTTPLogOutSuccess();
	public void onHTTPLogOutFailed(int status, String msg);
	public void onBlockedUser();
}
