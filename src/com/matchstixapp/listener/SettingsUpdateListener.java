package com.matchstixapp.listener;

import com.xabber.android.data.BaseUIListener;

public interface SettingsUpdateListener extends BaseUIListener {
	public void onSettingsUpdateSuccess();
	public void onSettingsUpdateFailed(String msg);
	public void onBlockedUser();
	
}
