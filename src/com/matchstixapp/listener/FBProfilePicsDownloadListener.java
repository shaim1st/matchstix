package com.matchstixapp.listener;

import com.matchstixapp.modal.FBAlbumPhotos;
import com.xabber.android.data.BaseUIListener;

public interface FBProfilePicsDownloadListener extends BaseUIListener {
	public void onFBProfilePicsDownloadSucceess(FBAlbumPhotos profilePhotos);
	public void onFBProfilePicsDownloadFailed(String msg);
}
