package com.matchstixapp.listener;

import com.xabber.android.data.BaseUIListener;

public interface LastSeenListener extends BaseUIListener {

	public void onInternetConnectionNotFound();
	public void onLastSeenRequestSuccess(long userid, long milliseconds);
	public void onLastSeenRequestFailed();
}
