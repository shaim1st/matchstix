package com.matchstixapp.listener;

public class Keys {
	public static final String KEY_FBALBUMS="key_fbalbums";
	public static final String KEY_SUGGESTIONS="key_suggestions";
	public static final String KEY_FBALBUM_PHOTOS="key_fbalbum_photos";
	public static final String KEY_STATUS_CODE="key_status_code";
	public static final String KEY_ERROR_MESSAGE="key_error_message";
	public static final String KEY_DATA="data";
	public static final String KEY_IS_ABOUT_ME_UPDATED = "key_is_about_me_updated";
	public static final String KEY_IS_PICS_UPDATED = "key_is_pics_updated";
	public static final String KEY_ALBUM_ID = "key_album_id";
	public static final String KEY_PHOTO_ID = "key_photo_id";
	public static final String KEY_PHOTO_URI= "key_photo_uri";
	public static final String KEY_FRIEND_ID= "key_friend_id";
	public static final String KEY_MODE_EDIT = "key_mode_edit";
	public static final String KEY_SELECTED_COUNTRY = "key_selected_country";
	public static final String KEY_NUMBER = "key_number";
	
	
}
