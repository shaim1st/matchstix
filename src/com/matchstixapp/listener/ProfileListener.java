package com.matchstixapp.listener;

import com.matchstixapp.modal.SuggestionDetail;
import com.matchstixapp.modal.SuggestionProfile;
import com.xabber.android.data.BaseUIListener;

public interface ProfileListener extends BaseUIListener {
	public void onProfileDownloadSuccess(SuggestionProfile profile,SuggestionDetail detail);
	public void onProfileDownloadFailed(String msg);
	public void onBlockedUser();
}
