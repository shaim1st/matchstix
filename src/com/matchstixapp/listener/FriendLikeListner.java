package com.matchstixapp.listener;

import com.xabber.android.data.BaseUIListener;

public interface FriendLikeListner extends BaseUIListener{
	
	public abstract void onFriendLikeSuccess();
	public abstract void onFriendPassSuccess();
	public abstract void onLikeBlocked();

}
