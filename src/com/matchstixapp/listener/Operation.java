package com.matchstixapp.listener;

public class Operation
{
	public static final int STATE_NONE = 0;
	public static final int STATE_SUCCESS = 1;
	public static final int STATE_FAILED = 2;
	public static final int STATE_ERROR = 3;
	private int state;
	private DataBundle bundle;

	public Operation()
	{
		this(STATE_NONE);
	}

	public Operation(int state)
	{
		this.state = state;
		bundle = new DataBundle();
	}

	public void setState(int state)
	{
		this.state = state;
	}

	public int getState()
	{
		return state;
	}

	public DataBundle getBundle()
	{
		return bundle;
	}

}
