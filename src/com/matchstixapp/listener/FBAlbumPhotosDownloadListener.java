package com.matchstixapp.listener;

import com.matchstixapp.modal.FBAlbumPhotos;
import com.xabber.android.data.BaseUIListener;

public interface FBAlbumPhotosDownloadListener extends BaseUIListener {

	public void onFBAlbumPhotosDownloadSucceess(String albumId,FBAlbumPhotos photos);
	public void onFBAlbumPhotosDownloadFailed(String msg);
}
