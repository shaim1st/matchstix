package com.matchstixapp.listener;

import com.matchstixapp.modal.SuggestionDetailsList;
import com.xabber.android.data.BaseUIListener;

public interface SuggestionsListener extends BaseUIListener {

	public void onSuccess(SuggestionDetailsList suggestions);
	public void onFailed(String msg);
	public void onBlockedUser();
	
}
