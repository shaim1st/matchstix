package com.matchstixapp.listener;

public interface OnBackPressedListener {
	
	public void onBackPressed();
}
