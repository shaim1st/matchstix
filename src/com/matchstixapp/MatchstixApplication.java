package com.matchstixapp;

import java.util.Locale;
import java.util.Set;
import java.util.TreeSet;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import com.craterzone.czimageloader.universalImageLoader.UniversalImageLoaderUtil;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.matchstixapp.db.DataController;
import com.matchstixapp.db.MatchstixPreferences;
import com.matchstixapp.listener.SettingsUpdateListener;
import com.matchstixapp.modal.DiscoverySettings;
import com.matchstixapp.pojo.JSONSettings;
import com.matchstixapp.push.PushCustomHandler;
import com.matchstixapp.requester.UpdateDiscoverySettingsRequester;
import com.matchstixapp.utils.BackgroundExecutor;
import com.pushbots.push.Pushbots;
import com.xabber.android.data.Application;

//import android.app.Application;

public class MatchstixApplication extends Application implements com.matchstixapp.listener.ActivityLifecycleCallbacks {
	
	public static GoogleAnalytics analytics;
	public static Tracker tracker;

	public static final String TAG = MatchstixApplication.class.getName();
	private static MatchstixApplication _instance;
    private static Set<String> set;

	@Override
	public boolean isInitialized() {
		return super.isInitialized();
	}

	@Override
	public void onServiceStarted() {
		super.onServiceStarted();
	}

	@Override
	public void onServiceDestroy() {
		super.onServiceDestroy();
	}

	public MatchstixApplication() {
		_instance = this;
		set = new TreeSet<String>(String.CASE_INSENSITIVE_ORDER);
	}

	public static MatchstixApplication getInstance() {
		if (_instance == null) {
			_instance = new MatchstixApplication();
		}
		return _instance;
	}

	/**
	 * Submits request to be executed in UI thread.
	 * 
	 * @param runnable
	 */
	/*
	 * public void runOnUiThread(final Runnable runnable) {
	 * _handler.post(runnable); }
	 */

	public void inform(final String msg) {
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				Toast.makeText(getInstance(), msg, Toast.LENGTH_SHORT).show();
			}
		});
	}

	
	private void initTracker() {
		analytics = GoogleAnalytics.getInstance(this);
		analytics.setLocalDispatchPeriod(1800);
		tracker = analytics.newTracker("UA-65351048-2");
		tracker.enableExceptionReporting(true);
		tracker.enableAdvertisingIdCollection(true);
		tracker.enableAutoActivityTracking(true);
	}
	
	@Override 
	public void onCreate() {
		super.onCreate();
		initTracker();
		_instance = this;
		FacebookSdk.sdkInitialize(getApplicationContext());
		AppEventsLogger.activateApp(this);
		UniversalImageLoaderUtil
				.initUniversalImageLoader(getApplicationContext());
		//registerForPush();
		//Load saved data
		DataController.getInstance();
		//if (ConnectivityController.isNetworkAvailable(_instance)) {
			//SuggestionsProcessor.getInstance().process();
	//	}
	}

	private void registerForPush() {
		//new Thread(new GCMRegistrationThread(this)).start();
		Pushbots.sharedInstance().init(this);
		Pushbots.sharedInstance().setAlias(String.valueOf(DataController.getInstance().getUser().getId()));
		Pushbots.sharedInstance().setNotificationEnabled(false);
		Pushbots.sharedInstance().setCustomHandler(PushCustomHandler.class);
		String deviceToken =  Pushbots.sharedInstance().regID();
		MatchstixPreferences.getInstance().saveGCMRegId(deviceToken);
	}


	public void saveSettingsIfRequired() {

		BackgroundExecutor.getInstance().execute(new Runnable() {

			@Override
			public void run() {
				DiscoverySettings settings = DataController.getInstance()
						.getSettings();
				int distance = settings.getDistance();
				int minAge = settings.getMinAge();
				int maxAge = settings.getMaxAge();
				JSONSettings settingRequest = new JSONSettings();
				settingRequest.setAgeMax(maxAge);
				settingRequest.setAgeMin(minAge);
				settingRequest.setGenders(settings.getGender());
				settingRequest.setDistance(distance);
				UpdateDiscoverySettingsRequester requester = new UpdateDiscoverySettingsRequester(
						settingRequest);
				requester.update(new SettingsUpdateListener() {

					@Override
					public void onSettingsUpdateSuccess() {
						runOnUiThread(new Runnable() {

							@Override
							public void run() {
								Toast.makeText(
										getInstance(),
										getResources()
												.getString(
														R.string.msg_settings_update_sucess),
										Toast.LENGTH_SHORT).show();
							}
						});
					}

					@Override
					public void onSettingsUpdateFailed(final String msg) {
						runOnUiThread(new Runnable() {

							@Override
							public void run() {
								Toast.makeText(getInstance(), msg,
										Toast.LENGTH_SHORT).show();

							}
						});
					}

					@Override
					public void onUnauthorizedListener() {
						
					}

					@Override
					public void onBlockedUser() {
					}
				});
			}
		});
	}

	
	public boolean isAppInBackGround() {
		return set.isEmpty();
	}

	@Override
	public void onActivityStopped(Activity activity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onActivityStarted(Activity activity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onActivityResumed(Activity activity) {
        set.add(activity.getClass().getName());
        AppEventsLogger.activateApp(this);
        Log.d(TAG, "Is App in BackGround " + isAppInBackGround());
	}

	@Override
	public void onActivityPaused(Activity activity) {
		set.remove(activity.getClass().getName());
		AppEventsLogger.deactivateApp(this);
		Log.d(TAG, "Is App in BackGround " + isAppInBackGround());
	}

	@Override
	public void onActivityDestroyed(Activity activity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
	}
	public String getUserAgent() {
		return "Mozilla/5.0 (X11; mandroid) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.91  Safari/537.36" +
				((TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
	}
	
	public static void setLocale(String lang){
		
	    Locale locale = new Locale((getLanguageValues(lang)));
	    Locale.setDefault(locale);
	    Configuration config = new Configuration();
	    config.locale = locale;
	    getInstance().getApplicationContext().getResources().updateConfiguration(config, null);
	}
	public static String getLanguageValues(String lang){
		String result="en";
		switch (lang) {
		case "English":
			result="en";
			break;
		case "ខ្មែរ":
			result="km";
			break;
		case "မြန်မာ":
			result="mm";
			break;
		case "বাংলা":
			result="bn";
			break;
		default:
			break;
		}
		return result;
	}
}
