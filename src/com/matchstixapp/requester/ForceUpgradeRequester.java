package com.matchstixapp.requester;

import java.net.HttpURLConnection;

import com.craterzone.cz_commons_lib.JsonUtil;
import com.craterzone.httpclient.model.CZResponse;
import com.matchstixapp.WS.HTTPOperationController;
import com.matchstixapp.db.MatchstixPreferences;
import com.matchstixapp.modal.ForceUpgrade;

public class ForceUpgradeRequester implements Runnable {
	
	 public int versionCode;
	 
	    public ForceUpgradeRequester(int versionCode){
	        this.versionCode = versionCode;
	    }

	    @Override
	    public void run() {
	    	
	    	ForceUpgrade forceUpgrade = null;
	    	CZResponse response = HTTPOperationController.getInstance().checkForceUpgrade(versionCode, 0, 0);
	    	if(response != null && response.getResponseCode() == HttpURLConnection.HTTP_OK){
	    		forceUpgrade = (ForceUpgrade)JsonUtil.toModel(response.getResponseString(), ForceUpgrade.class);
	    		MatchstixPreferences.getInstance().setUpgradeAvailable(forceUpgrade.isUpgradeAvailable());
	    	}
	    }
}
