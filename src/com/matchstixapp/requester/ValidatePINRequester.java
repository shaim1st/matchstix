package com.matchstixapp.requester;


import java.net.HttpURLConnection;

import com.craterzone.httpclient.model.CZResponse;
import com.matchstixapp.R;
import com.matchstixapp.MatchstixApplication;
import com.matchstixapp.WS.HTTPOperationController;
import com.matchstixapp.db.DataController;
import com.matchstixapp.listener.PINValidateListener;
import com.matchstixapp.modal.User;
import com.matchstixapp.pojo.JSONPhone;

public class ValidatePINRequester implements Runnable{

	public static final String TAG = ValidatePINRequester.class.getSimpleName();
	private JSONPhone  request;

	public ValidatePINRequester(JSONPhone request) {
		this.request = request;
	}
	@Override
	public void run() {
		User user = DataController.getInstance().getUser();
		long id = user.getId();
		String accessToken = user.getAccessToken();
		CZResponse res = HTTPOperationController.getInstance().requestVarifyCode(id,accessToken, request);
		for (PINValidateListener listener : MatchstixApplication.getInstance()
				.getUIListeners(PINValidateListener.class)) {
		
		if (res == null) {
					String msg = (MatchstixApplication.getInstance().getResources()
							.getString(R.string.no_internet));
					listener.onPINValidationFailed(-1,msg);
		} else if (res.getResponseCode() != HttpURLConnection.HTTP_OK) {
				String msg = MatchstixApplication.getInstance().getResources().getString(R.string.unknown_error);
				msg +="\n STATUS CODE= "+res.getResponseCode();
				listener.onPINValidationFailed(res.getResponseCode(),msg);
		} else {
			listener.onPINValidationSuccess(request);
		}
		}
	}

}