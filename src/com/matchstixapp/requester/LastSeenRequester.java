package com.matchstixapp.requester;

import java.net.HttpURLConnection;
import java.util.ArrayList;

import android.util.Log;

import com.craterzone.cz_commons_lib.JsonUtil;
import com.craterzone.httpclient.HttpUrlConnectionUtil;
import com.craterzone.httpclient.model.CZResponse;
import com.craterzone.httpclient.model.CustomHttpParams;
import com.matchstixapp.MatchstixApplication;
import com.matchstixapp.WS.URLConstants;
import com.matchstixapp.db.DataController;
import com.matchstixapp.listener.LastSeenListener;

public class LastSeenRequester implements Runnable {
	public static final String TAG = LastSeenRequester.class.getSimpleName();
	private long userId;
	
	public LastSeenRequester(long userId) {
		this.userId = userId;
	}

	@Override
	public void run() {
		CZResponse response = null;
		LastSeen lastSeen = null;
		try {
			String url = URLConstants.LAST_SEEN_URL + userId;
			
			CustomHttpParams param = new CustomHttpParams("token", 
					DataController.getInstance().getUser().getAccessToken());
			ArrayList<CustomHttpParams> params = new ArrayList<CustomHttpParams>();
			params.add(param);
			
			response = HttpUrlConnectionUtil.get(url, "application/json", "application/json", params);
			lastSeen = (LastSeen) JsonUtil.toModel(response.getResponseString(), LastSeen.class);
			
		} catch(Exception e) {
			Log.e(TAG, "Request Denied: "+e.getMessage());
		}
		
		for(LastSeenListener listener : MatchstixApplication.getInstance().getUIListeners(LastSeenListener.class)) {
			if(lastSeen == null || response == null) {
//				listener.onInternetConnectionNotFound();
			} else if(response.getResponseCode() != HttpURLConnection.HTTP_OK) {
				listener.onLastSeenRequestFailed();
			} else {
				listener.onLastSeenRequestSuccess(userId, lastSeen.getSeconds());
			}
		}
	}
	
	class LastSeen {
		int seconds;

		public int getSeconds() {
			return seconds;
		}

		public void setSeconds(int seconds) {
			this.seconds = seconds;
		}
		
	}
	
}
