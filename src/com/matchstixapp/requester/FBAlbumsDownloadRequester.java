package com.matchstixapp.requester;

import java.net.HttpURLConnection;

import com.craterzone.cz_commons_lib.JsonUtil;
import com.craterzone.httpclient.HttpUrlConnectionUtil;
import com.craterzone.httpclient.model.CZResponse;
import com.matchstixapp.R;
import com.matchstixapp.MatchstixApplication;
import com.matchstixapp.db.DataController;
import com.matchstixapp.listener.FBAlbumsDownloadListener;
import com.matchstixapp.modal.FBAlbums;
import com.matchstixapp.modal.User;
import com.matchstixapp.services.FBWebServices;

public class FBAlbumsDownloadRequester implements Runnable {
	@Override
	public void run() {
		DataController dataController = DataController.getInstance();
		User user = dataController.getUser();
		String accessToken = user.getFbAccessToken();
		CZResponse res = HttpUrlConnectionUtil.get(
				FBWebServices.getMyAlbumsUrl(accessToken), null);
		for (FBAlbumsDownloadListener listener : MatchstixApplication.getInstance()
				.getUIListeners(FBAlbumsDownloadListener.class)) {
			if (res == null) {
						listener.onFBAlbumsDownloadFailed(MatchstixApplication.getInstance().getResources()
								.getString(R.string.no_internet));
			} else if (res.getResponseCode() != HttpURLConnection.HTTP_OK) {
				String msg = MatchstixApplication.getInstance().getResources()
						.getString(R.string.unknown_error);
				msg += "\n STATUS CODE= " + res.getResponseCode();
				listener.onFBAlbumsDownloadFailed(msg);
			} else {
				String albumJson = res.getResponseString();
				FBAlbums mAlbums = (FBAlbums) JsonUtil.toModel(albumJson,
						FBAlbums.class);
				dataController.updateFBAlbums(mAlbums);
				listener.onFBAlbumsDownloadSucceess(mAlbums);
			}
		}
	}
}