package com.matchstixapp.requester;

import java.net.HttpURLConnection;

import android.text.TextUtils;

import com.craterzone.cz_commons_lib.JsonUtil;
import com.craterzone.cz_commons_lib.StringUtil;
import com.craterzone.httpclient.model.CZResponse;
import com.matchstixapp.R;
import com.matchstixapp.MatchstixApplication;
import com.matchstixapp.WS.HTTPOperationController;
import com.matchstixapp.db.DataController;
import com.matchstixapp.listener.HTTPLogInListener;
import com.matchstixapp.modal.DiscoverySettings;
import com.matchstixapp.modal.User;
import com.matchstixapp.pojo.JSONLogin;
import com.matchstixapp.pojo.JSONSignUp;

public class LogInRequester implements Runnable {

	private JSONSignUp requestSignup;

	public LogInRequester(JSONSignUp user) {
		this.requestSignup = user;
	}
	@Override
	public void run() {
		
		CZResponse res = HTTPOperationController.getInstance().requestSignup(
				requestSignup);
		for(HTTPLogInListener listener : MatchstixApplication.getInstance().getUIListeners(HTTPLogInListener.class)) {
			if (res == null) {
				listener.onHTTPLogInFailed(MatchstixApplication.getInstance().getResources()
						.getString(R.string.no_internet));
			} else if(res.getResponseCode() == HttpURLConnection.HTTP_UNAUTHORIZED){
				listener.onUnauthorizedListener();
			}else if (res.getResponseCode() != HttpURLConnection.HTTP_OK) {
				String msg = MatchstixApplication.getInstance().getResources()
						.getString(R.string.unknown_error);
				msg += "\n STATUS CODE= " + res.getResponseCode();
				listener.onHTTPLogInFailed(msg);
			} else {
				JSONLogin responseLogin = (JSONLogin) JsonUtil.toModel(res.getResponseString(),
						JSONLogin.class);

				/********User details********/
				DataController dataController = DataController.getInstance();
				User user = dataController.getUser();
				user.setId(responseLogin.getUserId());
				user.setAccessToken(responseLogin.getToken());
				user.setPassword(responseLogin.getPassword());
				user.setImageSync(responseLogin.isImageSync());
				user.setRegistered(true);
				user.setLoggedIn(true);
				if( !StringUtil.isEmpty( responseLogin.getAbout() ) ) {
					user.setAbout(responseLogin.getAbout());					
				}
				if (TextUtils.isEmpty(user.about)) {
					user.setAbout(requestSignup.getProfile().getAboutUser());
				}
				user.setAge(requestSignup.getProfile().getAge());
				user.setDisplayName(requestSignup.getProfile().getFirstName());
				user.setEmail(requestSignup.getEmailId());
				user.setFirstName(requestSignup.getProfile().getFirstName());
				user.setGender(requestSignup.getProfile().getGender());
				user.setLastName(requestSignup.getProfile().getLastName());
				user.setName(requestSignup.getProfile().getFirstName());
				user.setUserName(requestSignup.getProfile().getFirstName());
				DataController.getInstance().saveUser();

				/********Settings********/
				DiscoverySettings settings = dataController.getSettings();
				settings.setDistance((int) responseLogin.getDiscoverySettings()
						.getDistance());
				settings.setGender(responseLogin.getDiscoverySettings()
						.getGenders());
				settings.setMaxAge(responseLogin.getDiscoverySettings().getAgeMax());
				settings.setMinAge(responseLogin.getDiscoverySettings().getAgeMin());
				DataController.getInstance().saveSettings();
				
				listener.onHTTPLogInSuccess(responseLogin);
			}
		}
	}

}