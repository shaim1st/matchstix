package com.matchstixapp.requester;

import java.net.HttpURLConnection;

import com.craterzone.httpclient.model.CZResponse;
import com.matchstixapp.R;
import com.matchstixapp.MatchstixApplication;
import com.matchstixapp.WS.HTTPOperationController;
import com.matchstixapp.db.DataController;
import com.matchstixapp.listener.SettingsUpdateListener;
import com.matchstixapp.pojo.JSONSettings;
import com.matchstixapp.utils.Constants;

public class UpdateDiscoverySettingsRequester implements Runnable {

	private JSONSettings settings;

	public UpdateDiscoverySettingsRequester(JSONSettings settings) {
		this.settings = settings;
	}

	@Override
	public void run() {

		String token = DataController.getInstance().getUser().getAccessToken();
		long id = DataController.getInstance().getUser().getId();
		CZResponse res = HTTPOperationController.getInstance()
				.requestdiscoverysettings(id, token, settings);
		for (SettingsUpdateListener listener : MatchstixApplication
				.getInstance().getUIListeners(SettingsUpdateListener.class)) {

			if (res == null) {
				String msg = MatchstixApplication.getInstance().getResources()
						.getString(R.string.no_internet);
				listener.onSettingsUpdateFailed(msg);
			}else if (res.getResponseCode() == Constants.BLOCKED_CODE) {
				listener.onBlockedUser();
			}else if (res.getResponseCode() != HttpURLConnection.HTTP_OK) {
				String msg = MatchstixApplication.getInstance().getResources()
						.getString(R.string.unknown_error);
				msg += "\n STATUS CODE= " + res.getResponseCode();
				listener.onSettingsUpdateFailed(msg);

			}else {
				listener.onSettingsUpdateSuccess();
			}
		}
	}

	public void update(SettingsUpdateListener listener) {
		String token = DataController.getInstance().getUser().getAccessToken();
		long id = DataController.getInstance().getUser().getId();
		CZResponse res = HTTPOperationController.getInstance()
				.requestdiscoverysettings(id, token, settings);

		if (res == null) {
			String msg = MatchstixApplication.getInstance().getResources()
					.getString(R.string.no_internet);
			listener.onSettingsUpdateFailed(msg);
		}else if (res.getResponseCode() == Constants.BLOCKED_CODE) {
			listener.onBlockedUser();
		} else if (res.getResponseCode() != HttpURLConnection.HTTP_OK) {
			String msg = MatchstixApplication.getInstance().getResources()
					.getString(R.string.unknown_error);
			msg += "\n STATUS CODE= " + res.getResponseCode();
			listener.onSettingsUpdateFailed(msg);

		}else {
			listener.onSettingsUpdateSuccess();
		}
	}

}