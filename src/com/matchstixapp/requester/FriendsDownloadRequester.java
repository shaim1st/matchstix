package com.matchstixapp.requester;

import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Arrays;

import android.util.Log;
import android.widget.Toast;

import com.craterzone.cz_commons_lib.JsonUtil;
import com.craterzone.httpclient.model.CZResponse;
import com.matchstixapp.MatchstixApplication;
import com.matchstixapp.R;
import com.matchstixapp.WS.HTTPOperationController;
import com.matchstixapp.db.DataController;
import com.matchstixapp.db.MatchstixPreferences;
import com.matchstixapp.listener.FriendsDownloadListener;
import com.matchstixapp.modal.Friend;
import com.matchstixapp.modal.Roster;
import com.matchstixapp.modal.User;
import com.matchstixapp.utils.Constants;

public class FriendsDownloadRequester implements Runnable {

	private boolean isPending;

	@Override
	public void run() {
		isPending = true;
		DataController dataController = DataController.getInstance();
		User user = dataController.getUser();
		String accessToken = user.getAccessToken();
		CZResponse res = HTTPOperationController.getInstance().requestFriends(
				user.getId(), accessToken);
		for (FriendsDownloadListener listener : MatchstixApplication
				.getInstance().getUIListeners(FriendsDownloadListener.class)) {
			if (res == null) {
				listener.onFriendsDownloadFailed(MatchstixApplication
						.getInstance().getResources()
						.getString(R.string.no_internet));
			}else if (res.getResponseCode() == Constants.BLOCKED_CODE) {
				listener.onBlockedUser();
			} else if (res.getResponseCode() == HttpURLConnection.HTTP_UNAUTHORIZED) {
				listener.onUnauthorizedListener();
			} else if (res.getResponseCode() != HttpURLConnection.HTTP_OK) {
				String msg = MatchstixApplication.getInstance().getResources()
						.getString(R.string.unknown_error);
				msg += "\n STATUS CODE= " + res.getResponseCode();
				listener.onFriendsDownloadFailed(msg);
			} else {
				String json = res.getResponseString();
				/*Roster roster = (Roster) JsonUtil.toModel(json, Roster.class);
				Roster.init(roster);
				roster.setNeedsRefresh(false);
				saveRosterInDB();
				*/

				byte[] decodedPhraseAsBytes;
				int hashCode = 0;
				try{
					decodedPhraseAsBytes = json.getBytes();
					hashCode = Arrays.deepHashCode(new Object[] { decodedPhraseAsBytes });
			}catch(Exception e){
				Log.d("",e.toString());
			   }
				MatchstixPreferences pref=MatchstixPreferences.getInstance();
				if (pref.getJsonHashCode() == 0) {
					pref.setJsonHashCodeFlag(1);
					Roster roster = (Roster) JsonUtil.toModel(json, Roster.class);
					Roster.init(roster);
					roster.setNeedsRefresh(false);
					saveRosterInDB();
					pref.setJsonHashCode(
							hashCode);
				} else if (pref.getJsonHashCode() != hashCode) 
				{
						pref.setJsonHashCodeFlag(0);
						Roster roster = (Roster) JsonUtil.toModel(json, Roster.class);
						for(int i=0;i<Roster.getInstance().size();i++){
							Friend frd=getFriendCompare(Roster.getInstance().getFriendList(),roster.getFriendList().get(i).getUserId());
						if(frd.getUserId()==0 && frd.getMatchDate()==0){
							Friend frd1=roster.getFriendList().get(i);
							Roster.getInstance().addFriend(frd1.getUserId(), frd1.getFirstName(),
									frd1.getLastName(), frd1.getProfilePicURL(), 
									frd1.getMatchDate(), 0);
							saveRosterInDB(frd1);
						}else{
						Roster.getInstance().setFriendByUserId(frd.getUserId(),frd,roster);
						Roster.getInstance().setNeedsRefresh(false);
						saveRosterInDB(frd);
						}
						}
						pref.setJsonHashCode(
								hashCode);
					}
				listener.onFriendsDownloadSucceess();
				Log.d("Calling Friend", ""+json);
			}
		}
		isPending = false;
	}

	public boolean isPending() {
		return isPending;
	}

	private void saveRosterInDB() {
		int count = Roster.getInstance().size();
		for (int i = 0; i < count; i++) {
			DataController.getInstance().updateFriend(
					Roster.getInstance().getFriend(i));
		}
	}
	private void saveRosterInDB(Friend frd) {
			DataController.getInstance().updateFriend(frd);
		}

	/*
	 * private void getPositionFriendByUserId(long userId) { int count =
	 * Roster.getInstance().size(); for (int i = 0; i < count; i++) {
	 * DataController.getInstance().updateFriend(
	 * Roster.getInstance().getFriend(i)); } }
	 */
	
	 public Friend getFriendCompare(ArrayList<Friend> list,long id) {
			for (int i = 0; i < list.size(); i++) {
				if (list.get(i).getUserId() == id) {
					return list.get(i);
				}
			}
			return new Friend(0, null, null, null, 0);
		}

}