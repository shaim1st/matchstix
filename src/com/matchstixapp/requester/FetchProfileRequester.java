package com.matchstixapp.requester;

import java.net.HttpURLConnection;

import android.util.Log;

import com.craterzone.cz_commons_lib.JsonUtil;
import com.craterzone.httpclient.model.CZResponse;
import com.matchstixapp.MatchstixApplication;
import com.matchstixapp.WS.HTTPOperationController;
import com.matchstixapp.db.DataController;
import com.matchstixapp.listener.FetchProfileListener;
import com.matchstixapp.modal.SuggestionProfile;
import com.matchstixapp.utils.Constants;

public class FetchProfileRequester implements Runnable {
	private final String TAG = FetchProfileRequester.class.getName();
	private long userId;
	
	public FetchProfileRequester(long userId) {
		this.userId = userId;
	}

	@Override
	public void run() {
		CZResponse response = null;
		
		try {
			response = HTTPOperationController.getInstance().requestProfile(
					DataController.getInstance().getUser().getId(),
					userId,
					DataController.getInstance().getUser().getAccessToken());
		} catch(Exception e) {
			Log.e(TAG, "Request Denied: "+e.getMessage());
		}
		
		for(FetchProfileListener listener : MatchstixApplication.getInstance().getUIListeners(FetchProfileListener.class)) {
			if(response == null) {
				listener.onInternetConnectionNotFound();
			}else if(response.getResponseCode() ==Constants.BLOCKED_CODE) {
				listener.onBlockedUser();
			} else if(response.getResponseCode() != HttpURLConnection.HTTP_OK) {
				listener.onFetchProfileRequestFailed();
			} else {
				listener.onFetchProfileRequestSuccess((SuggestionProfile) JsonUtil.toModel(
						response.getResponseString(), SuggestionProfile.class));
			}
		}
	}
	
}
