package com.matchstixapp.requester;

import java.net.HttpURLConnection;

import android.util.Log;

import com.craterzone.httpclient.model.CZResponse;
import com.matchstixapp.MatchstixApplication;
import com.matchstixapp.WS.HTTPOperationController;
import com.matchstixapp.db.DataController;
import com.matchstixapp.listener.UnmatchUserListener;
import com.matchstixapp.modal.Friend;
import com.matchstixapp.modal.Roster;
import com.matchstixapp.modal.User;

public class UnmatchUserRequester implements Runnable {
	
	private User user;
	private Friend friend;

	public UnmatchUserRequester(User user, Friend friend) {
		this.user = user;
		this.friend = friend;
	}
	
	@Override
	public void run() {
		CZResponse response = null;
		
		try {
			response = HTTPOperationController.getInstance()
					.requestDislikeUser(user.getAccessToken(),
							createJSON());
			if(response != null && response.getResponseCode() == HttpURLConnection.HTTP_OK) {
				Roster.getInstance().deleteFriend(friend.getUserId());
				DataController.getInstance().deleteFriend(friend);
			}
		} catch(Exception e) {
			Log.e("UnmatchUserRequester", "Request Denied: "+e.getMessage());
		}
		
		for(UnmatchUserListener listener : MatchstixApplication.getInstance().getUIListeners(UnmatchUserListener.class)) {
			if(response != null && response.getResponseCode() == HttpURLConnection.HTTP_OK) {
				listener.onUnmatchSucceess(friend.getUserId());
			} else if(response.getResponseCode() == HttpURLConnection.HTTP_UNAUTHORIZED){
				listener.onUnauthorizedListener();
			}else if(response.getResponseCode() == 423) {
				listener.onBlockedUser();
			}
			else{
				listener.onUnmatchFailed();
			}
		}
	}
	
	private String createJSON() {
		StringBuilder sb = new StringBuilder();
		sb.append('{');
		sb.append("\"").append("userId").append("\"").append(':')
				.append(user.getId()).append(',');
		sb.append("\"").append("friendId").append("\"").append(':')
				.append(friend.getUserId());
		sb.append('}');
		return sb.toString();
	}

}
