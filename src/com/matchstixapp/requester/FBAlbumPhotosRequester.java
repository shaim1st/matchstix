package com.matchstixapp.requester;

import java.net.HttpURLConnection;

import com.craterzone.cz_commons_lib.JsonUtil;
import com.craterzone.httpclient.HttpUrlConnectionUtil;
import com.craterzone.httpclient.model.CZResponse;
import com.matchstixapp.R;
import com.matchstixapp.MatchstixApplication;
import com.matchstixapp.db.DataController;
import com.matchstixapp.listener.FBAlbumPhotosDownloadListener;
import com.matchstixapp.modal.FBAlbumPhotos;
import com.matchstixapp.modal.User;
import com.matchstixapp.services.FBWebServices;

public class FBAlbumPhotosRequester implements Runnable{
	String id;
	public FBAlbumPhotosRequester(String id) {
		this.id = id;
	}
	@Override
	public void run() {
		DataController dataController=DataController.getInstance();
		User user = dataController.getUser();
		String accessToken = user.getFbAccessToken();
		CZResponse res = HttpUrlConnectionUtil.get(
				FBWebServices.getAlbumPhotosUrl(id, accessToken), null);
		for(FBAlbumPhotosDownloadListener listener : MatchstixApplication.getInstance().getUIListeners(FBAlbumPhotosDownloadListener.class)) {
			if (res == null) {
				listener.onFBAlbumPhotosDownloadFailed(MatchstixApplication.getInstance().getResources()
								.getString(R.string.no_internet));
			} else if (res.getResponseCode() != HttpURLConnection.HTTP_OK) {
				String msg = MatchstixApplication.getInstance().getResources().getString(R.string.unknown_error);
				msg +="\n STATUS CODE= "+res.getResponseCode();
				listener.onFBAlbumPhotosDownloadFailed(msg);
			} else {
				String photoJson = res.getResponseString();
				FBAlbumPhotos details = (FBAlbumPhotos) JsonUtil.toModel(
						photoJson, FBAlbumPhotos.class);
				dataController.refreshFBAlbumPhotos(id,details);
				listener.onFBAlbumPhotosDownloadSucceess(id,details);
			}
		}
	}
}
