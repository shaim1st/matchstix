package com.matchstixapp.requester;

import java.net.HttpURLConnection;

import android.util.Log;

import com.craterzone.httpclient.model.CZResponse;
import com.matchstixapp.MatchstixApplication;
import com.matchstixapp.WS.HTTPOperationController;
import com.matchstixapp.db.DataController;
import com.matchstixapp.listener.UpdateUserProfileDetailsListener;
import com.matchstixapp.modal.JSONUpdateImages;
import com.matchstixapp.pojo.JSONUpdateAbout;
import com.matchstixapp.utils.Constants;

public class UpdateUserProfileDetailsRequester implements Runnable {
	private JSONUpdateAbout userAboutMe;
	private JSONUpdateImages imageRequest;
	
	public UpdateUserProfileDetailsRequester(JSONUpdateAbout userAboutMe,
			JSONUpdateImages imageRequest) {
		this.userAboutMe = userAboutMe;
		this.imageRequest = imageRequest;
	}
	
	@Override
	public void run() {
		CZResponse updateAboutMeResponse = null;
		CZResponse updateImageResponse = null;
		
		try {
			if(userAboutMe != null) {
				updateAboutMeResponse = HTTPOperationController.getInstance().requestUpdateUserAboutMe(
						DataController.getInstance().getUser().getId(),
						DataController.getInstance().getUser().getAccessToken(),
						userAboutMe);;
			}
		} catch(Exception e) {
			Log.e("UpdateUserProfileDetailsRequester", "Request Denied: "+e.getMessage());
		}
		
		try {
			if(imageRequest != null) {
				updateImageResponse = HTTPOperationController.getInstance().requestAddOrUpdateImage(
						DataController.getInstance().getUser().getId(),
						DataController.getInstance().getUser().getAccessToken(),
						imageRequest);;
			}
		} catch(Exception e) {
			Log.e("UpdateUserProfileDetailsRequester", "Request Denied: "+e.getMessage());
		}
		
		for(UpdateUserProfileDetailsListener listener : MatchstixApplication.getInstance().getUIListeners(UpdateUserProfileDetailsListener.class)) {
			if(updateImageResponse != null && updateImageResponse.getResponseCode() == HttpURLConnection.HTTP_OK) {
				if(updateAboutMeResponse != null && updateAboutMeResponse.getResponseCode() == HttpURLConnection.HTTP_OK) {
					listener.onUserProfileDetailsUpdated(true, true);
				}else if(updateImageResponse.getResponseCode() == Constants.BLOCKED_CODE){
					listener.onBlockedUser();
				} else if(updateImageResponse.getResponseCode() == HttpURLConnection.HTTP_UNAUTHORIZED){
					listener.onUnauthorizedListener();
				}else {
					listener.onUserProfileDetailsUpdated(true, false);
				}
			} else if(updateAboutMeResponse != null && updateAboutMeResponse.getResponseCode() == HttpURLConnection.HTTP_OK) {
				listener.onUserProfileDetailsUpdated(false, true);
			} else if(updateAboutMeResponse != null && updateAboutMeResponse.getResponseCode() == HttpURLConnection.HTTP_UNAUTHORIZED){
				listener.onUnauthorizedListener();
			} 
			else if (updateAboutMeResponse.getResponseCode() == 423) {
				listener.onBlockedUser();
			}
				
			else {
				listener.onUserProfileDetailsUpdationFailed();
			}
		}
	}
	
}
