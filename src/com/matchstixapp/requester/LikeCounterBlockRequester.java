package com.matchstixapp.requester;

import android.os.CountDownTimer;

public class LikeCounterBlockRequester extends CountDownTimer {
	private static LikeCounterBlockRequester _instance=new LikeCounterBlockRequester();
	public static LikeCounterBlockRequester getInstance(){
		if(_instance==null)
			return new LikeCounterBlockRequester();
		return _instance;
	}
	private LikeCounterBlockRequester() {
		super(1*60*60*1000, 1000);
	}
	public LikeCounterBlockRequester(long millisInFuture, long countDownInterval) {
		super(millisInFuture, countDownInterval);
	}

	@Override
	public void onTick(long millisUntilFinished) {
		
	}

	@Override
	public void onFinish() {
	}
	
}
