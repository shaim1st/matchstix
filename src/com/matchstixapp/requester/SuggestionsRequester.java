package com.matchstixapp.requester;

import java.net.HttpURLConnection;

import com.craterzone.cz_commons_lib.JsonUtil;
import com.craterzone.httpclient.model.CZResponse;
import com.matchstixapp.R;
import com.matchstixapp.MatchstixApplication;
import com.matchstixapp.WS.HTTPOperationController;
import com.matchstixapp.db.DataController;
import com.matchstixapp.listener.SuggestionsListener;
import com.matchstixapp.modal.SuggestionDetailsList;
import com.matchstixapp.utils.Constants;

public class SuggestionsRequester implements Runnable {

	private int count;
	private boolean pending = false;
	
	private boolean moreSuggestionAvailable;

	public SuggestionsRequester(int count) {
		moreSuggestionAvailable = true;
		this.count = count;
	}

	@Override
	public void run() {
		pending = true;
		long id = DataController.getInstance().getUser().getId();
		String token = DataController.getInstance().getUser().getAccessToken();
		CZResponse res = HTTPOperationController.getInstance()
				.requestGetSuggestions(id, token, count);
		for (SuggestionsListener listener : MatchstixApplication.getInstance()
				.getUIListeners(SuggestionsListener.class)) {
			if (res == null) {
				String msg = MatchstixApplication.getInstance().getResources()
						.getString(R.string.no_internet);
				listener.onFailed(msg);
			}else if (res.getResponseCode() == Constants.BLOCKED_CODE) {
				listener.onBlockedUser();
				moreSuggestionAvailable = false;
			}else if(res.getResponseCode() == HttpURLConnection.HTTP_UNAUTHORIZED){
				listener.onUnauthorizedListener();
			} else if (res.getResponseCode() == HttpURLConnection.HTTP_NO_CONTENT) {
				String msg = MatchstixApplication.getInstance().getResources()
						.getString(R.string.unknown_error);
				msg += "\n STATUS CODE= " + res.getResponseCode();
				listener.onFailed(msg);
				moreSuggestionAvailable = false;
			}else if (res.getResponseCode() != HttpURLConnection.HTTP_OK) {
				String msg = MatchstixApplication.getInstance().getResources()
						.getString(R.string.unknown_error);
				msg += "\n STATUS CODE= " + res.getResponseCode();
				listener.onFailed(msg);
			
			} else {
				SuggestionDetailsList suggestions = (SuggestionDetailsList) JsonUtil
						.toModel(res.getResponseString(),
								SuggestionDetailsList.class);
				listener.onSuccess(suggestions);
				count++;
				moreSuggestionAvailable = true;
			}
		}
		pending = false;
	}
	
	public boolean isPending() {
		return pending;
	}
	
	public boolean isMoreSuggestionAvailable() {
		return moreSuggestionAvailable;
	}
}