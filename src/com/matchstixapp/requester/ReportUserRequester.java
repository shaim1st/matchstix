package com.matchstixapp.requester;

import java.net.HttpURLConnection;

import android.util.Log;

import com.craterzone.httpclient.model.CZResponse;
import com.matchstixapp.MatchstixApplication;
import com.matchstixapp.WS.HTTPOperationController;
import com.matchstixapp.db.DataController;
import com.matchstixapp.listener.ReportUserListener;
import com.matchstixapp.modal.Friend;
import com.matchstixapp.modal.Roster;
import com.matchstixapp.modal.User;
import com.matchstixapp.pojo.JSONReportUser;
import com.matchstixapp.utils.Constants;

public class ReportUserRequester implements Runnable {

	private User user;
	private Friend friend;
	
	public ReportUserRequester(User user, Friend friend) {
		this.user = user;
		this.friend = friend;
	}
	
	@Override
	public void run() {
		CZResponse response = null;
		
		try {
			JSONReportUser jsonUser = new JSONReportUser();
			jsonUser.setFriendId(friend.getUserId());
			jsonUser.setUserId(user.getId());
			
			response = HTTPOperationController.getInstance()
					.requestReportUser(user.getAccessToken(),
							jsonUser);
			if(response != null && response.getResponseCode() == HttpURLConnection.HTTP_OK) {
				Roster.getInstance().deleteFriend(friend.getUserId());
				DataController.getInstance().deleteFriend(friend);
			}
		} catch(Exception e) {
			Log.e("ReportUserRequester", "Request Denied: "+e.getMessage());
		}
		
		for(ReportUserListener listener : MatchstixApplication.getInstance().getUIListeners(ReportUserListener.class)) {
			if(response != null && response.getResponseCode() == HttpURLConnection.HTTP_OK) {
				listener.onReportSucceess();
			}else if(response.getResponseCode() == Constants.BLOCKED_CODE){
				listener.onBlockedUser();
			}else if(response.getResponseCode() == HttpURLConnection.HTTP_UNAUTHORIZED){
				listener.onUnauthorizedListener();
			}else {
				listener.onReportFailed();
			}
		}
	}

}
