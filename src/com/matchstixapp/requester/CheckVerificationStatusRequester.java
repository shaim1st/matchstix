package com.matchstixapp.requester;

import java.net.HttpURLConnection;

import com.craterzone.cz_commons_lib.JsonUtil;
import com.craterzone.httpclient.model.CZResponse;
import com.matchstixapp.R;
import com.matchstixapp.MatchstixApplication;
import com.matchstixapp.WS.HTTPOperationController;
import com.matchstixapp.db.DataController;
import com.matchstixapp.listener.NumberVerificationStatusListener;
import com.matchstixapp.modal.User;
import com.matchstixapp.pojo.JSONNumber;

public class CheckVerificationStatusRequester implements Runnable{

	@Override
	public void run() {
		User user = DataController.getInstance().getUser();
		CZResponse res = HTTPOperationController.getInstance()
				.requestCheckVarificationStatus(user.getId(),
						user.getAccessToken());
		for(NumberVerificationStatusListener listener : MatchstixApplication.getInstance().getUIListeners(NumberVerificationStatusListener.class)) {
			if (res == null) {
				listener.onNumberVerificationError(MatchstixApplication.getInstance().getResources()
						.getString(R.string.no_internet));
			} else if (res.getResponseCode() != HttpURLConnection.HTTP_OK) {
				int code = -1;
				String msg = null;
				if (code != 403) {
					code = res.getResponseCode();
					msg = MatchstixApplication.getInstance().getResources()
							.getString(R.string.unknown_error);
					msg += "\n STATUS CODE= " + code;
				}
				listener.onNumberVerificationFailed(code, msg);
			} else {
				JSONNumber number = (JSONNumber) JsonUtil.toModel(res.getResponseString(), JSONNumber.class);
				listener.onNumberVerificationSucceess(number);
			}
		}
		
	}
}