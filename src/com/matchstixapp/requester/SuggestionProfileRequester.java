package com.matchstixapp.requester;

import java.net.HttpURLConnection;

import com.craterzone.cz_commons_lib.JsonUtil;
import com.craterzone.httpclient.model.CZResponse;
import com.matchstixapp.R;
import com.matchstixapp.MatchstixApplication;
import com.matchstixapp.WS.HTTPOperationController;
import com.matchstixapp.db.DataController;
import com.matchstixapp.listener.ProfileListener;
import com.matchstixapp.modal.SuggestionDetail;
import com.matchstixapp.modal.SuggestionProfile;
import com.matchstixapp.utils.Constants;

public class SuggestionProfileRequester implements Runnable{

	private SuggestionDetail detail;
	public SuggestionProfileRequester(SuggestionDetail detail) {
		this.detail = detail;
	}
	@Override
	public void run() {
		long id = DataController.getInstance().getUser().getId();
		String token=DataController.getInstance().getUser().getAccessToken();
		long friendId=detail.getUserId();
		CZResponse res = HTTPOperationController.getInstance().requestProfile(id,friendId,token);
		
		for(ProfileListener listener : MatchstixApplication.getInstance().getUIListeners(ProfileListener.class)) {
			if (res == null) {
				
				String msg = MatchstixApplication.getInstance().getResources()
						.getString(R.string.no_internet);
				
				listener.onProfileDownloadFailed(msg);
				
			}else if(res.getResponseCode() == Constants.BLOCKED_CODE){
				listener.onBlockedUser();
			}else if(res.getResponseCode() == HttpURLConnection.HTTP_UNAUTHORIZED){
				listener.onUnauthorizedListener();
			} else if (res.getResponseCode() != HttpURLConnection.HTTP_OK) {
				String msg = MatchstixApplication.getInstance().getResources().getString(R.string.unknown_error);
				msg +="\n STATUS CODE= "+res.getResponseCode();
				listener.onProfileDownloadFailed(msg);
			} else {
				SuggestionProfile profile = (SuggestionProfile) JsonUtil.toModel(res.getResponseString(), SuggestionProfile.class);
				listener.onProfileDownloadSuccess(profile,detail);
			}
		}
	}
}