package com.matchstixapp.requester;

import java.net.HttpURLConnection;

import com.craterzone.httpclient.model.CZResponse;
import com.matchstixapp.R;
import com.matchstixapp.MatchstixApplication;
import com.matchstixapp.WS.HTTPOperationController;
import com.matchstixapp.db.DataController;
import com.matchstixapp.listener.Keys;
import com.matchstixapp.listener.Operation;
import com.matchstixapp.listener.OperationListener;
import com.matchstixapp.pojo.JSONUpdateLocation;
import com.matchstixapp.utils.Constants;

public class UserLocationUpdater implements Runnable {

	public static final String TAG = UserLocationUpdater.class
			.getSimpleName();
	private JSONUpdateLocation request;
	private OperationListener listener;

	public UserLocationUpdater(JSONUpdateLocation request,
			OperationListener listener) {
		this.request = request;
		this.listener = listener;
	}

	@Override
	public void run() {
		Operation operation = new Operation();
		String token = DataController.getInstance().getUser().getAccessToken();
		long id = DataController.getInstance().getUser().getId();
		CZResponse res = HTTPOperationController.getInstance()
				.requestUpdateUserLatLng(id, token, request);
		if (res == null) {
			operation.setState(Operation.STATE_ERROR);
			operation.getBundle().putString(
					Keys.KEY_ERROR_MESSAGE,
					MatchstixApplication.getInstance().getResources()
							.getString(R.string.no_internet));
		} else if(res.getResponseCode() == Constants.BLOCKED_CODE){
			listener.onBlockedUser();
		}else if (res.getResponseCode() != HttpURLConnection.HTTP_OK) {
			operation.setState(Operation.STATE_FAILED);
			operation.getBundle().putInt(Keys.KEY_STATUS_CODE,
					res.getResponseCode());
			String msg = MatchstixApplication.getInstance().getResources()
					.getString(R.string.unknown_error);
			msg += "\n STATUS CODE= " + res.getResponseCode();
			operation.getBundle().putString(Keys.KEY_ERROR_MESSAGE, msg);
		} else {
			operation.setState(Operation.STATE_SUCCESS);

		}
		listener.onOperationFinished(operation);
	}
	public void start() {
		Thread t = new Thread(this);
		t.setPriority(Thread.MIN_PRIORITY);
		t.setDaemon(true);
		t.start();
	}
}