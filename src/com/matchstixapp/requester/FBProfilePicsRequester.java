package com.matchstixapp.requester;

import java.net.HttpURLConnection;
import java.util.ArrayList;

import android.text.TextUtils;

import com.craterzone.cz_commons_lib.JsonUtil;
import com.craterzone.httpclient.HttpUrlConnectionUtil;
import com.craterzone.httpclient.model.CZResponse;
import com.matchstixapp.R;
import com.matchstixapp.MatchstixApplication;
import com.matchstixapp.db.DataController;
import com.matchstixapp.listener.FBProfilePicsDownloadListener;
import com.matchstixapp.modal.AlbumPhoto;
import com.matchstixapp.modal.FBAlbumPhotos;
import com.matchstixapp.modal.FBAlbums;
import com.matchstixapp.modal.FBProfilePic;
import com.matchstixapp.modal.User;
import com.matchstixapp.services.FBWebServices;

public class FBProfilePicsRequester implements Runnable {

	@Override
	public void run() {

		DataController dataController = DataController.getInstance();
		User user = dataController.getUser();
		String accessToken = user.getFbAccessToken();
		String userId = user.getFbUserId();
		CZResponse res = HttpUrlConnectionUtil.get(
				FBWebServices.getMyAlbumsUrl(accessToken), null);
		if (res==null) {
			onFailed(MatchstixApplication.getInstance().getResources()
					.getString(R.string.no_internet));
			return;
		}
		if (res.getResponseCode()!=HttpURLConnection.HTTP_OK) {
			String msg = MatchstixApplication.getInstance().getResources()
					.getString(R.string.unknown_error);
			msg += "\n STATUS CODE= " + res.getResponseCode();
			onFailed(msg);
			return;
		}
		String albumJson = res.getResponseString();
		FBAlbums mAlbums = (FBAlbums) JsonUtil.toModel(albumJson,
				FBAlbums.class);
		dataController.updateFBAlbums(mAlbums);
		String id = dataController.getProfileAlbumId();
		if (TextUtils.isEmpty(id)) {
			syncAndUploadDefaultProfilePic(userId); 
		} else {
			syncAndUploadProfilePictureAlbumPhotos(id, accessToken);
		}
	}

	private void onFailed(String msg) {
		for(FBProfilePicsDownloadListener listener : MatchstixApplication.getInstance().getUIListeners(FBProfilePicsDownloadListener.class)) {
			listener.onFBProfilePicsDownloadFailed(msg);
		}
	}
	
	private void syncAndUploadDefaultProfilePic(String userId) {
		CZResponse res = HttpUrlConnectionUtil.get(FBWebServices.getUserThumbnailUrl(userId), null);
		if( res == null || res.getResponseCode() != HttpURLConnection.HTTP_OK ) {
			onFailed(MatchstixApplication.getInstance().getResources()
					.getString(R.string.no_internet));
		} else {
			String photoJson = res.getResponseString();
			FBProfilePic fbProfilePic = (FBProfilePic) JsonUtil.toModel(
					photoJson, FBProfilePic.class);
			String url = fbProfilePic.getData().getUrl();
			DataController.getInstance().getUser().setAvatarURL(url);
			DataController.getInstance().saveUser();
			AlbumPhoto aPhoto = new AlbumPhoto();
			aPhoto.setId(""+1);
			aPhoto.setLocalAvatar(url);
			aPhoto.setRemoteAvatar(url);
			FBAlbumPhotos fbAlbumPhotos = new FBAlbumPhotos();
			ArrayList<AlbumPhoto> photos = new ArrayList<AlbumPhoto>();
			photos.add(aPhoto);
			fbAlbumPhotos.setData(photos);
			DataController.getInstance().setUserProfilePics(photos, true);
			for(FBProfilePicsDownloadListener listener : MatchstixApplication.getInstance().getUIListeners(FBProfilePicsDownloadListener.class)) {
				listener.onFBProfilePicsDownloadSucceess(fbAlbumPhotos);
			}
		}
	}
	
	private void syncAndUploadProfilePictureAlbumPhotos(String albumId, String accessToken) {
		CZResponse res = HttpUrlConnectionUtil.get(FBWebServices.
				getAlbumPhotosUrl(albumId, accessToken), null);
		
		if (res==null) {
			onFailed(MatchstixApplication.getInstance().getResources()
					.getString(R.string.no_internet));
		} else if (res.getResponseCode()!=HttpURLConnection.HTTP_OK) {
			String msg = MatchstixApplication.getInstance().getResources()
					.getString(R.string.unknown_error);
			msg += "\n STATUS CODE= " + res.getResponseCode();
			onFailed(msg);
		} else {
			String photoJson = res.getResponseString();
			FBAlbumPhotos photos = (FBAlbumPhotos) JsonUtil.toModel(
					photoJson, FBAlbumPhotos.class);
			DataController.getInstance().getUser().setAvatarURL(FBWebServices.getPicURL(photos.getData().get(0).getId()));
			DataController.getInstance().saveUser();
			DataController.getInstance().setUserProfilePics(photos.getData(), true);
			DataController.getInstance().refreshFBAlbumPhotos(albumId,photos);
			for(FBProfilePicsDownloadListener listener : MatchstixApplication.getInstance().getUIListeners(FBProfilePicsDownloadListener.class)) {
				listener.onFBProfilePicsDownloadSucceess(photos);
			}
		}
	}

}
