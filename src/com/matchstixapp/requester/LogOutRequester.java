package com.matchstixapp.requester;


import java.net.HttpURLConnection;

import com.craterzone.cz_commons_lib.StringUtil;
import com.craterzone.httpclient.model.CZResponse;
import com.matchstixapp.R;
import com.matchstixapp.MatchstixApplication;
import com.matchstixapp.WS.HTTPOperationController;
import com.matchstixapp.db.DataController;
import com.matchstixapp.db.MatchstixPreferences;
import com.matchstixapp.listener.HTTPLogOutListener;
import com.matchstixapp.utils.Constants;

public class LogOutRequester implements Runnable {

	@Override
	public void run() {
		long id = DataController.getInstance().getUser().getId();
		String token = DataController.getInstance().getUser().getAccessToken();
		String deviceToken = MatchstixPreferences.getInstance().getGCMRegId();
		if(StringUtil.isEmpty(deviceToken)) {
			deviceToken = "dummytoken";
		}
		CZResponse res = HTTPOperationController.getInstance().requestLogout(
				id, token,deviceToken);
		for(HTTPLogOutListener listener : MatchstixApplication.getInstance().getUIListeners(HTTPLogOutListener.class)) {
			
			if (res == null) {
				String msg =MatchstixApplication.getInstance().getResources()
						.getString(R.string.no_internet);
				listener.onHTTPLogOutFailed(res.getResponseCode(), msg);
			}else if(res.getResponseCode() == Constants.BLOCKED_CODE){
			//	listener.onHTTPLogOutSuccess();// Here I want to logout if blocked user want
			}else if(res.getResponseCode() == HttpURLConnection.HTTP_UNAUTHORIZED){
				listener.onUnauthorizedListener();
			}
			else if (res.getResponseCode() != HttpURLConnection.HTTP_OK) {
				String msg = MatchstixApplication.getInstance().getResources()
						.getString(R.string.unknown_error);
				msg += "\n STATUS CODE= " + res.getResponseCode();
				listener.onHTTPLogOutFailed(res.getResponseCode(), msg);
			} else {
				listener.onHTTPLogOutSuccess();
			}
		}
	}

}
