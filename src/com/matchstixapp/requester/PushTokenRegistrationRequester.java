package com.matchstixapp.requester;

import java.net.HttpURLConnection;

import com.craterzone.cz_commons_lib.JsonUtil;
import com.craterzone.httpclient.model.CZResponse;
import com.matchstixapp.WS.HTTPOperationController;
import com.matchstixapp.db.DataController;
import com.matchstixapp.db.MatchstixPreferences;

public class PushTokenRegistrationRequester implements Runnable {

	private String deviceToken;

	public PushTokenRegistrationRequester(String pushToken) {
		this.deviceToken = pushToken;
	}
	@Override
	public void run() {
		UserPushToken userPushToken = new UserPushToken(DataController.getInstance().getUser().getId(), deviceToken);
		CZResponse res = HTTPOperationController.getInstance().registerPushToken(JsonUtil.toJson(userPushToken));
		if(res != null && res.getResponseCode() == HttpURLConnection.HTTP_OK) {
			MatchstixPreferences.getInstance().setGCmTokenRegistered(true);
		}
	}

	
	class UserPushToken {
		private long userId;
		private String deviceToken;
		public UserPushToken(long userId, String pushToken) {
			this.userId = userId;
			this.deviceToken = pushToken;
		}
		public long getUserId() {
			return userId;
		}
		public void setUserId(long userId) {
			this.userId = userId;
		}
		public String getDeviceToken() {
			return deviceToken;
		}
		public void setDeviceToken(String pushToken) {
			this.deviceToken = pushToken;
		}
	}
}
