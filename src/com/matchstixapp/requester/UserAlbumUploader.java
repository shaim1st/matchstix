package com.matchstixapp.requester;

import java.net.HttpURLConnection;

import com.craterzone.cz_commons_lib.JsonUtil;
import com.craterzone.httpclient.HttpUrlConnectionUtil;
import com.craterzone.httpclient.model.CZResponse;
import com.matchstixapp.db.DataController;
import com.matchstixapp.modal.Album;
import com.matchstixapp.modal.FBAlbums;
import com.matchstixapp.modal.User;
import com.matchstixapp.services.FBWebServices;
import com.matchstixapp.utils.Constants;

public class UserAlbumUploader implements Runnable {

	private boolean pending;

	@Override
	public void run() {
		DataController dataController = DataController.getInstance();
		User user = dataController.getUser();
		String accessToken = user.getFbAccessToken();
		CZResponse res = HttpUrlConnectionUtil.get(
				FBWebServices.getMyAlbumsUrl(accessToken), null);
		if (res != null && res.getResponseCode() == HttpURLConnection.HTTP_OK) {
			FBAlbums mAlbums = (FBAlbums) JsonUtil.toModel(
					res.getResponseString(), FBAlbums.class);
			DataController.getInstance().setMyFBAlbums(mAlbums);
			String profilePictureAlbumId = null;
			for (Album album : mAlbums.getData()) {
				if (album.getName().equalsIgnoreCase(Constants.PROFILE_PIC_ALBUM_NAME)) {
					profilePictureAlbumId = album.getId();
				}
			}

			/**
			 * if user has profile pic album then upload it otherwise default
			 * profile pic.
			 */
			if (profilePictureAlbumId != null) {
				uploadUserProfilePicAlbum(profilePictureAlbumId, accessToken);
			} else {
				uploadUserDefaultProfilePic(accessToken);
			}

		}
	}

	public boolean isPending() {
		return pending;
	}

	private String getDefaultProfilePic() {
		return null;
	}

	private boolean isDefaultProfilePic() {
		return false;
	}

	private void uploadUserProfilePicAlbum(String albumId, String accessToken) {
		CZResponse res = HttpUrlConnectionUtil.get(
				FBWebServices.getAlbumPhotosUrl(albumId, accessToken), null);
	}

	private void uploadUserDefaultProfilePic(String accessToken) {

	}
}
