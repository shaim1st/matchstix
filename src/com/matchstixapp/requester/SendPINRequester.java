package com.matchstixapp.requester;

import java.net.HttpURLConnection;

import com.craterzone.cz_commons_lib.JsonUtil;
import com.craterzone.httpclient.model.CZResponse;
import com.matchstixapp.R;
import com.matchstixapp.MatchstixApplication;
import com.matchstixapp.WS.HTTPOperationController;
import com.matchstixapp.db.DataController;
import com.matchstixapp.listener.PINSendListener;
import com.matchstixapp.modal.JSONError;
import com.matchstixapp.modal.User;
import com.matchstixapp.pojo.JSONNumber;

public class SendPINRequester implements Runnable {

	private JSONNumber request;

	public SendPINRequester(JSONNumber request) {
		this.request = request;
	}

	@Override
	public void run() {
		User user = DataController.getInstance().getUser();
		long id = user.getId();
		String accessToken = user.getAccessToken();
		user.setNumber(request.phoneNumber);
		user.setCC(request.cc);
		DataController.getInstance().saveUser();
		CZResponse res = HTTPOperationController.getInstance()
				.requestSendValidationCode(id, accessToken, request);
		if(res != null && res.getResponseCode() == 406){
			
		}
		
		
		for (PINSendListener listener : MatchstixApplication.getInstance()
				.getUIListeners(PINSendListener.class)) {

			if (res == null) {
				String msg = MatchstixApplication.getInstance().getResources()
						.getString(R.string.no_internet);
				listener.onPINSendFailed(-1,msg);
			}else if(res.getResponseCode() == 406){
				JSONError jsonError = (JSONError)JsonUtil.toModel(res.getResponseString(), JSONError.class);
				listener.onPINSendError(jsonError.getErrorCode());
			} else if (res.getResponseCode() != HttpURLConnection.HTTP_OK) {
				String msg = MatchstixApplication.getInstance().getResources()
						.getString(R.string.unknown_error);
				msg += "\n STATUS CODE= " + res.getResponseCode();
				user.setNumberVerified(true);
				listener.onPINSendFailed(res.getResponseCode(),msg);
			} else {
				listener.onPINSendSuccess(request);
			}
		}

	}

}