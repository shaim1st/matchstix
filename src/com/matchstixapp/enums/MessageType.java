package com.matchstixapp.enums;

public enum MessageType {

	SENDER(1), RECEIVER(2),TIME_BREAK(0) ;

	private int value;

	MessageType( int value ) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}
}
