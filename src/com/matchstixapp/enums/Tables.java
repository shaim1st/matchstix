package com.matchstixapp.enums;

public enum Tables {
	USER("user"), SUGGESTION_ACTIONS("suggestion_actions"),SUGGESTIONS("suggestions"),FRIENDS("friends"), LOCATION("location");

	private String label;

	private Tables(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}

	public enum Users {
		COL_USER_ID("user_id"), COL_USER_NAME("user_name"), COL_DISPLAY_NAME(
				"display_name"), COL_GENDER("gender"), COL_DOB("dob"), COL_ABOUT_ME(
				"about_me"), COL_AGE("age");

		private String label;

		private Users(String label) {
			this.label = label;
		}

		public String getLabel() {
			return label;
		}

		public static String createTableQuery() {
			return "CREATE TABLE IF NOT EXISTS " + USER + " ("
					+ COL_USER_ID.getLabel() + " INTEGER PRIMARY KEY, "
					+ COL_USER_NAME.getLabel() + " TEXT, "
					+ COL_DISPLAY_NAME.getLabel() + " TEXT, "
					+ COL_GENDER.getLabel() + " INTEGER, "
					+ COL_DOB.getLabel() + " LONG, "
					+ COL_ABOUT_ME.getLabel() + " TEXT, "
					+ COL_ABOUT_ME.getLabel() + " INTEGER, " + ")";
		}
	}

	public enum SuggestionActions {
		COL_USER_ID("user_id"), COL_LIKE("like");

		private String label;

		private SuggestionActions(String label) {
			this.label = label;
		}

		public String getLabel() {
			return label;
		}

		public static String createTableQuery() {
			return "CREATE TABLE IF NOT EXISTS " + SUGGESTION_ACTIONS + " ("
					+ COL_USER_ID.getLabel() + " LONG PRIMARY KEY, "
					+ COL_LIKE.getLabel() + " TEXT"
					+ ")";
		}
	}
	
	public enum Suggestions {
		COL_USER_ID("user_id"), COL_FIRST_NAME("first_name"),COL_LAST_NAME("last_name"),COL_COMMON_INTEREST("common_interest"),COL_COMMON_FRIEND("common_friend"),COL_AGE("age");

		private String label;

		private Suggestions(String label) {
			this.label = label;
		}

		public String getLabel() {
			return label;
		}

		public static String createTableQuery() {
			
			return "CREATE TABLE IF NOT EXISTS " + SUGGESTIONS + "("
					+ COL_USER_ID.getLabel() + " LONG PRIMARY KEY, "
					+ COL_FIRST_NAME.getLabel() + " TEXT, "
					+ COL_LAST_NAME.getLabel() + " TEXT, "
					+ COL_AGE.getLabel() + " INTEGER, "
					+ COL_COMMON_FRIEND.getLabel() + " INTEGER, "
					+ COL_COMMON_INTEREST.getLabel() + " INTEGER "
					+ ")";
			
		}
	}

	public enum Friends {
		COL_USER_ID("user_id"), COL_FIRST_NAME("first_name"),COL_LAST_NAME("last_name"),COL_PIC_URL("pic_url"),COL_MATCHED_DATE("matched_date"),
		COL_CHAT_DATE("lastchat_date");

		private String label;

		private Friends(String label) {
			this.label = label;
		}

		public String getLabel() {
			return label;
		}

		public static String createTableQuery() {
			return "CREATE TABLE IF NOT EXISTS " + FRIENDS.label + "("
					+ COL_USER_ID.getLabel() + " LONG PRIMARY KEY, "
					+ COL_FIRST_NAME.getLabel() + " TEXT, "
					+ COL_LAST_NAME.getLabel() + " TEXT, "
					+ COL_PIC_URL.getLabel() + " TEXT, "
					+ COL_MATCHED_DATE.getLabel() + " LONG, "
					+ COL_CHAT_DATE.getLabel() + " LONG "//
					+ ")";
			
		}
	}
	
	public enum Location {

		COL_CC("cc"), COL_MCC("mcc"), COL_NAME("name");
		private String label;

		private Location(String label) {
			this.label = label;
		}

		public String getLabel() {
			return label;
		}

		public static String createTableQuery() {
			return "CREATE TABLE IF NOT EXISTS " + LOCATION + "("
					+ COL_CC.getLabel() + " text, " + COL_MCC.getLabel()
					+ " text, " + COL_NAME.getLabel() + " text" + ")";
		}

	}

}
