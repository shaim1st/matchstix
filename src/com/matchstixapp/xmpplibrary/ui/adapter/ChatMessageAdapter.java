/**
 * Copyright (c) 2013, Redsolution LTD. All rights reserved.
 * 
 * This file is part of Xabber project; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License, Version 3.
 * 
 * Xabber is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License,
 * along with this program. If not, see http://www.gnu.org/licenses/.
 */
package com.matchstixapp.xmpplibrary.ui.adapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.app.Activity;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.CharacterStyle;
import android.text.style.TextAppearanceSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.craterzone.cz_commons_lib.EncodingDecodingUtil;
import com.craterzone.czimageloader.imageLoader.CZImageLoader;
import com.craterzone.czimageloader.universalImageLoader.UniversalImageLoaderUtil;
import com.matchstixapp.R;
import com.matchstixapp.modal.Friend;
import com.xabber.android.data.SettingsManager;
import com.xabber.android.data.SettingsManager.ChatsDivide;
import com.xabber.android.data.account.AccountManager;
import com.xabber.android.data.extension.avatar.HttpAvatarManager;
import com.xabber.android.data.extension.muc.MUCManager;
import com.xabber.android.data.message.ChatAction;
import com.xabber.android.data.message.MessageManager;
import com.xabber.android.data.message.type.AbstractMessageItem;
import com.xabber.android.data.message.type.FileDownloadStatus;
import com.xabber.android.data.message.type.FileMessageItem;
import com.xabber.android.data.message.type.MessageType;
import com.xabber.android.data.message.type.MessageTypeImpl;
import com.xabber.android.data.roster.AbstractContact;
import com.xabber.android.data.roster.RosterManager;
import com.xabber.android.ui.adapter.UpdatableAdapter;
import com.xabber.android.utils.Emoticons;
import com.xabber.android.utils.StringUtils;
import com.xabber.xmpp.address.Jid;

/**
 * Adapter for the list of messages in the chat.
 * 
 * @author craetrzone
 * 
 */
@SuppressWarnings("rawtypes")
public class ChatMessageAdapter extends BaseAdapter implements
		UpdatableAdapter, MessageTypeImpl {

	private static final String TAG = ChatMessageAdapter.class.getName();

	private CZImageLoader mImageLoader = null;
	private final Activity activity;
	private String account;
	private String user;
	private boolean isMUC;
	private List<AbstractMessageItem> messages;

	/**
	 * Message font appearance.
	 */
	private final int appearanceStyle;

	/**
	 * Divider between header and body.
	 */
	private final String divider;

	/**
	 * Text with extra information.
	 */
	private String hint;
	private LayoutInflater _inflater = null;
	private TextView loadMore;
	private View moreLayout;

	private static final class MessageViewType {

		private MessageViewType() {
		}

		public static final int TEXT_VIEW = 0;
		public static final int IMAGE_VIEW = 1;
		public static final int HINT_VIEW = 2;
		public static final int EMPTY_VIEW = 3;
		public static final int ACTION_VIEW = 4;
	}

	public ChatMessageAdapter(final Activity activity) {
		this.activity = activity;
		messages = Collections.emptyList();
		account = null;
		user = null;
		hint = null;
		appearanceStyle = SettingsManager.chatsAppearanceStyle();
		_inflater = LayoutInflater.from(activity);
		ChatsDivide chatsDivide = SettingsManager.chatsDivide();
		if (chatsDivide == ChatsDivide.always
				|| (chatsDivide == ChatsDivide.portial && !activity
						.getResources().getBoolean(R.bool.landscape)))
			divider = "\n";
		else
			divider = " ";
		mImageLoader = CZImageLoader.getInstance(activity);
//		moreLayout = (View)view.findViewById(R.id.more_btn_layout);
	}

	@Override
	public int getCount() {
		return messages.size() + 1;
	}

	@Override
	public Object getItem(int position) {
		if (position < messages.size()) {
			return messages.get(position);
		} else {
			return null;
		}
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public int getViewTypeCount() {
		return 11;
	}

	public List<AbstractMessageItem> getItems() {
		return messages;
	}

	@Override
	public int getItemViewType(int position) {
		if (position < messages.size()) {
			AbstractMessageItem messageItem = (AbstractMessageItem) getItem(position);
			MessageType messageType = MessageType
					.getMessageTypeFromValue(messageItem.getType());
			switch (messageType) {
			case FILE:
				return MessageViewType.IMAGE_VIEW;
			default:// default is text view
				if(messageItem.getAction() != null){
					return MessageViewType.ACTION_VIEW;
				}else{
				    return MessageViewType.TEXT_VIEW;
				}
			}
		} else {
			return hint == null ? MessageViewType.EMPTY_VIEW
					: MessageViewType.HINT_VIEW;
		}
	}

	private void append(SpannableStringBuilder builder, CharSequence text,
			CharacterStyle span) {
		int start = builder.length();
		builder.append(text);
		builder.setSpan(span, start, start + text.length(),
				Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
	}

	// added by navrattan : 4 july 2013
	private ViewHolder createCorrectViewHolder(int type, View convertView) {
		ViewHolder holder = new ViewHolder();
		switch (type) {
		case MessageViewType.TEXT_VIEW:
		//	holder.dateTextView = (TextView) convertView.findViewById(R.id.datetextview);
			holder.txtSenderText = (TextView) convertView.findViewById(R.id.message_sender_text);
		//	holder.senFileTimeTextView = (TextView) convertView.findViewById(R.id.message_sender_time);
			holder.txtReceiverText = (TextView) convertView.findViewById(R.id.reciever_message_sender_text);
		//	holder.recFileTimeTextView = (TextView) convertView.findViewById(R.id.reciever_message_sender_time);
			//holder.imageReceiver = (ImageView) convertView.findViewById(R.id.receiver_image);
			//holder.messageReceiveStatusLayout = (LinearLayout) convertView.findViewById(R.id.receiver_read_time);
			holder.senderLayout = (LinearLayout) convertView.findViewById(R.id.sendermessagelayout);
			holder.receiverLayout = (LinearLayout) convertView.findViewById(R.id.rec_mzs_layout);
			holder.messageSentStatusIcon = (ImageView) convertView.findViewById(R.id.sent_status_icon);
			holder.messageDeliveredStatusIcon = (ImageView) convertView.findViewById(R.id.delivered_status_icon);
			holder.messageReadStatusIcon = (ImageView) convertView.findViewById(R.id.read_status_icon);
			holder.receiverName = (TextView) convertView.findViewById(R.id.receiver_name);
			holder.recLocIcon = (ImageView) convertView.findViewById(R.id.receiver_location_icon);
			holder.recLocMszHeading = (TextView) convertView.findViewById(R.id.receiver_loctaion);
			holder.senLocIcon = (ImageView) convertView.findViewById(R.id.sender_location_icon);
			holder.senLocMszHeading = (TextView) convertView.findViewById(R.id.sender_loctaion);
			holder.senderLayout.setVisibility(View.GONE);
			holder.receiverLayout.setVisibility(View.GONE);
			break;
		case MessageViewType.IMAGE_VIEW:
			holder.receiveFileLayout = (RelativeLayout) convertView.findViewById(R.id.rec_image_mzs_layout);
			holder.recFileThumbnailImageView = (ImageView) convertView.findViewById(R.id.rec_image_thumb);
		//	holder.recFileTimeTextView = (TextView) convertView.findViewById(R.id.receive_image_time);
			holder.recFileDownloadImageView = (ImageView) convertView.findViewById(R.id.recieve_image_download);
			//holder.imageReceiver = (ImageView) convertView.findViewById(R.id.receiver_image);
			holder.recFileProgressBarLayout = (LinearLayout) convertView.findViewById(R.id.receiver_progress_bar);
			holder.sendFileLayout = (RelativeLayout) convertView.findViewById(R.id.sen_image_mzs_layout);
			holder.senFileThumbnailImageView = (ImageView) convertView.findViewById(R.id.sen_image_thumb);
			holder.senFileRetryUploadImageView = (ImageView) convertView.findViewById(R.id.sen_image_retry);
		//	holder.senFileTimeTextView = (TextView) convertView.findViewById(R.id.send_image_time);
			holder.senFileProgressBarLayout = (LinearLayout) convertView.findViewById(R.id.sender_progress_bar);
			holder.messageSentStatusIcon = (ImageView) convertView.findViewById(R.id.sent_status_icon);
			holder.messageDeliveredStatusIcon = (ImageView) convertView.findViewById(R.id.delivered_status_icon);
			holder.messageReadStatusIcon = (ImageView) convertView.findViewById(R.id.read_status_icon);
			holder.sendFileLayout.setVisibility(View.GONE);
			holder.receiveFileLayout.setVisibility(View.GONE);
			holder.senfileImageTypeIconImageView = (ImageView) convertView.findViewById(R.id.send_image_icon);
			holder.senfileVideoTypeIconImageView = (ImageView) convertView.findViewById(R.id.send_video_icon);
			holder.recFileThumbnailImageView.setLongClickable(true);
			holder.senFileThumbnailImageView.setLongClickable(true);
			break;
			case MessageViewType.ACTION_VIEW:
		//	holder.dateTextView = (TextView) convertView.findViewById(R.id.datetextview);
			holder.txtReceiverText = (TextView) convertView.findViewById(R.id.reciever_message_sender_text);
		//	holder.recFileTimeTextView = (TextView) convertView.findViewById(R.id.reciever_message_sender_time);
			//holder.messageReceiveStatusLayout = (LinearLayout) convertView.findViewById(R.id.receiver_read_time);
			holder.receiverActionLayout = (RelativeLayout) convertView.findViewById(R.id.rec_mzs_layout);
			holder.receiverActionLayout.setVisibility(View.GONE);
			break;
		}
		return holder;
	}

	/**
	 * @to-do : need to add separate layout for image , audio, video, free call,
	 *        share location Return correct view based on message type :
	 * @param type
	 * @param convertView
	 * @return \
	 */
	private View createCorrectConvertView(int type, View convertView) {
		switch (type) {
		case MessageViewType.TEXT_VIEW:
		case MessageViewType.HINT_VIEW:
		case MessageViewType.EMPTY_VIEW:
			convertView = _inflater.inflate(R.layout.message_item, null);
			break;
		case MessageViewType.IMAGE_VIEW:
			convertView = _inflater.inflate(R.layout.chat_image_item, null);
			break;
		case MessageViewType.ACTION_VIEW:
			//convertView = _inflater.inflate(R.layout.chat_action_item, null);
			break;
		}
		return convertView;
	}

	private ViewHolder createTextMessageView(int type, ViewHolder holder,
			AbstractMessageItem messageItem, boolean incoming, String name) {
		final AbstractContact abstractContact = RosterManager.getInstance()
				.getBestContact(account, user);
		if (incoming) {
			holder.receiverLayout.setVisibility(View.VISIBLE);
			//holder.messageReceiveStatusLayout.setVisibility(View.VISIBLE);
//			holder.imageReceiver.setVisibility(View.VISIBLE);
			holder.senderLayout.setVisibility(View.GONE);
			/*String lres = HttpAvatarManager.getInstance().getAvatarLRES(
					abstractContact.getUser());
			if (lres != null) {
				UniversalImageLoaderUtil.loadImage(lres, holder.imageReceiver, null, 40,
						40);
				holder.imageReceiver.setPadding(1, 1, 1, 1);
			} else {
				holder.imageReceiver.setImageResource(UniversalImageLoaderUtil
						.getDefaultAvaterId(activity));
			}*/
			if(isMUC){
			   holder.receiverName.setText(name.toUpperCase());
			}
		} else {
			holder.receiverLayout.setVisibility(View.GONE);
			holder.senderLayout.setVisibility(View.VISIBLE);
			// holder.imageReceiver.setVisibility(View.GONE);
			name = AccountManager.getInstance().getNickName(account);
		}
		Spannable text = messageItem.getSpannable();
		ChatAction action = messageItem.getAction();
		/*String time = StringUtils.getSmartTimeText(messageItem.getTimestamp());*/
		SpannableStringBuilder builder = new SpannableStringBuilder();
		if (action == null) {
//			if (!incoming) {
//				setSentAndDeliveredStatusIcon(holder, messageItem);
//			}
			/*Date timeStamp = messageItem.getDelayTimestamp();
			if (timeStamp != null) {
				String delay = activity.getString(
						incoming ? R.string.chat_delay : R.string.chat_typed,
						StringUtil.getSmartTimeText24(timeStamp));
				append(builder, delay, new TextAppearanceSpan(activity,
						R.style.ChatHeader_Delay));
				append(builder, divider, new TextAppearanceSpan(activity,
						R.style.ChatHeader));
			}*/
			if (messageItem.isUnencypted()) {
				append(builder,
						activity.getString(R.string.otr_unencrypted_message),
						new TextAppearanceSpan(activity,
								R.style.ChatHeader_Delay));
				append(builder, divider, new TextAppearanceSpan(activity,
						R.style.ChatHeader));
			}
			Emoticons.getSmiledText(activity.getApplication(), text);
			if (messageItem.getTag() == null)
				builder.append(text);
			else
				append(builder, text, new TextAppearanceSpan(activity,
						R.style.ChatRead));
		} else {
			/*append(builder, time, new TextAppearanceSpan(activity,
					R.style.ChatHeader_Time));*/
			append(builder, " ", new TextAppearanceSpan(activity,
					R.style.ChatHeader));
			text = Emoticons.newSpannable(action.getText(activity, name,
					text.toString()));
			Emoticons.getSmiledText(activity.getApplication(), text);
			append(builder, text, new TextAppearanceSpan(activity,
					R.style.ChatHeader_Delay));
		}

		if (incoming) {
			holder.txtReceiverText.setText(builder);
			//holder.recFileTimeTextView.setText(time);
			holder.txtReceiverText.setMovementMethod(LinkMovementMethod
					.getInstance());
/*			if (MessageType.isLocationMessage(messageItem.getType())) {
				final LocationMessage lMessage = (LocationMessage) messageItem;
				holder.recLocIcon.setVisibility(View.VISIBLE);
				holder.recLocMszHeading.setVisibility(View.VISIBLE);
				holder.receiverLayout.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						activity.startActivity(GoogleMapActivity
								.createOpenLocationIntent(activity,
										lMessage.getLat(), lMessage.getLng(),
										lMessage.getText()));
					}
				});
			} else {
				holder.recLocIcon.setVisibility(View.GONE);
				holder.recLocMszHeading.setVisibility(View.GONE);
			}*/

		} else {
			holder.txtSenderText.setText(builder);
			//holder.senFileTimeTextView.setText(time);
			holder.txtSenderText.setMovementMethod(LinkMovementMethod
					.getInstance());
		}
		return holder;
	}
	
		private ViewHolder createActionMessageView(int type, ViewHolder holder,
				AbstractMessageItem messageItem, boolean incoming, String name) {
			final AbstractContact abstractContact = RosterManager.getInstance()
					.getBestContact(account, user);
			holder.receiverActionLayout.setVisibility(View.VISIBLE);
			//holder.messageReceiveStatusLayout.setVisibility(View.VISIBLE);
			//String text = messageItem.getText();
			Spannable text = messageItem.getSpannable();
			ChatAction action = messageItem.getAction();
//			if( messageItem instanceof RoomActionMessage) {
//				if(MessageType.isRoomSubjectUpdate(messageItem.getType())) {
//					action = ChatAction.subject;
//				} else if(MessageType.isRoomAvatarUpdate(messageItem.getType())) {
//					action = ChatAction.avatar;
//				} else if(MessageType.isRoomOccupantsAdded(messageItem.getType())) {
//					action = ChatAction.action_invite;
//					text = Emoticons.newSpannable(RosterManager.getInstance().getName(text.toString()));
//				} else if(MessageType.isLeaveRoom(messageItem.getType())) {
//					action = ChatAction.leave;
//				}
//			}
			if(action == null ) {
				action = ChatAction.chat;
				Log.d(TAG, "Message type is = "+messageItem.getType()+name);
			}
			/*String time = StringUtils.getSmartTimeText(messageItem.getTimestamp());*/
			SpannableStringBuilder builder = new SpannableStringBuilder();
			text = Emoticons.newSpannable(action.getText(activity, name,EncodingDecodingUtil.decodeString(text.toString())));
			Emoticons.getSmiledText(activity.getApplication(), text);
			append(builder, text, new TextAppearanceSpan(activity, R.style.ChatHeader_Delay));
			//append(builder, time, new TextAppearanceSpan(activity,R.style.ChatHeader_Time));
			//append(builder, " ", new TextAppearanceSpan(activity,R.style.ChatHeader));
			//text =action.getText(activity, name,text);
			//Emoticons.getSmiledText(activity.getApplication(), text);
			///append(builder, text, new TextAppearanceSpan(activity, R.style.ChatHeader_Delay));
			//if (incoming) {
				holder.txtReceiverText.setText(builder);
				/*holder.recFileTimeTextView.setText(time);*/
				//holder.txtReceiverText.setMovementMethod(LinkMovementMethod.getInstance());
			//} 
			return holder;
		}

	/**
	 * @ TODO: Remove the extra code when message is coming or going message...
	 * 
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final int type = getItemViewType(position);
		final AbstractMessageItem messageItem = (AbstractMessageItem) getItem(position);

		ViewHolder holder;
		if (convertView == null) {
			convertView = createCorrectConvertView(type, convertView);
			holder = createCorrectViewHolder(type, convertView);
			if (type != MessageViewType.EMPTY_VIEW ) {
				convertView.setTag(holder);
			}
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		if (type == MessageViewType.HINT_VIEW) {
			holder.receiverLayout.setVisibility(View.VISIBLE);
			holder.senderLayout.setVisibility(View.GONE);
			/*holder.recFileTimeTextView.setVisibility(View.GONE);*/
			holder.txtReceiverText.setText(hint);
			convertView.setLongClickable(true);
			return convertView;
		}
		if (type == MessageViewType.EMPTY_VIEW) {
			return convertView;
		}
		String name = "";
		final String account = messageItem.getChat().getAccount();
		final String user = messageItem.getChat().getUser();
		final String resource = messageItem.getResource();
		final boolean incoming = messageItem.isIncoming();
		if (isMUC) {
			if (isSelf(resource)) {
				name = "You";//Application.getYouString();
			} else {
				name = RosterManager.getInstance().getName(account, resource);
			}
		} else {
			name = RosterManager.getInstance().getName(account, user);
		}
		// @to-do need to create for video , audio and location and call
		switch (type) {
		case MessageViewType.IMAGE_VIEW:
			final AbstractContact abstractContact = RosterManager.getInstance()
					.getBestContact(account, user);
			if (incoming) {
				createIncomingFileMessageView(type, holder, messageItem,
						abstractContact, name);
			} else {
				createOutgoingFileMessageView(type, holder, messageItem,
						abstractContact);
			}
			break;
		case MessageViewType.TEXT_VIEW:
//		case MessageViewType.LOCATION_VIEW:
			holder = createTextMessageView(type, holder, messageItem, incoming,
					name);
			break;
		case MessageViewType.ACTION_VIEW:
			holder = createActionMessageView(type, holder , messageItem, incoming, name);
			break;
		}

		convertView.setOnLongClickListener(new OnLongClickListener() {

			@Override
			public boolean onLongClick(View v) {
				activity.openContextMenu(v);
				return true;
			}
		});
		
//		convertView.setOnClickListener(new OnClickListener() {
//			
//			@Override
//			public void onClick(View v) {
//				moreLayout.setVisibility(View.GONE);
//			}
//		});
		return convertView;
	}

	static class ViewHolder {
		/*private TextView dateTextView;*/
		/*private TextView recFileTimeTextView;*/
		//private LinearLayout messageReceiveStatusLayout;
		/*private TextView senFileTimeTextView;*/
		private TextView txtSenderText;
		private TextView txtReceiverText;
		private LinearLayout senderLayout;
		private LinearLayout receiverLayout;
		private RelativeLayout receiverActionLayout;
		private RelativeLayout receiveFileLayout;
		private RelativeLayout sendFileLayout;
		private ImageView recFileDownloadImageView;
		private ImageView senFileRetryUploadImageView;
		private TextView receiveFileSizeTextView;
		private TextView senFileSizeTextView;
		private LinearLayout recFileProgressBarLayout;
		private LinearLayout senFileProgressBarLayout;
		private ImageView recFileThumbnailImageView;
		private ImageView senFileThumbnailImageView;
		private ImageView senfileImageTypeIconImageView;
		private ImageView recfileImageTypeIconImageView;
		private ImageView senfileVideoTypeIconImageView;
		private ImageView recfileVideoTypeIconImageView;

		private ImageView messageSentStatusIcon;
		private ImageView messageDeliveredStatusIcon;
		private ImageView messageReadStatusIcon;
		private TextView receiverName;
		private ImageView senLocIcon;
		private TextView senLocMszHeading;
		private ImageView recLocIcon;
		private TextView recLocMszHeading;
		private Button seeGift;
	}

	public String getAccount() {
		return account;
	}

	public String getUser() {
		return user;
	}

	/**
	 * Changes managed chat.
	 * 
	 * @param account
	 * @param user
	 */
	public void setChat(String account, String user) {
		this.account = account;
		this.user = user;
		this.isMUC = MUCManager.getInstance().hasRoom(account, user);
		onChange();
	}

	@Override
	public void onChange() {
		messages = new ArrayList<AbstractMessageItem>(MessageManager
				.getInstance().getMessages(account, user));
		hint = getHint();
		notifyDataSetChanged();
	}

	public List<AbstractMessageItem> addMessagesOnTop(
			ArrayList<AbstractMessageItem> loadMessages) {
		messages.addAll(loadMessages);
		return messages;

	}

/*	public void onLoadChat(Button loadMore) {
		MessageManager.getInstance().loadMore(account, user, getCount(), this,
				loadMore);
	}*/

	/**
	 * @return New hint.
	 */
	private String getHint() {
		/*AccountItem accountItem = AccountManager.getInstance().getAccount(
				account);
		boolean online;
		if (accountItem == null)
			online = false;
		else
			online = accountItem.getState().isConnected();
		final AbstractContact abstractContact = RosterManager.getInstance()
				.getBestContact(account, user);
		if (!online) {
			if (abstractContact instanceof RoomContact)
				return activity.getString(R.string.muc_is_unavailable);
			else
				return activity.getString(R.string.account_is_offline);
		} else if (!abstractContact.getStatusMode().isOnline()) {
			if (abstractContact instanceof RoomContact)
				return activity.getString(R.string.muc_is_unavailable);
			 else return activity.getString(R.string.contact_is_offline,
			  abstractContact.getName());
			 
		}*/
		return null;
	}

	/**
	 * Contact information has been changed. Renews hint and updates data if
	 * necessary.
	 */
	public void updateInfo() {
		String info = getHint();
		if (this.hint == info || (this.hint != null && this.hint.equals(info)))
			return;
		this.hint = info;
		notifyDataSetChanged();
	}

	private void createIncomingFileMessageView(final int type,
			final ViewHolder holder, final AbstractMessageItem messageItem,
			final AbstractContact abstractContact, String name) {
		if (!(messageItem instanceof FileMessageItem)) {
			return;
		}
		FileMessageItem fileMessage = (FileMessageItem) messageItem;
		holder.receiveFileLayout.setVisibility(View.VISIBLE);
		holder.sendFileLayout.setVisibility(View.GONE);
		/*String time = StringUtils.getSmartTimeText(fileMessage.getTimestamp());
		holder.recFileTimeTextView.setText(time);*/
		//holder.imageReceiver.setVisibility(View.VISIBLE);
		onImageReceive(holder, fileMessage);
	}
	
	
	private void onImageReceive(final ViewHolder holder,
			final FileMessageItem fileMessage) {
		final FileDownloadStatus fileDownloadStatus = FileDownloadStatus
				.getFileDownloadStatus(fileMessage.getDownloadStatus());
		switch (fileDownloadStatus) {
		case COMPLETE:
			setThumbnail(MessageViewType.IMAGE_VIEW, fileMessage.getFileUri(),
					holder.recFileThumbnailImageView);
			holder.recFileDownloadImageView.setVisibility(View.GONE);
			break;
		case ERROR:
		case PENDING:
			holder.recFileDownloadImageView.setVisibility(View.VISIBLE);
			UniversalImageLoaderUtil.loadImageWithDefaultImage(fileMessage.getLresURL(),
					holder.recFileThumbnailImageView,null, R.drawable.account_background);
			break;
		}
	}
	
	private void createOutgoingFileMessageView(final int type,
			final ViewHolder holder, final AbstractMessageItem messageItem,
			final AbstractContact abstractContact) {
		if (!(messageItem instanceof FileMessageItem)) {
			return;
		}
		final FileMessageItem fileMessage = (FileMessageItem) messageItem;
		holder.sendFileLayout.setVisibility(View.VISIBLE);
		holder.receiveFileLayout.setVisibility(View.GONE);
		/*String time = StringUtils.getSmartTimeText(fileMessage.getTimestamp());
		holder.senFileTimeTextView.setText(time);*/
			final FileDownloadStatus fileDownloadStatus = FileDownloadStatus
					.getFileDownloadStatus(fileMessage.getDownloadStatus());
			switch (fileDownloadStatus) {
			case COMPLETE:
				holder.senFileRetryUploadImageView.setOnClickListener(null);
				holder.senFileProgressBarLayout.setVisibility(View.GONE);
				holder.senFileRetryUploadImageView.setVisibility(View.GONE);
				break;
			case ERROR:
				// @to-do we may show error msz view
				holder.senFileProgressBarLayout.setVisibility(View.GONE);
				holder.senFileRetryUploadImageView.setVisibility(View.VISIBLE);
				holder.senFileRetryUploadImageView
						.setOnClickListener(new OnClickListener() {
							@Override
							public void onClick(View v) {
								holder.senFileProgressBarLayout.setVisibility(View.VISIBLE);
								holder.senFileRetryUploadImageView.setVisibility(View.GONE);
								fileMessage.setDownloadStatus(FileDownloadStatus.PENDING
												.getValue());
								/*MessageManager.getInstance().reSendMessage(
										account, user, fileMessage);*/
							}
						});
				break;
			case PENDING:
				holder.senFileRetryUploadImageView.setOnClickListener(null);
				holder.senFileProgressBarLayout.setVisibility(View.VISIBLE);
				holder.senFileRetryUploadImageView.setVisibility(View.GONE);
				break;
		}
		onSendImage(holder, fileMessage);
	}

	private void onSendImage(ViewHolder holder, final FileMessageItem fileMessage) {
		holder.senfileImageTypeIconImageView.setVisibility(View.VISIBLE);
		setThumbnail(MessageViewType.IMAGE_VIEW, fileMessage.getFileUri(),
				holder.senFileThumbnailImageView);
	}

	/**
	 * This method return thumbnail bitmap : if Image type then get from path if
	 * video type then create from video
	 * 
	 * @param messageViewType
	 * @param path
	 * @return
	 */
	private void setThumbnail(int messageViewType, String filePath,
			ImageView imageView) {
		if (imageView != null) {
			if (mImageLoader == null) {
				mImageLoader = CZImageLoader.getInstance(activity);
			}
			mImageLoader.display(filePath, imageView, messageViewType);
		}
	}


	private boolean isSelf(String resource) {
		return Jid.getName(account).equalsIgnoreCase(Jid.getName(resource));
	}

}
