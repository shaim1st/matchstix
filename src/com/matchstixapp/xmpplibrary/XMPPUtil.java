package com.matchstixapp.xmpplibrary;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.matchstixapp.db.DataController;
import com.xabber.android.data.Application;
import com.xabber.android.data.NetworkException;
import com.xabber.android.data.account.AccountItem;
import com.xabber.android.data.account.AccountManager;
import com.xabber.android.data.account.AccountType;
import com.xabber.android.data.intent.AccountIntentBuilder;

public class XMPPUtil {

	public void doXMPPLogin(Activity activity, AccountType accountType) {
		String oldUserName = DataController.getInstance().getFullJid();
		/*String userName = StringUtil.isEmpty(oldUserName) ? String
				.valueOf(DataController.getInstance().getUser().getId())
				: oldUserName;*/
		String userName = String
				.valueOf(DataController.getInstance().getUser().getId());
		String password = DataController.getInstance().getUser().getPassword();
		String account;
		try {
			account = AccountManager.getInstance().addAccount(userName,
					password, accountType, true, true, false);
			DataController.getInstance().setFullJid(account);
		} catch (NetworkException e) {
			Application.getInstance().onError(e);
			return;
		}
		activity.setResult(activity.RESULT_OK,
				createAuthenticatorResult(activity, account));
	}

	private Intent createAuthenticatorResult(Context context, String account) {
		return new AccountIntentBuilder(null, null).setAccount(account).build();
	}

	public void enableAccount(AccountItem accountItem) {
		AccountManager.getInstance().updateAccount(accountItem.getAccount(),
				accountItem.getConnectionSettings().isCustom(),
				accountItem.getConnectionSettings().getHost(),
				accountItem.getConnectionSettings().getPort(),
				accountItem.getConnectionSettings().getServerName(),
				accountItem.getConnectionSettings().getUserName(),
				accountItem.isStorePassword(),
				accountItem.getConnectionSettings().getPassword(),
				accountItem.getConnectionSettings().getResource(),
				accountItem.getPriority(), true,
				accountItem.getConnectionSettings().isSaslEnabled(),
				accountItem.getConnectionSettings().getTlsMode(),
				accountItem.getConnectionSettings().useCompression(),
				accountItem.getConnectionSettings().getProxyType(),
				accountItem.getConnectionSettings().getProxyHost(),
				accountItem.getConnectionSettings().getProxyPort(),
				accountItem.getConnectionSettings().getProxyUser(),
				accountItem.getConnectionSettings().getProxyPassword(), true,
				accountItem.getArchiveMode());
	}
}
