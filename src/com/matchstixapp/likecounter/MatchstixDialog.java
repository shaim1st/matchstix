package com.matchstixapp.likecounter;

import com.matchstixapp.R;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

/**
 * Created by Atiqul Alam on 12/10/15.
 */
public class MatchstixDialog {
    @SuppressWarnings("deprecation")
	public MatchstixDialog(final Context context, String countTime){
        // custom dialog
        final Dialog dialog = new Dialog(context,R.style.Theme_Dialog1);
        TextView backText;
		final TextView countText;
        CircularProgressBar c4;

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);

        dialog.setContentView(R.layout.dialog);
//        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        countText     =(TextView)dialog.findViewById(R.id.count_time);
        backText     =(TextView)dialog.findViewById(R.id.back);

        c4 = (CircularProgressBar) dialog.findViewById(R.id.circularprogressbar4);
        c4.setProgress(1);

        c4.animateProgressTo(0, 100, new CircularProgressBar.ProgressAnimationListener() {

            @Override
            public void onAnimationStart() {
            }

            @Override
            public void onAnimationProgress(int progress) {
                countText.setText(""+progress);
            }

            @Override
            public void onAnimationFinish() {
                //c4.setSubTitle("done");
            }
        });


        // if button is clicked, close the custom dialog
        backText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }
}
