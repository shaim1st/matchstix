package com.matchstixapp.PNV;

import java.util.ArrayList;
import java.util.Locale;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.matchstixapp.R;
import com.matchstixapp.modal.Location;

public class CountryAdapter extends ArrayAdapter<Location> {

	private ArrayList<Location> savedList = new ArrayList<Location>();
	private ArrayList<Location> populateList = new ArrayList<Location>();

	public CountryAdapter(Context context, int textViewResourceId) {
		this(context, textViewResourceId, new ArrayList<Location>());
	}

	public CountryAdapter(Context context, int textViewResourceId,
			ArrayList<Location> entries) {
		super(context, textViewResourceId, entries);

		populateList = entries;

		for (int i = 0; i < entries.size(); i++) {
			savedList.add(entries.get(i));
		}
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			convertView = LayoutInflater.from(getContext()).inflate(
					R.layout.country_list_item, null);
			holder = new ViewHolder();
			holder.countryName = (TextView)convertView.findViewById(R.id.country_name);
			holder.countryCode = (TextView)convertView.findViewById(R.id.country_code);
			convertView.setTag(holder);
		}else{
			holder = (ViewHolder) convertView.getTag();
		}
		
		holder.countryName.setText(populateList.get(position).getName());
		holder.countryCode.setText("+"+populateList.get(position).getPhoneCode());

		return convertView;
	}

	@Override
	public void add(Location object) {
		populateList.add(object);
		savedList.add(object);
	}

	public void addAll(ArrayList<Location> locations) {
		for (Location location : locations) {
			add(location);
		}
	}

	@Override
	public void clear() {
		populateList.clear();
		savedList.clear();

		notifyDataSetChanged();
	}

	@Override
	public Filter getFilter() {
		return new TextFilter();
	}

	/**
	 * Filter class to filter the result according to need.
	 * 
	 */
	public class TextFilter extends Filter {

		@Override
		protected FilterResults performFiltering(CharSequence constraint) {
			populateList.clear();

			String filter = constraint.toString().toLowerCase(
					Locale.getDefault());

			int count = 0;
			for (int i = 0; i < savedList.size(); i++) {
				Location location = savedList.get(i);

				if (location.getName().toLowerCase(Locale.getDefault())
						.startsWith(filter)) {
					populateList.add(location);

					count++;
				}
			}

			FilterResults results = new FilterResults();
			results.count = count;
			results.values = populateList;

			return results;
		}

		protected void publishResults(CharSequence constraint,
				FilterResults results) {
			notifyDataSetChanged();
		}
	}

	public class ViewHolder {
		
		public TextView countryName;
		public TextView countryCode;
		
	}
}