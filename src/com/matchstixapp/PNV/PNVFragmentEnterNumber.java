package com.matchstixapp.PNV;

import static com.matchstixapp.utils.Validator.isValidNumber;

import java.io.Serializable;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.matchstixapp.MatchstixApplication;
import com.matchstixapp.R;
import com.matchstixapp.db.DataController;
import com.matchstixapp.listener.Keys;
import com.matchstixapp.listener.MatchstixDialogListener;
import com.matchstixapp.listener.PINSendListener;
import com.matchstixapp.modal.Location;
import com.matchstixapp.pojo.JSONNumber;
import com.matchstixapp.requester.SendPINRequester;
import com.matchstixapp.ui.activity.BoardingActivity;
import com.matchstixapp.ui.activity.DashBoardActivity;
import com.matchstixapp.ui.fragments.BaseFragment;
import com.matchstixapp.utils.BackgroundExecutor;
import com.matchstixapp.utils.Constants;
import com.matchstixapp.utils.TypeFaceTextView;
import com.matchstixapp.utils.Utils;
import com.matchstixapp.xmpplibrary.XMPPUtil;
import com.matchstixapp.xmpplibrary.ui.adapter.AccountTypeAdapter;
import com.xabber.android.data.account.AccountManager;
import com.xabber.android.data.account.AccountType;

public class PNVFragmentEnterNumber extends BaseFragment implements
		TextWatcher, OnClickListener, PINSendListener {
	public static final String TAG = PNVFragmentEnterNumber.class
			.getSimpleName();
	
	
	private View mRootView;
	private EditText mPhoneNo;
	private ImageView mCountryFlag;
	private Button mSend;
	private Location selectedLocation;

	private TypeFaceTextView tvTitle;
	//private TypeFaceTextView tvDesc1;
	private TypeFaceTextView tvDesc2;
	private int inputNumberCount;

	private Spinner accountTypeView;

	public static PNVFragmentEnterNumber newInstance() {
		return new PNVFragmentEnterNumber();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mRootView = inflater.inflate(R.layout.fragment_pnv_enter_ph_no,
				container, false);
		mPhoneNo = (EditText) mRootView.findViewById(R.id.id_phone_no_et);
		mCountryFlag = (ImageView) mRootView
				.findViewById(R.id.id_country_flag_iv);
		mSend = (Button) mRootView.findViewById(R.id.id_send_btn);

		tvTitle = (TypeFaceTextView) mRootView.findViewById(R.id.tv_title);
		/*tvDesc1 = (TypeFaceTextView) mRootView
				.findViewById(R.id.textView_sentpin);*/
		tvDesc2 = (TypeFaceTextView) mRootView
				.findViewById(R.id.free_of_charge_textview);

		boolean editMode = getActivity().getIntent().getBooleanExtra(
				Keys.KEY_MODE_EDIT, false);
		if (editMode) {
			tvTitle.setText(getResources().getString(R.string.enter_new_num));
			/*tvDesc1.setVisibility(View.INVISIBLE);*/
			SpannableString spanString = new SpannableString(getResources()
					.getString(R.string.skip));
			spanString.setSpan(new UnderlineSpan(), 0, spanString.length(), 0);
			tvDesc2.setText(spanString);
			tvDesc2.setOnClickListener(this);
			mRootView.findViewById(R.id.num_not_public_txtview).setVisibility(View.INVISIBLE);
		}
		if (selectedLocation == null) {
			selectedLocation = LocationDAO.getInstance(getActivity())
					.getLocation("kh");
			mPhoneNo.setText("");
		}
		setCountryCode(selectedLocation);
		accountTypeView = (Spinner) mRootView.findViewById(R.id.account_type);
		accountTypeView.setAdapter(new AccountTypeAdapter(getActivity()));
		accountTypeView.setSelection(0);
		return mRootView;
	}

	@Override
	public void onResume() {
		super.onResume();
		dismissProgressDialog();
		MatchstixApplication.getInstance().addUIListener(PINSendListener.class,
				this);
		if (getActivity().getIntent()
				.getBooleanExtra(Keys.KEY_MODE_EDIT, false)) {
			mPhoneNo.setText("");
			mPhoneNo.requestFocus();
		}
		mPhoneNo.addTextChangedListener(this);
		mCountryFlag.setOnClickListener(this);
		mSend.setOnClickListener(this);
	}

	@Override
	public void onPause() {
		super.onPause();
		MatchstixApplication.getInstance().removeUIListener(
				PINSendListener.class, this);
	}

	private BroadcastReceiver mUpdateUIReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			if (intent.getAction().equals(Constants.RECEIVER_COUNTRY_SELECTION)) {
				Serializable obj = intent
						.getSerializableExtra(Constants.DATA_COUNTRY_SELECTION);
				if (obj != null && obj instanceof Location) {
					setCountryCode((Location) obj);
				}
				LocalBroadcastManager.getInstance(getActivity())
						.unregisterReceiver(mUpdateUIReceiver);
			}
		}
	};

	private void setCountryCode(Location location) {
		Resources resources = getResources();
		if (location == null) {
			return;
		}
		selectedLocation = location;
		int id = resources.getIdentifier(location.getTwoLetterCode()
				.toLowerCase(), "drawable", getActivity().getPackageName());
		if (id == 0) {
			id = resources.getIdentifier("au", "drawable", getActivity()
					.getPackageName());
		}
		mCountryFlag.setImageResource(id);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.id_country_flag_iv:
			LocalBroadcastManager.getInstance(getActivity()).registerReceiver(
					mUpdateUIReceiver,
					new IntentFilter(Constants.RECEIVER_COUNTRY_SELECTION));
			Intent cntryIntent = new Intent(getActivity(),
					CountrySelectionActivity.class);
			startActivity(cntryIntent);
			break;

		case R.id.id_send_btn:
			if(mPhoneNo.getText().toString().startsWith("0")) {
				sendVerificationCode(mPhoneNo.getText().toString().substring(1));
				return;
			}
			sendVerificationCode(mPhoneNo.getText().toString());
			break;

		case R.id.free_of_charge_textview:
			Utils.showFadeinAnimation(getActivity(), tvDesc2);
			DataController.getInstance().getUser().setNumberVerified(true);
			DataController.getInstance().saveUser();
			if(AccountManager.getInstance().getAccounts().size() > 0){
				String account = DataController.getInstance().getFullJid();
					new XMPPUtil().enableAccount(AccountManager.getInstance().getAccount(account));
			}else{
				new XMPPUtil().doXMPPLogin(getActivity(),
						(AccountType) accountTypeView.getSelectedItem());
			}
			startActivity(new Intent(getActivity(), DashBoardActivity.class));
			getActivity().finish();
			break;
		}
	}

	private ProgressDialog progressDialog;

	private void dismissProgressDialog() {
		if (progressDialog != null && progressDialog.isShowing()) {
			progressDialog.dismiss();
		}
	}

	private void sendVerificationCode(String number) {
		JSONNumber request = new JSONNumber();
		request.phoneNumber = number;
		request.cc = selectedLocation.getPhoneCode();
		progressDialog = ProgressDialog.show(getActivity(), "", getResources()
				.getString(R.string.sending));
		BackgroundExecutor.getInstance().execute(
				new SendPINRequester(request));
	}

	@Override
	public void afterTextChanged(Editable text) {

	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {

	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {

		if (s.length() != 0 && !isValidNumber(s.toString())) {
			Toast.makeText(getActivity(),
					getResources().getString(R.string.error_not_a_number),
					Toast.LENGTH_SHORT).show();

		} else if (s.length() >= 7) {
			mSend.setEnabled(true);
			mSend.setAlpha(1f);
			return;
		}

		mSend.setEnabled(false);
		mSend.setAlpha(0.6f);
	}

	@Override
	public void onPINSendSuccess(final JSONNumber request) {
		getActivity().runOnUiThread(new Runnable() {

			@Override
			public void run() {
				dismissProgressDialog();
				FragmentManager fm = getActivity().getSupportFragmentManager();
				FragmentTransaction ft = fm.beginTransaction();
				ft.replace(R.id.fragment_container,
						PNVFragmentEnterPin.newInstance(request));
				ft.addToBackStack(PNVFragmentEnterPin.TAG);
				ft.commit();
			}
		});

	}

	@Override
	public void onPINSendFailed(final int code, final String msg) {
		getActivity().runOnUiThread(new Runnable() {

			@Override
			public void run() {
				dismissProgressDialog();
				if (code == 409) {
					// already registered

					Toast.makeText(
							getActivity(),
							getResources().getString(
									R.string.error_already_registered),
							Toast.LENGTH_SHORT).show();
				} else if (code == 403) {
					// invalid number
					inputNumberCount++;
					if(inputNumberCount == 2){
						showErrorDialog();
					}else{
					Toast.makeText(
							getActivity(),
							getResources().getString(
									R.string.error_invalid_number),
							Toast.LENGTH_SHORT).show();
					}
				} else if (code == 400) {
					inputNumberCount++;
					if(inputNumberCount == 2){
						showErrorDialog();
					}else{
					Toast.makeText(
							getActivity(),
							getResources().getString(
									R.string.error_invalid_number),
							Toast.LENGTH_SHORT).show();
					}
				} else if (code == -1) {
					Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT)
							.show();

				}
			}
		});
	}
	
	@Override
	public void onPINSendError(final int code) {
		getActivity().runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				dismissProgressDialog();
				String msg = "";
				switch (code) {
				case 114:
					   msg = getString(R.string.error_send_verification_code);
					break;
				case 121:
					 	msg = getString(R.string.block_number_try_again);
					break;
				case 128 :
					 msg = getString(R.string.delisted_number_try_again);
					break;
				case 901:
					 msg = getString(R.string.please_try_again);
					break;
				}
				//Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
				Utils.showDialog(getActivity(),
						"",
						msg,
						getString(R.string.skip),
						getString(R.string.cancel),
						new MatchstixDialogListener() {

							@Override
							public void onButtonTwoClick() {
								
							}

							@Override
							public void onButtonOneClick() {
								if(AccountManager.getInstance().getAccounts().size() > 0){
									String account = DataController.getInstance().getFullJid();
										new XMPPUtil().enableAccount(AccountManager.getInstance().getAccount(account));
								}else{
									new XMPPUtil().doXMPPLogin(getActivity(),
											(AccountType) accountTypeView.getSelectedItem());
								}
								startActivity(new Intent(getActivity(), DashBoardActivity.class));
								((Activity)getActivity()).finish();
							}
						});
			}
		});
	}
	
	private void showErrorDialog(){
		Utils.showDialog(getActivity(),"",
				getString(R.string.please_try_again),
				getString(R.string.skip),
				getString(R.string.cancel),
				new MatchstixDialogListener() {

					@Override
					public void onButtonTwoClick() {
						inputNumberCount = 0;
					}

					@Override
					public void onButtonOneClick() {
						if(AccountManager.getInstance().getAccounts().size() > 0){
							String account = DataController.getInstance().getFullJid();
								new XMPPUtil().enableAccount(AccountManager.getInstance().getAccount(account));
						}else{
							new XMPPUtil().doXMPPLogin(getActivity(),
									(AccountType) accountTypeView.getSelectedItem());
						}
						startActivity(new Intent(getActivity(), DashBoardActivity.class));
						((Activity)getActivity()).finish();
					}
				});
	}

	public void onUnauthorizedListener() {
		
	}
}
