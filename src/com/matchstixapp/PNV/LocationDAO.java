package com.matchstixapp.PNV;

import java.util.ArrayList;
import java.util.Locale;

import org.xmlpull.v1.XmlPullParser;

import com.matchstixapp.R;
import com.matchstixapp.db.StorageController;
import com.matchstixapp.enums.Tables;
import com.matchstixapp.modal.Location;
import com.matchstixapp.utils.Constants;

import android.content.Context;
import android.content.res.XmlResourceParser;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

public class LocationDAO {
	public static final String TAG = LocationDAO.class.getName();

	private SQLiteDatabase database;
	private StorageController dbHelper = null;
	private static LocationDAO _instance;
	private Context _context;

	private LocationDAO(Context context) {
		if (dbHelper == null) {
			_context = context;
			dbHelper = StorageController.getInstance(context);
		}
	}

	public static LocationDAO getInstance(Context context) {
		if (_instance == null) {
			_instance = new LocationDAO(context);
		}
		return _instance;
	}

	public boolean isOpen() {
		if (database != null) {
			return database.isOpen();
		}
		return false;
	}

	public void open() throws SQLException {
		if (dbHelper == null) {
			dbHelper = StorageController.getInstance(_context);
		} else {
			database = dbHelper.getWritableDatabase();
		}
	}

	public void close() {
		if (dbHelper != null) {
			dbHelper.close();
		}
	}

	public void insertCountryDataInDBFromXML() {
		ArrayList<Location> locations = getCountryFromXML();
		addCountriesInDB(locations);
	}

	private ArrayList<Location> getCountryFromXML() {
		ArrayList<Location> locations = new ArrayList<Location>();
		try {
			XmlResourceParser parser = _context.getResources().getXml(
					R.xml.countrycode);
			parser.next();
			int eventType = parser.getEventType();
			while (eventType != XmlPullParser.END_DOCUMENT) {
				switch (eventType) {
				case XmlPullParser.START_DOCUMENT:
					break;
				case XmlPullParser.START_TAG:
					if (parser.getName().equals(Constants.COUNTRY)) {
						String countryCode = parser.getAttributeValue(0);
						String phoneCode = parser.getAttributeValue(1);
						String name = parser.getAttributeValue(2);
						if (countryCode != null && phoneCode != null
								&& name != null) {
							locations.add(new Location(phoneCode, name,
									countryCode, ""));
						}
					}
					break;
				default:
					break;
				}
				eventType = parser.next();
			}
			parser.close();
		} catch (Exception e) {
			Log.e(TAG, "Unable toread country from  xml ", e);
		}
		return locations;
	}

	private void addCountriesInDB(ArrayList<Location> locations) {
		if (!isOpen()) {
			open();
		}
		try {
			if (database != null) {
				String sql = "INSERT INTO " + Tables.LOCATION.getLabel()
						+ " VALUES (?,?,?);";
				SQLiteStatement statement = database.compileStatement(sql);
				database.beginTransaction();
				for (Location location : locations) {
					statement.clearBindings();
					statement.bindString(1, location.getTwoLetterCode());
					statement.bindString(2, location.getPhoneCode());
					statement.bindString(3, location.getName());
					statement.execute();
				}
				database.setTransactionSuccessful();
				database.endTransaction();
			}
		} catch (Exception e) {
			Log.e(TAG, "Enable to Add Country :", e);
		}
		/*
		 * if (isOpen()) { close(); }
		 */
	}

	public String getPhoneCode(String countryCode) {
		if (!isOpen()) {
			open();
		}

		String result = null;
		try {
			if (database != null) {
				Cursor cursor = database.query(Tables.LOCATION.getLabel(),
						new String[] { Tables.Location.COL_MCC.getLabel() },
						Tables.Location.COL_CC.getLabel() + "=?",
						new String[] { countryCode }, null, null, null);
				if (cursor != null && cursor.moveToFirst()) {
					result = cursor.getString(0);
				}
				cursor.close();
				if (isOpen()) {
					close();
				}
			}
		} catch (SQLiteException e) {
			Log.e(TAG, "Unable to get data from database", e);
		} catch (SQLException e) {
			Log.e(TAG, "Unable to get data from database", e);
		} catch (Exception e) {
			Log.e(TAG, "Unable to get data from database", e);
		} finally {
			close();
		}

		return result;
	}

	public String getCountryCode(String phoneCode) {
		if (!isOpen()) {
			open();
		}
		String result = null;
		try {
			if (database != null) {
				Cursor cursor = database.query(Tables.LOCATION.getLabel(),
						new String[] { Tables.Location.COL_CC.getLabel() },
						Tables.Location.COL_MCC.getLabel() + "=?",
						new String[] { phoneCode }, null, null, null);
				if (cursor != null && cursor.moveToFirst()) {
					result = cursor.getString(0);
				}
				cursor.close();
				if (isOpen()) {
					close();
				}
			}
		} catch (SQLiteException e) {
			Log.e(TAG, "Unable to get data from database", e);
		} catch (SQLException e) {
			Log.e(TAG, "Unable to get data from database", e);
		} catch (Exception e) {
			Log.e(TAG, "Unable to get data from database", e);
		} finally {
			close();
		}

		return result;
	}

	public Location getLocation(String countryCode) {
		if (!isOpen()) {
			open();
		}

		Location location = null;
		try {
			if (database != null) {
				String query = "SELECT * FROM " + Tables.LOCATION.getLabel()
						+ " WHERE  " + Tables.Location.COL_CC.getLabel() + "='"
						+ countryCode + "' COLLATE NOCASE";
				Cursor cursor = database.rawQuery(query, null);

				if (cursor != null && cursor.moveToFirst()) {
					location = new Location();

					location.setName(cursor.getString(cursor
							.getColumnIndex(Tables.Location.COL_NAME.getLabel())));
					location.setPhoneCode(cursor.getString(cursor
							.getColumnIndex(Tables.Location.COL_MCC.getLabel())));
					location.setTwoLetterCode(cursor.getString(
							cursor.getColumnIndex(Tables.Location.COL_CC
									.getLabel())).toUpperCase(
							Locale.getDefault()));
				}

				cursor.close();
			}
		} catch (SQLiteException e) {
			Log.e(TAG, "Unable to get data from database", e);
		} catch (SQLException e) {
			Log.e(TAG, "Unable to get data from database", e);
		} catch (Exception e) {
			Log.e(TAG, "Unable to get data from database", e);
		}

		if (isOpen()) {
			close();
		}

		return location;
	}
	public Location getLocationByMCC(String phoneCode) {
		if (!isOpen()) {
			open();
		}

		Location location = null;
		try {
			if (database != null) {
				String query = "SELECT * FROM " + Tables.LOCATION.getLabel()
						+ " WHERE  " + Tables.Location.COL_MCC.getLabel() + "='"
						+ phoneCode + "' COLLATE NOCASE";
				Cursor cursor = database.rawQuery(query, null);

				if (cursor != null && cursor.moveToFirst()) {
					location = new Location();

					location.setName(cursor.getString(cursor
							.getColumnIndex(Tables.Location.COL_NAME.getLabel())));
					location.setPhoneCode(cursor.getString(cursor
							.getColumnIndex(Tables.Location.COL_MCC.getLabel())));
					location.setTwoLetterCode(cursor.getString(
							cursor.getColumnIndex(Tables.Location.COL_CC
									.getLabel())).toUpperCase(
							Locale.getDefault()));
				}

				cursor.close();
			}
		} catch (SQLiteException e) {
			Log.e(TAG, "Unable to get data from database", e);
		} catch (SQLException e) {
			Log.e(TAG, "Unable to get data from database", e);
		} catch (Exception e) {
			Log.e(TAG, "Unable to get data from database", e);
		}

		if (isOpen()) {
			close();
		}

		return location;
	}

	public boolean isCountryListExist() {
		int count = 0;
		try {
			if (!isOpen()) {
				open();
			}
			String sqlquery = " SELECT count( "
					+ Tables.Location.COL_CC.getLabel() + ")" + " FROM "
					+ Tables.LOCATION.getLabel();
			if (database != null) {
				Cursor cursor = database.rawQuery(sqlquery, null);
				if (cursor != null && cursor.moveToFirst()) {
					count = cursor.getInt(0);
				}
				if (cursor != null)
					cursor.close();
				if (isOpen()) {
					close();
				}
			}
		} catch (SQLiteException e) {
			Log.e(TAG, "Unable to check is contry list exist", e);
		} catch (SQLException e) {
			Log.e(TAG, "Unable to check is contry list exist", e);
		} catch (Exception e) {
			Log.e(TAG, "Unable to check is contry list exist", e);
		}
		return count > 0 ? true : false;
	}

	public ArrayList<Location> getAllLocation() {
		if (!isOpen()) {
			open();
		}

		ArrayList<Location> list = new ArrayList<Location>();
		try {
			if (database != null) {
				Cursor cursor = database.query(Tables.LOCATION.getLabel(),
						null, null, null, null, null, null);
				if (cursor != null) {
					while (cursor.moveToNext()) {
						Location location = new Location();
						location.setName(cursor.getString(cursor
								.getColumnIndex(Tables.Location.COL_NAME
										.getLabel())));
						location.setPhoneCode(cursor.getString(cursor
								.getColumnIndex(Tables.Location.COL_MCC
										.getLabel())));
						location.setTwoLetterCode(cursor.getString(
								cursor.getColumnIndex(Tables.Location.COL_CC
										.getLabel())).toUpperCase(
								Locale.getDefault()));
						list.add(location);
					}
					cursor.close();
				}
			}
		} catch (SQLiteException e) {
			Log.e(TAG, "Unable to get data from database", e);
		} catch (SQLException e) {
			Log.e(TAG, "Unable to get data from database", e);
		} catch (Exception e) {
			Log.e(TAG, "Unable to get data from database", e);
		}
		if (isOpen()) {
			close();
		}
		return list;
	}

}