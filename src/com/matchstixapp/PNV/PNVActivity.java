package com.matchstixapp.PNV;

import java.io.File;
import java.io.FileInputStream;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.matchstixapp.GAScreen;
import com.matchstixapp.MatchstixApplication;
import com.matchstixapp.R;
import com.matchstixapp.db.DataController;
import com.matchstixapp.language.LanguageSelection;
import com.matchstixapp.language.LanguageSelectionDefault;
import com.matchstixapp.logger.Logger;
import com.matchstixapp.modal.User;
import com.matchstixapp.ui.activity.BaseFragmentActivity;
import com.matchstixapp.ui.activity.BoardingActivity;
import com.matchstixapp.ui.activity.DashBoardActivity;
import com.matchstixapp.ui.activity.SplashActivity;
import com.matchstixapp.ui.fragments.SettingsFragment;
import com.matchstixapp.utils.Constants;
import com.matchstixapp.utils.FileUtil.ImageDownloadListener;
import com.matchstixapp.xmpplibrary.ui.OAuthActivity;
import com.matchstixapp.xmpplibrary.ui.adapter.AccountTypeAdapter;
import com.xabber.android.data.Application;
import com.xabber.android.data.NetworkException;
import com.xabber.android.data.account.AccountManager;
import com.xabber.android.data.account.AccountType;
import com.xabber.android.data.intent.AccountIntentBuilder;

public class PNVActivity extends BaseFragmentActivity implements ImageDownloadListener,OnClickListener, OnItemSelectedListener{

	
	private static final String TAG = PNVActivity.class.getSimpleName();
	
	private static final String SAVED_ACCOUNT_TYPE = "com.xabber.android.ui.AccountAdd.ACCOUNT_TYPE";
	private static final int OAUTH_WML_REQUEST_CODE = 1;

	private ImageView ivBg; 
	private Spinner accountTypeView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		MatchstixApplication.tracker.setScreenName(GAScreen.VERIFY_MSISDN);
		setContentView(R.layout.activity_pnv);
		ivBg= (ImageView) findViewById(R.id.id_action_bar_left_img_view);
		User user = DataController.getInstance().getUser();
		String url=user.getAvatarURL();
		//FileUtil.download(this, url);
		addLanguageFragment();
		//addFragment();
		accountTypeView = (Spinner) findViewById(R.id.account_type);
		accountTypeView.setAdapter(new AccountTypeAdapter(this));
		accountTypeView.setOnItemSelectedListener(this);
		accountTypeView.setSelection(0);
		String accountType;
		if (savedInstanceState == null)
			accountType = null;
		else
			accountType = savedInstanceState.getString(SAVED_ACCOUNT_TYPE);
		accountTypeView.setSelection(0);
		for (int position = 0; position < accountTypeView.getCount(); position++)
			if (((AccountType) accountTypeView.getItemAtPosition(position))
					.getName().equals(accountType)) {
				accountTypeView.setSelection(position);
				break;
			}
	}
	@Override
	protected void onResume() {
		ivBg.setOnClickListener(this);
		super.onResume();
	}
	private void addFragment() {
		FragmentManager fm = getSupportFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();
		ft.replace(R.id.fragment_container, PNVFragmentEnterNumber.newInstance());
		ft.commit();
	}

	private void addLanguageFragment() {
		FragmentManager fm = getSupportFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();
		ft.replace(R.id.fragment_container, LanguageSelectionDefault.newInstance()).addToBackStack("LanguageSelectionDefault");
		ft.commit();
	}
	
	@Override
	public void onDownloadingFinished(final File file, String url) {
		if (file!=null) {
			new Thread(new Runnable() {
				
				@Override
				public void run() {
					try {
						Bitmap pic = BitmapFactory.decodeStream(new FileInputStream(file));
						final Bitmap blur = fastblur(pic, 5);
						pic=null;
						runOnUiThread(new Runnable() {
							@Override
							public void run() {
								ivBg.setImageBitmap(blur);
							}
						});
					} catch (Exception e) {
						Logger.d(TAG, "ERROR: decoding bitmap: "+e);
					}
				}
			}).start();
		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == Constants.REQ_LANGUAGE_SELECTION
				&& resultCode == Constants.RES_LANGUAGE_SELECTION) {
			//addFragment();
			
		}
		if (requestCode == OAUTH_WML_REQUEST_CODE) {
			if (resultCode == RESULT_OK && !OAuthActivity.isInvalidated(data)) {
				String token = OAuthActivity.getToken(data);
				if (token == null) {
					Application.getInstance().onError(
							R.string.AUTHENTICATION_FAILED);
				} else {
					String account;
					try {
						account = AccountManager.getInstance()
								.addAccount(
										null,
										token,
										(AccountType) accountTypeView
												.getSelectedItem(),
										true,
										true,
										false);
					} catch (NetworkException e) {
						Application.getInstance().onError(e);
						return;
					}
					setResult(RESULT_OK,
							createAuthenticatorResult(this, account));
					finish();
				}
			}
		}
	}
	
	
	private static Intent createAuthenticatorResult(Context context,
			String account) {
		return new AccountIntentBuilder(null, null).setAccount(account).build();
	}

	public static String getAuthenticatorResultAccount(Intent intent) {
		return AccountIntentBuilder.getAccount(intent);
	}

	Bitmap fastblur(Bitmap sentBitmap, int radius) {

		Bitmap bitmap = sentBitmap.copy(sentBitmap.getConfig(), true);

		if (radius < 1) {
			return (null);
		}

		int w = bitmap.getWidth();
		int h = bitmap.getHeight();

		int[] pix = new int[w * h];
		Log.e("pix", w + " " + h + " " + pix.length);
		bitmap.getPixels(pix, 0, w, 0, 0, w, h);

		int wm = w - 1;
		int hm = h - 1;
		int wh = w * h;
		int div = radius + radius + 1;

		int r[] = new int[wh];
		int g[] = new int[wh];
		int b[] = new int[wh];
		int rsum, gsum, bsum, x, y, i, p, yp, yi, yw;
		int vmin[] = new int[Math.max(w, h)];

		int divsum = (div + 1) >> 1;
		divsum *= divsum;
		int dv[] = new int[256 * divsum];
		for (i = 0; i < 256 * divsum; i++) {
			dv[i] = (i / divsum);
		}

		yw = yi = 0;

		int[][] stack = new int[div][3];
		int stackpointer;
		int stackstart;
		int[] sir;
		int rbs;
		int r1 = radius + 1;
		int routsum, goutsum, boutsum;
		int rinsum, ginsum, binsum;

		for (y = 0; y < h; y++) {
			rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
			for (i = -radius; i <= radius; i++) {
				p = pix[yi + Math.min(wm, Math.max(i, 0))];
				sir = stack[i + radius];
				sir[0] = (p & 0xff0000) >> 16;
				sir[1] = (p & 0x00ff00) >> 8;
				sir[2] = (p & 0x0000ff);
				rbs = r1 - Math.abs(i);
				rsum += sir[0] * rbs;
				gsum += sir[1] * rbs;
				bsum += sir[2] * rbs;
				if (i > 0) {
					rinsum += sir[0];
					ginsum += sir[1];
					binsum += sir[2];
				} else {
					routsum += sir[0];
					goutsum += sir[1];
					boutsum += sir[2];
				}
			}
			stackpointer = radius;

			for (x = 0; x < w; x++) {

				r[yi] = dv[rsum];
				g[yi] = dv[gsum];
				b[yi] = dv[bsum];

				rsum -= routsum;
				gsum -= goutsum;
				bsum -= boutsum;

				stackstart = stackpointer - radius + div;
				sir = stack[stackstart % div];

				routsum -= sir[0];
				goutsum -= sir[1];
				boutsum -= sir[2];

				if (y == 0) {
					vmin[x] = Math.min(x + radius + 1, wm);
				}
				p = pix[yw + vmin[x]];

				sir[0] = (p & 0xff0000) >> 16;
				sir[1] = (p & 0x00ff00) >> 8;
				sir[2] = (p & 0x0000ff);

				rinsum += sir[0];
				ginsum += sir[1];
				binsum += sir[2];

				rsum += rinsum;
				gsum += ginsum;
				bsum += binsum;

				stackpointer = (stackpointer + 1) % div;
				sir = stack[(stackpointer) % div];

				routsum += sir[0];
				goutsum += sir[1];
				boutsum += sir[2];

				rinsum -= sir[0];
				ginsum -= sir[1];
				binsum -= sir[2];

				yi++;
			}
			yw += w;
		}
		for (x = 0; x < w; x++) {
			rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
			yp = -radius * w;
			for (i = -radius; i <= radius; i++) {
				yi = Math.max(0, yp) + x;

				sir = stack[i + radius];

				sir[0] = r[yi];
				sir[1] = g[yi];
				sir[2] = b[yi];

				rbs = r1 - Math.abs(i);

				rsum += r[yi] * rbs;
				gsum += g[yi] * rbs;
				bsum += b[yi] * rbs;

				if (i > 0) {
					rinsum += sir[0];
					ginsum += sir[1];
					binsum += sir[2];
				} else {
					routsum += sir[0];
					goutsum += sir[1];
					boutsum += sir[2];
				}

				if (i < hm) {
					yp += w;
				}
			}
			yi = x;
			stackpointer = radius;
			for (y = 0; y < h; y++) {
				// Preserve alpha channel: ( 0xff000000 & pix[yi] )
				pix[yi] = (0xff000000 & pix[yi]) | (dv[rsum] << 16)
						| (dv[gsum] << 8) | dv[bsum];

				rsum -= routsum;
				gsum -= goutsum;
				bsum -= boutsum;

				stackstart = stackpointer - radius + div;
				sir = stack[stackstart % div];

				routsum -= sir[0];
				goutsum -= sir[1];
				boutsum -= sir[2];

				if (x == 0) {
					vmin[y] = Math.min(y + r1, hm) * w;
				}
				p = x + vmin[y];

				sir[0] = r[p];
				sir[1] = g[p];
				sir[2] = b[p];

				rinsum += sir[0];
				ginsum += sir[1];
				binsum += sir[2];

				rsum += rinsum;
				gsum += ginsum;
				bsum += binsum;

				stackpointer = (stackpointer + 1) % div;
				sir = stack[stackpointer];

				routsum += sir[0];
				goutsum += sir[1];
				boutsum += sir[2];

				rinsum -= sir[0];
				ginsum -= sir[1];
				binsum -= sir[2];

				yi += w;
			}
		}

		Log.e("pix", w + " " + h + " " + pix.length);
		bitmap.setPixels(pix, 0, w, 0, 0, w, h);

		return (bitmap);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.id_action_bar_left_img_view:
			if (getSupportFragmentManager().getBackStackEntryCount() > 1){
		        getSupportFragmentManager().popBackStack();
		    }
			else{
				Intent i = new Intent(this, BoardingActivity.class);
				startActivity(i);
				finish();
			}
			break;

		default:
			break;
		}
	}

@Override
public void onBackPressed() {
	if (getSupportFragmentManager().getBackStackEntryCount() > 0){
        getSupportFragmentManager().popBackStack();
    }
	else
	super.onBackPressed();
}

@Override
public void onItemSelected(AdapterView<?> adapterView, View view,
		int position, long id) {
	AccountType accountType = (AccountType) accountTypeView
			.getSelectedItem();
	if (accountType.getProtocol().isOAuth())
		findViewById(R.id.auth_panel).setVisibility(View.GONE);
	else
		findViewById(R.id.auth_panel).setVisibility(View.VISIBLE);
	((TextView) findViewById(R.id.account_user_name)).setHint(accountType
			.getHint());
	((TextView) findViewById(R.id.account_help)).setText(accountType
			.getHelp());
}

@Override
public void onNothingSelected(AdapterView<?> adapterView) {
	accountTypeView.setSelection(0);
}


}
