package com.matchstixapp.PNV;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.craterzone.cz_commons_lib.KeyboardUtil;
import com.matchstixapp.BuildConfig;
import com.matchstixapp.R;
import com.matchstixapp.modal.Location;
import com.matchstixapp.utils.Constants;

public class CountrySelectionActivity extends Activity implements TextWatcher,
		OnItemClickListener, OnClickListener {
	public static final String TAG = CountrySelectionActivity.class.getName();

	private ListView listView;
	private CountryAdapter adapter;
	private ImageView ivBack, ivSearchCancel;
	private TextView tvText;
	private EditText etSearch;
	private boolean searching;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_country_selection);
		
		ivBack = (ImageView) findViewById(R.id.iv_action_back);
		tvText = (TextView) findViewById(R.id.tv_action_txt);
		etSearch = (EditText) findViewById(R.id.et_action_search);
		ivSearchCancel = (ImageView) findViewById(R.id.iv_action_search_cancel);
		listView = (ListView) findViewById(R.id.lv_countries);
	}

	@Override
	protected void onStart() {
		ArrayList<Location> list = LocationDAO.getInstance(this)
				.getAllLocation();
		if (adapter == null) {
			adapter = new CountryAdapter(this, R.layout.country_list_item, list);
			listView.setAdapter(adapter);
		}

		super.onStart();
	}

	@Override
	protected void onResume() {
		super.onResume();
		ivBack.setOnClickListener(this);
		ivSearchCancel.setOnClickListener(this);
		listView.setOnItemClickListener(this);
		etSearch.addTextChangedListener(this);
	}

	@Override
	protected void onPause() {
		super.onPause();

		ivBack.setOnClickListener(null);
		ivSearchCancel.setOnClickListener(null);
		listView.setOnItemClickListener(null);
		etSearch.addTextChangedListener(null);
	}

	@Override
	public void afterTextChanged(Editable s) {
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		adapter.getFilter().filter(s);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		Location location = null;
		try {
			location = (Location) parent.getAdapter().getItem(position);
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (BuildConfig.DEBUG) {
			Log.d(TAG, "country selected : " + location.getName());
		}

		Intent data = new Intent(Constants.RECEIVER_COUNTRY_SELECTION);
		data.putExtra(Constants.DATA_COUNTRY_SELECTION, location);

		LocalBroadcastManager.getInstance(this).sendBroadcast(data);

		finish();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

		if (adapter != null) {
			adapter.clear();
			adapter = null;
		}

		listView = null;
		etSearch = null;
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		LocalBroadcastManager.getInstance(this).sendBroadcast(
				new Intent(Constants.RECEIVER_COUNTRY_SELECTION));
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.iv_action_back:
			KeyboardUtil.hideKeyboard(this);
			finish();
			break;

		case R.id.iv_action_search_cancel:
			if (!searching) {
				// when user is about to search
				searching = true;
				ivBack.setVisibility(View.GONE);
				tvText.setVisibility(View.GONE);
				ivSearchCancel
						.setImageResource(android.R.drawable.ic_menu_close_clear_cancel);

				etSearch.setVisibility(View.VISIBLE);
				etSearch.requestFocus();
				((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE))
						.showSoftInput(etSearch, InputMethodManager.SHOW_FORCED);
				break;
			}

			if (etSearch.getText().length() != 0) {
				// when search field is already filled clear it first
				etSearch.setText("");
				break;
			}

			// when user cancel searching
			((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE))
					.hideSoftInputFromWindow(etSearch.getWindowToken(), 0);
			searching = false;
			ivBack.setVisibility(View.VISIBLE);
			tvText.setVisibility(View.VISIBLE);
			ivSearchCancel.setImageResource(android.R.drawable.ic_menu_search);
			etSearch.setVisibility(View.GONE);
			break;

		default:
			break;
		}
	}

}
