package com.matchstixapp.PNV;

import static com.matchstixapp.utils.Validator.isValidNumber;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.facebook.appevents.AppEventsLogger;
import com.matchstixapp.MatchstixApplication;
import com.matchstixapp.R;
import com.matchstixapp.db.DataController;
import com.matchstixapp.listener.Keys;
import com.matchstixapp.listener.PINSendListener;
import com.matchstixapp.listener.PINValidateListener;
import com.matchstixapp.modal.User;
import com.matchstixapp.pojo.JSONNumber;
import com.matchstixapp.pojo.JSONPhone;
import com.matchstixapp.requester.SendPINRequester;
import com.matchstixapp.requester.ValidatePINRequester;
import com.matchstixapp.ui.activity.DashBoardActivity;
import com.matchstixapp.ui.fragments.BaseFragment;
import com.matchstixapp.utils.BackgroundExecutor;
import com.matchstixapp.utils.TypeFaceTextView;
import com.matchstixapp.utils.Utils;
import com.matchstixapp.xmpplibrary.XMPPUtil;
import com.matchstixapp.xmpplibrary.ui.adapter.AccountTypeAdapter;
import com.xabber.android.data.account.AccountManager;
import com.xabber.android.data.account.AccountType;


public class PNVFragmentEnterPin extends BaseFragment implements TextWatcher,
		OnClickListener,PINSendListener,PINValidateListener {
	public static final String TAG = PNVFragmentEnterPin.class.getSimpleName();
	private View mRootView;
	private EditText mPin;
	private Button mEnter;
	private JSONNumber jsonNumber;
	private TypeFaceTextView tvTitle;
	private TypeFaceTextView tvTryAgain;
	private TypeFaceTextView tvTryAgainDesc;

	private boolean tryAgainDone;
	private Spinner accountTypeView;

	public static PNVFragmentEnterPin newInstance(JSONNumber jsonNumber) {
		PNVFragmentEnterPin fragment = new PNVFragmentEnterPin();
		fragment.jsonNumber = jsonNumber;
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mRootView = inflater.inflate(R.layout.fragment_pnv_varification,
				container, false);
		tvTitle = (TypeFaceTextView) mRootView.findViewById(R.id.tv_title);
		tvTryAgain = (TypeFaceTextView) mRootView
				.findViewById(R.id.try_again_textview);
		tvTryAgainDesc = (TypeFaceTextView) mRootView
				.findViewById(R.id.textView_sentpin);

		mPin = (EditText) mRootView.findViewById(R.id.et_pin);
		mEnter = (Button) mRootView.findViewById(R.id.id_enter_pin_btn);
		mEnter.setOnClickListener(this);
		tvTryAgain.setOnClickListener(this);
		accountTypeView = (Spinner) mRootView.findViewById(R.id.account_type);
		accountTypeView.setAdapter(new AccountTypeAdapter(getActivity()));
		accountTypeView.setSelection(0);

		return mRootView;
	}
	
	@Override
	public void onResume() {
		super.onResume();
		dismissProgressDialog();
		MatchstixApplication.getInstance().addUIListener(PINSendListener.class,
				this);
		MatchstixApplication.getInstance().addUIListener(PINValidateListener.class,
				this);

		mPin.addTextChangedListener(this);
	}
	@Override
	public void onPause() {
		super.onPause();
		MatchstixApplication.getInstance().removeUIListener(PINSendListener.class,
				this);
		MatchstixApplication.getInstance().removeUIListener(PINValidateListener.class,
				this);

	}
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.id_enter_pin_btn:
			JSONPhone request = new JSONPhone();
			request.code = Integer.parseInt(mPin.getText().toString().trim());
			request.number = jsonNumber;
			validatePINCode(request);
			break;
			
		case R.id.try_again_textview:
			Utils.showFadeinAnimation(getActivity(), tvTryAgain);
			if (!tryAgainDone) {
				sendVerificationCode();
			} else {
				launchEnterNumberFragment();
			}
			break;

		}
	}

	private void launchEnterNumberFragment() {
		getActivity().getIntent().putExtra(Keys.KEY_MODE_EDIT, true);
		getActivity().getSupportFragmentManager().popBackStack();
	}
	private ProgressDialog progressDialog;

	private void dismissProgressDialog() {
		if (progressDialog != null && progressDialog.isShowing()) {
			progressDialog.dismiss();
		}
	}
	private void sendVerificationCode() {
		progressDialog = ProgressDialog.show(
				getActivity(), "", getResources().getString(R.string.sending));
		BackgroundExecutor.getInstance().execute(new SendPINRequester(jsonNumber));
	}

	private void validatePINCode(final JSONPhone request) {

		progressDialog = ProgressDialog.show(
				getActivity(), "",
				getResources().getString(R.string.please_wait));
		BackgroundExecutor.getInstance().execute(new ValidatePINRequester(request));
	}

	@Override
	public void afterTextChanged(Editable arg0) {
		
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
		
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		
		if (s.length()!=0 && !isValidNumber(s.toString())) {			
			Toast.makeText(getActivity(),
					getResources().getString(R.string.error_not_a_number),
					Toast.LENGTH_SHORT).show();
			
		} else if(s.length() >= 4){
			mEnter.setEnabled(true);
			mEnter.setAlpha(1f);
			return;
		}

		mEnter.setEnabled(false);
		mEnter.setAlpha(0.6f);
		
	}

	@Override
	public void onPINSendSuccess(final JSONNumber request) {
		getActivity().runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				dismissProgressDialog();
				tryAgainDone = true;
				// Change try again text
				tvTitle.setText(getResources().getString(
						R.string.enter_new_pin));
				tvTryAgainDesc
				.setText(getResources().getString(
						R.string.did_not_receive_new_pin));
				
				SpannableString spanString = new SpannableString(
						getResources().getString(
								R.string.enter_new_num_u));
				spanString.setSpan(new UnderlineSpan(), 0,
						spanString.length(), 0);
				tvTryAgain.setText(spanString);
			}
		});

	}

	@Override
	public void onPINSendFailed(final int code, final String msg) {
		getActivity().runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				dismissProgressDialog();
				Toast.makeText(getActivity(), msg,
						Toast.LENGTH_SHORT).show();
			}
		});

	}

	@Override
	public void onPINValidationSuccess(final JSONPhone phone) {
		getActivity().runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				dismissProgressDialog();
				User user = DataController.getInstance().getUser();
				user.setNumber(phone.number.phoneNumber);
				user.setCC(phone.number.cc);
				user.setNumberVerified(true);
				DataController.getInstance().saveUser();
				
				/*doXMPPLogin();*/
				if(AccountManager.getInstance().getAccounts().size() > 0){
					String account = DataController.getInstance().getFullJid();
						new XMPPUtil().enableAccount(AccountManager.getInstance().getAccount(account));
				}else{
					new XMPPUtil().doXMPPLogin(getActivity(),
							(AccountType) accountTypeView.getSelectedItem());
				}
				AppEventsLogger logger = AppEventsLogger.newLogger(getActivity());
				logger.logEvent("Completed Mobile Verification");
				startActivity(new Intent(getActivity(),
						DashBoardActivity.class));
				getActivity().finish();
			}
		});
	}

	@Override
	public void onPINValidationFailed(final int code,final String msg) {
		getActivity().runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				dismissProgressDialog();
				Toast.makeText(getActivity(), getResources().getString(R.string.error_invalid_pin),
						Toast.LENGTH_SHORT).show();
			}
		});		
	}

	@Override
	public void onPINSendError(final int code) {
		getActivity().runOnUiThread(new Runnable() {

			
			@Override
			public void run() {
				dismissProgressDialog();
				Log.d(TAG, "Error in pin sending statuc code " + code );
			}
		});
		
	}

	public void onUnauthorizedListener() {
		
	}

}
