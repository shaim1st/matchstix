package com.matchstixapp.services;

import com.matchstixapp.db.DataController;
import com.matchstixapp.modal.User;

public enum FBWebServices {

	FACEBOOK_ROOT_URL("https://graph.facebook.com/");

	private String url;

	private FBWebServices(String url) {
		this.url = url;
	}

	public String getUrl() {
		return url;
	}

	// https://graph.facebook.com/me?access_token=53455nkjn43j53
	public static String getFacebookUserIdUrl(String accessToken) {

		return FACEBOOK_ROOT_URL.getUrl() + "me/profile?access_token="
				+ accessToken;
	}
	// https://graph.facebook.com/me?access_token=53455nkjn43j53
		public static String getFacebookUserInfoUrl(String accessToken) {

			return FACEBOOK_ROOT_URL.getUrl() + "me?access_token="
					+ accessToken;
		}

	/*// https://graph.facebook.com/{user_id}
	public static String getFacebookUserProfileInfoUrl(String userId) {
		return FACEBOOK_ROOT_URL.getUrl() + userId;
	}*/

	// https://graph.facebook.com/{user_id}/picture?type=large
	public static String getUserThumbnailUrl(String userId) {
		return FACEBOOK_ROOT_URL.getUrl() + userId + "/picture?type=large&redirect=false";
	}

	// https://graph.facebook.com/{user-id}/friendlists
	// https://graph.facebook.com/me/friends?access_token=53455nkjn43j53
	public static String getFriendsListUrl(String accessToken) {
		/* return FACEBOOK_ROOT_URL.getUrl() + userId + "/friendlists"; */
		return FACEBOOK_ROOT_URL.getUrl() + "me/friends?access_token="
				+ accessToken;
	}
	
	public static String getMyAlbumsUrl(String accessToken) {
		/* return FACEBOOK_ROOT_URL.getUrl() + userId + "/friendlists"; */
		return FACEBOOK_ROOT_URL.getUrl() + "me/albums/?access_token="
				+ accessToken;
	}
	
	public static String getAlbumPhotosUrl(String albumId, String accessToken) {
		return FACEBOOK_ROOT_URL.getUrl() + albumId
				+"/photos/?access_token="
				+ accessToken;
	}
	public static String getFriendsAlbumPhotosUrl(String albumId, String accessToken) {
		return FACEBOOK_ROOT_URL.getUrl() + albumId
				+"/photos/?access_token="
				+ accessToken;
	}
	
	public static String getPicURL(String picId) {
		User user = DataController.getInstance().getUser();
		return FACEBOOK_ROOT_URL.getUrl() + "v2.4/" + picId +"/picture?access_token="
				+ user.getFbAccessToken();
	}
	
}
