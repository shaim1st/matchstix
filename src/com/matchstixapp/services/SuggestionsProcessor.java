package com.matchstixapp.services;


import java.net.HttpURLConnection;

import com.craterzone.cz_commons_lib.JsonUtil;
import com.craterzone.httpclient.model.CZResponse;
import com.facebook.appevents.AppEventsLogger;
import com.matchstixapp.MatchstixApplication;
import com.matchstixapp.WS.HTTPOperationController;
import com.matchstixapp.db.DataController;
import com.matchstixapp.db.SuggestionAction;
import com.matchstixapp.listener.FriendLikeListner;
import com.matchstixapp.utils.Constants;

public class SuggestionsProcessor implements Runnable {

	SuggestionAction action;

	public SuggestionsProcessor(SuggestionAction action) {
		this.action = action;
	}

	@Override
	public void run() {
		boolean process = processAction(action);
		for(FriendLikeListner listener : MatchstixApplication.getInstance().getUIListeners(FriendLikeListner.class)){
			if(process){
				listener.onFriendLikeSuccess();
			}
			else listener.onFriendPassSuccess();
		}
	}

	private boolean processAction(SuggestionAction action) {
		if (action.isLiked()) {
			UserFriend userFriend = new UserFriend(DataController.getInstance()
					.getUser().getId(), action.getUserId());
			HTTPOperationController.likeCounter++;
			if(HTTPOperationController.likeCounter>Constants.LIKE_LIMIT){
				return false;
			}
			CZResponse response = HTTPOperationController.getInstance()
					.requestLikeUser(
							DataController.getInstance().getUser()
									.getAccessToken(),
							JsonUtil.toJson(userFriend));
			return response != null
					&& response.getResponseCode() == HttpURLConnection.HTTP_OK;
		}
		else if(!action.isLiked()){
			UserFriend userFriend = new UserFriend(DataController.getInstance()
					.getUser().getId(), action.getUserId());
			CZResponse response = HTTPOperationController.getInstance()
					.requestPassUser(
							DataController.getInstance().getUser()
									.getAccessToken(),
							JsonUtil.toJson(userFriend));
			return response != null
					&& response.getResponseCode() == HttpURLConnection.HTTP_OK;
		}
		return false;
	}

	class UserFriend {
		private long userId;
		private long friendId;

		public UserFriend(long userId, long friendId) {
			this.userId = userId;
			this.friendId = friendId;
		}

		public long getUserId() {
			return userId;
		}

		public void setUserId(long userId) {
			this.userId = userId;
		}

		public long getFriendId() {
			return friendId;
		}

		public void setFriendId(long friendId) {
			this.friendId = friendId;
		}

	}

}
