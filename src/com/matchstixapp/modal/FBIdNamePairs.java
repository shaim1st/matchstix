package com.matchstixapp.modal;

import java.util.ArrayList;

public class FBIdNamePairs {

	private ArrayList<FBIdName> data;

	public ArrayList<FBIdName> getData() {
		return data;
	}

	public void setData(ArrayList<FBIdName> data) {
		this.data = data;
	}

}
