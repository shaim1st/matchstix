package com.matchstixapp.modal;

public class Album {
	private String id;
	private String link;
	private String name;
	private int count;
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	public int getCount() {
		return count;
	}

}
