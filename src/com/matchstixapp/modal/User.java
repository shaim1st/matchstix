package com.matchstixapp.modal;

import com.craterzone.cz_commons_lib.EncodingDecodingUtil;
import com.matchstixapp.utils.Constants;

public class User {

	private long id;
	private String name;
	private String fName;
	private String lName;

	private String displayName;
	private long latitude;
	private long longtitude;
	private String avatarURL;
	private int age;
	private String gender;
	private boolean registered;
	private boolean loggedIn;
	
	public String about;
	private String accessToken;
	private String userName;
	private String email;
	private String localAvatar;
	private String password;
	private String fbAccessToken;
	
	private boolean numberVerified;
	private String number;
	private String CC;
	private String fbUserId;
	private boolean imageSync;
	private boolean isExpire;
	private long fbTokenExpireDate;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	public void setLocalAvatar(String url){
		this.localAvatar = url;
	}
	
	public String getName() {
		return EncodingDecodingUtil.decodeString(name);
	}

	public void setName(String name) {
		this.name = EncodingDecodingUtil.encodeString(name);
	}

	public String getDisplayName() {
		return EncodingDecodingUtil.decodeString(displayName);
	}

	public void setDisplayName(String displayName) {
		this.displayName = EncodingDecodingUtil.encodeString(displayName);
	}

	public long getLatitude() {
		return latitude;
	}

	public void setLatitude(long latitude) {
		this.latitude = latitude;
	}

	public long getLongtitude() {
		return longtitude;
	}

	public void setLongtitude(long longtitude) {
		this.longtitude = longtitude;
	}

	public String getAvatarURL() {
		return localAvatar!=null && localAvatar.trim().length()!=0 ? localAvatar:avatarURL;
	}

	public void setAvatarURL(String avatarURL) {
		this.avatarURL = avatarURL;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getGender() {
		return gender == null ? "" : gender;
	}

	public void setGender(String gender) {
		if (gender != null) {
			this.gender = gender.trim();
		}
	}

	public boolean isRegistered() {
		return registered;
	}

	public void setRegistered(boolean registered) {
		this.registered = registered;
	}

	public String getAbout() {
		return about;
	}

	public void setAbout(String about) {
		this.about = about;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public boolean isMale() {
		return getGender().equalsIgnoreCase(Constants.MALE);
	}

	public boolean isFemale() {
		return getGender().equalsIgnoreCase(Constants.FEMALE);
	}

	public String getFirstName() {
		return EncodingDecodingUtil.decodeString(fName);
	}

	public void setFirstName(String fame) {
		this.fName = EncodingDecodingUtil.encodeString(fame);
	}

	public String getLastName() {
		return EncodingDecodingUtil.decodeString(lName);
	}

	public void setLastName(String lame) {
		this.lName = EncodingDecodingUtil.encodeString(lame);
	}

	public void setUserName(String username) {
		this.userName = EncodingDecodingUtil.encodeString(username);
	}
	public String getUserName() {
		return EncodingDecodingUtil.decodeString(userName);
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setPassword(String password) {
		this.password = password;
		
	}
	public String getPassword() {
		return password;
	}

	public String getFbAccessToken() {
		return fbAccessToken;
	}

	public void setFbAccessToken(String fbAccessToken) {
		this.fbAccessToken = fbAccessToken;
	}

	public boolean isLoggedIn() {
		return loggedIn;
	}

	public void setLoggedIn(boolean loggedIn) {
		this.loggedIn = loggedIn;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public boolean isNumberVerified() {
		return numberVerified;
	}

	public void setNumberVerified(boolean numberVerified) {
		this.numberVerified = numberVerified;
	}

	public String getCC() {
		return CC;
	}

	public void setCC(String cC) {
		CC = cC;
	}

	public String getFbUserId() {
		return fbUserId;
	}

	public void setFbUserId(String fbUserId) {
		this.fbUserId = fbUserId;
	}

	public boolean getImageSync() {
		return imageSync;
	}

	public void setImageSync(boolean imageSync) {
		this.imageSync = imageSync;
	}

	public boolean isExpire() {
		return isExpire;
	}

	public void setExpire(boolean isExpire) {
		this.isExpire = isExpire;
	}

	public long getFbTokenExpireDate() {
		return fbTokenExpireDate;
	}

	public void setFbTokenExpireDate(long fbTokenExpireDate) {
		this.fbTokenExpireDate = fbTokenExpireDate;
	}

}
