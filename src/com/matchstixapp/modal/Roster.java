package com.matchstixapp.modal;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import com.matchstixapp.db.DataController;
import com.matchstixapp.ui.adapter.DrawerRightListAdapter;
import com.xabber.android.data.message.MessageManager;
import com.xabber.android.data.message.type.AbstractMessageItem;

public class Roster {

	private static Roster instance=new Roster();
	private ArrayList<Friend> friends;
	private boolean needsRefresh;
	private Roster() {
		friends = new ArrayList<Friend>();
		needsRefresh=true;
	}

	public void setFriendByUserId(long userId,Friend friend,Roster roster ) {
		int pos=getFriendPosition(userId);
		int size=roster.size();
		if(friends.size()==size){
		friends.get(pos).setFirstName(friend.getFirstName());
		friends.get(pos).setLastName(friend.getLastName());
		friends.get(pos).setProfilePicURL(friend.getProfilePicURL());
	//	friends.get(pos).setMatchDate(friend.getMatchDate());
		friends.get(pos).setUserId(friend.getUserId());
		}else if(friends.size()<size){
			friends.add(friend);
		}else{
			removedFriend(roster);
		}
		
	}
	
	private boolean removedFriend(Roster roster) {
		for (int i = 0; i < roster.size(); i++) {
			if (friends.contains(roster.getFriend(i))){//friends.get(i) == roster.getFriend(i)) {
				 return true;
			}
		}
		return false;
	}
	private int getFriendPosition(long userId) {
		for (int i = 0; i < friends.size(); i++) {
			if (friends.get(i).getUserId() == userId) {
				return i;
			}
		}
		return -1;
	}

	public static void init(Roster roster) {
		instance = roster;
	}

	public static Roster getInstance() {
		return instance;
	}

	

	
	public int getFriendPostion(long id) {
		ArrayList<Friend> friends=Roster.getInstance().getFriendList();
		for (int i = 0; i < friends.size(); i++) {
			if (friends.get(i).getUserId() == id) {
				return i;
			}
		}
		return -1;
	}
	
	public void setFriendChatTime(Friend frd) {
		int position=getFriendPostion(frd.getUserId());
		if(position!=-1){
			@SuppressWarnings("rawtypes")
			List<AbstractMessageItem> messageItemsList =new ArrayList<AbstractMessageItem>(MessageManager.
					getInstance().getMessages(DataController.getInstance().getFullJid(), DrawerRightListAdapter.getUser(frd.getUserId())));
			if(messageItemsList.size()>0)
			friends.get(position).setMatchDate(messageItemsList.get(messageItemsList.size()-1).getTimestamp().getTime());
			else
				friends.get(position).setMatchDate(new Date().getTime());
		}
	}
	public void setFriendChatTime(Friend frd,long time) {
		int position=getFriendPostion(frd.getUserId());
		if(position!=-1)
		 friends.get(position).setMatchDate(time);
	}
	public void setFriendChatTimeOnNotification(long uid,long time) {
		int position=getFriendPostion(uid);
		if(position!=-1)
		 friends.get(position).setMatchDate(time);
	}
	public Friend getFriend(long id) {
		for (int i = 0; i < friends.size(); i++) {
			if (friends.get(i).getUserId() == id) {
				return friends.get(i);
			}
		}
		return null;
	}
	
	public String getFriendImageURL(long id) {
		for (int i = 0; i < friends.size(); i++) {
			if (friends.get(i).getUserId() == id) {
				return friends.get(i).getProfilePicURL();
			}
		}
		return null;
	}

	public Friend[] getFriends() {
		Friend[] list = new Friend[friends.size()];
		return friends.toArray(list);
	}

	public ArrayList<Friend> getFriendList() {
		Collections.sort(friends);
		return friends;
	}
	
	public boolean deleteFriend(long id) {
		for (int i = 0; i < friends.size(); i++) {
			if (friends.get(i).getUserId() == id) {
				friends.remove(i);
				return true;
			}
		}
		return false;
	}

	public int size() {
		return friends.size();
	}

	public boolean needsRefresh() {
		return needsRefresh;
	}

	public void setNeedsRefresh(boolean needsRefresh) {
		this.needsRefresh = needsRefresh;
	}

	public void addFriend(long userId, String firstName, String lastName,
			String profilePicURL, long matchDate, long chatDate) {
		Friend friend = new Friend(userId, firstName, lastName, profilePicURL, matchDate);
		if (!friends.contains(friend)) {
			friends.add(friend);
			Collections.sort(friends);
		}
		
	}

	public Friend getFrienduser(int id) {
		for (int i = 0; i < friends.size(); i++) {
			if (friends.get(i).getUserId() == id) {
				return friends.get(i);
			}
		}
		return new Friend(0, null, null, null, 0);
	}

}
