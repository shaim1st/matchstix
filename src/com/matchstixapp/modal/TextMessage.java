package com.matchstixapp.modal;

import com.craterzone.cz_commons_lib.EncodingDecodingUtil;

public class TextMessage extends MessageItem {
	private String textMessage;
	
	public TextMessage() {
		super();
	}

	public TextMessage(String textMessage) {
		super();
		this.textMessage = textMessage;
	}

	public String getTextMessage() {
		return EncodingDecodingUtil.decodeString(textMessage);
	}

	public void setTextMessage(String textMessage) {
		this.textMessage = EncodingDecodingUtil.encodeString(textMessage);
	}

}
