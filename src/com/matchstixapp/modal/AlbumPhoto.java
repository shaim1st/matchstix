package com.matchstixapp.modal;

import android.text.TextUtils;

public class AlbumPhoto {
	private String id;
	private String source;
	private String localAvatar;
	
	public String getAvatar() {
		return hasLocalAvatar()?localAvatar:source;
	}
	public boolean hasLocalAvatar() {
		return !TextUtils.isEmpty(localAvatar);
	}
	public String getLocalAvatar() {
		return localAvatar;
	}
	public String getRemoteAvatar() {
		return source;
	}
	public void setRemoteAvatar(String source) {
		this.source = source;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public void setLocalAvatar(String loaclAvatar) {
		this.localAvatar = loaclAvatar;
	}
	public AlbumPhoto copy() {
		AlbumPhoto p = new AlbumPhoto();
		p.id=this.id;
		p.source=this.source;
		p.localAvatar=this.localAvatar;
		return p;
	}
	
	
}
