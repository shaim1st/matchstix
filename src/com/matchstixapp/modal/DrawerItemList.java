package com.matchstixapp.modal;



public class DrawerItemList {
	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	private int imageId;
	private String title;
	private android.support.v4.app.Fragment viewId;
	private String tag;
	

	public int getImageId() {
		return imageId;
	}

	public void setImageId(int imageId) {
		this.imageId = imageId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public android.support.v4.app.Fragment getViewId() {
		return viewId;
	}

	public void setViewId(android.support.v4.app.Fragment viewId) {
		this.viewId = viewId;
	}

	public DrawerItemList(String string, int icGotomatchstix, String title, android.support.v4.app.Fragment viewId) {
		super();
		this.imageId = icGotomatchstix;
		tag = string;
		this.title = title;
		this.viewId = viewId;
	}

}
