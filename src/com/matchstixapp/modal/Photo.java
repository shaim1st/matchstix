package com.matchstixapp.modal;

public class Photo {
	public String imageURL;
	public boolean isProfilePic;
	public boolean isProfilePic() {
		return isProfilePic;
	}
	public String getImageURL() {
		return imageURL;
	}
}
