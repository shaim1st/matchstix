package com.matchstixapp.modal;

import java.util.ArrayList;

public class FBAlbumPhotos {
	private ArrayList<AlbumPhoto> data;

	public ArrayList<AlbumPhoto> getData() {
		return data;
	}

	public void setData(ArrayList<AlbumPhoto> data) {
		this.data = data;
	}

	public int countPhotos() {
		return data == null ? 0 : data.size();
	}

}
