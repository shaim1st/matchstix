package com.matchstixapp.modal;

import java.util.ArrayList;

import com.craterzone.cz_commons_lib.DateFormatter;
import com.craterzone.cz_commons_lib.EncodingDecodingUtil;
import com.matchstixapp.pojo.FacebookGraphUser;

public class FBFetchedDettails {
	String firstName;
	String lastName;
	String userName;
	String aboutMe;
	int age;
	private String location;
	private ArrayList<UserFBAvatar> avatarList;
	private FacebookGraphUser facebookGraphUser;

	public String getLocation() {
		return location;
	}

	public ArrayList<UserFBAvatar> getAvatarList() {
		return avatarList;
	}

	public FBFetchedDettails() {
		super();
		// TODO Auto-generated constructor stub
	}

	public FBFetchedDettails(String id, String name, String birthday,
			String firstName, String lastName, String userName, String email,
			String aboutMe, int age) {
		super();
		this.userName = userName;
		this.aboutMe = aboutMe;
		this.age = age;
	}



	public String getUserName() {
		return EncodingDecodingUtil.decodeString(userName);
	}

	public void setUserName(String userName) {
		this.userName = EncodingDecodingUtil.encodeString(userName);
	}

	public String getAboutMe() {
		return aboutMe;
	}

	public void setAboutMe(String aboutMe) {
		this.aboutMe = aboutMe;
	}

	public int getAge() {
		if(facebookGraphUser != null) {
			String date = DateFormatter.getDateInyyyyMMddWithHighphenAsSepFormat(facebookGraphUser.getBirthday(), "MM/dd/yyyy", "yyyy-MM-dd");	
			this.age = DateFormatter.getNumberOfYearFromCurrDate(DateFormatter.dateToMiliSec(date, "yyyy-MM-dd"));
		}
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public void setLocation(String loc) {
		this.location = loc;
		
	}

	public void setAvatarList(ArrayList<UserFBAvatar> avatarList) {
		this.avatarList = avatarList;
		
	}

	public FacebookGraphUser getFacebookGraphUser() {
		return facebookGraphUser;
	}

	public void setFacebookGraphUser(FacebookGraphUser facebookGraphUser) {
		this.facebookGraphUser = facebookGraphUser;
	}

	public String getFirstName() {
		return EncodingDecodingUtil.decodeString(firstName);
	}

	public void setFirstName(String firstName) {
		this.firstName = EncodingDecodingUtil.encodeString(firstName);
	}

	public String getLastName() {
		return EncodingDecodingUtil.decodeString(lastName);
	}

	public void setLastName(String lastName) {
		this.lastName = EncodingDecodingUtil.encodeString(lastName);
	}
	

}
 