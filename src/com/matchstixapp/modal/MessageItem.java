package com.matchstixapp.modal;

import com.craterzone.cz_commons_lib.EncodingDecodingUtil;


public class MessageItem {
	protected String ReceiverName; // this is display name
	private String receiverImageURL;
	private String receiverJID;
	private long messageDateAndTime;
	private int messageDeliveryStatus; // is sent or not
	private int messageType; // send, receive
	private int chatType;
	private String id;
	private String msg;
	

	public String getMessage() {
		return msg;
	}



	public void setMsg(String msg) {
		this.msg = msg;
	}



	public MessageItem(String receiverName, String receiverImageURL,
			String receiverJID, long messageDateAndTime,
			int messageDeliveryStatus,
			int messageType, int chatType,String id) {
		this.ReceiverName = receiverName;
		this.receiverImageURL = receiverImageURL;
		this.receiverJID = receiverJID;
		this.messageDateAndTime = messageDateAndTime;
		this.messageDeliveryStatus = messageDeliveryStatus;
		this.messageType = messageType;
		this.chatType = chatType;
		this.id=id;
		
	}
	
	

	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public MessageItem() {
	
	}

	public String getReceiverName() {
		return EncodingDecodingUtil.decodeString(ReceiverName);
	}

	public void setReceiverName(String receiverName) {
		ReceiverName = EncodingDecodingUtil.encodeString(receiverName);
	}

	public String getReceiverImageURL() {
		return receiverImageURL;
	}

	public void setReceiverImageURL(String receiverImageURL) {
		this.receiverImageURL = receiverImageURL;
	}

	public String getReceiverJID() {
		return receiverJID;
	}

	public void setReceiverJID(String receiverJID) {
		this.receiverJID = receiverJID;
	}

	public long getTime() {
		return messageDateAndTime;
	}

	public void setTime(long messageDateAndTime) {
		this.messageDateAndTime = messageDateAndTime;
	}

	public int getMessageDeliveryStatus() {
		return messageDeliveryStatus;
	}

	public void setMessageDeliveryStatus(
			int messageDeliveryStatus) {
		this.messageDeliveryStatus = messageDeliveryStatus;
	}

	public int getMessageType() {
		return messageType;
	}

	public void setMessageType(int messageType) {
		this.messageType = messageType;
	}

	public int getType() {
		return chatType;
	}

	public void setType(int chatType) {
		this.chatType = chatType;
	}
	
}
