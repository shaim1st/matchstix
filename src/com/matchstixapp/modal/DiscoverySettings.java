package com.matchstixapp.modal;

import com.matchstixapp.utils.Constants;

public class DiscoverySettings {

	private int distance;// in meters
	private int minAge;
	private int maxAge;
	private String[] genders;

	public DiscoverySettings() {
		// distance = Constants.MIN_DISTANCE;// in meters
		// minAge = Constants.MIN_AGE;
		// maxAge = Constants.MAX_AGE;
	}

	public DiscoverySettings(int distance, int minAge, int maxAge,
			String[] gender) {
		this.distance = distance;
		this.minAge = minAge;
		this.maxAge = maxAge;
		this.genders = gender;
	}

	/**
	 * sets distance in meters
	 */
	public void setDistance(int distance) {
		this.distance = distance;
	}

	/**
	 * @return distance in meters
	 */
	public int getDistance() {
		return distance;
	}

	public void setMinAge(int minAge) {
		this.minAge = minAge;
	}

	public int getMinAge() {
		return minAge;
	}

	public void setMaxAge(int maxAge) {
		this.maxAge = maxAge;
	}

	public int getMaxAge() {
		return maxAge;
	}

	public void setGender(String[] genders) {
		this.genders = genders;
	}

	public String[] getGender() {
		return genders;
	}

	public boolean isMale() {
		if (hasGenders() && contains(Constants.MALE)) {
			return true;
		}
		return false;
	}
	public boolean isFemale() {
		if (hasGenders() && contains(Constants.FEMALE)) {
			return true;
		}
		return false;
	}

	public boolean contains(String gender) {
		if (this.genders == null) {
			return false;
		}
		for (int i = 0; i < genders.length; i++) {
			String g = genders[i];
			if (g != null && g.equalsIgnoreCase(gender)) {
				return true;
			}
		}
		return false;
	}

	public boolean hasGenders() {
		return genders != null && genders.length > 0;
	}

}
