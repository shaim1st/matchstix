package com.matchstixapp.modal;


import java.io.Serializable;

public class Location implements Serializable {
	public static final String TAG = Location.class.getName();

	private static final long serialVersionUID = 7421018329313205235L;
	private String phoneCode = "";
	private String name = "";
	private String twoLetterCode = "";
	private String city = "";

	public Location() {
		super();
	}

	public void setParams(Location location) {
		this.phoneCode = location.phoneCode;
		this.name = location.name;
		this.twoLetterCode = location.twoLetterCode;
		this.city = location.city;
	}

	public Location(String phoneCode, String name, String twoLetterCode, String city) {
		this.phoneCode = phoneCode;
		this.name = name;
		this.twoLetterCode = twoLetterCode;
		this.city = city;
	}

	public String getPhoneCode() {
		return phoneCode;
	}

	public void setPhoneCode(String phoneCode) {
		this.phoneCode = phoneCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTwoLetterCode() {
		return twoLetterCode;
	}

	public void setTwoLetterCode(String twoLetterCode) {
		this.twoLetterCode = twoLetterCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Override
	public String toString() {
		return name;
	}

}

