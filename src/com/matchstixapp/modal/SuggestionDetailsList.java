package com.matchstixapp.modal;

import java.util.ArrayList;


public class SuggestionDetailsList {
	private ArrayList<SuggestionDetail> suggestions;

	public ArrayList<SuggestionDetail> getSuggestionDetails() {
		if (suggestions==null) {
			return new ArrayList<SuggestionDetail>();
		}
		return suggestions;
	}

	public boolean addDetail(SuggestionDetail suggestion) {
		if (!suggestions.contains(suggestion)) {
			suggestions.add(suggestion);
			return true;
		}
		return false;
	}
	public boolean deleteDetail(SuggestionDetail detail) {
		return suggestions.remove(detail);
	}
	public boolean deleteDetail(long suggetionId) {
		for (int i = 0; i < suggestions.size(); i++) {
			if (suggestions.get(i).getUserId()==suggetionId) {
				suggestions.remove(i);
				return true;
			}
		}
		return false;

	}
	public int size() {
		return getSuggestionDetails().size();
	}
	
}
