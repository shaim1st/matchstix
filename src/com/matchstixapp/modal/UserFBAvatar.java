package com.matchstixapp.modal;

public class UserFBAvatar {

	private String lres;
	private boolean profilePic;
	
	
	public UserFBAvatar(String lres, boolean profilePic) {
		super();
		this.lres = lres;
		this.profilePic = profilePic;
	}
	public String getLres() {
		return lres;
	}
	public void setLres(String lres) {
		this.lres = lres;
	}
	public boolean isProfilePic() {
		return profilePic;
	}
	public void setProfilePic(boolean profilePic) {
		this.profilePic = profilePic;
	}
	
	
}
