package com.matchstixapp.modal;

import com.craterzone.cz_commons_lib.EncodingDecodingUtil;

public class FBIdName {

	private String id;
	private String name;

	public FBIdName(String id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public FBIdName(String id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FBIdName other = (FBIdName) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return EncodingDecodingUtil.decodeString(name);
	}

	public void setName(String name) {
		this.name = EncodingDecodingUtil.encodeString(name);
	}
}
