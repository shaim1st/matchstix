package com.matchstixapp.modal;

import java.io.Serializable;
import com.craterzone.cz_commons_lib.EncodingDecodingUtil;
import com.matchstixapp.MatchstixApplication;
import com.matchstixapp.R;
import com.matchstixapp.db.MatchstixPreferences;

public class Friend implements Serializable,Comparable<Friend>{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long userId;
	private String firstName;
	private String lastName;
	private String profilePicURL;
	private long matchDate;
	public Friend(long userId, String firstName, String lastName,
			String profilePicURL, long matchDate) {
		super();
		this.userId = userId;
		this.firstName = EncodingDecodingUtil.encodeString(firstName);
		this.lastName = EncodingDecodingUtil.encodeString(lastName);
		this.profilePicURL = profilePicURL;
		if(MatchstixPreferences.getInstance().getJsonHashCodeFlag()==0)
		this.matchDate = matchDate;
	}
	
	public Friend(long userId, String firstName, String lastName,
			String profilePicURL, long matchDate,long lastchattime) {
		super();
		this.userId = userId;
		this.firstName = EncodingDecodingUtil.encodeString(firstName);
		this.lastName = EncodingDecodingUtil.encodeString(lastName);
		this.profilePicURL = profilePicURL;
		if(MatchstixPreferences.getInstance().getJsonHashCodeFlag()==0)
		  this.matchDate = matchDate;
		/*if(MatchstixPreferences.getInstance().getJsonHashCodeFlag()==1){
			@SuppressWarnings("rawtypes")
			List<AbstractMessageItem> messageItemsList =new ArrayList<AbstractMessageItem>(MessageManager.
					getInstance().getMessages(DataController.getInstance().getFullJid(), getUser(userId)));
			this.lastchattime=messageItemsList.get(messageItemsList.size()-1).getTimestamp().getTime();
		}*/
	}
	
	public static String getUser(long userId){
		return userId + "@" + MatchstixApplication.getInstance().getResources().getString(R.string.chat_server);
	}
	//*************************************************************
	public void setFriendWithoutLastChat(Friend friend) {
		setFirstName(friend.firstName);
		setLastName(friend.lastName);
		setProfilePicURL(friend.profilePicURL);
		setMatchDate(friend.getMatchDate());
		setUserId(friend.getUserId());
	}
	//*************************************************************

	public String getFirstName() {
		return EncodingDecodingUtil.decodeString(firstName);
	}

	public String getLastName() {
		return EncodingDecodingUtil.decodeString(lastName);
	}

	public long getMatchDate() {
		return matchDate;
	}

	public String getProfilePicURL() {
		return profilePicURL;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public void setProfilePicURL(String profilePicURL) {
		this.profilePicURL = profilePicURL;
	}
	public void setMatchDate(long matchDate) {
		this.matchDate = matchDate;
	}
	public long getUserId() {
		return userId;
	}
	
	public void setUserId(long id) {
		userId=id;
	}
	@Override
	public int compareTo(Friend another) {
        if(this.matchDate>another.getMatchDate()){
            return -1;
        }else if(this.matchDate<another.getMatchDate()){
            return 1;
        }   
		return 0;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result
				+ ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result
				+ ((profilePicURL == null) ? 0 : profilePicURL.hashCode());
		result = prime * result + (int) (userId ^ (userId >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Friend other = (Friend) obj;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (profilePicURL == null) {
			if (other.profilePicURL != null)
				return false;
		} else if (!profilePicURL.equals(other.profilePicURL))
			return false;
		if (userId != other.userId)
			return false;
		return true;
	}
	
	/*@Override
	public int compareTo(Friend another) {
	//	if(this.lastchattime==0){
		 if(this.matchDate>another.getMatchDate()){
	            return -1;
	        }else if(this.matchDate<another.getMatchDate()){
	            return 1;
	        }   
		}
		else{
        if(this.lastchattime>another.getLastchattime()){
            return -1;
        }else if(this.lastchattime<another.getLastchattime()){
            return 1;
        }  
		}
		return 0;
	}*/
	
	
}
