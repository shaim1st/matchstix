package com.matchstixapp.modal;

import com.craterzone.cz_commons_lib.EncodingDecodingUtil;



public class FBUserInterest {
	private String id;
	private String category;
	private String name;
	
	
	public FBUserInterest() {
		super();
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getName() {
		return EncodingDecodingUtil.decodeString(name);
	}
	public void setName(String name) {
		this.name = EncodingDecodingUtil.encodeString(name);
	}
	
	
	
	

}
