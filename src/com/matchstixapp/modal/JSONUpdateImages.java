package com.matchstixapp.modal;

import java.util.ArrayList;

public class JSONUpdateImages {
	private ArrayList<UserImage> imageList;

	public ArrayList<UserImage> getList() {
		return imageList;
	}

	public void setList(ArrayList<UserImage> list) {
		this.imageList = list;
	}

	
}
