package com.matchstixapp.modal;

public class ForceUpgrade {

	private boolean upgradeAvailable;

	public boolean isUpgradeAvailable() {
		return upgradeAvailable;
	}

	public void setUpgradeAvailable(boolean upgradeAvailable) {
		this.upgradeAvailable = upgradeAvailable;
	}
}
