package com.matchstixapp.modal;

import com.craterzone.cz_commons_lib.EncodingDecodingUtil;

public class SuggestionDetail {
	

	private String lastName;
	private String profilePicURL;
	private int noOfCommonInterest;
	private int age;
	private long userId;
	private String firstName;
	private int noOfCommonFriend;
	

	public String getLastName() {
		return EncodingDecodingUtil.decodeString(lastName);
	}

	public void setLastName(String lastName) {
		this.lastName = EncodingDecodingUtil.encodeString(lastName);
	}

	public String getProfilePicURL() {
		return profilePicURL;
	}

	public void setProfilePicURL(String profilePicURL) {
		this.profilePicURL = profilePicURL;
	}

	public int getNoOfCommonInterest() {
		return noOfCommonInterest;
	}

	public void setNoOfCommonInterest(int noOfCommonInterest) {
		this.noOfCommonInterest = noOfCommonInterest;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getFirstName() {
		return EncodingDecodingUtil.decodeString(firstName);
	}

	public void setFirstName(String firstName) {
		this.firstName = EncodingDecodingUtil.encodeString(firstName);
	}

	public int getNoOfCommonFriend() {
		return noOfCommonFriend;
	}

	public void setNoOfCommonFriend(int noOfCommonFriend) {
		this.noOfCommonFriend = noOfCommonFriend;
	}
	@Override
	public boolean equals(Object o) {
		if (o == this) {
			return true;
		} else if (!(o instanceof SuggestionDetail)) {
			return false;
		}
		SuggestionDetail detail = (SuggestionDetail) o;
		return detail.userId == this.userId;
	}

	@Override
	public int hashCode() {
		return String.valueOf(userId).hashCode();
	}
}
