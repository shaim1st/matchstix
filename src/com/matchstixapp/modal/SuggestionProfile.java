package com.matchstixapp.modal;

import java.util.ArrayList;

import com.craterzone.cz_commons_lib.EncodingDecodingUtil;

public class SuggestionProfile {

	private int distance;
	private long lastLoginTime;
	private String firstName;
	private String aboutUser;
	private int age;
	private ArrayList<Photo> imageURLs;
	private int likePerDay;

	public ArrayList<Photo> getImageURLs() {
		return imageURLs==null?new ArrayList<Photo>():imageURLs;
	}

	public long getLastLoginTime() {
		return lastLoginTime;
	}

	/**
	 * @return distance in meters
	 */
	public int getDistance() {
		return distance;
	}
	public String getAboutUser() {
		return aboutUser==null?"":aboutUser;
	}
	public String getFirstName() {
		return firstName;
	}
	public int getAge() {
		return age;
	}

	public int getLikePerDay() {
		return likePerDay;
	}

	public void setLikePerDay(int likePerDay) {
		this.likePerDay = likePerDay;
	}
	
	
}
