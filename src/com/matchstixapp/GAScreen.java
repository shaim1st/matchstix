package com.matchstixapp;

public interface GAScreen {

	public static final String SPLASH_SCREEN = "Splash Screen Android";
	public static final String FULL_SCREEN_IMG_SCREEN = "Full Screen Image Android";
	
	public static final String MATCH_FOUND_SCREEN = "Match Screen Android";
	public static final String SEARCHED_FRIEND_PROFILE_SCREEN = "Profile Info Android";
	public static final String ONBOARD_SCREEN = "Login Page Android";
	public static final String SEARCHED_FRIEND_SCREEN = "Search Page Android";
	public static final String MENU_SCREEN = "Main Menu Android";
	public static final String PROFILE_SCREEN = "My Profile Android";
	public static final String EDIT_PROFILE_SCREEN = "Edit Profile Android";
	public static final String DELETE_PHOTO_SCREEN = "Delete Photo Screen Android";
	public static final String ALBUM_LIST_SCREEN = "Album List Screen Android";
	public static final String SETTINGS_SCREEN = "Search Settings Android";
	public static final String CHAT_LIST_SCREEN = "Chat List Android";
	public static final String CHAT_SCREEN = "Chat Screen Android";
	public static final String PRIVACY_SCREEN = "Privacy Policy Android";
	public static final String EDIT_SCREEN = "Edit Screen Android";
	public static final String VERIFY_MSISDN = "Verify MSISDN Android";
}
