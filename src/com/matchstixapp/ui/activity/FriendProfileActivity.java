package com.matchstixapp.ui.activity;

import static com.matchstixapp.utils.Constants.KEY_INTENT_UNMATCH_REPORT;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.TextView;

import com.craterzone.cz_commons_lib.StringUtil;
import com.matchstixapp.R;
import com.matchstixapp.ConnectivityController;
import com.matchstixapp.GAScreen;
import com.matchstixapp.MatchstixApplication;
import com.matchstixapp.db.DataController;
import com.matchstixapp.listener.FetchProfileListener;
import com.matchstixapp.listener.LastSeenListener;
import com.matchstixapp.listener.ReportUserListener;
import com.matchstixapp.listener.UnmatchUserListener;
import com.matchstixapp.modal.Friend;
import com.matchstixapp.modal.Photo;
import com.matchstixapp.modal.Roster;
import com.matchstixapp.modal.SuggestionProfile;
import com.matchstixapp.modal.User;
import com.matchstixapp.requester.FetchProfileRequester;
import com.matchstixapp.requester.LastSeenRequester;
import com.matchstixapp.requester.ReportUserRequester;
import com.matchstixapp.requester.UnmatchUserRequester;
import com.matchstixapp.ui.pager.FriendProfilePicsSlidePage;
import com.matchstixapp.ui.pager.PagerController;
import com.matchstixapp.ui.pager.SlideHorizontalPager;
import com.matchstixapp.ui.pager.SlidePage;
import com.matchstixapp.ui.pager.SlidePageAdapter;
import com.matchstixapp.utils.BackgroundExecutor;
import com.matchstixapp.utils.CustomToast;
import com.matchstixapp.utils.TypeFaceTextView;
import com.matchstixapp.utils.Utils;

public class FriendProfileActivity extends BaseFragmentActivity implements
		OnClickListener, OnMenuItemClickListener, UnmatchUserListener,
		ReportUserListener, FetchProfileListener, LastSeenListener {

	private PopupMenu popupMenu;
	private final static int ONE = 1;
	private final static int TWO = 2;
	// private final static int THREE = 3;

	private ImageView rightPop;
	private ImageView leftBack;
	private SlidePageAdapter mPagerAdapter;
	private PagerController controller = null;
	private SlideHorizontalPager mPager = null;

	private int slideLastPage = 0;
	private TypeFaceTextView tvName;
	private TypeFaceTextView tvAge;
	private TypeFaceTextView tvStatus;
	private TypeFaceTextView tvLastSeen;
	private TypeFaceTextView tvDistance;

	private SuggestionProfile profile;
	private Friend friend;
	private User user;

	private ProgressDialog progressDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		MatchstixApplication.tracker.setScreenName(GAScreen.PROFILE_SCREEN);
		setContentView(R.layout.activity_friend_profile);

		progressDialog = ProgressDialog.show(this, "", getResources()
				.getString(R.string.please_wait));

		long friendId = getIntent().getLongExtra("friendId", -1);
		if (friendId != -1) {
			friend = Roster.getInstance().getFriend(friendId);
		}
		user = DataController.getInstance().getUser();

		if (friend != null) {
			BackgroundExecutor.getInstance().execute(
					new LastSeenRequester(friend.getUserId()));
		}

		init();
	}

	@Override
	protected void onResume() {
		super.onResume();
		MatchstixApplication.getInstance().addUIListener(
				UnmatchUserListener.class, this);
		MatchstixApplication.getInstance().addUIListener(
				ReportUserListener.class, this);
		MatchstixApplication.getInstance().addUIListener(
				FetchProfileListener.class, this);
		MatchstixApplication.getInstance().addUIListener(
				LastSeenListener.class, this);
	}

	@Override
	protected void onPause() {
		super.onPause();
		MatchstixApplication.getInstance().removeUIListener(
				UnmatchUserListener.class, this);
		MatchstixApplication.getInstance().removeUIListener(
				ReportUserListener.class, this);
		MatchstixApplication.getInstance().removeUIListener(
				FetchProfileListener.class, this);
		MatchstixApplication.getInstance().removeUIListener(
				LastSeenListener.class, this);
	}

	public void init() {
		viewInit();
		popupViewInit();
		// popupViewInit();
	}

	public void viewInit() {

		tvName = (TypeFaceTextView) findViewById(R.id.name_friend);
		tvAge = (TypeFaceTextView) findViewById(R.id.age_friend);
		tvStatus = (TypeFaceTextView) findViewById(R.id.tv_status);
		tvLastSeen = (TypeFaceTextView) findViewById(R.id.tv_last_seen);
		tvDistance = (TypeFaceTextView) findViewById(R.id.tv_distance);

		findViewById(R.id.id_action_bar_mid_img_view).setVisibility(View.GONE);
		((TextView) findViewById(R.id.id_action_bar_mid_txt_view))
				.setText(friend.getFirstName() + "'s Profile");
		findViewById(R.id.id_action_bar_mid_txt_view).setVisibility(
				View.VISIBLE);

		findViewById(R.id.friend_info_layout).setVisibility(View.GONE);
	}

	private void popupViewInit() {
		popupMenu = new PopupMenu(this,
				findViewById(R.id.id_action_bar_right_img_view));
		popupMenu.getMenu().add(Menu.NONE, ONE, Menu.NONE,
				getResources().getString(R.string.pop_unmatch));
		popupMenu.getMenu().add(Menu.NONE, TWO, Menu.NONE,
				getResources().getString(R.string.pop_report));
		/*
		 * popupMenu.getMenu().add(Menu.NONE, THREE, Menu.NONE,
		 * getResources().getString(R.string.pop_block));
		 */
		popupMenu.setOnMenuItemClickListener(this);
		rightPop = (ImageView) findViewById(R.id.id_action_bar_right_img_view);
		leftBack = (ImageView) findViewById(R.id.id_action_bar_left_img_view);

		rightPop.setImageResource(R.drawable.ic_profile_info);
		leftBack.setImageResource(R.drawable.ic_back);

		rightPop.setOnClickListener(this);
		leftBack.setOnClickListener(this);

		if (profile == null && progressDialog != null
				&& progressDialog.isShowing()) {
			BackgroundExecutor.getInstance().execute(
					new FetchProfileRequester(friend.getUserId()));
		}
		setPager();
		setDetails();
	}

	private void setDetails() {
		if (profile != null) {
			String fname = friend.getFirstName();
			int age = profile.getAge();
			String status = profile.getAboutUser();
			if (!TextUtils.isEmpty(fname)) {
				tvName.setText(fname);
			}
			if (!(age < 0)) {
				tvAge.setText("" + age);
			}
			if (StringUtil.isEmpty(status)) {
				status = getString(R.string.default_status);
			}
			tvStatus.setText(status);
			tvDistance
					.setText(Utils.formatDistance(this, profile.getDistance()));
		}
	}

	private void setPager() {

		mPagerAdapter = new SlidePageAdapter(getSupportFragmentManager());
		addSlidePages();
		mPager = (SlideHorizontalPager) findViewById(R.id.horizontal_pager);
		controller = (PagerController) findViewById(R.id.pager_controller);
		mPager.setAdapter(mPagerAdapter);
		mPager.setPagerController(controller);
		controller.setPager(mPager);
		controller.setPageCount(mPagerAdapter.getCount());
		controller.setCurrentPage(slideLastPage);
		controller.setFillCircle(false);
		mPager.setPagerController(controller);
		mPager.setCurrentItem(slideLastPage);
	}

	private void addSlidePages() {

		if (profile != null) {
			SlidePage page = null;

			ArrayList<Photo> pics = profile.getImageURLs();
			for (int i = 0; i < pics.size(); i++) {
				page = new FriendProfilePicsSlidePage();
				HashMap<String, Object> pageData = new HashMap<String, Object>();
				pageData.put(FriendProfilePicsSlidePage.KEY_PIC_URL, pics
						.get(i).getImageURL());
				page.setData(pageData);
				mPagerAdapter.addPage(page);
			}

			mPagerAdapter.notifyDataSetChanged();
			return;
		}
	}

	@Override
	public boolean onMenuItemClick(MenuItem item) {
		switch (item.getItemId()) {
		case ONE: // unmatch
			BackgroundExecutor.getInstance().execute(
					new UnmatchUserRequester(user, friend));
			break;

		case TWO: // report
			BackgroundExecutor.getInstance().execute(
					new ReportUserRequester(user, friend));
			break;

		/*
		 * case THREE: // block Toast.makeText(this, item.getTitle(),
		 * Toast.LENGTH_SHORT).show(); break;
		 */
		}
		return false;
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.id_action_bar_right_img_view:
			popupMenu.show();
			Utils.showFadeinAnimation(this, rightPop);
			break;

		case R.id.id_action_bar_left_img_view:
			Utils.showFadeinAnimation(this, leftBack);
			finish();
			break;

		}
	}

	@Override
	public void onUnmatchSucceess(long friendId) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				CustomToast.showShortToast(getApplicationContext(),
						R.string.success);

				Intent intent = new Intent();
				intent.putExtra(KEY_INTENT_UNMATCH_REPORT, true);
				setResult(RESULT_OK, intent);
				finish();

				// remove disliked friend from the friend list and from db
				// DataController.getInstance().deleteFriend(friend);
				// Roster.getInstance().deleteFriend(friend.getUserId());

				// open the chat window
			}
		});
	}

	@Override
	public void onUnmatchFailed() {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if (!ConnectivityController
						.isNetworkAvailable(FriendProfileActivity.this)) {
					CustomToast.showShortToast(getApplicationContext(),
							R.string.no_internet);

				} else {
					CustomToast.showShortToast(getApplicationContext(),
							R.string.error_try_again);
				}
			}
		});
	}

	@Override
	public void onReportSucceess() {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				CustomToast.showShortToast(getApplicationContext(),
						R.string.success);

				Intent intent = new Intent();
				intent.putExtra(KEY_INTENT_UNMATCH_REPORT, true);
				setResult(RESULT_OK, intent);
				finish();
			}
		});
	}

	@Override
	public void onReportFailed() {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if (!ConnectivityController
						.isNetworkAvailable(FriendProfileActivity.this)) {
					CustomToast.showShortToast(getApplicationContext(),
							R.string.no_internet);

				} else {
					CustomToast.showShortToast(getApplicationContext(),
							R.string.error_try_again);
				}
			}
		});
	}

	@Override
	public void onInternetConnectionNotFound() {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				CustomToast.showShortToast(getApplicationContext(),
						R.string.no_internet);

				if (FriendProfileActivity.this.progressDialog != null
						&& FriendProfileActivity.this.progressDialog
								.isShowing()) {
					FriendProfileActivity.this.progressDialog.dismiss();
					finish();
				}
			}
		});
	}

	@Override
	public void onFetchProfileRequestSuccess(final SuggestionProfile profile) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				FriendProfileActivity.this.profile = profile;
				setPager();
				setDetails();

				if (FriendProfileActivity.this.progressDialog != null
						&& FriendProfileActivity.this.progressDialog
								.isShowing()) {
					FriendProfileActivity.this.progressDialog.dismiss();
				}
			}
		});
	}

	@Override
	public void onFetchProfileRequestFailed() {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				CustomToast.showShortToast(getApplicationContext(),
						R.string.unknown_error);

				if (FriendProfileActivity.this.progressDialog != null
						&& FriendProfileActivity.this.progressDialog
								.isShowing()) {
					FriendProfileActivity.this.progressDialog.dismiss();
					finish();
				}
			}
		});
	}

	// need to compare user id here .
	@Override
	public void onLastSeenRequestSuccess(final long userId,
			final long milliseconds) {
		if (friend.getUserId() == userId) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {

					String lastSeen = Utils.formatTime(
							FriendProfileActivity.this, milliseconds);
					tvLastSeen.setText(lastSeen);
				}
			});
		}
	}

	@Override
	public void onLastSeenRequestFailed() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onUnauthorizedListener() {
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				Utils.showUnAuthorizedDialog(FriendProfileActivity.this);
			}
		});
	}

	@Override
	public void onBlockedUser() {
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				Utils.showBlockedUserDialog(FriendProfileActivity.this);
			}
		});
	}
}
