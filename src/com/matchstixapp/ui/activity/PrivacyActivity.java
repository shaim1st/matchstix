package com.matchstixapp.ui.activity;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;

import com.matchstixapp.R;
import com.matchstixapp.GAScreen;
import com.matchstixapp.MatchstixApplication;
import com.matchstixapp.utils.TypeFaceTextView;
import com.matchstixapp.utils.Utils;

class PrivacyItem {
	String title;
	String desc;
	int id;

	PrivacyItem(int id, String title, String desc) {
		this.id = id;
		this.title = title;
		this.desc = desc;
	}
}

class PrivacyAdapter extends BaseAdapter {

	List<PrivacyItem> list;
	LayoutInflater inflator;

	PrivacyAdapter(Context context, List<PrivacyItem> list) {
		inflator = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.list = list;
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public PrivacyItem getItem(int pos) {
		return list.get(pos);
	}

	@Override
	public long getItemId(int pos) {
		return pos;
	}

	@Override
	public View getView(int pos, View container, ViewGroup parent) {
		View view = container;
		PrivacyItem item = getItem(pos);
		ViewHolder holder;
		if (view == null) {
			view = inflator.inflate(R.layout.privacy_item, null, false);
			ImageView img = (ImageView) view.findViewById(R.id.id_privacy_icon);
			TypeFaceTextView heading = (TypeFaceTextView) view.findViewById(R.id.id_privacy_heading);
			TypeFaceTextView description = (TypeFaceTextView) view.findViewById(R.id.id_privacy_dis);		
			holder = new ViewHolder();
			holder.img = img;
			holder.heading = heading;
			holder.description = description;
			 view.setTag(holder);
		} else {
			 holder = (ViewHolder) view.getTag();
		}
		
		holder.img.setImageResource(item.id);
		holder.heading.setText(item.title);
		holder.description.setText(item.desc);

		return view;
	}

	class ViewHolder {
		ImageView img;
		TypeFaceTextView heading;
		TypeFaceTextView description;
	}

}

public class PrivacyActivity extends BaseActivity {

	ListView listView;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		MatchstixApplication.tracker.setScreenName(GAScreen.PRIVACY_SCREEN);
		setContentView(R.layout.privacy_layout);
		final ImageView cancel = (ImageView) findViewById(R.id.id_photo_cancel);
		listView = (ListView) findViewById(R.id.id_privacy_list);	
		listView.setClickable(false);
		cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				Utils.showFadeinAnimation(PrivacyActivity.this,cancel );
				finish();
			}
		});
		setupData();
	}
	public void setupData() {
		List<PrivacyItem> list = new ArrayList<PrivacyItem>();
		Resources res = getResources();
		list.add(new PrivacyItem(R.drawable.ic_fb_nopost, res.getString(R.string.privacy_ploicy_1_title), res.getString(R.string.privacy_ploicy_1_desc)));
		list.add(new PrivacyItem(R.drawable.ic_vision, res.getString(R.string.privacy_ploicy_2_title), res.getString(R.string.privacy_ploicy_2_desc)));
		list.add(new PrivacyItem(R.drawable.ic_cannot_contact, res.getString(R.string.privacy_ploicy_3_title), res.getString(R.string.privacy_ploicy_3_desc)));
		list.add(new PrivacyItem(R.drawable.ic_location, res.getString(R.string.privacy_ploicy_4_title), res.getString(R.string.privacy_ploicy_4_desc)));
		list.add(new PrivacyItem(R.drawable.ic_free_trial, res.getString(R.string.privacy_ploicy_5_title), res.getString(R.string.privacy_ploicy_5_desc)));
		PrivacyAdapter adapter = new PrivacyAdapter(this, list);
		listView.setAdapter(adapter);
		adapter.notifyDataSetChanged();
	}
}
