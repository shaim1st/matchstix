package com.matchstixapp.ui.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.view.Window;

import com.matchstixapp.R;
import com.matchstixapp.GAScreen;
import com.matchstixapp.MatchstixApplication;
import com.matchstixapp.db.DataController;
import com.matchstixapp.listener.FetchProfileListener;
import com.matchstixapp.listener.FriendsDownloadListener;
import com.matchstixapp.listener.Keys;
import com.matchstixapp.modal.Friend;
import com.matchstixapp.modal.Roster;
import com.matchstixapp.modal.SuggestionProfile;
import com.matchstixapp.requester.FetchProfileRequester;
import com.matchstixapp.requester.FriendsDownloadRequester;
import com.matchstixapp.ui.fragments.LetsTalkFragment;
import com.matchstixapp.ui.fragments.LetsTalkFragment.LetsTalkActionListener;
import com.matchstixapp.utils.BackgroundExecutor;
import com.matchstixapp.utils.CustomToast;
import com.matchstixapp.utils.Utils;
import com.matchstixapp.xmpplibrary.ui.ChatViewer;
import com.xabber.android.data.LogManager;

public class LetsTalkActivity extends BaseFragmentActivity implements
		LetsTalkActionListener, FriendsDownloadListener, FetchProfileListener {

	private static final String TAG = LetsTalkActivity.class.getSimpleName();
	private Friend friend;
	private long id;
	private ProgressDialog progressDialog;

	public static void createIntent(Activity activity, long friendId) {
		Intent intent = new Intent(activity, LetsTalkActivity.class);
		intent.putExtra(Keys.KEY_FRIEND_ID, friendId);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
				| Intent.FLAG_ACTIVITY_CLEAR_TASK);
		ActivityCompat.startActivity(
				activity,
				intent,
				ActivityOptionsCompat.makeCustomAnimation(activity,
						R.anim.grow_from_bottom, R.anim.grow_from_top)
						.toBundle());
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		MatchstixApplication.tracker.setScreenName(GAScreen.MATCH_FOUND_SCREEN);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_lets_talk);
		overridePendingTransition(R.anim.grow_from_bottom, R.anim.grow_from_top);
		/*
		 * progressDialog = ProgressDialog.show( this, "",
		 * getResources().getString(R.string.please_wait));
		 */

		// finding the friend
		id = getIntent().getLongExtra(Keys.KEY_FRIEND_ID, -1);
		if (id != -1) {
			// friend=Roster.getInstance().getFriend(id);
			BackgroundExecutor.getInstance().execute(
					new FetchProfileRequester(id));
			DataController.getInstance().setNewFriendList(id);

		} else {
			Log.i("LetsTalk-Missing", "Friend-ID missing");
			startActivity(new Intent(this, SplashActivity.class));
			// progressDialog.dismiss();
			finish();
		}

		// if(friend == null) {
		// Log.i("LetsTalk-No Such Friend", "Friend list not updated");
		//
		// //update friend list
		// startSyncFriends(false);
		// friend = Roster.getInstance().getFriend(id);
		//
		// if(friend == null) {
		// Log.i("LetsTalk-No Such Friend", "Friend list not updated");
		// startActivity(new Intent(this,
		// SplashActivity.class));
		// }
		// }

		FragmentManager fm = getSupportFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();
		ft.add(R.id.fragment_container, new LetsTalkFragment(),
				LetsTalkFragment.TAG);
		ft.commit();
	}

	@Override
	public void onClickStartTalk(View view) {
		if (friend != null) {
			DataController.getInstance().removeNewFriend(friend.getUserId());
			startActivity(ChatViewer.createIntent(LetsTalkActivity.this,
					DataController.getInstance().getFullJid(),
					String.valueOf(friend.getUserId())));
			/*
			 * Intent intent = new Intent(this, ChatActivity.class);
			 * intent.putExtra(Keys.KEY_FRIEND_ID, friend.getUserId());
			 * startActivity(intent);
			 */

		}
		finish();
	}

	@Override
	public void onClickSendMsg(View view) {
		onClickStartTalk(view);
	}

	@Override
	public void onClickKeepPlaying(View view) {
		/*
		 * startActivity(new Intent(this, SplashActivity.class));
		 */
		finish();
	}

	@Override
	protected void onResume() {
		super.onResume();
		MatchstixApplication.getInstance().addUIListener(
				FriendsDownloadListener.class, this);
		MatchstixApplication.getInstance().addUIListener(
				FetchProfileListener.class, this);
	}

	@Override
	protected void onPause() {
		super.onPause();
		MatchstixApplication.getInstance().removeUIListener(
				FriendsDownloadListener.class, this);
		MatchstixApplication.getInstance().removeUIListener(
				FetchProfileListener.class, this);
	}

	public void startSyncFriends(final boolean showMsg) {

		FriendsDownloadRequester requester = new FriendsDownloadRequester();
		if (!requester.isPending()) {
			BackgroundExecutor.getInstance().execute(requester);

		}
	}

	@Override
	public void onFriendsDownloadSucceess() {
		LogManager.d(TAG, "********* onFriendsDownloadSucceess()");
	}

	@Override
	public void onFriendsDownloadFailed(String msg) {
		LogManager.d(TAG, "********* onFriendsDownloadFailed()");

	}

	@Override
	public void onInternetConnectionNotFound() {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				CustomToast.showShortToast(getApplicationContext(),
						R.string.no_internet);
				if (LetsTalkActivity.this.progressDialog != null
						&& LetsTalkActivity.this.progressDialog.isShowing()) {
					LetsTalkActivity.this.progressDialog.dismiss();
					finish();
				}
			}
		});
	}

	@Override
	public void onFetchProfileRequestSuccess(final SuggestionProfile profile) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if (profile != null) {
					friend = Roster.getInstance().getFriend(id);
					((LetsTalkFragment) getSupportFragmentManager()
							.findFragmentByTag(LetsTalkFragment.TAG))
							.setProfileAvatars(DataController.getInstance()
									.getUser(), profile);

					if (LetsTalkActivity.this.progressDialog != null
							&& LetsTalkActivity.this.progressDialog.isShowing()) {
						LetsTalkActivity.this.progressDialog.dismiss();
					}

				} else {
					onClickKeepPlaying(null);
					if (LetsTalkActivity.this.progressDialog != null
							&& LetsTalkActivity.this.progressDialog.isShowing()) {
						LetsTalkActivity.this.progressDialog.dismiss();
					}
				}
			}
		});
	}

	@Override
	public void onFetchProfileRequestFailed() {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				CustomToast.showShortToast(getApplicationContext(),
						R.string.unknown_error);

				if (LetsTalkActivity.this.progressDialog != null
						&& LetsTalkActivity.this.progressDialog.isShowing()) {
					LetsTalkActivity.this.progressDialog.dismiss();
					finish();
				}
			}
		});
	}

	@Override
	public void onUnauthorizedListener() {
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				if (progressDialog != null) {
					progressDialog.dismiss();
				}
				Utils.showUnAuthorizedDialog(LetsTalkActivity.this);
			}
		});
	}

	@Override
	public void onBlockedUser() {
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				if (progressDialog != null) {
					progressDialog.dismiss();
				}
				Utils.showBlockedUserDialog(LetsTalkActivity.this);
			}
		});
	}

	@Override
	protected void onDestroy() {
		startSyncFriends(false);
		super.onDestroy();
	}

}
