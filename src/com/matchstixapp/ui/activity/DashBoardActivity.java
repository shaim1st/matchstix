package com.matchstixapp.ui.activity;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.DrawerLayout.DrawerListener;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.craterzone.cz_commons_lib.StringUtil;
import com.craterzone.czimageloader.universalImageLoader.UniversalImageLoaderUtil;
import com.facebook.appevents.AppEventsLogger;
import com.matchstixapp.GAScreen;
import com.matchstixapp.MatchstixApplication;
import com.matchstixapp.R;
import com.matchstixapp.PNV.LocationDAO;
import com.matchstixapp.db.DataController;
import com.matchstixapp.db.MatchstixPreferences;
import com.matchstixapp.language.LastChatListener;
import com.matchstixapp.listener.DashBoardDrawerListener;
import com.matchstixapp.listener.FBProfilePicsDownloadListener;
import com.matchstixapp.listener.FriendLikeListner;
import com.matchstixapp.listener.FriendsDownloadListener;
import com.matchstixapp.listener.MatchstixDialogListener;
import com.matchstixapp.listener.OnBackPressedListener;
import com.matchstixapp.listener.ProfileListener;
import com.matchstixapp.listener.ReportUserListener;
import com.matchstixapp.listener.SettingsUpdateListener;
import com.matchstixapp.listener.UnmatchUserListener;
import com.matchstixapp.listener.UpdateUserProfileDetailsListener;
import com.matchstixapp.modal.AlbumPhoto;
import com.matchstixapp.modal.DrawerItemList;
import com.matchstixapp.modal.FBAlbumPhotos;
import com.matchstixapp.modal.Friend;
import com.matchstixapp.modal.JSONUpdateImages;
import com.matchstixapp.modal.Photo;
import com.matchstixapp.modal.Roster;
import com.matchstixapp.modal.SuggestionDetail;
import com.matchstixapp.modal.SuggestionProfile;
import com.matchstixapp.modal.User;
import com.matchstixapp.modal.UserImage;
import com.matchstixapp.requester.FBProfilePicsRequester;
import com.matchstixapp.requester.FriendsDownloadRequester;
import com.matchstixapp.requester.PushTokenRegistrationRequester;
import com.matchstixapp.requester.SuggestionProfileRequester;
import com.matchstixapp.requester.SuggestionsRequester;
import com.matchstixapp.requester.UpdateUserProfileDetailsRequester;
import com.matchstixapp.services.FBWebServices;
import com.matchstixapp.ui.adapter.DrawerLeftListAdapter;
import com.matchstixapp.ui.adapter.DrawerRightListAdapter;
import com.matchstixapp.ui.fragments.EditFragment;
import com.matchstixapp.ui.fragments.InviteFragment;
import com.matchstixapp.ui.fragments.MatchFragment;
import com.matchstixapp.ui.fragments.MessageFragment;
import com.matchstixapp.ui.fragments.MyProfileFragment;
import com.matchstixapp.ui.fragments.SettingsFragment;
import com.matchstixapp.utils.BackgroundExecutor;
import com.matchstixapp.utils.BlockedPopup;
import com.matchstixapp.utils.Constants;
import com.matchstixapp.utils.CustomToast;
import com.matchstixapp.utils.LocationTracker;
import com.matchstixapp.utils.TypeFaceTextView;
import com.matchstixapp.utils.Utils;
import com.matchstixapp.xmpplibrary.ui.ChatViewer;
import com.pushbots.push.Pushbots;
import com.xabber.android.data.LogManager;
import com.xabber.android.data.extension.vcard.VCardManager;
import com.xabber.android.data.message.MessageManager;
import com.xabber.android.data.message.OnChatChangedListener;

@SuppressLint("DefaultLocale")
public class DashBoardActivity extends BaseFragmentActivity implements
		OnClickListener, DrawerListener, OnChatChangedListener,
		UpdateUserProfileDetailsListener, UnmatchUserListener,
		FriendsDownloadListener, FBProfilePicsDownloadListener,
		ReportUserListener, SettingsUpdateListener, ProfileListener,
		FriendLikeListner,LastChatListener {
	public static final byte OPTION_MATCHSTIX = 1;
	public static final byte OPTION_MESSAGE = 2;
	public static final byte OPTION_SETTINGS = 3;
	public static final byte OPTION_INVITE = 4;
	private SuggestionsRequester suggestionsRequester;

	private static final String TAG = DashBoardActivity.class.getName();
	private ListView mLeftDrawerList;
	private ListView mRightDrawerList;
	private DrawerLayout mLeftDrawerLayout;
	private DrawerLayout mRightDrawerLayout;
	private DrawerLeftListAdapter mDrawerLeftAdapter;
	private DrawerRightListAdapter mDrawerRightAdapter;
	ArrayList<DrawerItemList> mDrawerItems;
	private View actionBar;
	private LocationTracker locationTracker;
	private boolean isProfileShow = false;
	private FriendsDownloadRequester requester;
	public static boolean userBlock = false;
	public static long countDownTime=24*60*60*100;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		MatchstixApplication.tracker.setScreenName(GAScreen.MENU_SCREEN);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_dash_board);
		ivRightAction = ((ImageView) findViewById(R.id.id_action_bar_right_img_view));
		actionBar = findViewById(R.id.id_action_bar_layout);
		if (suggestionsRequester == null) {
			suggestionsRequester = new SuggestionsRequester(0);
		}
		locationTracker = new LocationTracker(this);
		requester = new FriendsDownloadRequester();
		findViewById(R.id.id_action_bar_layout).setBackgroundResource(
				R.color.transparent);
		initDrawer();
		findViewById(R.id.id_action_bar_left_img_view).setOnClickListener(this);
		findViewById(R.id.id_action_bar_right_img_view)
				.setOnClickListener(this);
		findViewById(R.id.id_list_header_layout).setOnClickListener(this);
		onDrawerOptionClicked(OPTION_MATCHSTIX);
		startSyncFriends();
		if (!DataController.getInstance().getUser().getImageSync()
				|| DataController.getInstance().getUser().isExpire()) {
			uploadProfilePics();
			DataController.getInstance().getUser().setExpire(false);
		} else {
			downloadProfilePics();
		}
		MatchstixApplication.getInstance().addUIListener(
				OnChatChangedListener.class, this);
		MatchstixApplication.getInstance().addUIListener(
				UnmatchUserListener.class, this);
		MatchstixApplication.getInstance().addUIListener(
				FriendsDownloadListener.class, this);
		MatchstixApplication.getInstance().addUIListener(
				ReportUserListener.class, this);
		MatchstixApplication.getInstance().addUIListener(
				SettingsUpdateListener.class, this);
		MatchstixApplication.getInstance().addUIListener(ProfileListener.class,
				this);
		MatchstixApplication.getInstance().addUIListener(
				FriendLikeListner.class, this);
		if (MatchstixPreferences.getInstance().isUpgradeAvailable()) {
			showUpgradeDialog();
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (!locationTracker.isGPSEnabled()) {
			locationTracker.showSettingsAlert();
		}
		MatchstixApplication.getInstance().addUIListener(
				UpdateUserProfileDetailsListener.class, this);
		MatchstixApplication.getInstance().addUIListener(
				FBProfilePicsDownloadListener.class, this);
		mLeftDrawerLayout.setDrawerListener(this);
		mRightDrawerLayout.setDrawerListener(this);
		mDrawerLeftAdapter.notifyDataSetChanged();
		mDrawerRightAdapter.notifyDataSetChanged();
		int totalCount = MessageManager.getInstance()
				.getTotalUnreadMessageCount();
		resetRightActionBarAction((totalCount > 0) ? R.drawable.ic_notification
				: R.drawable.ic_massage, this);
		String deviceToken = MatchstixPreferences.getInstance().getGCMRegId();
		if (StringUtil.isEmpty(deviceToken)) {
			deviceToken = Pushbots.sharedInstance().regID();
			MatchstixPreferences.getInstance().saveGCMRegId(deviceToken);
		}
		if (!MatchstixPreferences.getInstance().isGCMTokenRegistered()
				&& !StringUtil.isEmpty(deviceToken)) {
			BackgroundExecutor.getInstance().execute(
					new PushTokenRegistrationRequester(deviceToken));
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		MatchstixApplication.getInstance().removeUIListener(
				UpdateUserProfileDetailsListener.class, this);
		MatchstixApplication.getInstance().removeUIListener(
				FBProfilePicsDownloadListener.class, this);
		mLeftDrawerLayout.setDrawerListener(null);
	}

	private void showUpgradeDialog() {
		Utils.showUpgradeDialog(this, new MatchstixDialogListener() {

			@Override
			public void onButtonTwoClick() {

			}

			@Override
			public void onButtonOneClick() {
				final String appPackageName = getPackageName();
				try {
					startActivity(new Intent(Intent.ACTION_VIEW, Uri
							.parse(Constants.PLAYSTORE_URL + appPackageName)));
				} catch (android.content.ActivityNotFoundException anfe) {
					startActivity(new Intent(Intent.ACTION_VIEW, Uri
							.parse(Constants.PLAYSTORE_URL + appPackageName)));
				}
			}
		});
	}

	@Override
	public void onBackPressed() {
		if (mLeftDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
			mLeftDrawerLayout.closeDrawer(Gravity.LEFT);
			return;
		}
		if (mRightDrawerLayout.isDrawerOpen(Gravity.RIGHT)) {
			mRightDrawerLayout.closeDrawer(Gravity.RIGHT);
			return;
		}
		FragmentManager fm = getSupportFragmentManager();
		Fragment f = fm.findFragmentByTag(EditFragment.TAG);

		if (f != null && f instanceof OnBackPressedListener) {
			((OnBackPressedListener) f).onBackPressed();
		}
		super.onBackPressed();
	}


	/*
	 * Managing both left & right drawer layout.
	 */
	private void initDrawer() {
		mLeftDrawerList = (ListView) findViewById(R.id.left_drawer);
		mLeftDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mRightDrawerList = (ListView) findViewById(R.id.id_right_drawer);
		mRightDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerItems = new ArrayList<DrawerItemList>();

		/*
		 * adding header to the left & right drawer
		 */
		View headerLeft = getLayoutInflater().inflate(
				R.layout.left_list_header_item, mLeftDrawerList, false);
		mLeftDrawerList.addHeaderView(headerLeft, null, false);
		View headerRight = getLayoutInflater().inflate(
				R.layout.right_list_header_item, mRightDrawerList, false);
		mRightDrawerList.addHeaderView(headerRight, null, false);
		mDrawerItems.add(new DrawerItemList("match",
				R.drawable.ic_gotomatchstix, getString(R.string.app_name),
				new MatchFragment()
						.setSuggestionRequester(suggestionsRequester)));
		mDrawerItems.add(new DrawerItemList("message",
				R.drawable.ic_starttalking, getString(R.string.message),
				new MessageFragment()));
		mDrawerItems.add(new DrawerItemList("setting", R.drawable.ic_setting,
				getString(R.string.setting), new SettingsFragment()));
		mDrawerItems.add(new DrawerItemList("invite",
				R.drawable.ic_invite_frnd, getString(R.string.invite),
				new InviteFragment()));
		
		mDrawerLeftAdapter = new DrawerLeftListAdapter(this, mDrawerItems);
		mDrawerRightAdapter = new DrawerRightListAdapter(this);
		mLeftDrawerList
				.setOnItemClickListener(new AdapterView.OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> parent, View view,
							int position, long id) {
						onDrawerOptionClicked(position);
					}
				});

		mRightDrawerList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					final int position, long id) {
				mLeftDrawerLayout.closeDrawer(Gravity.RIGHT);
				if (userBlock) {
					BlockedPopup.getInstance().showonDrawerBlockedUserDialog(DashBoardActivity.this);
					//Log.d(TAG,"UserBlocked="+userBlock);
				} else {
					startChatWindow((Friend)parent.getItemAtPosition(position));
					mDrawerRightAdapter.setSelcted(position);
					if (view.findViewById(R.id.id_left_msg_indicator_view)
							.getVisibility() == View.VISIBLE) {
						view.findViewById(R.id.id_left_msg_indicator_view)
								.setVisibility(View.INVISIBLE);
						int count = MessageManager.getInstance()
								.getTotalUnreadMessageCount();
						resetRightActionBarAction(
								count > 0 ? R.drawable.ic_notification
										: R.drawable.ic_massage,
								DashBoardActivity.this);
					}
				}
			}
		});
		mLeftDrawerList.setAdapter(mDrawerLeftAdapter);
		mRightDrawerList.setAdapter(mDrawerRightAdapter);
		showAvatarAndNumber();
	}

	private ImageView ivRightAction;
	private static final long DELAY = 200;

	public void onDrawerOptionClicked(int position) {
		mDrawerLeftAdapter.setSelcted(position);
		mLeftDrawerLayout.closeDrawer(Gravity.LEFT);
		Fragment fragment = null;
		if (position == OPTION_MATCHSTIX) {
			
			actionBar.findViewById(R.id.id_action_bar_mid_img_view)
					.setVisibility(View.VISIBLE);
			actionBar.findViewById(R.id.id_action_bar_mid_txt_view)
					.setVisibility(View.GONE);
			suggestionsRequester = new SuggestionsRequester(0);
			// get suggestion from 0
			fragment = new MatchFragment()
					.setSuggestionRequester(suggestionsRequester);
			int totalCount = MessageManager.getInstance()
					.getTotalUnreadMessageCount();
			resetRightActionBarAction(
					(totalCount > 0) ? R.drawable.ic_notification
							: R.drawable.ic_massage, this);
		} else if (position == OPTION_MESSAGE) {
			
			mDrawerRightAdapter.setSelcted(0);
			mRightDrawerLayout.openDrawer(Gravity.RIGHT);
			return;
		} else if (position == OPTION_SETTINGS) {
			actionBar.findViewById(R.id.id_action_bar_mid_img_view)
					.setVisibility(View.GONE);
			((TextView) actionBar.findViewById(R.id.id_action_bar_mid_txt_view))
					.setText(getString(R.string.settings));
			actionBar.findViewById(R.id.id_action_bar_mid_txt_view)
					.setVisibility(View.VISIBLE);
			fragment = new SettingsFragment();

		} else if (position == OPTION_INVITE) {
			AppEventsLogger logger= AppEventsLogger.newLogger(this);
			logger.logEvent("Completed a Invite");
			MatchstixApplication.getInstance().postDelayed(new Runnable() {

				@Override
				public void run() {
					Utils.showInviteOptions(DashBoardActivity.this);
				}
			}, DELAY);
			return;

		}
		int count = MessageManager.getInstance().getTotalUnreadMessageCount();
		if (count > 0) {
			ivRightAction.setImageResource(R.drawable.ic_notification);
		} else {
			ivRightAction.setImageResource(R.drawable.ic_massage);
		}

		final Fragment f = fragment;
		fragment = null;
		MatchstixApplication.getInstance().postDelayed(new Runnable() {

			@Override
			public void run() {
				FragmentManager fm = getSupportFragmentManager();
				if (fm.getBackStackEntryCount() > 0) {
					fm.popBackStack(null,
							FragmentManager.POP_BACK_STACK_INCLUSIVE);
				}
				FragmentTransaction ft = fm.beginTransaction();
				if (f instanceof SettingsFragment) {
					ft.replace(R.id.fragment_container, f, SettingsFragment.TAG);
				} else {
					ft.replace(R.id.fragment_container, f);
				}
				// ft.commit();
				// done by navrattan to remove crash
				// http://stackoverflow.com/questions/14177781/java-lang-illegalstateexception-can-not-perform-this-action-after-onsaveinstanc
				ft.commitAllowingStateLoss();

			}
		}, DELAY);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.id_action_bar_left_img_view:
			mLeftDrawerLayout.openDrawer(Gravity.LEFT);

			break;
		case R.id.id_left_header_image_view:
			isProfileShow = true;
			mLeftDrawerLayout.closeDrawer(Gravity.LEFT);
			((ImageView) findViewById(R.id.id_action_bar_right_img_view))
					.setImageResource(R.drawable.ic_edit_profile);
			MatchstixApplication.getInstance().postDelayed(new Runnable() {

				@Override
				public void run() {
					FragmentTransaction ft = getSupportFragmentManager()
							.beginTransaction();
					ft.replace(R.id.fragment_container, new MyProfileFragment());
					ft.commit();
				}
			}, DELAY);
			break;
		case R.id.id_action_bar_right_img_view:
			mDrawerRightAdapter.setSelcted(0);
			mRightDrawerLayout.openDrawer(Gravity.RIGHT);
			if(userBlock){
				BlockedPopup.getInstance().showonDrawerBlockedUserDialog(this);
				return;
			}
			int totalCount = MessageManager.getInstance()
					.getTotalUnreadMessageCount();
			resetRightActionBarAction(
					(totalCount > 0) ? R.drawable.ic_notification
							: R.drawable.ic_massage, this);
			if (!requester.isPending()) {
				BackgroundExecutor.getInstance().execute(requester);
			}
			break;
		}
	}

	public void showAvatarAndNumber() {
		User user = DataController.getInstance().getUser();
		ImageView avatar = (ImageView) findViewById(R.id.id_left_header_image_view);
		TypeFaceTextView tvTitle = (TypeFaceTextView) findViewById(R.id.row_title);
		tvTitle.setText(user.getFirstName());
		avatar.setOnClickListener(this);
		final String avatarUrl = user.getAvatarURL();
		if (!TextUtils.isEmpty(avatarUrl)) {
			UniversalImageLoaderUtil.loadImageWithDefaultImage(avatarUrl,
					avatar, null, R.drawable.default_profile);
		}
		View viewNumber = findViewById(R.id.view_number);
		if (!TextUtils.isEmpty(user.getNumber())) {
			viewNumber.setVisibility(View.VISIBLE);
			ImageView ivFlag = (ImageView) viewNumber
					.findViewById(R.id.iv_flag);
			TypeFaceTextView tvNumber = (TypeFaceTextView) viewNumber
					.findViewById(R.id.tv_number);
			tvNumber.setText(user.getNumber());
			int id = getResources().getIdentifier(
					LocationDAO.getInstance(this).getCountryCode(user.getCC())
							.toLowerCase(), "drawable", getPackageName());
			ivFlag.setImageResource(id);
		} else {
			viewNumber.setVisibility(View.INVISIBLE);
		}
	}

	public int getActionBarHeight() {
		int actionBarHeight = 0;
		TypedValue tv = new TypedValue();
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			if (getTheme().resolveAttribute(android.R.attr.actionBarSize, tv,
					true))
				actionBarHeight = TypedValue.complexToDimensionPixelSize(
						tv.data, getResources().getDisplayMetrics());
		} else {
			actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data,
					getResources().getDisplayMetrics());
		}
		Log.d(TAG, "Action bar height: " + actionBarHeight);
		return actionBarHeight;
	}
	public Friend getFriend(long id) {
		ArrayList<Friend> friends=Roster.getInstance().getFriendList();
		for (int i = 0; i < friends.size(); i++) {
			if (friends.get(i).getUserId() == id) {
				return friends.get(i);
			}
		}
		return null;
	}
	private void startChatWindow(Friend frd) {
	//	Roster.getInstance().setFriendChatTime(frd);
		Friend friend =frd;//Roster.getInstance().getFriend(position - 1);
		if (friend == null) {
			return;
		}
		DataController.getInstance().removeNewFriend(friend.getUserId());
		startActivity(ChatViewer.createIntent(DashBoardActivity.this,
				DataController.getInstance().getFullJid(),
				String.valueOf(friend.getUserId())));
	}

	public void resetRightActionBarAction(int imageResId,
			OnClickListener listener) {
		if (imageResId == R.drawable.ic_edit_profile) {
			// myprofile
			actionBar.findViewById(R.id.id_action_bar_mid_img_view)
					.setVisibility(View.GONE);
			((TextView) actionBar.findViewById(R.id.id_action_bar_mid_txt_view))
					.setText(getString(R.string.profile));
			actionBar.findViewById(R.id.id_action_bar_mid_txt_view)
					.setVisibility(View.VISIBLE);

		} else if (imageResId == R.drawable.ic_tick) {
			// edit myprofile
			actionBar.findViewById(R.id.id_action_bar_mid_img_view)
					.setVisibility(View.GONE);
			((TextView) actionBar.findViewById(R.id.id_action_bar_mid_txt_view))
					.setText(getString(R.string.edit_profile));
			actionBar.findViewById(R.id.id_action_bar_mid_txt_view)
					.setVisibility(View.VISIBLE);
		}

		ImageView imageview = (ImageView) findViewById(R.id.id_action_bar_right_img_view);
		imageview.setImageResource(imageResId);
		// if (listener != null) {
		imageview.setOnClickListener(listener);
		// }
	}

	public void resetLeftActionBarAction(int imageResId,
			OnClickListener listener) {
		ImageView imageview = (ImageView) findViewById(R.id.id_action_bar_left_img_view);
		imageview.setImageResource(imageResId);
		if (listener != null) {
			imageview.setOnClickListener(listener);
		}
	}

	@SuppressWarnings("deprecation")
	public void updateTitleBarBG(boolean remove) {
		if (remove) {
			actionBar.setBackgroundDrawable(null);
		} else {
			actionBar.setBackgroundResource(R.drawable.app_bg_gradient);
		}
	}

	@SuppressWarnings("deprecation")
	public void updateBG(boolean remove) {
		if (remove) {
			mLeftDrawerLayout.setBackgroundDrawable(null);
		} else {
			mLeftDrawerLayout.setBackgroundResource(R.drawable.app_bg_gradient);
		}
	}

	private void startSyncFriends() {
		requester = new FriendsDownloadRequester();
		if (!requester.isPending()) {
			BackgroundExecutor.getInstance().execute(requester);
		}
	}

	private void uploadProfilePics() {
		ArrayList<AlbumPhoto> photos = DataController.getInstance()
				.getUserProfilePics();
		if (photos != null && photos.size() > 0) {
			return;
		}
		BackgroundExecutor.getInstance().execute(new FBProfilePicsRequester());
	}

	private void downloadProfilePics() {
		ArrayList<AlbumPhoto> photos = DataController.getInstance()
				.getUserProfilePics();
		if (photos != null && photos.size() > 0) {
			return;
		}
		SuggestionDetail detail = new SuggestionDetail();
		detail.setUserId(DataController.getInstance().getUser().getId());
		BackgroundExecutor.getInstance().execute(
				new SuggestionProfileRequester(detail));
	}

	protected void uploadPics() {
		ArrayList<AlbumPhoto> photos = DataController.getInstance()
				.getUserProfilePics();
		ArrayList<UserImage> list = new ArrayList<UserImage>();
		for (int i = 0; i < photos.size(); i++) {
			UserImage image = new UserImage();
			String url = photos.get(i).getRemoteAvatar();
			if (url == null) {
				url = FBWebServices.getPicURL(photos.get(i).getId());
			}
			image.setImageURL(url);
			image.setProfilePic(i == 0);
			list.add(image);
		}
		JSONUpdateImages imageRequest = new JSONUpdateImages();
		imageRequest.setList(list);
		BackgroundExecutor.getInstance().execute(
				new UpdateUserProfileDetailsRequester(null, imageRequest));

	}

	@Override
	public void onDrawerClosed(View arg0) {

	}

	@Override
	public void onDrawerOpened(View view) {
		mDrawerRightAdapter.setSelcted(0);
		Fragment fragment = getSupportFragmentManager().findFragmentByTag(
				EditFragment.TAG);
		if (fragment != null) {
			// Current fragment : EditFragment
			if (fragment instanceof DashBoardDrawerListener) {
				((DashBoardDrawerListener) fragment).onDrawerOpened(view);
			} else {
				Log.e(EditFragment.TAG, "Must implement "
						+ DashBoardDrawerListener.class.getSimpleName()
						+ " to receive callbacks");
			}
		}

		fragment = getSupportFragmentManager().findFragmentByTag(
				SettingsFragment.TAG);
		if (fragment != null) {
			// Current fragment : SettingsFragment
			if (fragment instanceof DashBoardDrawerListener) {
				((DashBoardDrawerListener) fragment).onDrawerOpened(view);
			} else {
				Log.e(SettingsFragment.TAG, "Must implement "
						+ DashBoardDrawerListener.class.getSimpleName()
						+ " to receive callbacks");
			}
		}
	}

	@Override
	public void onDrawerSlide(View view, float arg1) {

	}

	@Override
	public void onDrawerStateChanged(int arg0) {

	}

	@Override
	public void onUserProfileDetailsUpdated(boolean userpic, boolean aboutme) {
		AppEventsLogger logger= AppEventsLogger.newLogger(this);
		logger.logEvent("Completed a Profile Update");
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if (getSupportFragmentManager().findFragmentByTag(
						EditFragment.TAG) != null) {
					CustomToast.showShortToast(getApplicationContext(),
							R.string.msg_profile_details_updated);
				}
				DataController dataController = DataController.getInstance();
				VCardManager.getInstance().updateVCard(
						dataController.getFullJid(), "",
						dataController.getUser().getAvatarURL(),
						dataController.getUser().getAvatarURL(),
						dataController.getUser().getFirstName());
			}
		});
		DashBoardActivity.userBlock=false;
	}

	@Override
	public void onUserProfileDetailsUpdationFailed() {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if (getSupportFragmentManager().findFragmentByTag(
						EditFragment.TAG) != null) {
					CustomToast.showShortToast(getApplicationContext(),
							R.string.msg_profile_details_not_updated);
				}
			}
		});
		DashBoardActivity.userBlock=false;
	}

	@Override
	public void onChatChanged(String account, String user, boolean incoming) {
		try{
			Roster.getInstance().setFriendChatTimeOnNotification(Long.parseLong(user.substring(0,user.indexOf("@")))
					,System.currentTimeMillis());
			}catch(Exception e){
				Log.d("DrawerRightListAdapter",e.toString());
			}
		
		if (incoming) {
			if (!isProfileShow) {
				resetRightActionBarAction(R.drawable.ic_notification, this);
			}
		}
		mDrawerLeftAdapter.notifyDataSetChanged();
		mDrawerRightAdapter.addAll();
		mDrawerRightAdapter.notifyDataSetChanged();
		mDrawerRightAdapter.setSelcted(0);
		mRightDrawerList.setSelection(0);
	}

	@Override
	public void onFriendsDownloadSucceess() {
		LogManager.d(TAG, "*********** onFriendsDownloadSucceess()");
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				mDrawerRightAdapter.addAll();
				mDrawerRightAdapter.notifyDataSetChanged();
				mDrawerRightAdapter.setSelcted(0);
			}
		});
		DashBoardActivity.userBlock=false;

	}

	@Override
	public void onFriendsDownloadFailed(String msg) {
		LogManager.d(TAG, "*********** onFriendsDownloadFailed()-  " + msg);
		DashBoardActivity.userBlock=false;
	}

	@Override
	public void onFBProfilePicsDownloadSucceess(FBAlbumPhotos profilePhotos) {
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				showAvatarAndNumber();
				uploadPics();
				DataController dataController = DataController.getInstance();
				VCardManager.getInstance().updateVCard(
						dataController.getFullJid(), "",
						dataController.getUser().getAvatarURL(),
						dataController.getUser().getAvatarURL(),
						dataController.getUser().getFirstName());
			}
		});
	}

	@Override
	public void onFBProfilePicsDownloadFailed(String msg) {
		LogManager.d(TAG, "*********** onFBProfilePicsDownloadFailed()-  "
				+ msg);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		BackgroundExecutor.getInstance().stop();
		MatchstixApplication.getInstance().removeUIListener(
				OnChatChangedListener.class, this);
		MatchstixApplication.getInstance().removeUIListener(
				UnmatchUserListener.class, this);
		MatchstixApplication.getInstance().removeUIListener(
				FriendsDownloadListener.class, this);
		MatchstixApplication.getInstance().removeUIListener(
				ReportUserListener.class, this);
		MatchstixApplication.getInstance().removeUIListener(
				SettingsUpdateListener.class, this);
		MatchstixApplication.getInstance().removeUIListener(
				ProfileListener.class, this);
		MatchstixApplication.getInstance().removeUIListener(
				FriendLikeListner.class, this);
	}

	@Override
	public void onUnmatchSucceess(long friendId) {
		if (mDrawerRightAdapter != null) {
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					mDrawerRightAdapter.addAll();
					mDrawerRightAdapter.notifyDataSetChanged();
				}
			});
		}

	}

	@Override
	public void onUnmatchFailed() {
		DashBoardActivity.userBlock=false;
	}

	@Override
	public void onReportSucceess() {
		userBlock = false;
		if (mDrawerRightAdapter != null) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					mDrawerRightAdapter.addAll();
					mDrawerRightAdapter.notifyDataSetChanged();
				}
			});
		}
	}

	@Override
	public void onReportFailed() {
		DashBoardActivity.userBlock=false;
	}

	@Override
	public void onSettingsUpdateSuccess() {
		suggestionsRequester = new SuggestionsRequester(0);
		DashBoardActivity.userBlock=false;
		AppEventsLogger logger= AppEventsLogger.newLogger(this);
		logger.logEvent("Completed a Setting Change");
	}

	@Override
	public void onSettingsUpdateFailed(String msg) {
		// TODO Auto-generated method stub
		DashBoardActivity.userBlock=false;
	}

	@Override
	public void onFriendLikeSuccess() {
		AppEventsLogger logger= AppEventsLogger.newLogger(this);
		logger.logEvent("Completed a Like");
		userBlock = false;
		if (requester != null && !requester.isPending()) {
			BackgroundExecutor.getInstance().execute(requester);
		}
	}

	@Override
	public void onProfileDownloadSuccess(final SuggestionProfile profile,
			final SuggestionDetail detail) {
		userBlock = false;
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				if (DataController.getInstance().getUser().getId() != detail
						.getUserId()) {
					return;
				}
				ArrayList<AlbumPhoto> albumPhotoList = new ArrayList<AlbumPhoto>();
				ArrayList<Photo> userPhoto = profile.getImageURLs();
				if (userPhoto == null || userPhoto.size() == 0) {
					return;
				}
				for (int i = 0; i < userPhoto.size(); i++) {
					AlbumPhoto albumPhoto = new AlbumPhoto();
					albumPhoto.setId(String.valueOf(i + 1));
					albumPhoto.setLocalAvatar(userPhoto.get(i).getImageURL());
					albumPhoto.setRemoteAvatar(userPhoto.get(i).getImageURL());
					albumPhotoList.add(albumPhoto);
				}
				DataController dataController = DataController.getInstance();
				DataController.getInstance().getUser()
						.setAvatarURL(userPhoto.get(0).getImageURL());
				DataController.getInstance().setUserProfilePics(albumPhotoList,
						false);
				DataController.getInstance().saveUser();
				String id = dataController.getProfileAlbumId();
				FBAlbumPhotos fbAlbumPhoto = new FBAlbumPhotos();
				fbAlbumPhoto.setData(albumPhotoList);
				DataController.getInstance().refreshFBAlbumPhotos(id,
						fbAlbumPhoto);
				showAvatarAndNumber();
			}
		});
	}

	@Override
	public void onProfileDownloadFailed(String msg) {
		userBlock = false;
	}

	@Override
	public void onUnauthorizedListener() {
		userBlock = false;
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				Utils.showUnAuthorizedDialog(DashBoardActivity.this);
			}
		});
	}

	@Override
	public void onBlockedUser() {
		userBlock = true;
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				BlockedPopup.getInstance().showBlockedUserDialog(
						DashBoardActivity.this);
			}
		});
	}

	@Override
	public void lastChatUpdate(Friend user,int position) {
		Toast.makeText(this, "Call lastChatUpdate", Toast.LENGTH_LONG).show();
			Toast.makeText(this, "NOTIFY", Toast.LENGTH_LONG).show();
			mDrawerRightAdapter.notifyDataSetChanged();
	}

	@Override
	public void onLikeBlocked() {
		
	}

	@Override
	public void onFriendPassSuccess() {
		AppEventsLogger logger= AppEventsLogger.newLogger(this);
		logger.logEvent("Completed a Pass");
	}

}
