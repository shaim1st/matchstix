package com.matchstixapp.ui.activity;

import java.io.File;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Window;

import com.craterzone.cz_commons_lib.Logger;
import com.matchstixapp.R;
import com.matchstixapp.GAScreen;
import com.matchstixapp.MatchstixApplication;
import com.matchstixapp.db.DataController;
import com.matchstixapp.listener.Keys;
import com.matchstixapp.modal.Album;
import com.matchstixapp.modal.AlbumPhoto;
import com.matchstixapp.services.FBWebServices;
import com.matchstixapp.ui.adapter.FBPhotoGridAdapter;
import com.matchstixapp.ui.fragments.FBPhotoGridFragment;
import com.matchstixapp.ui.fragments.FBPhotoGridFragment.OnGridItemClick;
import com.matchstixapp.utils.Constants;
import com.matchstixapp.utils.CustomToast;
import com.matchstixapp.utils.FileCache;
import com.matchstixapp.utils.FileUtil;

import eu.janmuller.android.simplecropimage.CropImage;

public class AlbumPhotosGridActivity extends BaseFragmentActivity implements
		OnGridItemClick, FileUtil.ImageDownloadListener {
	private static final int REQUEST_CODE_CROP_IMAGE = 123;
	private FBPhotoGridAdapter adapter;
	private FragmentManager fragmentManager;
	private FBPhotoGridFragment photoGrid;
	AlbumPhoto photo;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		MatchstixApplication.tracker.setScreenName(GAScreen.ALBUM_LIST_SCREEN);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_fb_photo_grid);
		String albumId = getIntent().getExtras().getString(Keys.KEY_ALBUM_ID);
		if (albumId != null) {
			fragmentManager = getSupportFragmentManager();
			DataController controller = DataController.getInstance();
			adapter = new FBPhotoGridAdapter(this, controller.getFBAlbumPhotos(
					albumId).getData());
			Album album = controller.getFBAlbum(albumId);
			photoGrid = new FBPhotoGridFragment(this, adapter, album.getName());
			FragmentTransaction transaction = fragmentManager
					.beginTransaction();
			transaction.add(R.id.fragment_container, photoGrid);
			transaction.commit();
		}

	}

	ProgressDialog progressDialog;

	@Override
	public void show(final AlbumPhoto photo) {
		this.photo = photo;
		if (photo != null) {
				progressDialog = ProgressDialog.show(this, null, getResources()
						.getString(R.string.loading));
			FileUtil.download(this, FBWebServices.getPicURL(photo.getId()));
			
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode,
			Intent result) {
		if (requestCode == REQUEST_CODE_CROP_IMAGE) {
			handleCrop(resultCode, result);
		}
	}

	private void beginCrop(File source, String url) {
		// create explicit intent
		Intent intent = new Intent(this, CropImage.class);

		// tell CropImage activity to look for image to crop
		String filePath = source.getPath();
		intent.putExtra(CropImage.IMAGE_PATH, filePath);

		// allow CropImage activity to rescale image
		intent.putExtra(CropImage.SCALE, true);

		/*// if the aspect ratio is fixed to ratio 3/2
		intent.putExtra(CropImage.ASPECT_X, 3);
		intent.putExtra(CropImage.ASPECT_Y, 2);*/
		
		// if the aspect ratio is fixed to ratio 1/1 = square
		intent.putExtra(CropImage.ASPECT_X, 1);
		intent.putExtra(CropImage.ASPECT_Y, 1);
		intent.putExtra("IMAGE_URL", url);

		// start activity CropImage with certain request code and listen
		// for result
		startActivityForResult(intent, REQUEST_CODE_CROP_IMAGE);
	}

	private void handleCrop(int resultCode, Intent result) {
		if (resultCode == RESULT_OK) {
			String path = result.getStringExtra(CropImage.IMAGE_PATH);
			String url = result.getStringExtra("IMAGE_URL");
			path = renameImage(path);
			if (path == null) {
				return;
			}
			Uri uri = Uri.fromFile(new File(path));
			path = uri.toString();
			Intent data = this.getIntent();
			data.putExtra(Keys.KEY_PHOTO_ID, photo.getId());
			data.putExtra(Keys.KEY_PHOTO_URI, path);
			data.putExtra("IMAGE_URL", url);
			setResult(RESULT_OK, data);
			finish();
		}
	}

	private String renameImage(String path) {
		try {
			String root = path.substring(0, path.lastIndexOf('/'));
			String name = FileCache.getInstance(this).getFileName(path);
			String ext = ".png";
			name = Constants.PREFIX_NAME_CROPED_FILE
					+ System.currentTimeMillis() + ext;
			String newPath = root + "/" + name;
			File oldFile = new File(path);
			if (oldFile.exists()) {
				oldFile.renameTo(new File(newPath));
			}
			return newPath;
		} catch (Exception e) {
			Logger.e("renameImage", e.toString());
		}
		return null;
	}

	@Override
	public void onDownloadingFinished(File file, String url) {
		if(progressDialog != null){
			progressDialog.dismiss();
		}
		if (file != null) {
			beginCrop(file, url);
		} else {
			CustomToast.showShortToast(AlbumPhotosGridActivity.this, R.string.error_try_again);
		}
	}

}
