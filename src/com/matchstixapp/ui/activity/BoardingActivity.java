package com.matchstixapp.ui.activity;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.craterzone.cz_commons_lib.JsonUtil;
import com.craterzone.cz_commons_lib.Logger;
import com.craterzone.cz_commons_lib.PlatformUtil;
import com.craterzone.cz_commons_lib.StringUtil;
import com.matchstixapp.R;
import com.facebook.GraphRequest;
import com.facebook.GraphRequest.Callback;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.login.widget.LoginButton;
import com.matchstixapp.ConnectivityController;
import com.matchstixapp.GAScreen;
import com.matchstixapp.MatchstixApplication;
import com.matchstixapp.PNV.PNVActivity;
import com.matchstixapp.db.DataController;
import com.matchstixapp.db.MatchstixPreferences;
import com.matchstixapp.listener.HTTPLogInListener;
import com.matchstixapp.listener.MatchstixDialogListener;
import com.matchstixapp.listener.NumberVerificationStatusListener;
import com.matchstixapp.modal.FBFetchedDettails;
import com.matchstixapp.modal.User;
import com.matchstixapp.pojo.JSONDeviceInfo;
import com.matchstixapp.pojo.FacebookGraphUser;
import com.matchstixapp.pojo.JSONLogin;
import com.matchstixapp.pojo.JSONNumber;
import com.matchstixapp.pojo.JSONSignUp;
import com.matchstixapp.pojo.JSONUserProfile;
import com.matchstixapp.requester.CheckVerificationStatusRequester;
import com.matchstixapp.requester.LogInRequester;
import com.matchstixapp.ui.pager.BoardingSlidePage;
import com.matchstixapp.ui.pager.PagerController;
import com.matchstixapp.ui.pager.SlideHorizontalPager;
import com.matchstixapp.ui.pager.SlidePage;
import com.matchstixapp.ui.pager.SlidePageAdapter;
import com.matchstixapp.utils.BackgroundExecutor;
import com.matchstixapp.utils.Constants;
import com.matchstixapp.utils.CustomToast;
import com.matchstixapp.utils.Utils;
import com.matchstixapp.xmpplibrary.XMPPUtil;
import com.matchstixapp.xmpplibrary.ui.OAuthActivity;
import com.matchstixapp.xmpplibrary.ui.adapter.AccountTypeAdapter;
import com.xabber.android.data.Application;
import com.xabber.android.data.NetworkException;
import com.xabber.android.data.account.AccountManager;
import com.xabber.android.data.account.AccountType;
import com.xabber.android.data.intent.AccountIntentBuilder;

public class BoardingActivity extends BaseFragmentActivity implements Callback,
		OnClickListener, OnItemSelectedListener,
		NumberVerificationStatusListener, HTTPLogInListener {

	private static final String SAVED_ACCOUNT_TYPE = "com.xabber.android.ui.AccountAdd.ACCOUNT_TYPE";
	private static final int OAUTH_WML_REQUEST_CODE = 1;

	ImageView ivPrivacy;
	private ImageView ivFacebook;
	private SlidePageAdapter mPagerAdapter;
	private PagerController controller = null;
	private SlideHorizontalPager mPager = null;
	private int slideLastPage = 0;
	private CallbackManager callbackManager;

	static final int RC_SIGN_IN = 0;
	private ProgressDialog progressDialog;
	Bundle params;
	LoginButton fbLoginBtn;
	static final String TAG = BoardingActivity.class.getName();

	private FBFetchedDettails mFbDetails;
	private String accessToken;
	private Spinner accountTypeView;
	private boolean loginTry = true;

	private final List<String> PERMISSIONS = Arrays.asList("public_profile",
			"email", "user_about_me", "user_birthday", "user_photos");

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		MatchstixApplication.tracker.setScreenName(GAScreen.ONBOARD_SCREEN);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_boarding);
		FacebookSdk.sdkInitialize(getApplicationContext());
		callbackManager = CallbackManager.Factory.create();
		setPager();
		LoginManager.getInstance().registerCallback(callbackManager,
				new FacebookCallback<LoginResult>() {
					@Override
					public void onSuccess(LoginResult loginResult) {
						Log.d(TAG, "success");
						accessToken = loginResult.getAccessToken().getToken();
						Date expire = loginResult.getAccessToken().getExpires();
						User user = DataController.getInstance().getUser();
						user.setFbAccessToken(accessToken);
						user.setFbTokenExpireDate(expire.getTime());
						GraphRequest request = GraphRequest.newMeRequest(
								loginResult.getAccessToken(),
								new GraphRequest.GraphJSONObjectCallback() {
									@Override
									public void onCompleted(JSONObject object,
											GraphResponse response) {
										Logger.d(TAG, response.toString());
										/*
										 * if(response == null){ return; }
										 */
										FacebookGraphUser userDetail = (FacebookGraphUser) JsonUtil
												.toModel(response
														.getJSONObject()
														.toString(),
														FacebookGraphUser.class);
										if (userDetail != null) {
											if (userDetail.getEmail() == null) {
												userDetail.setEmail(userDetail
														.getId()
														+ "@matchstixapp.com");
											}
											FBFetchedDettails details = fetchBasicDetails(userDetail);
											if (details == null) {
												closeProgressAndShowDialog(
														"Facebook Error",
														getString(R.string.fb_permission_error));
												return;
											}

											// Check for different user login
											String savedEmail = DataController
													.getInstance().getUser()
													.getEmail();
											if (!TextUtils.isEmpty(savedEmail)) {
												String newEmail = details
														.getFacebookGraphUser()
														.getEmail();
												if (!TextUtils
														.isEmpty(newEmail)
														&& !newEmail
																.equalsIgnoreCase(savedEmail)) {
													AccountManager
															.getInstance()
															.removeAccount(
																	DataController
																			.getInstance()
																			.getFullJid());
													DataController
															.getInstance()
															.clear();
												}
											}

											int age = details.getAge();
											if (age < Constants.MIN_AGE_FOR_SIGNUP) {
												// FacebookSdk.sdkInitialize(getApplicationContext());
												LoginManager.getInstance()
														.logOut();
												closeProgressAndShowDialog(
														getString(R.string.signup_error),
														getString(R.string.fb_age_error));
												return;
											}
											// fetchUserInterestsAndAvatar();
											JSONSignUp request = makeRequestData();
											BackgroundExecutor.getInstance()
													.execute(
															new LogInRequester(
																	request));
										}
									}

								});
						progressDialog = ProgressDialog.show(
								BoardingActivity.this,
								"",
								BoardingActivity.this.getResources().getString(
										R.string.login));
						Bundle parameters = new Bundle();
						parameters.putString("fields",
								"id,name,email,gender,birthday");
						request.setParameters(parameters);
						request.executeAsync();

					}

					@Override
					public void onCancel() {
						Log.d("facebook", "cancel");
					}

					@Override
					public void onError(FacebookException exception) {
						Log.d("facebook", "excption");
						LoginManager.getInstance().logOut();
					}
				});

		fbLoginBtn = new LoginButton(this);
		ivFacebook = (ImageView) findViewById(R.id.iv_facebook);
		ivPrivacy = ((ImageView) findViewById(R.id.id_privacy));

		accountTypeView = (Spinner) findViewById(R.id.account_type);
		accountTypeView.setAdapter(new AccountTypeAdapter(this));
		accountTypeView.setOnItemSelectedListener(this);
		accountTypeView.setSelection(0);
		String accountType;
		if (savedInstanceState == null)
			accountType = null;
		else
			accountType = savedInstanceState.getString(SAVED_ACCOUNT_TYPE);
		accountTypeView.setSelection(0);
		for (int position = 0; position < accountTypeView.getCount(); position++)
			if (((AccountType) accountTypeView.getItemAtPosition(position))
					.getName().equals(accountType)) {
				accountTypeView.setSelection(position);
				break;
			}
	}

	@Override
	protected void onResume() {
		super.onResume();
		MatchstixApplication.getInstance().addUIListener(
				NumberVerificationStatusListener.class, this);
		MatchstixApplication.getInstance().addUIListener(
				HTTPLogInListener.class, this);

		((TextView) findViewById(R.id.tv_signin_with)).setOnClickListener(this);
		((TextView) findViewById(R.id.tv_fb)).setOnClickListener(this);
		// ((TextView) findViewById(R.id.tv_privacy)).setOnClickListener(this);
		ivPrivacy.setOnClickListener(this);
		ivFacebook.setOnClickListener(this);
		fbLoginBtn.setOnClickListener(this);
	}

	@Override
	protected void onPause() {
		super.onPause();
		MatchstixApplication.getInstance().removeUIListener(
				NumberVerificationStatusListener.class, this);
		MatchstixApplication.getInstance().removeUIListener(
				HTTPLogInListener.class, this);

		closeProgressAndShowDialog(null, null);
	}

	@Override
	protected void onStop() {
		super.onStop();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
	}

	private void setPager() {
		mPagerAdapter = new SlidePageAdapter(getSupportFragmentManager());
		addSlidePages();

		mPager = (SlideHorizontalPager) findViewById(R.id.horizontal_pager);
		controller = (PagerController) findViewById(R.id.pager_controller);
		mPager.setAdapter(mPagerAdapter);
		mPager.setPagerController(controller);
		controller.setPager(mPager);
		controller.setPageCount(mPagerAdapter.getCount());
		controller.setCurrentPage(slideLastPage);
		mPager.setPagerController(controller);
		mPager.setCurrentItem(slideLastPage);
	}

	private void closeProgressAndShowDialog(String title, String msg) {
		if (progressDialog != null && progressDialog.isShowing()) {
			progressDialog.dismiss();
		}
		if (title == null || msg == null) {
			return;
		}
		Utils.showDialog(this, title, msg,
				getResources().getString(R.string.cancel), getResources()
						.getString(R.string.exit),
				new MatchstixDialogListener() {

					@Override
					public void onButtonTwoClick() {
						finish();
					}

					@Override
					public void onButtonOneClick() {

					}
				});
	}

	/*
	 * Fetching all basic details from fb.
	 */
	private FBFetchedDettails fetchBasicDetails(FacebookGraphUser fbuser) {
		if (fbuser != null) {
			mFbDetails = new FBFetchedDettails();
			mFbDetails.setFacebookGraphUser(fbuser);
			Profile profile = Profile.getCurrentProfile();
			if (profile == null) {
				return mFbDetails;
			}
			mFbDetails.setFirstName(profile.getFirstName());
			mFbDetails.setLastName(profile.getLastName());

			/*
			 * mFbDetails.setId(fbuser.getId()); if (fbuser.getLocation() !=
			 * null && fbuser.getLocation().getProperty("name") != null) {
			 * mFbDetails.setLocation(fbuser.getLocation().getProperty("name")
			 * .toString()); }
			 */
			return mFbDetails;
		}
		return null;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		callbackManager.onActivityResult(requestCode, resultCode, data);
		if (requestCode == OAUTH_WML_REQUEST_CODE) {
			if (resultCode == RESULT_OK && !OAuthActivity.isInvalidated(data)) {
				String token = OAuthActivity.getToken(data);
				if (token == null) {
					Application.getInstance().onError(
							R.string.AUTHENTICATION_FAILED);
				} else {
					String account;
					try {
						account = AccountManager.getInstance()
								.addAccount(
										null,
										token,
										(AccountType) accountTypeView
												.getSelectedItem(), true, true,
										false);
					} catch (NetworkException e) {
						Application.getInstance().onError(e);
						return;
					}
					setResult(RESULT_OK,
							createAuthenticatorResult(this, account));
					finish();
				}
			}
		}
	}

	private static Intent createAuthenticatorResult(Context context,
			String account) {
		return new AccountIntentBuilder(null, null).setAccount(account).build();
	}

	public static String getAuthenticatorResultAccount(Intent intent) {
		return AccountIntentBuilder.getAccount(intent);
	}

	private void addSlidePages() {
		TypedArray slideImageArray = getResources().obtainTypedArray(
				R.array.slide_page_image_array);
		TypedArray slideTextArrayLine1 = getResources().obtainTypedArray(
				R.array.slide_page_text_array_line1);
		TypedArray slideTextArrayLine2 = getResources().obtainTypedArray(
				R.array.slide_page_text_array_line2);
		SlidePage page = null;
		for (int i = 0; i < slideImageArray.length(); i++) {
			page = new BoardingSlidePage();
			HashMap<String, Object> pageData = new HashMap<String, Object>();
			pageData.put(BoardingSlidePage.KEY_BACKGROUND_RES_ID, new Integer(
					slideImageArray.getResourceId(i, -1)));
			pageData.put(
					BoardingSlidePage.KEY_LINE1,
					getResources().getString(
							slideTextArrayLine1.getResourceId(i, -1)));
			pageData.put(
					BoardingSlidePage.KEY_LINE2,
					getResources().getString(
							slideTextArrayLine2.getResourceId(i, -1)));
			page.setData(pageData);
			mPagerAdapter.addPage(page);
		}
		mPagerAdapter.notifyDataSetChanged();
		slideImageArray.recycle();
		slideTextArrayLine1.recycle();
		slideTextArrayLine2.recycle();

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {

		case R.id.tv_signin_with:
		case R.id.tv_fb:
		case R.id.iv_facebook:
			boolean isUserRegistered = DataController.getInstance().getUser()
					.isRegistered();
			boolean isNumberVerified = DataController.getInstance().getUser()
					.isNumberVerified();
			if (isUserRegistered && !isNumberVerified) {
				startActivity(new Intent(BoardingActivity.this, PNVActivity.class));
				finish();
			}
			Utils.showFadeinAnimation(this, ivFacebook);
			Logger.d(TAG, "Facebook Logging In");
			if (!ConnectivityController
					.isNetworkAvailable(BoardingActivity.this)) {
				CustomToast.showShortToast(this, R.string.no_internet);
				return;
			}
			LoginManager.getInstance().logInWithReadPermissions(this,
					PERMISSIONS);
			break;

		case R.id.id_privacy:
			Utils.showFadeinAnimation(this, ivPrivacy);
			startActivity(new Intent(this, PrivacyActivity.class));
			break;
		default:
			break;
		}
	}

	private void validatePhoneNumber() {
		if (progressDialog != null && progressDialog.isShowing()) {
			progressDialog.setMessage(getResources().getString(
					R.string.please_wait));
		}
		BackgroundExecutor.getInstance().execute(
				new CheckVerificationStatusRequester());
	}

	/*
	 * Creating signIn Request JSON
	 */
	private JSONSignUp makeRequestData() {
		JSONUserProfile profile = null;
		JSONDeviceInfo devInfo = null;
		String email = null;
		if (mFbDetails != null) {
			DataController.getInstance().getUser()
					.setFbUserId(mFbDetails.getFacebookGraphUser().getId());
			DataController.getInstance().saveUser();
			if (mFbDetails.getFirstName() == null
					|| mFbDetails.getFirstName().length() == 0) {
				String[] nameString = mFbDetails.getFacebookGraphUser()
						.getName().split(" ");
				mFbDetails.setFirstName(nameString[0]);
				if (nameString.length > 1) {
					mFbDetails.setLastName(nameString[1]);
				}

			}
			profile = new JSONUserProfile(mFbDetails.getFirstName(),
					mFbDetails.getLastName(), mFbDetails.getAge(), mFbDetails
							.getFacebookGraphUser().getGender().toUpperCase(),
					mFbDetails.getAboutMe());
			email = mFbDetails.getFacebookGraphUser().getEmail();

			MatchstixPreferences sp = MatchstixPreferences.getInstance();
			String deviceToken = sp.getGCMRegId();
			if (StringUtil.isEmpty(deviceToken)) {
				sp.setGCmTokenRegistered(false);
				deviceToken = "dummyToken";
			} else {
				sp.setGCmTokenRegistered(true);
			}
			devInfo = new JSONDeviceInfo(Constants.DEVICE_TYPE, deviceToken,
					false, PlatformUtil.getUDID(this)); // for prod=true / for
														// dev=false
			double lattitude = sp.getLatitude();
			double longitude = sp.getLongitude();
			if (lattitude != 0 && longitude != 0) {
				profile.setLat(lattitude);
				profile.setLng(longitude);
			}
		}
		return new JSONSignUp("socialURL", email, "FB", devInfo, "www.fb.com",
				profile);
	}

	@Override
	public void onItemSelected(AdapterView<?> adapterView, View view,
			int position, long id) {
		AccountType accountType = (AccountType) accountTypeView
				.getSelectedItem();
		if (accountType.getProtocol().isOAuth())
			findViewById(R.id.auth_panel).setVisibility(View.GONE);
		else
			findViewById(R.id.auth_panel).setVisibility(View.VISIBLE);
		((TextView) findViewById(R.id.account_user_name)).setHint(accountType
				.getHint());
		((TextView) findViewById(R.id.account_help)).setText(accountType
				.getHelp());
	}

	@Override
	public void onNothingSelected(AdapterView<?> adapterView) {
		accountTypeView.setSelection(0);
	}

	@Override
	public void onNumberVerificationSucceess(final JSONNumber number) {
		AppEventsLogger logger= AppEventsLogger.newLogger(this);
		logger.logEvent(AppEventsConstants.EVENT_NAME_COMPLETED_REGISTRATION);
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				closeProgressAndShowDialog(null, null);
				User user = DataController.getInstance().getUser();
				user.setNumber(number.phoneNumber);
				user.setCC(number.cc);
				user.setNumberVerified(true);
				DataController.getInstance().saveUser();
				// doXMPPLogin();
				if (AccountManager.getInstance().getAccounts().size() > 0) {
					String account = DataController.getInstance().getFullJid();
					new XMPPUtil().enableAccount(AccountManager.getInstance()
							.getAccount(account));
				} else {
					new XMPPUtil().doXMPPLogin(BoardingActivity.this,
							(AccountType) accountTypeView.getSelectedItem());
				}
				// If number is verified this user then dont show PNV screen
				startActivity(new Intent(BoardingActivity.this,
						DashBoardActivity.class));
				finish();
			}
		});
	}

	@Override
	public void onNumberVerificationFailed(final int code, final String msg) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				closeProgressAndShowDialog(null, null);
				startActivity(new Intent(BoardingActivity.this,
						PNVActivity.class));
				finish();
			}
		});
	}

	@Override
	public void onNumberVerificationError(final String reason) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				// closeProgressAndShowDialog(null, null);
				CustomToast.showShortToast(BoardingActivity.this,
						R.string.no_internet);

			}
		});
	}

	@Override
	public void onHTTPLogInSuccess(JSONLogin responseLogin) {
		DataController.getInstance().getUser().setFbAccessToken(accessToken);
		DataController.getInstance().saveUser();
		/* verify user phone no */
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if (!DataController.getInstance().getUser().isNumberVerified()) {
					validatePhoneNumber();
				} else {
					if (AccountManager.getInstance().getAccounts().size() > 0) {
						String account = DataController.getInstance()
								.getFullJid();
						new XMPPUtil().enableAccount(AccountManager
								.getInstance().getAccount(account));
					} else {
						new XMPPUtil()
								.doXMPPLogin(BoardingActivity.this,
										(AccountType) accountTypeView
												.getSelectedItem());
					}
					startActivity(new Intent(BoardingActivity.this,
							DashBoardActivity.class));
					finish();
				}
			}
		});
	}

	@Override
	public void onHTTPLogInFailed(final String msg) {
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				if (loginTry) {
					JSONSignUp request = makeRequestData();
					BackgroundExecutor.getInstance().execute(
							new LogInRequester(request));
					loginTry = false;
				} else {
					closeProgressAndShowDialog(
							getString(R.string.signup_error),
							getString(R.string.error_try_again));
				}
			}
		});
	}

	@Override
	public void onCompleted(GraphResponse response) {
		Log.d(TAG, "Oncomplete() is called.");
	}

	@Override
	public void onUnauthorizedListener() {
		runOnUiThread(new Runnable() {

			@Override
			public void run() {
				if (progressDialog != null && progressDialog.isShowing()) {
					progressDialog.dismiss();
				}
				Utils.showDialog(BoardingActivity.this,
						getString(R.string.unauthorized_msg),
						getString(R.string.btn_logout),
						new MatchstixDialogListener() {

							@Override
							public void onButtonTwoClick() {

							}

							@Override
							public void onButtonOneClick() {
								JSONSignUp request = makeRequestData();
								BackgroundExecutor.getInstance().execute(
										new LogInRequester(request));
							}
						});
			}
		});
	}

}