package com.matchstixapp.ui.activity;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;

import com.facebook.AccessToken;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.matchstixapp.R;
import com.matchstixapp.GAScreen;
import com.matchstixapp.MatchstixApplication;
import com.matchstixapp.PNV.PNVActivity;
import com.matchstixapp.db.DataController;
import com.matchstixapp.db.MatchstixPreferences;
import com.matchstixapp.push.PushCustomHandler;
import com.matchstixapp.requester.ForceUpgradeRequester;
import com.matchstixapp.utils.BackgroundExecutor;
import com.matchstixapp.utils.Constants;
import com.matchstixapp.utils.LocationTracker;
import com.pushbots.push.Pushbots;
import com.xabber.android.data.account.AccountManager;

public class SplashActivity extends BaseFragmentActivity  {

	private LocationTracker locationTracker;
	private boolean isAlreadyActionPeformed = false;
	String deviceToken = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		MatchstixApplication.tracker.setScreenName(GAScreen.SPLASH_SCREEN);
		setContentView(R.layout.activity_splash);
		locationTracker = new LocationTracker(this);
		try{
			MatchstixPreferences.getInstance().setJsonHashCode(0);
			MatchstixPreferences.getInstance().setJsonHashCodeFlag(0);
		}catch(Exception e){
			
		}
		registerForPush();
	}
	private void registerForPush() {
		//new Thread(new GCMRegistrationThread(this)).start();
		Pushbots.sharedInstance().init(this);
		Pushbots.sharedInstance().setAlias(String.valueOf(DataController.getInstance().getUser().getId()));
		Pushbots.sharedInstance().setNotificationEnabled(false);
		Pushbots.sharedInstance().setCustomHandler(PushCustomHandler.class);
		String deviceToken =  Pushbots.sharedInstance().regID();
		MatchstixPreferences.getInstance().saveGCMRegId(deviceToken);
	}

	@Override
	protected void onResume() {
		super.onResume();
		BackgroundExecutor.getInstance().execute(new ForceUpgradeRequester(getPackageInfo()));
		if (locationTracker.isGPSEnabled()) {
			MatchstixApplication.getInstance().postDelayed(runnableLaunchActivity,
					Constants.DELAY_SPLASH);
		} else {
			locationTracker.showSettingsAlert();
		}
	}
	
	@Override
	protected void onPause() {
		super.onPause();
	}
	
	private int getPackageInfo(){
        try {
        	PackageInfo pinfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            return pinfo.versionCode;
         } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return 0;
    }

	
	
	private Runnable runnableLaunchActivity = new Runnable() {

		@Override
		public void run() {
			if(isAlreadyActionPeformed){
				return;
			}
			isAlreadyActionPeformed = true;
			boolean isUserRegistered = DataController.getInstance()
					.getUser().isRegistered();
			boolean isLoggedin = DataController.getInstance().getUser()
					.isLoggedIn();
			boolean isNumberVerified = DataController.getInstance()
					.getUser().isNumberVerified();
			FacebookSdk.sdkInitialize(SplashActivity.this);
			if(isUserRegistered && AccessToken.getCurrentAccessToken() == null){
				DataController dc = DataController.getInstance();
			}
			if(isUserRegistered && ((AccessToken.getCurrentAccessToken() == null) || (AccessToken.getCurrentAccessToken() != null && AccessToken.getCurrentAccessToken().isExpired()))){
				AccountManager.getInstance().disableAccount(DataController.getInstance().getFullJid());
				DataController.getInstance().getUser().setLoggedIn(false);
				DataController.getInstance().getUser().setExpire(true);
				DataController.getInstance().saveUser();
				LoginManager.getInstance().logOut();
				startActivity(new Intent(SplashActivity.this,
						BoardingActivity.class));
			}else if (isUserRegistered && !isNumberVerified) {
				startActivity(new Intent(SplashActivity.this,
						PNVActivity.class));
			} else if (isUserRegistered && isLoggedin) {
				startActivity(new Intent(SplashActivity.this,
						DashBoardActivity.class));
			} else {
				startActivity(new Intent(SplashActivity.this,
						BoardingActivity.class));
			}
			SplashActivity.this.finish();
		}
	};

}
