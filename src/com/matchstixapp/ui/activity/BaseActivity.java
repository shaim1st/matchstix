package com.matchstixapp.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.matchstixapp.MatchstixApplication;
import com.matchstixapp.xmpplibrary.ActivityManager;

public class BaseActivity extends Activity {
	
	protected MatchstixApplication nMyApplication;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		ActivityManager.getInstance().onCreate(this);
		super.onCreate(savedInstanceState);
		nMyApplication = (MatchstixApplication) getApplication();
		nMyApplication.onActivityCreated(this, savedInstanceState);
	}

	@Override
	protected void onResume() {
		ActivityManager.getInstance().onResume(this);
		super.onResume();
		nMyApplication.onActivityResumed(this);
	}

	@Override
	protected void onPause() {
		ActivityManager.getInstance().onPause(this);
		super.onPause();
		nMyApplication.onActivityPaused(this);
	}

	@Override
	protected void onDestroy() {
		ActivityManager.getInstance().onDestroy(this);
		super.onDestroy();
		nMyApplication.onActivityDestroyed(this);
	}

	@Override
	protected void onNewIntent(Intent intent) {
		ActivityManager.getInstance().onNewIntent(this, intent);
		super.onNewIntent(intent);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		ActivityManager.getInstance().onActivityResult(this, requestCode,
				resultCode, data);
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void startActivity(Intent intent) {
		ActivityManager.getInstance().updateIntent(this, intent);
		super.startActivity(intent);
		
	}

	@Override
	public void startActivityForResult(Intent intent, int requestCode) {
		ActivityManager.getInstance().updateIntent(this, intent);
		super.startActivityForResult(intent, requestCode);
	}

}
