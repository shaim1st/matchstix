package com.matchstixapp.ui.pager;

import java.util.HashMap;

import org.xbill.DNS.MRRecord;

import com.matchstixapp.R;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class BoardingSlidePage extends SlidePage {

	public static final String KEY_BACKGROUND_RES_ID = "bg_res_id";
	public static final String KEY_LINE1 = "line1";
	public static final String KEY_LINE2 = "line2";

	private int mBackgroundResId;
	private View rootView;
	private View textContainer;
	private String textLine1;
	private String textLine2;

	@Override
	public void setData(HashMap<String, Object> pageData) {
		mBackgroundResId = (Integer) pageData.get(KEY_BACKGROUND_RES_ID);
		textLine1 = pageData.get(KEY_LINE1).toString();
		textLine2 = pageData.get(KEY_LINE2).toString();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		rootView = (ViewGroup) inflater.inflate(R.layout.custom_pager, null);
		textContainer = rootView.findViewById(R.id.slide_page_text_container);
		return rootView;
	}

	protected void loadDefaultValues() {
		//rootView.setBackgroundResource(mBackgroundResId);
		((TextView) textContainer.findViewById(R.id.tv_line1))
				.setText(textLine1);
		((TextView) textContainer.findViewById(R.id.tv_line2))
				.setText(textLine2);
		((ImageView)rootView.findViewById(R.id.bording_image)).setImageResource(mBackgroundResId);

	}

	@Override
	public void onPageLoaded(boolean fromLeft) {
		// Animation animation = AnimationUtils.loadAnimation(getActivity(),
		// fromLeft ? android.R.anim.slide_out_right:
		// android.R.anim.slide_in_left);
		// textContainer.startAnimation(animation);
	}

	@Override
	public void setContainerVisible(boolean isVisible) {
		if (textContainer!=null) {
			textContainer.setVisibility(isVisible ? View.VISIBLE : View.INVISIBLE);
		}
			
	}

}
