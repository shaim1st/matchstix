package com.matchstixapp.ui.pager;

import java.util.HashMap;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ImageView;

import com.craterzone.czimageloader.universalImageLoader.UniversalImageLoaderUtil;
import com.matchstixapp.R;

public class FriendProfilePicsSlidePage extends SlidePage {

	public static final String KEY_PIC_URL = "pic_url";
	private ImageView ivAlbum;
	private View rootView;
	private String url;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.custom_album_pager, null);
		ivAlbum = (ImageView) rootView.findViewById(R.id.page_image_view);
		UniversalImageLoaderUtil.loadImageWithDefaultImage(url, ivAlbum, null,
				R.drawable.place_holder_photo);
		ivAlbum.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
//				getActivity().getSupportFragmentManager().popBackStack();				
			}
		});
		return rootView;
	}

	@Override
	public void setData(HashMap<String, Object> pageData) {
		url = pageData.get(KEY_PIC_URL).toString();
	}

	@Override
	protected void loadDefaultValues() {

	}

	@Override
	public void onPageLoaded(boolean fromLeft) {

	}

	@Override
	public void setContainerVisible(boolean isVisible) {

	}

}
