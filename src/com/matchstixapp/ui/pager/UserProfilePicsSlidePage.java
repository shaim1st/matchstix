package com.matchstixapp.ui.pager;

import java.util.HashMap;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.craterzone.czimageloader.universalImageLoader.UniversalImageLoaderUtil;
import com.matchstixapp.R;
import com.matchstixapp.modal.AlbumPhoto;
import com.matchstixapp.services.FBWebServices;

public class UserProfilePicsSlidePage extends SlidePage {

	public static final String KEY_FB_ALBUM = "fb_album";
	private AlbumPhoto album;
	private ImageView ivAlbum;
	private View rootView;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.custom_album_pager, null);
		ivAlbum = (ImageView) rootView.findViewById(R.id.page_image_view);
		if(album != null){
		String avatarUrl = album.getAvatar();
			if(avatarUrl == null){
				avatarUrl = FBWebServices.getPicURL(album.getId());
			}
			UniversalImageLoaderUtil.loadImageWithDefaultImage(avatarUrl,
				ivAlbum, null, R.drawable.place_holder_photo);
		}
		return rootView;
	}

	@Override
	protected void loadDefaultValues() {

	}

	@Override
	public void onPageLoaded(boolean fromLeft) {

	}

	@Override
	public void setContainerVisible(boolean isVisible) {

	}

	@Override
	public void setData(HashMap<String, Object> pageData) {
	 album = (AlbumPhoto) pageData.get(KEY_FB_ALBUM);	
	}

}
