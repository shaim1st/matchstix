package com.matchstixapp.ui.pager;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import com.craterzone.cz_commons_lib.DisplayUtil;
import com.matchstixapp.R;

public class PagerController extends View {

	private int RADIUS = 3;
	private int SPACING = 10;
	private int mCurrentPage = 0;
	private int startX;
	private int startY;
	private int mPageCount;
	private Paint paint;
	private ViewPager mPager;
	private int unselectColor;
	private int selectColor;
	private boolean fillCircle=true;

	/**
	 * Associate pager with pagerController
	 */
	public void setPager(ViewPager pager) {
		mPager = pager;
		this.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				int newPage = (int) ((event.getX() - startX + RADIUS + SPACING / 2))
						/ (2 * RADIUS + SPACING);
				if (newPage == mPageCount)
					newPage--;
				if (newPage >= 0 && newPage < mPageCount) {
					mPager.setCurrentItem(newPage, true);
				}
				return true;
			}
		});

	}

	/**
	 * Return the associated HorizonatlPager
	 * 
	 * @return view pager
	 */
	public ViewPager getPager() {
		return mPager;
	}

	public PagerController(Context context) {
		super(context);
	}

	/**
	 * Creates pagerController Object with the given attribute set
	 * 
	 * @param context
	 * @param attrs
	 */
	public PagerController(Context context, AttributeSet attrs) {
		super(context, attrs);

		paint = new Paint(Paint.ANTI_ALIAS_FLAG);
		paint.setStrokeWidth(2);
		paint.setStyle(Paint.Style.FILL_AND_STROKE);
		TypedArray attr = context.obtainStyledAttributes(attrs,
				R.styleable.pagerController, 0, 0);
		selectColor = attr.getColor(R.styleable.pagerController_selectColor,
				Color.WHITE);
		unselectColor = attr.getColor(
				R.styleable.pagerController_unselectColor, Color.GRAY);

		RADIUS = DisplayUtil.convertDpToPixel(RADIUS, context);
		SPACING = DisplayUtil.convertDpToPixel(SPACING, context);
	}

	@Override
	protected void onDraw(Canvas canvas) {
		startX = getWidth() / 2 - (mPageCount - 1) * (RADIUS + SPACING / 2);

		for (int i = 0; i < mPageCount; i++) {
			if (fillCircle) {
				paint.setColor((i == mCurrentPage) ? selectColor
						: unselectColor);
			} else {
				paint.setStyle(i == mCurrentPage ? Paint.Style.FILL_AND_STROKE
						: Paint.Style.STROKE);
				paint.setColor(selectColor);
			}
			startY = getHeight() / 2;
			canvas.drawCircle(startX + i * (2 * RADIUS + SPACING), startY,
					RADIUS, paint);
		}
		super.onDraw(canvas);
	}

	/**
	 * return the total number of pages indicated by the pagerController.
	 * 
	 * @return pageCount
	 */
	public int getPageCount() {
		return mPageCount;
	}

	/**
	 * This method set the visibility of loading circle according to page slide
	 */
	public void setPageCount(int pageCount) {
		if (pageCount < 0) {
			throw new IllegalArgumentException(
					"Page count must be positive number");
		}
		this.mPageCount = pageCount;
		this.setVisibility(this.mPageCount == 1 ? View.INVISIBLE : View.VISIBLE);
	}

	/**
	 * Return the current page value
	 * 
	 * @return
	 */
	public int getCurrentPage() {
		return mCurrentPage;
	}

	/**
	 * Set the current page of PagerController
	 * 
	 * @param currentPage
	 */
	public void setCurrentPage(int currentPage) {
		if (currentPage < 0 || currentPage > mPageCount) {
			Log.e("page", "Wrong Page Number");
		}
		this.mCurrentPage = currentPage;
		invalidate();
	}

	public void setFillCircle(boolean fillCircle) {
		this.fillCircle = fillCircle;
	}

}
