package com.matchstixapp.ui.pager;

import java.util.HashMap;

import android.support.v4.app.Fragment;

public abstract class SlidePage extends Fragment {


	@Override
	public void onResume() {
		super.onResume();
		loadDefaultValues();
	}

	public abstract void setData(HashMap<String, Object> pageData);
	/**
	 * Called on fragment resume
	 */
	protected abstract void loadDefaultValues();

	/**
	 * Called when a page gets loaded
	 * @param fromLeft -Direction
	 */
	public abstract void onPageLoaded(boolean fromLeft) ;

	/**
	 * Called when you want to make a container(if any) visible/invisible
	 * @param isVisible
	 */
	public abstract void setContainerVisible(boolean isVisible); 
	
}
