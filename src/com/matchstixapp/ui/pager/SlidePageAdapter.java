package com.matchstixapp.ui.pager;

import java.util.ArrayList;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class SlidePageAdapter extends FragmentStatePagerAdapter {

	ArrayList<Fragment> pages=new ArrayList<Fragment>();
	public SlidePageAdapter(FragmentManager fm) {
		super(fm);
	}

	public void addPage(Fragment page) {
		pages.add(page);
	}
	public Fragment getItem(int index) {
		return pages.get(index);
	}

	@Override
	public int getCount() {
		return pages.size();
	}
	public void setPages(ArrayList<Fragment> _pages) {
		pages = _pages;
		notifyDataSetChanged();
	}
	
	public void clearAndAddAll() {
		
	}
	
}
