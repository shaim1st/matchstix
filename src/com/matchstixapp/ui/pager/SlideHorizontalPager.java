package com.matchstixapp.ui.pager;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;

import com.matchstixapp.R;

/**
 * This is a ViewPager allow user to switch between multiple screen .
 */
public class SlideHorizontalPager extends ViewPager {

	private PagerController mPagerController;

	/**
	 * Constructor of slideHorizontalPager
	 * 
	 * @param Context
	 */
	public SlideHorizontalPager(Context context) {
		super(context);
	}

	/**
	 * This constructor is called when inflating view from xml.
	 * 
	 * @param Context
	 * @param attrs
	 */
	public SlideHorizontalPager(Context context, AttributeSet attrs) {
		super(context, attrs);
		TypedArray a = context.obtainStyledAttributes(attrs,
				R.styleable.HorizontalPager);
		int offScreenPageLimit = 3;//a.getInt(10, -1);
		if (offScreenPageLimit != -1) {
			setOffscreenPageLimit(offScreenPageLimit);
		}
		a.recycle();

		super.setOnPageChangeListener(mListener);
	}

	/**
	 * This method set the pager controller class into view pager
	 * 
	 * @param PagerController
	 */
	public void setPagerController(PagerController pagerController) {
		this.mPagerController = pagerController;
	}

	/**
	 * This method set the pager Adapter into the view pager
	 * 
	 * @param PagerAdapter
	 */

	@Override
	public void setAdapter(PagerAdapter adapter) {
		if (!(adapter instanceof SlidePageAdapter)) {
			throw new IllegalArgumentException(
					"Pager adapter is not valid, use FetishSlidePageAdapter");
		}
		super.setAdapter(adapter);
		makePagesInvisible(0);
	}

	/**
	 * This method make the previous and next page invisible
	 * 
	 * @param currentpageindex
	 */
	void makePagesInvisible(int currentPageIndex) {
		int preIndex = currentPageIndex - 1;
		int nextIndex = currentPageIndex + 1;

		SlidePageAdapter adapter = (SlidePageAdapter) getAdapter();
		if (adapter==null) {
			return;
		}
		if (preIndex >= 0 && adapter.getItem(preIndex) instanceof SlidePage) {
			((SlidePage) adapter.getItem(preIndex))
					.setContainerVisible(true);
		}
		if (nextIndex < adapter.getCount()
				&& adapter.getItem(nextIndex) instanceof SlidePage) {
			((SlidePage) adapter.getItem(nextIndex))
					.setContainerVisible(true);
		}
	}

	/**
	 * This OnPageChangeListener change the page when user scroll page
	 */
	private OnPageChangeListener mListener = new OnPageChangeListener() {

		int mPreviousPageIndex = getCurrentItem();

		@Override
		public void onPageSelected(int index) {
			SlidePageAdapter adapter = (SlidePageAdapter) getAdapter();
			if (adapter.getItem(index) instanceof SlidePage) {
				((SlidePage) adapter.getItem(index))
						.onPageLoaded(index > mPreviousPageIndex);
			}
			if (mPagerController != null) {
				mPagerController.setCurrentPage(index);
			}
			mPreviousPageIndex = getCurrentItem();
		}

		@Override
		public void onPageScrolled(int position, float offset, int pixcel) {
			makePagesInvisible(mPreviousPageIndex);
		}

		@Override
		public void onPageScrollStateChanged(int position) {
		}
	};

}
