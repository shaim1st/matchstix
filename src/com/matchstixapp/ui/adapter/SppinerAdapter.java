package com.matchstixapp.ui.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.matchstixapp.R;
import com.matchstixapp.utils.Utils;

public class SppinerAdapter extends ArrayAdapter<String> {

	private String[] objects;
	private Context context;

	public SppinerAdapter(Context context, int resourceId, String[] objects) {
		super(context, resourceId, objects);
		this.objects = objects;
		this.context = context;
	}

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		return getCustomView(position, convertView, parent);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return getCustomView(position, convertView, parent);
	}

	public View getCustomView(int position, View convertView, ViewGroup parent) {

		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View row = inflater.inflate(R.layout.listview, parent, false);
		TextView label = (TextView) row.findViewById(R.id.language);
		Typeface currentTypeFace = label.getTypeface();
		label.setTypeface(Typeface.createFromAsset(getContext().getAssets(),Utils.getFontPath(currentTypeFace)));
		label.setText(objects[position]);

		return row;
	}

}