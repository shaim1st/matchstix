package com.matchstixapp.ui.adapter;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.craterzone.cz_commons_lib.EncodingDecodingUtil;
import com.craterzone.cz_view_lib.RoundImageView;
import com.craterzone.czimageloader.universalImageLoader.UniversalImageLoaderUtil;
import com.matchstixapp.R;
import com.matchstixapp.db.DataController;
import com.matchstixapp.modal.Friend;
import com.matchstixapp.modal.Roster;
import com.matchstixapp.utils.TypeFaceTextView;
import com.xabber.android.data.message.MessageManager;
import com.xabber.android.data.message.type.AbstractMessageItem;

public class DrawerRightListAdapter extends BaseAdapter {
	public static Context mContext;
	private ArrayList<Friend> mFriends;
	private String account;
	//LastChatListener listener;
	public DrawerRightListAdapter(Context context) {
		mContext = context;
		ArrayList<Friend> list=new ArrayList<Friend>();
				list=Roster.getInstance().getFriendList();
		mFriends = list;
		account = DataController.getInstance().getFullJid();
	}
	
	public void setSelcted(int position) {
		Collections.sort(mFriends);
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		return mFriends.size();
	}

	@Override
	public Object getItem(int position) {
		return mFriends.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		DrawerViewHolder holder;
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(R.layout.drawer_right_list_item, null);
			holder = new DrawerViewHolder();
			holder.icon = (RoundImageView) convertView.findViewById(R.id.id_left_header_image_view);
			// hides default circle border
			//holder.icon.setCircleColor(parent.getResources().getColor(R.color.transparent));
			
			holder.incoming = (RoundImageView) convertView.findViewById(R.id.id_left_msg_indicator_view);
			holder.title = (TypeFaceTextView) convertView.findViewById(R.id.id_friend_name_txt_view);
			holder.date = (TypeFaceTextView) convertView.findViewById(R.id.id_matched_date_txt_view);
			holder.avatarBorder = convertView.findViewById(R.id.avatar_border);
			convertView.setTag(holder);
		} else {
			holder = (DrawerViewHolder) convertView.getTag();
		}
		Friend friend = (Friend) getItem(position);
		List<AbstractMessageItem> messageItemsList =new ArrayList<AbstractMessageItem>(MessageManager.
				getInstance().getMessages(account, getUser(friend.getUserId())));
		String statusText= "";
		if ( messageItemsList.size()== 0 ) {
			statusText = formatDate(friend.getMatchDate());
		}  else {
			statusText = String.valueOf(messageItemsList.get(messageItemsList.size()-1).getText());
		//	Roster.getInstance().setFriendChatTime(position,messageItemsList.get(messageItemsList.size()-1).getTimestamp());
			try{
				Roster.getInstance().setFriendChatTime(mFriends.get(position),messageItemsList.get(messageItemsList.size()-1).getTimestamp().getTime());
				}catch(Exception e){
					Log.d("DrawerRightListAdapter",e.toString());
				}
		} 
		if(messageItemsList.size() > 0 && !messageItemsList.get(messageItemsList.size() -1).isRead() &&
				messageItemsList.get(messageItemsList.size() -1).isIncoming()){
			holder.incoming.setVisibility(View.VISIBLE);
			try{
			Roster.getInstance().setFriendChatTime(mFriends.get(position),messageItemsList.get(messageItemsList.size()-1).getTimestamp().getTime());
			setSelcted(0);
			}catch(Exception e){
				Log.d("DrawerRightListAdapter",e.toString());
			}
		}else{
			setSelcted(0);
			holder.incoming.setVisibility(View.GONE);
		}
		UniversalImageLoaderUtil.loadImageWithDefaultImage(
								friend.getProfilePicURL(), holder.icon, null,
								R.drawable.default_profile);
		holder.title.setText(friend.getFirstName());
		holder.date.setText(EncodingDecodingUtil.decodeString(statusText));
		if(DataController.getInstance().getNewFriendList().contains(friend.getUserId())){
			holder.icon.setSelected(true);
			holder.title.setSelected(true);
		 	holder.date.setSelected(true);
		 	holder.avatarBorder.setSelected(true);
		}else{
			holder.icon.setSelected(false);
			holder.title.setSelected(false);
		 	holder.date.setSelected(false);
		 	holder.avatarBorder.setSelected(false);
		}
		return convertView;
	}
	
	public static String getUser(long userId){
		return userId + "@" + mContext.getResources().getString(R.string.chat_server);
	}
	public void addAll(){
		mFriends.clear();
		mFriends.addAll(Roster.getInstance().getFriendList());
		Collections.sort(mFriends);
		notifyDataSetChanged();
	}

	private String formatDate(long matchDate) {
		StringBuilder sb = new StringBuilder(mContext.getResources().getString(
				R.string.matched_on));
		sb.append(' ');
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
		Date date = new Date(matchDate);
		sb.append(sdf.format(date));
		return sb.toString();
	}

	private class DrawerViewHolder {
		RoundImageView icon;
		RoundImageView incoming;
		TypeFaceTextView title;
		TypeFaceTextView date;
		View avatarBorder;
	}
	
}