package com.matchstixapp.ui.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.craterzone.czimageloader.universalImageLoader.UniversalImageLoaderUtil;
import com.matchstixapp.R;
import com.matchstixapp.db.DataController;
import com.matchstixapp.modal.Album;
import com.matchstixapp.modal.User;

public class AlbumListAdapter extends BaseAdapter {

	Context mContext;
	ArrayList<Album> mAlbum;
	LayoutInflater mInflater;

	public AlbumListAdapter(Context context) {
		this.mContext = context;
		this.mAlbum = new ArrayList<Album>();
		this.mInflater = LayoutInflater.from(context);
	}
	public void setAlbums(ArrayList<Album> list) {
		this.mAlbum = list;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		if(mAlbum == null){
			return 0;
		}
		return mAlbum.size();
	}

	@Override
	public Object getItem(int position) {
		return mAlbum.get(position);
	}

	@Override
	public long getItemId(int position) {

		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			holder = new ViewHolder();
			convertView = mInflater.inflate(R.layout.list_items, null);
			holder.albumName = (TextView) convertView
					.findViewById(R.id.textview_albumname);
			holder.no_of_photos = (TextView) convertView
					.findViewById(R.id.textview_no_of_photos);
			holder.imageView = (ImageView) convertView
					.findViewById(R.id.album_image);
			convertView.setTag(holder);
		} else {

			holder = (ViewHolder) convertView.getTag();
		}
		Album fbAlbum = mAlbum.get(position);
		ImageView imageView = holder.imageView;
		String coverPicURL =getCoverPicURL(fbAlbum.getId());
		UniversalImageLoaderUtil.loadImageWithDefaultImage(coverPicURL,
				imageView, null, R.drawable.place_holder_album);
		holder.albumName.setText(fbAlbum.getName());
		return convertView;
	}

	private String getCoverPicURL(String id) {
		User user = DataController.getInstance().getUser();
		return "https://graph.facebook.com/v2.4/"+id+"/picture?access_token="
				+ user.getFbAccessToken();
	}

	class ViewHolder {
		ImageView imageView;
		TextView albumName;
		TextView no_of_photos;
	}

	public void onClick(View v) {
		switch (v.getId()) {

		default:
			break;
		}

	}

}
