package com.matchstixapp.ui.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.craterzone.czimageloader.universalImageLoader.UniversalImageLoaderUtil;
import com.matchstixapp.R;
import com.matchstixapp.modal.AlbumPhoto;
import com.matchstixapp.services.FBWebServices;

public class FBPhotoGridAdapter extends BaseAdapter {

	private ArrayList<AlbumPhoto> list;
	public Integer[] images;
	private LayoutInflater inflatter;

	List<String> imgs = new ArrayList<String>();

	public FBPhotoGridAdapter(Context context, ArrayList<AlbumPhoto> list) {
		this.list = list;
		inflatter = LayoutInflater.from(context);		
		inflatter = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {
		if(list == null){
			return 0;
		}
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		ViewHolder holder;
		AlbumPhoto photo = (AlbumPhoto) getItem(position);
		if (convertView == null) {
			view = (View) inflatter.inflate(R.layout.fb_photo_grid_item,
					parent, false);
			holder = new ViewHolder(
					(ImageView) view.findViewById(R.id.fb_grid_item));
			holder.image = (ImageView) view.findViewById(R.id.fb_grid_item);
			if (photo.getId() != null) {
				String avatar = FBWebServices.getPicURL(photo.getId());
				UniversalImageLoaderUtil.loadImageWithDefaultImage(
						avatar, holder.image, null,
						R.drawable.place_holder_photo);
			}

			view.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		// holder.image.setImageResource(images[position]);

		return view;
	}

	static class ViewHolder {
		ImageView image;

		private ViewHolder(ImageView image) {
			this.image = image;
		}

	}

}
