package com.matchstixapp.ui.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.matchstixapp.R;
import com.matchstixapp.modal.DrawerItemList;
import com.xabber.android.data.message.MessageManager;

public class DrawerLeftListAdapter extends ArrayAdapter<DrawerItemList>{
	private int selectPosition = -1;
	
	 public DrawerLeftListAdapter(Context context, ArrayList<DrawerItemList> items) {
         super(context, 0, items);
         selectPosition = 0;
     }

     @Override
     public View getView(int position, View convertView, ViewGroup parent) {
         if (convertView == null) {
             convertView = LayoutInflater.from(getContext()).inflate(R.layout.drawer_left_list_item, null);
         }
         DrawerItemList item = getItem(position);
         ImageView icon = (ImageView) convertView.findViewById(R.id.row_image);
    	 int count = MessageManager.getInstance().getTotalUnreadMessageCount();
    	 if(position == 1 && count > 0) {
    		 icon.setImageResource(R.drawable.ic_gotonotification);
    	 } else {
    		 icon.setImageResource((item.getImageId()));
         }
         TextView title = (TextView) convertView.findViewById(R.id.row_title);
         title.setText(item.getTitle());
         if(selectPosition-1 == position){
        	 icon.setSelected(true);
        	 title.setSelected(true);
        	// title.setTypeface(null, Typeface.BOLD);
         }else{
        	 icon.setSelected(false);
        	 title.setSelected(false);
        	// title.setTypeface(null, Typeface.NORMAL);
         }
         return convertView;
     }
     
     public void setSelcted(int position){    	 
    	 selectPosition = position;    	 
    	 notifyDataSetChanged();
     }
}
