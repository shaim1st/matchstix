package com.matchstixapp.ui.adapter;

import java.util.ArrayList;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.craterzone.czimageloader.universalImageLoader.UniversalImageLoaderUtil;
import com.matchstixapp.R;
import com.matchstixapp.modal.SuggestionDetail;

public class SwipeCardAdapter extends ArrayAdapter<SuggestionDetail> {
	private ArrayList<SuggestionDetail> details;
	private LayoutInflater inflater;

	public SwipeCardAdapter(Context context,int textViewResourceId, ArrayList<SuggestionDetail> details) {
		super(context, textViewResourceId, details);
		this.details = details;
		this.inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

	}
	
	@Override
	public int getCount() {
		if(details == null){
			return 0;
		}
		return super.getCount();
	}
	

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		SuggestionDetail detail = getItem(position);
		CountryItemHolder holder = null;
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.friend_details_items,
					parent, false);
			holder = new CountryItemHolder();
			holder.cardView = convertView.findViewById(R.id.layout_card_view);
			holder.image = (ImageView) convertView
					.findViewById(R.id.image_view_profile);
			holder.name = (TextView) convertView.findViewById(R.id.name_friend);
			holder.age = (TextView) convertView.findViewById(R.id.age_friend);
			convertView.setTag(holder);
		} else {
			holder = (CountryItemHolder) convertView.getTag();
		}

		String picURL = detail.getProfilePicURL();
		String fname = detail.getFirstName();
		int age = detail.getAge();

		String name = "";
		if (!TextUtils.isEmpty(fname)) {
			name = fname;
		}
		if (!TextUtils.isEmpty(picURL)) {

			UniversalImageLoaderUtil.loadImageWithDefaultImage(picURL,
					holder.image, null, R.drawable.place_holder_photo);
		}
		if (!TextUtils.isEmpty(name)) {

			holder.name.setText(name);
		}
		holder.age.setText(""+age);
		return convertView;
	}

	class CountryItemHolder {
		View cardView;
		ImageView image;
		TextView name;
		TextView age;

	}
}
