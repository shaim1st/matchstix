package com.matchstixapp.ui.fragments;

import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.ToggleButton;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.matchstixapp.ConnectivityController;
import com.matchstixapp.MatchstixApplication;
import com.matchstixapp.R;
import com.matchstixapp.PNV.PNVFragmentEnterNumber;
import com.matchstixapp.db.DataController;
import com.matchstixapp.listener.DashBoardDrawerListener;
import com.matchstixapp.listener.HTTPLogOutListener;
import com.matchstixapp.listener.MatchstixDialogListener;
import com.matchstixapp.listener.SettingsUpdateListener;
import com.matchstixapp.modal.DiscoverySettings;
import com.matchstixapp.modal.User;
import com.matchstixapp.pojo.JSONSettings;
import com.matchstixapp.requester.LogOutRequester;
import com.matchstixapp.requester.UpdateDiscoverySettingsRequester;
import com.matchstixapp.ui.activity.DashBoardActivity;
import com.matchstixapp.ui.activity.SplashActivity;
import com.matchstixapp.ui.adapter.SppinerAdapter;
import com.matchstixapp.utils.BackgroundExecutor;
import com.matchstixapp.utils.BlockedPopup;
import com.matchstixapp.utils.Constants;
import com.matchstixapp.utils.CustomToast;
import com.matchstixapp.utils.TypeFaceTextView;
import com.matchstixapp.utils.Utils;
import com.matchstixapp.widget.RangeSeekBar;
import com.matchstixapp.widget.RangeSeekBar.OnRangeSeekBarChangeListener;
import com.xabber.android.data.account.AccountManager;
import com.xabber.android.data.notification.NotificationManager;

public class SettingsFragment extends BaseFragment implements OnClickListener,
		DashBoardDrawerListener, HTTPLogOutListener, SettingsUpdateListener,OnItemSelectedListener {
	public static final String TAG = SettingsFragment.class.getSimpleName();
	private View mRootView;
	private int RQUEST_CODE = 100;
	private TypeFaceTextView ageTextView;
	private TypeFaceTextView languageTextView;
	private TypeFaceTextView distanceTextView;
	private LinearLayout logOutTextView;
	private DiscoverySettings settings;
	private ToggleButton toggleButton1;
	private ToggleButton toggleButton2;
	private ToggleButton toggleButton3;
	private ToggleButton toggleButton4;
	private Resources resources = null;
	ProgressDialog progressDialog = null;
	private int distance;// in meters
	private int minAge;
	private int maxAge;
	private boolean isSettingsChanged;
	public static String defaultLanguage = "English";
	private String lang;
	private ScrollView scroll;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		settings = DataController.getInstance().getSettings();
		mRootView = inflater.inflate(R.layout.fragment_setting, container,
				false);
		init();
		String[] options = getResources()
				.getStringArray(R.array.language_array);
		Spinner staticSpinner = (Spinner) mRootView.findViewById(R.id.static_spinner);
		ArrayAdapter<String> myAdapter = new SppinerAdapter(getActivity(),
				R.layout.listview, options);
		staticSpinner.setAdapter(myAdapter);
		scroll=(ScrollView) mRootView.findViewById(R.id.scroll);
		return mRootView;
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
	}

	public static SettingsFragment newInstance() {
		return new SettingsFragment();
	}

	private void init() {

		resources = getResources();
		User user = DataController.getInstance().getUser();
		toggleButton1 = (ToggleButton) mRootView
				.findViewById(R.id.toggleButton1);
		toggleButton2 = (ToggleButton) mRootView
				.findViewById(R.id.toggleButton2);
		toggleButton3 = (ToggleButton) mRootView
				.findViewById(R.id.toggleButton3);
		toggleButton4 = (ToggleButton) mRootView
				.findViewById(R.id.toggleButton4);

		toggleButton1.setClickable(false);
		toggleButton2.setClickable(false);

		toggleButton1.getBackground().setAlpha(100);
		toggleButton2.getBackground().setAlpha(100);

		toggleButton1.setChecked(user.isFemale());
		toggleButton2.setChecked(user.isMale());

		toggleButton3.setChecked(settings.isFemale());
		toggleButton4.setChecked(settings.isMale());

		toggleButton3.setOnClickListener(this);
		toggleButton4.setOnClickListener(this);

		languageTextView = (TypeFaceTextView) mRootView
				.findViewById(R.id.tv_language);
		ageTextView = (TypeFaceTextView) mRootView
				.findViewById(R.id.id_show_age);
		distanceTextView = (TypeFaceTextView) mRootView
				.findViewById(R.id.id_show_limit_distance);
		logOutTextView = (LinearLayout) mRootView
				.findViewById(R.id.id_setting_logout);
		((LinearLayout) mRootView.findViewById(R.id.id_setting_update))
				.setOnClickListener(this);
		((LinearLayout) mRootView.findViewById(R.id.id_contact_us))
				.setOnClickListener(this);

		logOutTextView.setOnClickListener(this);
		SeekBar mSeekBar = (SeekBar) mRootView.findViewById(R.id.seek_bar);
		mSeekBar.setProgressDrawable(resources
				.getDrawable(R.drawable.progress_bar));
		distance = settings.getDistance();
		int km = Utils.convertInKilloMeters(settings.getDistance());
		mSeekBar.setProgress(km);
		distanceTextView.setText(km + " km");// +
												// resources.getString(R.string.km));
		mSeekBar.setMax(200);

		mSeekBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

			@Override
			public void onStopTrackingTouch(SeekBar seekbar) {

			}

			@Override
			public void onStartTrackingTouch(SeekBar seekbar) {

			}

			@Override
			public void onProgressChanged(SeekBar seekbar, int progress,
					boolean fromUser) {
				if (fromUser && progress != 0) {
					isSettingsChanged = true;
					settings.setDistance(Utils.convertInMeters(progress));
					distanceTextView.setText(progress + " km");
					// + resources.getString(R.string.km));
				}
			}
		});
		initRangeBar();

	}

	private void initRangeBar() {
		RangeSeekBar<Integer> seekBar = new RangeSeekBar<Integer>(18, 58,
				getActivity());
		ageTextView.setText(settings.getMinAge() + "-" + settings.getMaxAge());
		languageTextView.setText(defaultLanguage);
		minAge = settings.getMinAge();
		maxAge = settings.getMaxAge();
		seekBar.setSelectedMinValue(settings.getMinAge());
		seekBar.setSelectedMaxValue(settings.getMaxAge());

		seekBar.setOnRangeSeekBarChangeListener(new OnRangeSeekBarChangeListener<Integer>() {
			@Override
			public void onRangeSeekBarValuesChanged(RangeSeekBar<?> bar,
					Integer minValue, Integer maxValue) {
				if ((maxValue - minValue) < 4) {
					maxValue = minValue + 4;
					((RangeSeekBar<Integer>) bar).setSelectedMaxValue(maxValue);
				}

				settings.setMinAge(minValue);
				settings.setMaxAge(maxValue);
				ageTextView.setText(minValue + "-" + maxValue);
				isSettingsChanged = true;
			}
		});

		LinearLayout la = (LinearLayout) mRootView
				.findViewById(R.id.id_age_range);
		la.addView(seekBar);
	}

	private void saveSettings() {
		if (!isSettingsChanged) {
			CustomToast.showShortToast(getActivity(),
					R.string.msg_profile_details_updated);
			return;
		}
		progressDialog = ProgressDialog.show(getActivity(), null,
				resources.getString(R.string.saving));

		saveSettingsLocally();
		distance = settings.getDistance();
		minAge = settings.getMinAge();
		maxAge = settings.getMaxAge();

		JSONSettings settingRequest = new JSONSettings();
		settingRequest.setAgeMax(maxAge);
		settingRequest.setAgeMin(minAge);
		settingRequest.setGenders(settings.getGender());
		settingRequest.setDistance(distance);

		BackgroundExecutor.getInstance().execute(
				new UpdateDiscoverySettingsRequester(settingRequest));
	}

	private void saveSettingsLocally() {
		String[] searchGender = null;
		boolean searchFemale = toggleButton3.isChecked();
		boolean searchMale = toggleButton4.isChecked();
		if (searchFemale && searchMale) {
			searchGender = new String[2];
			searchGender[0] = Constants.FEMALE;
			searchGender[1] = Constants.MALE;
		} else if (searchFemale) {
			searchGender = new String[1];
			searchGender[0] = Constants.FEMALE;
		} else if (searchMale) {
			searchGender = new String[1];
			searchGender[0] = Constants.MALE;
		}
		settings.setGender(searchGender);
		DataController.getInstance().saveSettings();
	}

	@Override
	public void onClick(View view) {

		switch (view.getId()) {
		case R.id.toggleButton3:
			isSettingsChanged = true;
			if (!toggleButton3.isChecked() && !toggleButton4.isChecked()) {
				toggleButton4.setChecked(true);
			}
			break;
		case R.id.toggleButton4:
			isSettingsChanged = true;
			if (!toggleButton4.isChecked() && !toggleButton3.isChecked()) {
				toggleButton3.setChecked(true);
			}
			break;
		case R.id.id_setting_logout:

			Utils.showDialog(getActivity(),
					resources.getString(R.string.dlg_title_logout),
					resources.getString(R.string.dlg_msg_logout),
					resources.getString(R.string.btn_logout),
					resources.getString(R.string.cancel),
					new MatchstixDialogListener() {

						@Override
						public void onButtonTwoClick() {

						}

						@Override
						public void onButtonOneClick() {
							logout();
						}
					});
			break;
		case R.id.id_setting_update:
			if (!ConnectivityController.isNetworkAvailable(getActivity())) {
				CustomToast.showShortToast(getActivity(), R.string.no_internet);
				return;
			}
			if (DashBoardActivity.userBlock) {
				BlockedPopup.getInstance().showonDrawerBlockedUserDialog(
						getActivity());
				return;
			} else
				saveSettings();
			break;
		case R.id.id_contact_us:
			// LetsTalkActivity.createIntent(getActivity(),Long.parseLong("9223370591873954517"));
			Date date = new Date();
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyy");
			String[] emailTo = { Constants.EMAIL_RECIPIENTS };
			String[] emailCC = {};
			String emailText = "";
			String subject = getString(R.string.matchstix_support) + "("
					+ sdf.format(date) + ")-"
					+ DataController.getInstance().getUser().getDisplayName()
					+ " " + DataController.getInstance().getUser().getId();
			Intent emailIntent = Utils.openEmail(getActivity(), emailTo,
					emailCC, subject, emailText);
			startActivityForResult((Intent.createChooser(
					emailIntent,
					getString(R.string.matchstix_support) + "("
							+ sdf.format(date) + ")")), RQUEST_CODE);
			break;
		case R.id.ll_language:
			mRootView.findViewById(R.id.ll_language).setVisibility(View.GONE);
		//	mRootView.findViewById(R.id.id_setting_layout_bottom).setVisibility(View.GONE);
			mRootView.findViewById(R.id.ll_language_sppiner).setVisibility(View.VISIBLE);
			scroll.post(new Runnable() {
                @Override
                public void run() {
                    scroll.fullScroll(ScrollView.FOCUS_DOWN);
                }
            });
			/*
			 * Intent i = new Intent(getActivity(), LanguageSelection.class);
			 * startActivityForResult(i, Constants.REQ_LANGUAGE_SELECTION);
			 */
			break;
		case R.id.ok:
			mRootView.findViewById(R.id.ll_language_sppiner).setVisibility(View.GONE);
		//	mRootView.findViewById(R.id.id_setting_layout_bottom).setVisibility(View.VISIBLE);
			mRootView.findViewById(R.id.ll_language).setVisibility(View.VISIBLE);
			mRootView.findViewById(R.id.ok).setFocusable(false);
			if(!defaultLanguage.equalsIgnoreCase(lang)){
				if(lang!=null){
					SettingsFragment.defaultLanguage=lang;
			//		if(!getResources().getStringArray(R.array.language_array)[0].equalsIgnoreCase(SettingsFragment.defaultLanguage)){
						MatchstixApplication.setLocale(lang);
						logout();
						//addFragment();
						
				//	}
				}
			}
			/*
			 * Intent i = new Intent(getActivity(), LanguageSelection.class);
			 * startActivityForResult(i, Constants.REQ_LANGUAGE_SELECTION);
			 */
			break;
		default:
			break;
		}

	}

	protected void logout() {
		progressDialog = ProgressDialog.show(getActivity(), null,
				resources.getString(R.string.please_wait));
		AppEventsLogger logger= AppEventsLogger.newLogger(getActivity());
		logger.logEvent("Completed a Logout");
		BackgroundExecutor.getInstance().execute(new LogOutRequester());
	}

	@Override
	public void onResume() {

		super.onResume();
		dismissProgressDialog();
		MatchstixApplication.getInstance().addUIListener(
				SettingsUpdateListener.class, this);
		MatchstixApplication.getInstance().addUIListener(
				HTTPLogOutListener.class, this);
		toggleButton3.getBackground().setAlpha(255);
		toggleButton4.getBackground().setAlpha(255);
		mRootView.findViewById(R.id.ll_language).setOnClickListener(this);
		mRootView.findViewById(R.id.ok).setOnClickListener(this);
		((Spinner)mRootView.findViewById(R.id.static_spinner))
		.setOnItemSelectedListener(this);
	}

	@Override
	public void onPause() {
		super.onPause();
		MatchstixApplication.getInstance().removeUIListener(
				SettingsUpdateListener.class, this);
		MatchstixApplication.getInstance().removeUIListener(
				HTTPLogOutListener.class, this);
		saveSettingsLocally();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if (!isSettingsChanged) {
			return;
		}
		MatchstixApplication.getInstance().saveSettingsIfRequired();
	}

	@Override
	public void onDrawerOpened(View view) {

	}

	@Override
	public void onHTTPLogOutSuccess() {

		AccountManager.getInstance().disableAccount(
				DataController.getInstance().getFullJid());
		NotificationManager.getInstance().onClearNotifications();
		// MatchstixPreferences.getInstance().setJsonHashCode(0);
		// MatchstixPreferences.getInstance().setJsonHashCodeFlag(0);
		if (progressDialog != null) {
			progressDialog.dismiss();
		}
		DataController.getInstance().getUser().setLoggedIn(false);
		DataController.getInstance().saveUser();
		FacebookSdk.sdkInitialize(getActivity());
		LoginManager.getInstance().logOut();
		Intent mStartActivity = new Intent(getActivity(), SplashActivity.class);
		((Activity) getActivity()).startActivity(mStartActivity);
		((Activity) getActivity()).finish();
		AppEventsLogger logger= AppEventsLogger.newLogger(getActivity());
		logger.logEvent("Completed a Log out");
	}

	protected void dismissProgressDialog() {
		if (progressDialog != null && progressDialog.isShowing()) {
			progressDialog.dismiss();
		}
	}

	@Override
	public void onHTTPLogOutFailed(final int statusCode, final String msg) {
		getActivity().runOnUiThread(new Runnable() {

			@Override
			public void run() {
				dismissProgressDialog();
				if (!ConnectivityController.isNetworkAvailable(getActivity())) {
					CustomToast.showShortToast(getActivity(),
							R.string.no_internet);

				} else {
					CustomToast.showShortToast(getActivity(),
							R.string.error_try_again);
				}
			}
		});
	}

	@Override
	public void onSettingsUpdateSuccess() {
		getActivity().runOnUiThread(new Runnable() {

			@Override
			public void run() {
				dismissProgressDialog();
				isSettingsChanged = false;
				CustomToast.showShortToast(getActivity(),
						R.string.msg_settings_update_sucess);
			}
		});
	}

	@Override
	public void onSettingsUpdateFailed(final String msg) {
		getActivity().runOnUiThread(new Runnable() {

			@Override
			public void run() {
				dismissProgressDialog();
				CustomToast.showShortToast(getActivity(),
						getString(R.string.please_try_again));
			}
		});
	}

	@Override
	public void onUnauthorizedListener() {
		getActivity().runOnUiThread(new Runnable() {

			@Override
			public void run() {
				if (progressDialog != null) {
					progressDialog.dismiss();
				}
				Utils.showUnAuthorizedDialog(getActivity());
			}
		});
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == Constants.REQ_LANGUAGE_SELECTION
				&& resultCode == Constants.RES_LANGUAGE_SELECTION) {
			MatchstixApplication.getInstance();
			MatchstixApplication.setLocale(data
					.getStringExtra(Constants.KEY_LANGUAGE));
			logout();
			/*
			 * getActivity().startActivity(new
			 * Intent(getActivity(),DashBoardActivity.class));
			 * getActivity().finish();
			 */
		}
	}

	@Override
	public void onBlockedUser() {
		BlockedPopup.getInstance().showBlockedUserDialog(getActivity());
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position,
			long id) {
		lang = parent.getItemAtPosition(position).toString();
	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {
		// TODO Auto-generated method stub
		
	}
	private void addFragment() {
		FragmentManager fm = getFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();
		ft.replace(R.id.fragment_container, new SettingsFragment());
		ft.commit();
	}
}
