package com.matchstixapp.ui.fragments;

import java.util.ArrayList;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.matchstixapp.R;
import com.craterzone.cz_commons_lib.Logger;
import com.example.swipelibrary.SwipeFlingAdapterView;
import com.example.swipelibrary.SwipeFlingAdapterView.onFlingListener;
import com.matchstixapp.MatchstixApplication;
import com.matchstixapp.db.MatchstixPreferences;
import com.matchstixapp.db.SuggestionAction;
import com.matchstixapp.likecounter.MatchstixDialog;
import com.matchstixapp.listener.BlockedUserListener;
import com.matchstixapp.listener.LikeDislikeListner;
import com.matchstixapp.listener.ProfileListener;
import com.matchstixapp.modal.SuggestionDetail;
import com.matchstixapp.modal.SuggestionProfile;
import com.matchstixapp.requester.SuggestionProfileRequester;
import com.matchstixapp.services.SuggestionsProcessor;
import com.matchstixapp.ui.activity.DashBoardActivity;
import com.matchstixapp.ui.adapter.SwipeCardAdapter;
import com.matchstixapp.utils.BackgroundExecutor;
import com.matchstixapp.utils.CustomToast;
import com.matchstixapp.utils.Utils;

public class FriendProfileFragment extends BaseFragment implements
		OnClickListener, onFlingListener, ProfileListener, LikeDislikeListner {
	public static final String TAG = FriendProfileFragment.class.getName();

	private ArrayList<SuggestionDetail> suggestions;
	private View mRootView, cardLeftOne, cardLeftMore;
	private SwipeCardAdapter swipeAdapter;
	private ImageView likeImage, dislikeImage, infoImage;
	private SwipeFlingAdapterView flingContainer;
	private static long removeFriendId;
	private static int likeCounter=0;

	public static FriendProfileFragment newInstance(
			ArrayList<SuggestionDetail> suggestions) {
		FriendProfileFragment fragment = new FriendProfileFragment();
		fragment.suggestions = suggestions;
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		mRootView = inflater.inflate(R.layout.fragment_suggestion_layout,
				container, false);
		mRootView.setBackgroundColor(getResources().getColor(
				R.color.transparent));
		dislikeImage = (ImageView) mRootView.findViewById(R.id.image_dislike);
		likeImage = (ImageView) mRootView.findViewById(R.id.image_like);
		infoImage = (ImageView) mRootView.findViewById(R.id.image_info);

		cardLeftOne = mRootView.findViewById(R.id.view_card_left_one);
		cardLeftMore = mRootView.findViewById(R.id.view_card_left_two);

		dislikeImage.setOnClickListener(this);
		likeImage.setOnClickListener(this);
		infoImage.setOnClickListener(this);
		setAdapter();
		MatchstixApplication.getInstance().addUIListener(
				LikeDislikeListner.class, this);

		return mRootView;
	}

	private void setAdapter() {
		flingContainer = (SwipeFlingAdapterView) mRootView
				.findViewById(R.id.card_view);
		if (suggestions != null && suggestions.size() > 0) {
			if (suggestions.get(0).getUserId() == removeFriendId) {
				removeFriendId = 0;
				suggestions.remove(0);

				validateCards();
			}
		}
		swipeAdapter = new SwipeCardAdapter(getActivity(), R.id.card_view,
				suggestions);

		// set the listener and the adapter
		flingContainer.setAdapter(swipeAdapter);

		// Optionally add an OnItemClickListener
		flingContainer
				.setOnItemClickListener(new SwipeFlingAdapterView.OnItemClickListener() {
					@Override
					public void onItemClicked(int itemPosition,
							Object dataObject) {
						
						showFriendInfo(itemPosition);
					}
				});
	}

	@Override
	public void onClick(final View v) {
		switch (v.getId()) {
		case R.id.image_dislike:
			if (flingContainer == null) {
				return;
			}
			flingContainer.getSelectedView().findViewById(R.id.iv_slider_nope)
					.setVisibility(View.VISIBLE);
			onLikeDislikeButtonCliked(false);
			break;
		case R.id.image_like:
			if (flingContainer == null) {
				return;
			}
			flingContainer.getSelectedView().findViewById(R.id.iv_slider_like)
					.setVisibility(View.VISIBLE);
			onLikeDislikeButtonCliked(true);
			break;
		case R.id.image_info:
			showFriendInfo(0);
			break;
		}
		v.setEnabled(false);

		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				v.setEnabled(true);

			}
		}, 1000);
	}

	@Override
	public void onResume() {
		super.onResume();
		dismissProgressDialog();
		flingContainer.setFlingListener(this);

		((DashBoardActivity) getActivity()).updateTitleBarBG(true);

		if (suggestions != null && suggestions.size() == 1) {
			cardLeftMore.setVisibility(View.GONE);
			cardLeftOne.setVisibility(View.GONE);
		} else if (suggestions != null && suggestions.size() == 2) {
			cardLeftMore.setVisibility(View.GONE);
		}
		MatchstixApplication.getInstance().addUIListener(ProfileListener.class,
				this);
		mRootView.findViewById(R.id.image_dislike).setOnTouchListener(null);
		mRootView.findViewById(R.id.image_like).setOnTouchListener(null);
	}

	@Override
	public void onPause() {
		super.onPause();
		((DashBoardActivity) getActivity()).updateTitleBarBG(false);
		MatchstixApplication.getInstance().removeUIListener(
				ProfileListener.class, this);
	}

	private void onLikeDislikeButtonCliked(boolean liked) {
		if (liked) {
			flingContainer.getTopCardListener().selectRight();
		} else {
			flingContainer.getTopCardListener().selectLeft();
		}
	}

	public static void onCardExit(boolean liked, SuggestionDetail detail) {
		if (liked)
			BackgroundExecutor.getInstance().execute(
					new SuggestionsProcessor(new SuggestionAction(detail
							.getUserId(), liked)));
		else
			BackgroundExecutor.getInstance().execute(
					new SuggestionsProcessor(new SuggestionAction(detail
							.getUserId(), liked)));
	}

	private boolean validateCards() {
		if (suggestions.size() == 0) {
			close();
			return false;
		} else if (suggestions.size() == 1) {
			cardLeftOne.setVisibility(View.GONE);
		} else if (suggestions.size() == 2) {
			cardLeftMore.setVisibility(View.GONE);
		} else if (suggestions.size() > 2) {
			cardLeftMore.setVisibility(View.VISIBLE);
		}
		return true;
	}

	private void close() {
		try {
			FragmentManager fm = getActivity().getSupportFragmentManager();
			fm.popBackStack();
		} catch (Exception e) {
			Logger.e(TAG, e.getMessage());
		}
		/*
		 * FragmentTransaction ft = fm.beginTransaction();
		 * ft.replace(R.id.fragment_container, new MatchFragment());
		 * ft.commit();
		 */

	}

	private void showFriendInfo(int index) {
		if(likeCounter==2){
			MatchstixPreferences.getInstance().setLikeEnd(System.currentTimeMillis()+DashBoardActivity.countDownTime);
			likeCounter++;
		}
		else if(likeCounter>2){
			new MatchstixDialog(getActivity(), "");
			return;
		}
		if (getActivity() != null
				&& getActivity().getSupportFragmentManager() != null
				&& suggestions.size() > index) {
			SuggestionDetail details = (SuggestionDetail) suggestions
					.get(index);
			fetchProfile(details);
		}
		likeCounter++;
	}

	private void dismissProgressDialog() {
		if (progressDialog != null && progressDialog.isShowing()) {
			progressDialog.dismiss();
		}
	}

	ProgressDialog progressDialog;

	private void fetchProfile(final SuggestionDetail detail) {
		progressDialog = ProgressDialog.show(getActivity(), null,
				getResources().getString(R.string.loading));
		BackgroundExecutor.getInstance().execute(
				new SuggestionProfileRequester(detail));

	}

	@Override
	public void removeFirstObjectInAdapter() {
		if (suggestions != null && suggestions.size() > 0) {
			suggestions.remove(0);
			swipeAdapter.notifyDataSetChanged();
			validateCards();
		}
	}

	@Override
	public void onLeftCardExit(Object dataObject) {
		swipeAdapter.notifyDataSetChanged();
		onCardExit(false, (SuggestionDetail) dataObject);
		mRootView.findViewById(R.id.image_dislike).setEnabled(true);
		mRootView.findViewById(R.id.image_like).setEnabled(true);
		mRootView.findViewById(R.id.image_info).setEnabled(true);
	}

	@Override
	public void onRightCardExit(Object dataObject) {
		swipeAdapter.notifyDataSetChanged();
		onCardExit(true, (SuggestionDetail) dataObject);
		mRootView.findViewById(R.id.image_dislike).setEnabled(true);
		mRootView.findViewById(R.id.image_like).setEnabled(true);
		mRootView.findViewById(R.id.image_info).setEnabled(true);
	}

	@Override
	public void onAdapterAboutToEmpty(int itemsInAdapter) {
		// Ask for more data here
		swipeAdapter.notifyDataSetChanged();
		itemsInAdapter++;
	}

	@Override
	public void onScroll(float scrollProgressPercent) {
		if (flingContainer == null || flingContainer.getSelectedView() == null) {
			return;
		}
		View viewLike = flingContainer.getSelectedView().findViewById(
				R.id.iv_slider_like);
		View viewNope = flingContainer.getSelectedView().findViewById(
				R.id.iv_slider_nope);

		if (scrollProgressPercent > 0
				&& viewNope.getVisibility() == View.INVISIBLE) {
			viewLike.setVisibility(View.VISIBLE);

			if (suggestions.size() > 2) {
				cardLeftMore.setVisibility(View.INVISIBLE);

			} else if (suggestions.size() == 2) {
				cardLeftOne.setVisibility(View.INVISIBLE);
			}

		} else if (scrollProgressPercent < 0
				&& viewLike.getVisibility() == View.INVISIBLE) {
			viewNope.setVisibility(View.VISIBLE);

			if (suggestions.size() > 2) {
				cardLeftMore.setVisibility(View.INVISIBLE);

			} else if (suggestions.size() == 2) {
				cardLeftOne.setVisibility(View.INVISIBLE);
			}

		} else {
			viewLike.setVisibility(View.INVISIBLE);
			viewNope.setVisibility(View.INVISIBLE);

			if (suggestions.size() > 2) {
				cardLeftMore.setVisibility(View.VISIBLE);

			} else if (suggestions.size() == 2) {
				cardLeftOne.setVisibility(View.VISIBLE);
			}
		}
		mRootView.findViewById(R.id.image_dislike).setEnabled(false);
		mRootView.findViewById(R.id.image_like).setEnabled(false);
		mRootView.findViewById(R.id.image_info).setEnabled(false);
	}

	public void onCardClicked(int position) {
		showFriendInfo(position);
	}

	@Override
	public void onProfileDownloadSuccess(final SuggestionProfile profile,
			final SuggestionDetail detail) {
		getActivity().runOnUiThread(new Runnable() {

			@Override
			public void run() {
				dismissProgressDialog();
				if (detail != null) {
					FragmentTransaction ft = getActivity()
							.getSupportFragmentManager().beginTransaction();
					ft.replace(R.id.fragment_container,
							InfoFragment.newInstance(detail, profile));
					ft.addToBackStack(InfoFragment.TAG);
					ft.commit();
				}
			}
		});
	}

	@Override
	public void onProfileDownloadFailed(final String msg) {
		getActivity().runOnUiThread(new Runnable() {

			@Override
			public void run() {
				dismissProgressDialog();
				CustomToast.showShortToast(getActivity(), msg);
			}
		});
	}

	@Override
	public void onLikeListner(long friendId) {
		removeFriendId = friendId;
		if (suggestions != null && suggestions.size() > 0) {
			if (suggestions.get(0).getUserId() == friendId) {
				suggestions.remove(0);
			}
			swipeAdapter.notifyDataSetChanged();
			validateCards();
		}
	}

	@Override
	public void onDisLikeListner(long friendId) {
		removeFriendId = friendId;
		if (suggestions != null && suggestions.size() > 0) {
			if (suggestions.get(0).getUserId() == friendId) {
				suggestions.remove(0);
			}
			swipeAdapter.notifyDataSetChanged();
			validateCards();
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		MatchstixApplication.getInstance().removeUIListener(
				LikeDislikeListner.class, this);

	}

	@Override
	public void onUnauthorizedListener() {
		getActivity().runOnUiThread(new Runnable() {

			@Override
			public void run() {
				Utils.showUnAuthorizedDialog(getActivity());
			}
		});
	}

	@Override
	public void onBlockedUser() {
		getActivity().runOnUiThread(new Runnable() {

			@Override
			public void run() {
				Utils.showBlockedUserDialog(getActivity());
			}
		});
	}

}