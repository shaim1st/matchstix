package com.matchstixapp.ui.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.craterzone.cz_commons_lib.DisplayUtil;
import com.craterzone.czimageloader.universalImageLoader.UniversalImageLoaderUtil;
import com.facebook.appevents.AppEventsLogger;
import com.matchstixapp.R;
import com.matchstixapp.listener.FriendsDownloadListener;
import com.matchstixapp.modal.Friend;
import com.matchstixapp.modal.Photo;
import com.matchstixapp.modal.SuggestionProfile;
import com.matchstixapp.modal.User;

public class LetsTalkFragment extends Fragment {
	public static final String TAG = LetsTalkFragment.class.getSimpleName();
	private LetsTalkActionListener listener;
	private View mRootView;
	private ImageView viewProfile, viewSearch;
	private TextView tvTalkWith;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mRootView = inflater.inflate(R.layout.fragment_lets_talk, container,
				false);
		tvTalkWith = (TextView) mRootView.findViewById(R.id.tv_talk_with);
		viewProfile = (ImageView) mRootView.findViewById(R.id.user_avatar);
		viewSearch = (ImageView) mRootView.findViewById(R.id.friend_avatar);
		Log.d(TAG, "Match user");
		/* Implementing action buttons */
		ImageView viewStartTalk = (ImageView) mRootView
				.findViewById(R.id.iv_start_talk);
		TextView viewSendMsg = (TextView) mRootView
				.findViewById(R.id.tv_send_msg);
		TextView viewKeepPlaying = (TextView) mRootView
				.findViewById(R.id.tv_keep_playing);
		playAnimation();

		viewStartTalk.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				onClickStartTalk(v);
			}
		});
		viewSendMsg.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				onClickSendMsg(v);
			}
		});
		viewKeepPlaying.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				onClickKeepPlaying(v);
			}
		});
		AppEventsLogger logger= AppEventsLogger.newLogger(getActivity());
		logger.logEvent("Completed a Match");
		return mRootView;
	}

	private void playAnimation() {
		float userTransition = DisplayUtil.getScreenWidth(getActivity()) / 2
				- DisplayUtil.dp(67, getActivity());
		float friendTransition = DisplayUtil.getScreenWidth(getActivity()) / 2
				- DisplayUtil.dp(67, getActivity());
		TranslateAnimation anim1 = new TranslateAnimation(userTransition,-userTransition, Animation.ABSOLUTE, Animation.ABSOLUTE);
		anim1.setDuration(2000);
		anim1.setFillAfter(true);
		TranslateAnimation anim2 = new TranslateAnimation(-friendTransition,friendTransition, Animation.ABSOLUTE, Animation.ABSOLUTE);
		anim2.setDuration(2000);
		anim2.setFillAfter(true);
		mRootView.findViewById(R.id.user_avatar).startAnimation(anim1);
		mRootView.findViewById(R.id.friend_avatar).startAnimation(anim2);
	}

	public interface LetsTalkActionListener {
		public void onClickStartTalk(View view);

		public void onClickSendMsg(View view);

		public void onClickKeepPlaying(View view);
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

		if (activity instanceof LetsTalkActionListener) {
			listener = (LetsTalkActionListener) activity;
		} else {
			Log.e("Class Cast Exception", activity.getLocalClassName()
					+ " Class must implement LetsTalkActionListener");
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		listener = null;
	}

	private void onClickStartTalk(View view) {
		listener.onClickStartTalk(view);
	}

	private void onClickSendMsg(View view) {
		listener.onClickSendMsg(view);
	}

	private void onClickKeepPlaying(View view) {
		listener.onClickKeepPlaying(view);
	}

	private void setAvatar(String avatarUrl, ImageView avatar) {
		if (!TextUtils.isEmpty(avatarUrl)) {
			/*
			 * UniversalImageLoaderUtil.loadImage(avatarUrl, avatar, null);
			 */
			UniversalImageLoaderUtil.loadImageWithDefaultImage(avatarUrl,
					avatar, null, R.drawable.default_profile);
		}
	}

	public void setProfileAvatars(User user, Friend friend) {
		if (user != null && !user.getAvatarURL().isEmpty()) {
			setAvatar(user.getAvatarURL(), viewProfile);
		}

		if (friend != null) {
			if (!friend.getProfilePicURL().isEmpty()) {
				setAvatar(friend.getProfilePicURL(), viewSearch);
			}
			if (!friend.getFirstName().isEmpty()) {
				tvTalkWith.setText(friend.getFirstName() + " "
						+ getResources().getString(R.string.msg_wants_talk));
			}
		}
	}

	public void setProfileAvatars(User user, SuggestionProfile friendProfile) {
		if (user != null && user.getAvatarURL() != null
				&& !user.getAvatarURL().isEmpty()) {
			setAvatar(user.getAvatarURL(), viewProfile);
		}

		if (friendProfile != null) {
			for (Photo pic : friendProfile.getImageURLs()) {
				if (!pic.getImageURL().isEmpty() && pic.isProfilePic()) {
					setAvatar(pic.getImageURL(), viewSearch);
				}
			}
			if (!friendProfile.getFirstName().isEmpty()) {
				tvTalkWith.setText(friendProfile.getFirstName() + " "
						+ getResources().getString(R.string.msg_wants_talk));
			}
		}
	}

}
