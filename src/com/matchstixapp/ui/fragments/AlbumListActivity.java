package com.matchstixapp.ui.fragments;

import java.util.ArrayList;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.craterzone.cz_commons_lib.JsonUtil;
import com.facebook.AccessToken;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.matchstixapp.R;
import com.matchstixapp.GAScreen;
import com.matchstixapp.MatchstixApplication;
import com.matchstixapp.db.DataController;
import com.matchstixapp.listener.FBAlbumPhotosDownloadListener;
import com.matchstixapp.listener.FBAlbumsDownloadListener;
import com.matchstixapp.listener.Keys;
import com.matchstixapp.modal.Album;
import com.matchstixapp.modal.FBAlbumPhotos;
import com.matchstixapp.modal.FBAlbums;
import com.matchstixapp.ui.activity.AlbumPhotosGridActivity;
import com.matchstixapp.ui.activity.BaseFragmentActivity;
import com.matchstixapp.ui.adapter.AlbumListAdapter;
import com.matchstixapp.utils.Constants;
import com.matchstixapp.utils.CustomToast;

public class AlbumListActivity extends BaseFragmentActivity implements FBAlbumsDownloadListener,FBAlbumPhotosDownloadListener{

	protected static final String TAG = AlbumListActivity.class.getName();
	private ListView listView;
	private AlbumListAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		MatchstixApplication.tracker.setScreenName(GAScreen.ALBUM_LIST_SCREEN);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_album);
		listView = (ListView) findViewById(R.id.listview);
		adapter = new AlbumListAdapter(this);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				Album album = (Album) adapter.getItem(position);
				launchPhotoGridActivity(album);
			}
		});
			loadAlbums();
	}
	
	private ProgressDialog progressDialog;

	private void loadAlbums() {
		progressDialog = ProgressDialog.show(this, null,
				getResources().getString(R.string.loading));
		//BackgroundExecutor.getInstance().execute(new FBAlbumsDownloadRequester());
		FacebookSdk.sdkInitialize(this);
		GraphRequest request =  new GraphRequest(
			    AccessToken.getCurrentAccessToken(),
			    DataController.getInstance().getUser().getFbUserId() + "/albums",
			    null,
			    HttpMethod.GET,
			    new GraphRequest.Callback() {
			        public void onCompleted(GraphResponse response) {
			        	if(response.getError() ==  null){
			        		FBAlbums albumList = (FBAlbums) JsonUtil.toModel(response.getRawResponse(),
									FBAlbums.class);
							if (albumList != null && albumList.getData() != null  && albumList.getData().size()>0) {
								DataController.getInstance().updateFBAlbums(albumList);
								adapter.setAlbums(albumList.getData());
							}else{
								CustomToast.showShortToast(AlbumListActivity.this, R.string.you_have_no_album);
								finish();
							}
			        	}else{
			        		CustomToast.showShortToast(AlbumListActivity.this, R.string.unknown_error);
			        	}
			        }
			    }
			);
		  Bundle parameters = new Bundle();
          parameters.putString("fields", "id,name,cover_photo,count,created_time");
          request.setParameters(parameters);
          request.executeAsync();
		
	}

	
	protected void launchPhotoGridActivity(final Album album) {
		final String albumId = album.getId();
			progressDialog = ProgressDialog.show(this,
					null, getResources().getString(R.string.loading));
			//BackgroundExecutor.getInstance().execute(new FBAlbumPhotosRequester(albumId));
			GraphRequest request =  new GraphRequest(
				    AccessToken.getCurrentAccessToken(),
				    "/" + albumId + "/photos",
				    null,
				    HttpMethod.GET,
				    new GraphRequest.Callback() {
				        public void onCompleted(GraphResponse response) {
				        	if(response.getError() == null){
				        		FBAlbumPhotos details = (FBAlbumPhotos) JsonUtil.toModel(
										response.getRawResponse(), FBAlbumPhotos.class);
				        		DataController.getInstance().refreshFBAlbumPhotos(albumId, details);
				        		onPhotoFetched(albumId,details);
				        	}else{
				        		CustomToast.showShortToast(AlbumListActivity.this, R.string.unknown_error);
				        	}
				        }
				    }
				);
			  Bundle parameters = new Bundle();
              parameters.putString("type", "thumbnail");
              request.setParameters(parameters);
              request.executeAsync();
	}

	private void onPhotoFetched(String albumId, FBAlbumPhotos albumDetails) {
		Intent intent = new Intent(AlbumListActivity.this,
				AlbumPhotosGridActivity.class);
		intent.putExtra(Keys.KEY_ALBUM_ID, albumId);
		startActivityForResult(intent, Constants.REQUEST_CODE_ALBUM_PICS_GRID);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == Constants.REQUEST_CODE_ALBUM_PICS_GRID
				&& resultCode == RESULT_OK) {
			String localUrl = data.getStringExtra(Keys.KEY_PHOTO_URI);
			String url = data.getStringExtra("IMAGE_URL");
			if (!TextUtils.isEmpty(localUrl)) {
				setResult(resultCode, data);
				finish();
			}
		}
	}


	@Override
	public void onFBAlbumPhotosDownloadSucceess(final String albumId,final FBAlbumPhotos photos) {
		runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				onPhotoFetched(albumId,photos);
			}
		});
		
	}


	@Override
	public void onFBAlbumPhotosDownloadFailed(final String msg) {
		runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				CustomToast.showShortToast(AlbumListActivity.this, msg);
			}
		});
		
	}


	@Override
	public void onFBAlbumsDownloadSucceess(final FBAlbums albums) {
		runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				progressDialog.dismiss();
				ArrayList<Album> data=albums.getData();
				if (data!=null && data.size()>0) {
					
					adapter.setAlbums(data);
				}else{
					CustomToast.showShortToast(AlbumListActivity.this, R.string.you_have_no_album);
					finish();
				}
			}
		});

	}


	@Override
	public void onFBAlbumsDownloadFailed(final String msg) {
		runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				progressDialog.dismiss();
				CustomToast.showShortToast(AlbumListActivity.this, msg);
				finish();
			}
		});
		
	}
	@Override
	protected void onResume() {
		super.onResume();
		MatchstixApplication.getInstance().addUIListener(FBAlbumsDownloadListener.class, this);
		MatchstixApplication.getInstance().addUIListener(FBAlbumPhotosDownloadListener.class, this);
		if (progressDialog!=null && progressDialog.isShowing()) {
			progressDialog.dismiss();
		}
		
	}
	@Override
	protected void onPause() {
		super.onPause();
		MatchstixApplication.getInstance().removeUIListener(FBAlbumsDownloadListener.class, this);
		MatchstixApplication.getInstance().removeUIListener(FBAlbumPhotosDownloadListener.class, this);
		
	}


	@Override
	public void onUnauthorizedListener() {
		
	}

}
