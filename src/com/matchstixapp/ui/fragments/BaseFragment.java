package com.matchstixapp.ui.fragments;

import com.matchstixapp.xmpplibrary.ActivityManager;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class BaseFragment extends android.support.v4.app.Fragment{

	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		ActivityManager.getInstance().onCreate(getActivity());
		super.onCreate(savedInstanceState);
		return super.onCreateView(inflater, container, savedInstanceState);
	}
	
	@Override
	public void onResume() {
		ActivityManager.getInstance().onResume(getActivity());
		super.onResume();
	}

	@Override
	public void onPause() {
		ActivityManager.getInstance().onPause(getActivity());
		super.onPause();
	}

	@Override
	public void onDestroy() {
		ActivityManager.getInstance().onDestroy(getActivity());
		super.onDestroy();
	}

	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		ActivityManager.getInstance().onActivityResult(getActivity(), requestCode,
				resultCode, data);
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void startActivity(Intent intent) {
		ActivityManager.getInstance().updateIntent(getActivity(), intent);
		super.startActivity(intent);
	}

	@Override
	public void startActivityForResult(Intent intent, int requestCode) {
		ActivityManager.getInstance().updateIntent(getActivity(), intent);
		super.startActivityForResult(intent, requestCode);
	}
}
