package com.matchstixapp.ui.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

import com.matchstixapp.R;
import com.matchstixapp.modal.AlbumPhoto;
import com.matchstixapp.ui.adapter.FBPhotoGridAdapter;
import com.matchstixapp.utils.TypeFaceTextView;

public class FBPhotoGridFragment extends BaseFragment implements OnItemClickListener{
	private OnGridItemClick click;
	private GridView fbGridView;
	private FBPhotoGridAdapter adapter;
	private View mRootView;
	private String title;
	
	public interface OnGridItemClick{
		public void show(AlbumPhoto photo);
	}
	
	public FBPhotoGridFragment(OnGridItemClick click,FBPhotoGridAdapter adapter,String title){
		this.adapter = adapter;
		this.title = title;
		this.click = click;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View mRootView = inflater.inflate(R.layout.fragment_fb_photo_grid, null);
		fbGridView = (GridView) mRootView.findViewById(R.id.id_fb_album_grid);
		TypeFaceTextView tvTitle = (TypeFaceTextView) mRootView.findViewById(R.id.tv_grid_title);
		tvTitle.setText(title);
		fbGridView.setOnItemClickListener(this);
		fbGridView.setAdapter(adapter);
		return mRootView;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		if(click!=null){
			click.show((AlbumPhoto) adapter.getItem(position));			
		}
	}

}
