package com.matchstixapp.ui.fragments;

import java.util.ArrayList;
import java.util.HashMap;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.matchstixapp.R;
import com.matchstixapp.MatchstixApplication;
import com.matchstixapp.db.SuggestionAction;
import com.matchstixapp.listener.LastSeenListener;
import com.matchstixapp.listener.LikeDislikeListner;
import com.matchstixapp.modal.Photo;
import com.matchstixapp.modal.SuggestionDetail;
import com.matchstixapp.modal.SuggestionProfile;
import com.matchstixapp.requester.LastSeenRequester;
import com.matchstixapp.services.SuggestionsProcessor;
import com.matchstixapp.ui.pager.FriendProfilePicsSlidePage;
import com.matchstixapp.ui.pager.PagerController;
import com.matchstixapp.ui.pager.SlideHorizontalPager;
import com.matchstixapp.ui.pager.SlidePage;
import com.matchstixapp.ui.pager.SlidePageAdapter;
import com.matchstixapp.utils.BackgroundExecutor;
import com.matchstixapp.utils.TypeFaceTextView;
import com.matchstixapp.utils.Utils;

public class InfoFragment extends BaseFragment implements OnClickListener , LastSeenListener{
	public static final String TAG = InfoFragment.class.getSimpleName();
	private SlidePageAdapter mPagerAdapter;
	private PagerController controller = null;
	private SlideHorizontalPager mPager = null;
	private int slideLastPage = 0;
	private View mRootView;
	private SuggestionDetail detail;

	private ImageView ivLike;
	private ImageView ivDislike;
	private TypeFaceTextView tvName;
	private TypeFaceTextView tvAge;
	private TypeFaceTextView tvStatus;
	private TypeFaceTextView tvLastSeen;
	private TypeFaceTextView tvDistance;
	private SuggestionProfile profile;

	public static InfoFragment newInstance(SuggestionDetail details,
			SuggestionProfile profile) {
		InfoFragment fragment = new InfoFragment();
		fragment.detail = details;
		fragment.profile = profile;
		return fragment;
	}
	@Override
	public void onResume() {
		super.onResume();
		MatchstixApplication.getInstance().addUIListener(LastSeenListener.class, this);
	}
	
	@Override
	public void onPause() {
		super.onPause();
		MatchstixApplication.getInstance().removeUIListener(LastSeenListener.class, this);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mRootView = inflater.inflate(R.layout.friend_info_layout, container,
				false);

		ivDislike = (ImageView) mRootView.findViewById(R.id.image_frnd_dislike);
		ivLike = (ImageView) mRootView.findViewById(R.id.image_frnd_like);
		tvName = (TypeFaceTextView) mRootView.findViewById(R.id.name_friend);
		tvAge = (TypeFaceTextView) mRootView.findViewById(R.id.age_friend);
		tvStatus = (TypeFaceTextView) mRootView.findViewById(R.id.tv_status);
		tvLastSeen = (TypeFaceTextView) mRootView.findViewById(R.id.tv_last_seen);
		tvDistance = (TypeFaceTextView) mRootView.findViewById(R.id.tv_distance);
		ivLike.setOnClickListener(this);
		ivDislike.setOnClickListener(this);
		setDetails();
		BackgroundExecutor.getInstance().execute(new LastSeenRequester(detail.getUserId()));
		ArrayList<Photo> pics = profile.getImageURLs();
		if(pics != null && pics.size() > 0){
			setPager();
		}else{
		//	mRootView.findViewById(R.id.avatar).setVisibility(View.VISIBLE);
		}
		return mRootView;
	}

	private void setDetails() {
		String fname = detail.getFirstName();
		int age = detail.getAge();
		String status = profile.getAboutUser();
		String lastSeen = Utils.formatTime(getActivity(),profile.getLastLoginTime());
		String distance = Utils.formatDistance(getActivity(),profile.getDistance());
		if (TextUtils.isEmpty(fname)) {
			fname = "";
		}
		tvName.setText(fname);
		
		if (!(age < 0)) {
			tvAge.setText(""+ age);
		}
		if (!TextUtils.isEmpty(status)) {
			tvStatus.setText(status);
		}
		tvLastSeen.setText(lastSeen);
		tvDistance.setText(distance);

	}

	private void setPager() {
		if (getActivity() == null) {
			return;
		}
		mPagerAdapter = new SlidePageAdapter(getActivity()
				.getSupportFragmentManager());
		addSlidePages();
		mPager = (SlideHorizontalPager) mRootView
				.findViewById(R.id.horizontal_pager);
		controller = (PagerController) mRootView
				.findViewById(R.id.pager_controller);
		mPager.setAdapter(mPagerAdapter);
		mPager.setPagerController(controller);
		controller.setPager(mPager);
		controller.setPageCount(mPagerAdapter.getCount());
		controller.setCurrentPage(slideLastPage);
		controller.setFillCircle(false);
		mPager.setPagerController(controller);
		mPager.setCurrentItem(slideLastPage);
	}

	private void addSlidePages() {
		SlidePage page = null;

		ArrayList<Photo> pics = profile.getImageURLs();
		for (int i = 0; i < pics.size(); i++) {
			page = new FriendProfilePicsSlidePage();
			HashMap<String, Object> pageData = new HashMap<String, Object>();
			pageData.put(FriendProfilePicsSlidePage.KEY_PIC_URL, pics.get(i).getImageURL());
			page.setData(pageData);
			mPagerAdapter.addPage(page);
		}
		mPagerAdapter.notifyDataSetChanged();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.image_frnd_dislike:
			onClick(false);
			v.setEnabled(false);
			break;
		case R.id.image_frnd_like:
			onClick(true);
			v.setEnabled(false);
			break;

		}
	}

	private void onClick(boolean liked) {
		for(LikeDislikeListner listener : MatchstixApplication.getInstance().getUIListeners(LikeDislikeListner.class)){
			if(liked){
				listener.onLikeListner(detail.getUserId());
				BackgroundExecutor.getInstance().execute(new SuggestionsProcessor(new SuggestionAction(detail.getUserId(), liked)));
				getActivity().getSupportFragmentManager().popBackStack();
			}else{
				listener.onDisLikeListner(detail.getUserId());
			//	BackgroundExecutor.getInstance().execute(new SuggestionsProcessor(new SuggestionAction(detail.getUserId(), liked)));
				getActivity().getSupportFragmentManager().popBackStack();
			}
			return;
		}
	}

	@Override
	public void onInternetConnectionNotFound() {
		
	}

	@Override
	public void onLastSeenRequestSuccess(final long userid, final long milliseconds) {
		if( detail.getUserId() == userid ) {
			getActivity().runOnUiThread(new Runnable() {
				@Override
				public void run() {
					
					String lastSeen = Utils.formatTime(getActivity(), milliseconds);
					tvLastSeen.setText(lastSeen);
				}
			});
		}
	}

	@Override
	public void onLastSeenRequestFailed() {
		
	}
	@Override
	public void onUnauthorizedListener() {
		getActivity().runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				Utils.showUnAuthorizedDialog(getActivity());
			}
		});
	}
	


}
