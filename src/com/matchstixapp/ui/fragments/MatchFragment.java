package com.matchstixapp.ui.fragments;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.craterzone.cz_commons_lib.DisplayUtil;
import com.craterzone.czimageloader.universalImageLoader.UniversalImageLoaderUtil;
import com.matchstixapp.MatchstixApplication;
import com.matchstixapp.R;
import com.matchstixapp.db.DataController;
import com.matchstixapp.listener.SuggestionsListener;
import com.matchstixapp.modal.SuggestionDetail;
import com.matchstixapp.modal.SuggestionDetailsList;
import com.matchstixapp.requester.SuggestionsRequester;
import com.matchstixapp.ui.RippleBackground;
import com.matchstixapp.ui.WaveDrawable;
import com.matchstixapp.ui.activity.DashBoardActivity;
import com.matchstixapp.utils.BackgroundExecutor;
import com.matchstixapp.utils.BlockedPopup;
import com.matchstixapp.utils.Constants;
import com.matchstixapp.utils.TypeFaceTextView;
import com.matchstixapp.utils.Utils;

public class MatchFragment extends BaseFragment implements SuggestionsListener,
		OnClickListener {
	public static final String TAG = MatchFragment.class.getSimpleName();
	private View mRootView;
	private ImageView ivInvite, myImg;
	private TypeFaceTextView tvInvite;
	private TypeFaceTextView tvInstruction;
	private SuggestionsRequester suggestionsRequester;
	private WaveDrawable waveDrawable;
	RippleBackground rippleBackground;
	private boolean isRefresh = false;

	public MatchFragment setSuggestionRequester(
			SuggestionsRequester suggestionsRequester) {
		this.suggestionsRequester = suggestionsRequester;
		return this;
	}

	@SuppressWarnings("deprecation")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mRootView = inflater.inflate(R.layout.fragment_match_people, container,
				false);
		ivInvite = (ImageView) mRootView.findViewById(R.id.invite_frnd);
		tvInvite = (TypeFaceTextView) mRootView
				.findViewById(R.id.people_near_you);
		tvInstruction = (TypeFaceTextView) mRootView
				.findViewById(R.id.tv_invite_friends);
		setAvatarImg();
		waveDrawable = new WaveDrawable(R.color.app_theme_color,
				DisplayUtil.dp(100, getActivity()));
		ProgressBar progressView = (ProgressBar) mRootView
				.findViewById(R.id.id_search_profress);
		progressView.setBackgroundDrawable(waveDrawable);
		isRefresh = false;
		return mRootView;
	}

	private void setAvatarImg() {
		myImg = (ImageView) mRootView.findViewById(R.id.progressimage);
		String avatarUrl = DataController.getInstance().getUser()
				.getAvatarURL();
		if (!TextUtils.isEmpty(avatarUrl)) {
			UniversalImageLoaderUtil.loadImageWithDefaultImage(avatarUrl,
					myImg, null, R.drawable.default_profile);
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		MatchstixApplication.getInstance().addUIListener(
				SuggestionsListener.class, this);
		if (getActivity() instanceof DashBoardActivity) {
			DashBoardActivity activity = (DashBoardActivity) getActivity();
			activity.updateTitleBarBG(true);
			activity.updateBG(false);
		}
		loadSuggestions();
		mRootView.findViewById(R.id.progressimage).setOnClickListener(this);
		refreshOptions(isRefresh);
	}

	@Override
	public void onPause() {
		super.onPause();
		MatchstixApplication.getInstance().removeUIListener(
				SuggestionsListener.class, this);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		if (getActivity() instanceof DashBoardActivity) {
			DashBoardActivity activity = (DashBoardActivity) getActivity();
			activity.updateTitleBarBG(false);
			activity.updateBG(true);
		}
	}

	private long timeLoadStartTime;

	private void loadSuggestions() {
		refreshOptions(true);
		isRefresh = true;
		timeLoadStartTime = System.currentTimeMillis();
		if (suggestionsRequester != null
				&& suggestionsRequester.isMoreSuggestionAvailable()) {
			BackgroundExecutor.getInstance().execute(suggestionsRequester);
		} else {
			isRefresh = false;
		}
	}

	private void refreshOptions(final boolean searching) {
		if (getActivity() == null) {
			return;
		}
		ivInvite.setVisibility(searching ? View.INVISIBLE : View.VISIBLE);
		tvInstruction.setVisibility(searching ? View.INVISIBLE : View.VISIBLE);
		int invite = searching ? R.string.matches : R.string.no_matches;
		int resId = searching ? R.drawable.ic_invite_frnd
				: R.drawable.ic_setting;
		int instruction = searching ? R.string.invite_ur_frnd
				: R.string.change_settings;
		tvInvite.setText(invite);
		ivInvite.setImageResource(resId);
		tvInstruction.setText(instruction);
		if (searching) {
			AccelerateDecelerateInterpolator interpolator = new AccelerateDecelerateInterpolator();
			waveDrawable.setWaveInterpolator(interpolator);
			waveDrawable.startAnimation();
		} else {
			waveDrawable.startAnimation();
		}

		ivInvite.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				Utils.showFadeinAnimation(getActivity(), ivInvite);
				if (searching) {
					Utils.showInviteOptions(getActivity());
				} else {
					SettingsFragment fragment = new SettingsFragment();
					FragmentManager fm = getActivity()
							.getSupportFragmentManager();
					FragmentTransaction ft = fm.beginTransaction();
					ft.replace(R.id.fragment_container, fragment);
					ft.commit();
				}
			}
		});
	}

	protected void validateSuggestionDownloadTime(
			final ArrayList<SuggestionDetail> suggestionDetails) {
		final Runnable runnable = new Runnable() {

			@Override
			public void run() {
				onSuggestionsDownloaded(suggestionDetails);
			}
		};
		TimerTask task = new TimerTask() {

			@Override
			public void run() {
				Activity act = getActivity();
				if (act != null) {
					act.runOnUiThread(runnable);
				}
			}
		};
		if (System.currentTimeMillis() - timeLoadStartTime > Constants.WAIT_LOAD_TIME) {
			runnable.run();
		} else {
			new Timer().schedule(task, 1000);
		}
	}

	private void onSuggestionsDownloaded(
			ArrayList<SuggestionDetail> suggestionList) {
		if (!getActivity().isFinishing() && suggestionList != null
				&& getActivity() != null
				&& getActivity().getSupportFragmentManager() != null) {
			FragmentTransaction ft = getActivity().getSupportFragmentManager()
					.beginTransaction();
			ft.replace(R.id.fragment_container,
					FriendProfileFragment.newInstance(suggestionList),
					"suggestionsFragment").addToBackStack("matchfragment");
			ft.commit();
		}
	}

	@Override
	public void onSuccess(final SuggestionDetailsList suggestions) {
		getActivity().runOnUiThread(new Runnable() {

			@Override
			public void run() {
				if (suggestions != null) {
					validateSuggestionDownloadTime(suggestions
							.getSuggestionDetails());
				}
				// onSuggestionsDownloaded(suggestions.getSuggestionDetails());
			}
		});
	}

	@Override
	public void onFailed(final String msg) {
		getActivity().runOnUiThread(new Runnable() {

			@Override
			public void run() {
				refreshOptions(false);
				isRefresh = false;
			}
		});
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.progressimage:
			if (suggestionsRequester == null) {
				suggestionsRequester = new SuggestionsRequester(0);
				BackgroundExecutor.getInstance().execute(suggestionsRequester);
				refreshOptions(true);
				isRefresh = true;
			} else if (!suggestionsRequester.isMoreSuggestionAvailable()
					&& !suggestionsRequester.isPending()) {
				suggestionsRequester = new SuggestionsRequester(0);
				BackgroundExecutor.getInstance().execute(suggestionsRequester);
				refreshOptions(true);
				isRefresh = true;
			}
			break;

		default:
			break;
		}
	}

	@Override
	public void onUnauthorizedListener() {
		getActivity().runOnUiThread(new Runnable() {

			@Override
			public void run() {
				Utils.showUnAuthorizedDialog(getActivity());
			}
		});
	}

	@Override
	public void onBlockedUser() {
		getActivity().runOnUiThread(new Runnable() {

			@Override
			public void run() {
				BlockedPopup.getInstance().showBlockedUserDialog(getActivity());
			}
		});
	}

}