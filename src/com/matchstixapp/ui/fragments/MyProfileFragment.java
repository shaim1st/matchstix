package com.matchstixapp.ui.fragments;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.craterzone.cz_commons_lib.StringUtil;
import com.matchstixapp.R;
import com.matchstixapp.MatchstixApplication;
import com.matchstixapp.db.DataController;
import com.matchstixapp.listener.FBProfilePicsDownloadListener;
import com.matchstixapp.modal.AlbumPhoto;
import com.matchstixapp.modal.FBAlbumPhotos;
import com.matchstixapp.modal.User;
import com.matchstixapp.ui.activity.DashBoardActivity;
import com.matchstixapp.ui.pager.PagerController;
import com.matchstixapp.ui.pager.SlideHorizontalPager;
import com.matchstixapp.ui.pager.SlidePage;
import com.matchstixapp.ui.pager.SlidePageAdapter;
import com.matchstixapp.ui.pager.UserProfilePicsSlidePage;
import com.matchstixapp.utils.BlockedPopup;
import com.matchstixapp.utils.CustomToast;
import com.matchstixapp.utils.Utils;

public class MyProfileFragment extends BaseFragment implements OnClickListener,FBProfilePicsDownloadListener {
	public static final String TAG = MyProfileFragment.class.getSimpleName();

	private SlidePageAdapter mPagerAdapter;
	private PagerController controller = null;
	private SlideHorizontalPager mPager = null;	
	private int slideLastPage = 0;
	private View mRootView;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mRootView = inflater.inflate(R.layout.profile_layout, container, false);
		setUserDetails();
		loadProfilePicsFromFacebook();
		MatchstixApplication.getInstance().addUIListener(FBProfilePicsDownloadListener.class, this);
		return mRootView;
	}
	
	@Override
	public void onDestroyView() {
		// TODO Auto-generated method stub
		super.onDestroyView();
		MatchstixApplication.getInstance().removeUIListener(FBProfilePicsDownloadListener.class, this);
	}

	private ProgressDialog progressDialog;

	private void loadProfilePicsFromFacebook() {
		ArrayList<AlbumPhoto> photos = DataController.getInstance()
				.getUserProfilePics();
		if (photos != null && photos.size() > 0) {
			setPager();
			return;
		}
		mRootView.findViewById(R.id.avatar).setVisibility(View.VISIBLE);
	/*	progressDialog = ProgressDialog.show(getActivity(), null,
				getResources().getString(R.string.loading));
		*/
		//BackgroundExecutor.getInstance().execute(new FBProfilePicsRequester());
	}

	private void setUserDetails() {
		User user = DataController.getInstance().getUser();
		int age = user.getAge();
		String fname = user.getFirstName();

		String name = "";
		if (!TextUtils.isEmpty(fname)) {
			name += fname.trim();
		}
		if (TextUtils.isEmpty(name)
				&& !TextUtils.isEmpty(user.getDisplayName())) {
			name = user.getDisplayName();
		}
		String about = user.getAbout();
		if(StringUtil.isEmpty(about) ) {
			about = getString(R.string.default_status);
		}
		((TextView) mRootView.findViewById(R.id.id_name_user_txt_view))
				.setText(name);
		((TextView) mRootView.findViewById(R.id.id_age_user_txt_view))
				.setText(String.valueOf(age));
		((TextView) mRootView.findViewById(R.id.status_myprofile))
				.setText(about);
	}

	@Override
	public void onResume() {
		super.onResume();
		
		DashBoardActivity act = ((DashBoardActivity) getActivity());
		act.showAvatarAndNumber();
		if (getActivity() instanceof DashBoardActivity) {
			DashBoardActivity activity = ((DashBoardActivity) getActivity());
			activity.resetRightActionBarAction(R.drawable.ic_edit_profile, this);
			//MatchstixApplication.getInstance().removeUIListener(FBProfilePicsDownloadListener.class, activity);
		}

	}

	@Override
	public void onPause() {
		super.onPause();
	}

	private void setPager() {
		if (getActivity() == null) {
			return;
		}
		mPagerAdapter = new SlidePageAdapter(getActivity()
				.getSupportFragmentManager());
		addSlidePages();
		mPager = (SlideHorizontalPager) mRootView
				.findViewById(R.id.horizontal_pager);
		controller = (PagerController) mRootView
				.findViewById(R.id.pager_controller);
		mPager.setAdapter(mPagerAdapter);
		mPager.setPagerController(controller);
		controller.setPager(mPager);
		controller.setPageCount(mPagerAdapter.getCount());
		controller.setCurrentPage(slideLastPage);
		controller.setFillCircle(false);
		mPager.setPagerController(controller);
		mPager.setCurrentItem(slideLastPage);
	}

	private void addSlidePages() {
		ArrayList<AlbumPhoto> photos = DataController.getInstance()
				.getUserProfilePics();
		if (photos != null) {
			SlidePage page = null;
			for (int i = 0; i < photos.size(); i++) {
				page = new UserProfilePicsSlidePage();
				AlbumPhoto photo = photos.get(i);
				if (i == 0) {
					photo = new AlbumPhoto();
					photo.setId(String.valueOf(i));
					photo.setRemoteAvatar(DataController.getInstance()
							.getUser().getAvatarURL());
				}
				HashMap<String, Object> pageData = new HashMap<String, Object>();
				pageData.put(UserProfilePicsSlidePage.KEY_FB_ALBUM, photo);
				page.setData(pageData);
				mPagerAdapter.addPage(page);
			}
			mPagerAdapter.notifyDataSetChanged();
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.id_action_bar_right_img_view:
			if(DashBoardActivity.userBlock){
				BlockedPopup.getInstance().showonDrawerBlockedUserDialog(getActivity());
				break;
			}
			onEditIconClicked();
			break;
		}
	}

	private void onEditIconClicked() {
		if (getActivity() != null
				&& getActivity().getSupportFragmentManager() != null) {
			FragmentTransaction ft = getActivity().getSupportFragmentManager()
					.beginTransaction();
			ft.replace(R.id.fragment_container, EditFragment.newInstance(), EditFragment.TAG);
			ft.commit();

		}
	}

	@Override
	public void onFBProfilePicsDownloadSucceess(final FBAlbumPhotos profilePhotos) {
		getActivity().runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				
				progressDialog.dismiss();
				ArrayList<AlbumPhoto> pics=profilePhotos.getData();
				if (pics!=null) {
					setPager();
				}
			}
		});
		
	}

	@Override
	public void onFBProfilePicsDownloadFailed(final String msg) {
		getActivity().runOnUiThread(new Runnable() {
			@Override
			public void run() {
				progressDialog.dismiss();
				CustomToast.showShortToast(getActivity(), msg);
			}
		});
	}

	@Override
	public void onUnauthorizedListener() {
		getActivity().runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				if(progressDialog != null){
					progressDialog.dismiss();
				}
				Utils.showUnAuthorizedDialog(getActivity());
			}
		});
		}

}
