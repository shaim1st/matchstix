package com.matchstixapp.ui.fragments;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.craterzone.cz_commons_lib.StringUtil;
import com.craterzone.czimageloader.universalImageLoader.UniversalImageLoaderUtil;
import com.matchstixapp.R;
import com.matchstixapp.ConnectivityController;
import com.matchstixapp.db.DataController;
import com.matchstixapp.listener.DashBoardDrawerListener;
import com.matchstixapp.listener.Keys;
import com.matchstixapp.listener.MatchstixDialogListener;
import com.matchstixapp.listener.OnBackPressedListener;
import com.matchstixapp.modal.AlbumPhoto;
import com.matchstixapp.modal.JSONUpdateImages;
import com.matchstixapp.modal.User;
import com.matchstixapp.modal.UserImage;
import com.matchstixapp.pojo.JSONUpdateAbout;
import com.matchstixapp.requester.UpdateUserProfileDetailsRequester;
import com.matchstixapp.services.FBWebServices;
import com.matchstixapp.ui.activity.DashBoardActivity;
import com.matchstixapp.utils.BackgroundExecutor;
import com.matchstixapp.utils.Constants;
import com.matchstixapp.utils.CustomToast;
import com.matchstixapp.utils.Utils;

public class EditFragment extends BaseFragment implements OnClickListener,
		DashBoardDrawerListener, OnBackPressedListener {

	public static final String TAG = EditFragment.class.getSimpleName();
	private static final int INDEX_MAIN_PROFILE_PIC = 0;
	private View root;
	private EditText editTextStatus;
	private TextView statusLengthtv;

	private class TileViewHolder {
		AlbumPhoto photo;
		ImageView ivTile;
		ImageView ivIndicator;
		boolean isModified;

		boolean hasPic() {
			return photo != null;
		}
	}

	private final TileViewHolder[] TILES = new TileViewHolder[6];
	private int selected = -1;
	private static String localAvatarUri;

	public static EditFragment newInstance() {
		EditFragment fragment = new EditFragment();
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		root = inflater.inflate(R.layout.fragment_profile_edit, null);
		findTiles(root);
		loadDefaultTilesValues();
		editTextStatus = (EditText) root.findViewById(R.id.edittext_status);
		statusLengthtv = (TextView) root.findViewById(R.id.status_length);

		User user = DataController.getInstance().getUser();
		String about = user.getAbout();
		if (!StringUtil.isEmpty(about)) {
			/* about = "..."; */
			editTextStatus.setText(about);
			statusLengthtv.setText(String.valueOf(50 - about.length()));
		}
		((TextView) root.findViewById(R.id.id_name_user_txt_view)).setText(user
				.getFirstName());
		((TextView) root.findViewById(R.id.id_age_user_txt_view))
				.setText(String.valueOf(user.getAge()));
		return root;
	}

	private void findTiles(View root) {
		View tileLeft = root.findViewById(R.id.tileLeft);
		View tileTop = tileLeft.findViewById(R.id.tileTop);
		View tileBottom = tileLeft.findViewById(R.id.tileBottom);
		View tileRight = root.findViewById(R.id.tileRight);

		// current profile pic big view
		createTileViewHolderFor(INDEX_MAIN_PROFILE_PIC, tileTop);
		// Bottom tile 1
		createTileViewHolderFor(5, tileBottom.findViewById(R.id.tile1));
		// Bottom tile 2
		createTileViewHolderFor(4, tileBottom.findViewById(R.id.tile2));
		// Right top tile 1
		createTileViewHolderFor(1, tileRight.findViewById(R.id.tile1));
		// Right tile 2
		createTileViewHolderFor(2, tileRight.findViewById(R.id.tile2));
		// Right tile 3
		createTileViewHolderFor(3, tileRight.findViewById(R.id.tile3));

	}

	private void createTileViewHolderFor(int index, View view) {
		TileViewHolder holder = new TileViewHolder();
		TILES[index] = holder;
		holder.ivTile = (ImageView) view.findViewById(R.id.iv_tile_image);
		// set view index to access holder fast later on
		holder.ivTile.setTag(index);
		holder.ivTile.setOnClickListener(this);
		holder.ivIndicator = (ImageView) view.findViewById(R.id.iv_tile_icon);
	}

	@Override
	public void onResume() {
		super.onResume();

		if (getActivity() instanceof DashBoardActivity) {
			DashBoardActivity activity = ((DashBoardActivity) getActivity());
			activity.resetRightActionBarAction(R.drawable.ic_tick, this);
			activity.resetLeftActionBarAction(R.drawable.ic_back, this);
		}
		editTextStatus.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				statusLengthtv.setText(String.valueOf(50 - s.length()));
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			@Override
			public void afterTextChanged(Editable s) {
			}
		});
	}

	@Override
	public void onPause() {
		super.onPause();

		if (getActivity() instanceof DashBoardActivity) {
			DashBoardActivity activity = ((DashBoardActivity) getActivity());
			activity.resetRightActionBarAction(R.drawable.ic_massage, activity);
			activity.resetLeftActionBarAction(R.drawable.ic_menu, activity);
		}
	}

	private boolean isStatusUpdate() {
		String about = DataController.getInstance().getUser().getAbout();
		if ((StringUtil.isEmpty(about) && !StringUtil.isEmpty(editTextStatus
				.getText().toString()))
				|| (!editTextStatus.getText().toString()
						.equalsIgnoreCase(about))) {
			return true;
		}
		return false;
	}

	@Override
	public void onClick(View v) {

		if (v.getId() == R.id.id_action_bar_left_img_view) {
			close();

		} else if (!ConnectivityController.isNetworkAvailable(getActivity())) {
			CustomToast.showShortToast(getActivity(), R.string.no_internet);

		} else if ((v instanceof ImageView) && v.getTag() != null) {
			onTileClicked((Integer) v.getTag());

		} else if (v.getId() == R.id.id_action_bar_right_img_view) {

			if (!isAnyImageChanged() && !isStatusUpdate()) {
				close();
				return;
			}
			updateProfileDetails();
			close();
			if (selectedTileIndex == 0 && localAvatarUri != null
					&& !localAvatarUri.isEmpty()) {
				DataController.getInstance().getUser()
						.setLocalAvatar(localAvatarUri);
				DataController.getInstance().saveUser();
				localAvatarUri = null;
			}

		}
	}

	private void close() {

		FragmentManager fm = getActivity().getSupportFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();
		ft.replace(R.id.fragment_container, new MyProfileFragment());
		ft.commit();

	}

	private void updateProfileDetails() {
		JSONUpdateAbout userAboutMe = null;
		if (isStatusUpdate()) {
			User user = DataController.getInstance().getUser();
			user.setAbout(editTextStatus.getText().toString());
			DataController.getInstance().saveUser();
			userAboutMe = new JSONUpdateAbout(user.getAbout());
		}
		JSONUpdateImages imageRequest = null;
		if (isAnyImageChanged()) {
			imageRequest = new JSONUpdateImages();
			imageRequest.setList(getUpdatedProfilePics());
			saveModifiedPhotos();
		}

		BackgroundExecutor.getInstance()
				.execute(
						new UpdateUserProfileDetailsRequester(userAboutMe,
								imageRequest));

	}

	private void saveModifiedPhotos() {
		ArrayList<AlbumPhoto> pics = DataController.getInstance()
				.getUserProfilePics();
		pics.clear();
		for (int i = 0; i < TILES.length; i++) {
			TileViewHolder holder = TILES[i];
			if (holder.hasPic()) {
				pics.add(holder.photo);
			}
		}
		AlbumPhoto profilePic = pics.get(0);
		User user = DataController.getInstance().getUser();
		user.setAvatarURL(profilePic.getAvatar());
		user.setLocalAvatar(profilePic.getAvatar());
		DataController.getInstance().saveUser();
		DataController.getInstance().saveUserPics();
	}

	private void loadDefaultTilesValues() {
		ArrayList<AlbumPhoto> photos = DataController.getInstance()
				.getUserProfilePics();

		for (int i = 0; i < TILES.length; i++) {
			TileViewHolder holder = TILES[i];
			AlbumPhoto photo = null;
			if (i == INDEX_MAIN_PROFILE_PIC && photos.size() == 1) {
				photo = photos.get(i).copy();
				setTileHolderValues(holder, photo);
				holder.ivIndicator.setVisibility(View.GONE);
			} else if (i < photos.size()) {
				photo = photos.get(i).copy();
				setTileHolderValues(holder, photo);
			} else {
				resetTileHolder(holder);
			}
		}
	}

	private void resetTileHolder(TileViewHolder holder) {
		holder.photo = null;
		holder.ivIndicator.setVisibility(View.VISIBLE);
		holder.ivIndicator.setImageResource(R.drawable.ic_add_pic);
		holder.ivTile.setImageResource(R.drawable.place_holder_photo);
		holder.isModified = false;

	}

	private void setTileHolderValues(TileViewHolder holder, AlbumPhoto photo) {
		String sourceURL = photo.getAvatar() == null ? FBWebServices
				.getPicURL(photo.getId()) : photo.getAvatar();
		photo.setLocalAvatar(sourceURL);
		photo.setRemoteAvatar(sourceURL);
		holder.photo = photo;
		holder.ivIndicator.setVisibility(View.VISIBLE);
		holder.ivIndicator.setImageResource(R.drawable.ic_edit_profile);
		UniversalImageLoaderUtil.loadImageWithDefaultImage(sourceURL,
				holder.ivTile, null, R.drawable.place_holder_photo);
	}

	static int selectedTileIndex = 0;

	private void onTileClicked(final int index) {
		selectedTileIndex = index;
		final TileViewHolder holder = TILES[index];
		if (holder.hasPic() && getHolderPicsCount() > 1) {

			Utils.showDialogCancel(getActivity(),
					getResources().getString(R.string.dlg_title_delete_photos),
					"",
					getResources().getString(R.string.default_Pic),
					getResources().getString(R.string.delete),
					new MatchstixDialogListener() {

						@Override
						public void onButtonTwoClick() {
							shiftHolderValues(index);
							if (getHolderPicsCount() == 1) {
								TileViewHolder holder = TILES[INDEX_MAIN_PROFILE_PIC];
								holder.ivIndicator.setVisibility(View.GONE);
							}
						}

						@Override
						public void onButtonOneClick() {// For making Default
														// Pic
							TileViewHolder mholder = TILES[index];
							AlbumPhoto photo=TILES[INDEX_MAIN_PROFILE_PIC].photo;
							setTileHolderValues(TILES[INDEX_MAIN_PROFILE_PIC], mholder.photo);
							TILES[INDEX_MAIN_PROFILE_PIC].isModified = true;
							setTileHolderValues(TILES[index],photo);
							TILES[index].isModified = true;
						}
					});
		} else {
			Intent intent = new Intent(getActivity(), AlbumListActivity.class);
			intent.putExtra("INDEX", index);
			startActivityForResult(intent, Constants.REQUEST_CODE_ALBUMLIST);
		}
	}

	private int getHolderPicsCount() {
		int count = 0;
		for (int i = 0; i < TILES.length; i++) {
			if (TILES[i].hasPic()) {
				count++;
			}
		}
		return count;
	}

	protected void shiftHolderValues(int index) {
		if (index == TILES.length - 1) {
			resetTileHolder(TILES[index]);
		} else {

			for (int i = index; i < TILES.length - 1; i++) {
				TileViewHolder cholder = TILES[i];
				TileViewHolder nholder = TILES[i + 1];
				TileViewHolder holder = cholder;
				if (nholder.hasPic()) {
					setTileHolderValues(cholder, nholder.photo);
					holder = nholder;
				}
				resetTileHolder(holder);
			}
		}
		TILES[index].isModified = true;
	}

	protected void shiftHolderValuesWithPic(int index, int indexDefault,
			TileViewHolder rholder) {
		/*if (index == TILES.length - 1) {
			resetTileHolder(TILES[index]);
		} else*/ {

			// for (int i = index; i < TILES.length - 1; i++) {
			TileViewHolder cholder = TILES[index];
			TileViewHolder nholder;
			if (index == INDEX_MAIN_PROFILE_PIC)
				nholder = rholder;
			else
				nholder = TILES[indexDefault];

			TileViewHolder holder = cholder;
			if (nholder.hasPic()) {
				setTileHolderValues(cholder, nholder.photo);
				holder = nholder;
			}
		//	resetTileHolder(holder);
			// }
		}
		TILES[index].isModified = true;
	}

	protected void shiftHolderValuesWithPic1(int index, TileViewHolder nholder) {
		if (index == TILES.length - 1) {
			resetTileHolder(TILES[index]);
		} else {

			for (int i = index; i < TILES.length - 1; i++) {
				TileViewHolder cholder = TILES[index];
				TileViewHolder holder = cholder;
				if (nholder.hasPic()) {
					setTileHolderValues(cholder, nholder.photo);
					holder = nholder;
				}
				resetTileHolder(holder);
			}
		}
		TILES[index].isModified = true;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == Constants.REQUEST_CODE_ALBUMLIST
				&& resultCode == Activity.RESULT_OK) {
			String photoId = data.getStringExtra(Keys.KEY_PHOTO_ID);
			if (photoId != null) {
				String albumId = data.getStringExtra(Keys.KEY_ALBUM_ID);
				String uri = data.getStringExtra(Keys.KEY_PHOTO_URI);
				String url = data.getStringExtra("IMAGE_URL");
				AlbumPhoto photo = DataController.getInstance()
						.getFBAlbumPhoto(albumId, photoId);
				TileViewHolder holder = selectedTileIndex == INDEX_MAIN_PROFILE_PIC ? TILES[INDEX_MAIN_PROFILE_PIC]
						: getHolderForPhoto();

				photo = photo != null ? photo.copy() : photo;
				photo.setLocalAvatar(url);
				photo.setRemoteAvatar(url);
				setTileHolderValues(holder, photo);
				holder.isModified = true;

				if (selectedTileIndex == INDEX_MAIN_PROFILE_PIC) {
					int count = getHolderPicsCount();
					holder.ivIndicator.setVisibility(count > 1 ? View.VISIBLE
							: View.GONE);
				}
			}
		}
	}

	private TileViewHolder getHolderForPhoto() {
		for (int i = 0; i < TILES.length; i++) {
			if (!TILES[i].hasPic()) {
				return TILES[i];
			}
		}
		return null;
	}

	private boolean isAnyImageChanged() {

		for (int i = 0; i < TILES.length; i++) {
			if (TILES[i].isModified) {
				return true;
			}

		}
		return false;
	}

	public ArrayList<UserImage> getUpdatedProfilePics() {
		ArrayList<UserImage> list = new ArrayList<UserImage>();
		for (int i = 0; i < TILES.length; i++) {
			if (TILES[i].hasPic()) {
				UserImage image = new UserImage();
				image.setImageURL(TILES[i].photo.getRemoteAvatar());
				image.setProfilePic(i == INDEX_MAIN_PROFILE_PIC);
				list.add(image);
			}
		}
		return list;

	}

	@Override
	public void onDrawerOpened(View view) {
		/*
		 * if (isAnyImageChanged() || isStatusUpdate()) {
		 * updateProfileDetails();
		 * 
		 * if (selectedTileIndex == 0 && localAvatarUri != null &&
		 * !localAvatarUri.isEmpty()) { DataController.getInstance().getUser()
		 * .setLocalAvatar(localAvatarUri);
		 * DataController.getInstance().saveUser(); localAvatarUri = null; } }
		 */
	}

	@Override
	public void onBackPressed() {
		// save new changes if made
		// TO DO

		// notify the user: that his preferences has been saved
		// TO DO

		// change the action bar color
		((DashBoardActivity) getActivity()).updateTitleBarBG(false);

		// close the fragment
		close();
	}
}
