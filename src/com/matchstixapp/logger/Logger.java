package com.matchstixapp.logger;

import android.util.Log;
import android.util.SparseArray;

import com.matchstixapp.AppConfig;

/**
 * @author Diwakar Mishra
 *
 */
public class Logger {

	private static final SparseArray<String> STRING_LEVELS = new SparseArray<String>(
			7);
	private static final String STR_LEVEL_PRINT = "PRINT";
//	private DateFormat format = DateFormat.getBestDateTimePattern(locale, skeleton)
	static {
		STRING_LEVELS.put(Log.ASSERT, "ASSERT");
		STRING_LEVELS.put(Log.DEBUG, "DEBUG");
		STRING_LEVELS.put(Log.ERROR, "ERROR");
		STRING_LEVELS.put(Log.INFO, "INFO");
		STRING_LEVELS.put(Log.VERBOSE, "VERBOSE");
		STRING_LEVELS.put(Log.WARN, "WARN");

	}

	private static void writeLog(int level, String tag, String msg) {
		if (AppConfig.ENABLE_LOGS) {
			switch (level) {
			case Log.DEBUG:
				Log.d(tag, msg);
				break;
			case Log.ASSERT:
				Log.wtf(tag, msg);
				break;
			case Log.ERROR:
				Log.e(tag, msg);
				break;
			case Log.INFO:
				Log.i(tag, msg);
				break;
			case Log.VERBOSE:
				Log.v(tag, msg);
				break;
			case Log.WARN:
				Log.w(tag, msg);
				break;

			default:
				Log.println(0, tag, msg);
			}
		}
		if (AppConfig.ENABLE_FILE_LOGS) {
			writeFileLog(level, tag, msg);
		}
	}

	private static void writeFileLog(int level, String tag, String msg) {
		String strLevel = STRING_LEVELS.get(level, STR_LEVEL_PRINT);
		// TODO:
	}

	public static void d(String tag, String msg) {
		writeLog(Log.DEBUG, tag, msg);
	}

	public static void e(String tag, String msg) {
		writeLog(Log.ERROR, tag, msg);
	}

	public static void i(String tag, String msg) {
		writeLog(Log.INFO, tag, msg);
	}

	public static void w(String tag, String msg) {
		writeLog(Log.WARN, tag, msg);
	}

	public static void print(String tag, String msg) {
		writeLog(-1, tag, msg);
	}

}
