package com.matchstixapp.language;

import com.matchstixapp.MatchstixApplication;
import com.matchstixapp.R;
import com.matchstixapp.PNV.PNVFragmentEnterNumber;
import com.matchstixapp.R.layout;
import com.matchstixapp.ui.activity.BoardingActivity;
import com.matchstixapp.ui.adapter.SppinerAdapter;
import com.matchstixapp.ui.fragments.BaseFragment;
import com.matchstixapp.ui.fragments.SettingsFragment;
import com.matchstixapp.utils.Constants;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.AdapterView.OnItemSelectedListener;

public class LanguageSelectionDefault extends BaseFragment implements
OnItemSelectedListener, OnClickListener {
private Intent i;
private String lang;
private String langDefault;
private View mRootView;

public static LanguageSelectionDefault newInstance() {
	return new LanguageSelectionDefault();
}
@Override
public View onCreateView(LayoutInflater inflater, ViewGroup container,
		Bundle savedInstanceState) {
super.onCreate(savedInstanceState);
mRootView = inflater.inflate(R.layout.activity_language_selection,container, false);
mRootView.findViewById(R.id.ll_actionbar).setVisibility(View.GONE);
String[] options=getResources().getStringArray(R.array.language_array);
Spinner staticSpinner = (Spinner) mRootView.findViewById(R.id.static_spinner);
ArrayAdapter<String> myAdapter = new SppinerAdapter(getActivity(), R.layout.listview, options);
staticSpinner.setAdapter(myAdapter);
return mRootView;
}

@Override
public void onResume() {
((Spinner) mRootView.findViewById(R.id.static_spinner))
		.setOnItemSelectedListener(this);
mRootView.findViewById(R.id.id_action_bar_left_img_view).setOnClickListener(this);
mRootView.findViewById(R.id.ok).setOnClickListener(this);
super.onResume();
}

@Override
public void onItemSelected(AdapterView<?> parent, View view, int position,
	long id) {
	lang=parent.getItemAtPosition(position).toString();
	
}

@Override
public void onNothingSelected(AdapterView<?> parent) {
// TODO Auto-generated method stub
}

private void addFragment() {
	FragmentManager fm = getFragmentManager();
	FragmentTransaction ft = fm.beginTransaction();
	ft.replace(R.id.fragment_container, PNVFragmentEnterNumber.newInstance()).addToBackStack("PNVFragmentEnterNumber");
	ft.commit();
}

@Override
public void onClick(View v) {
switch (v.getId()) {
case R.id.id_action_bar_left_img_view:
	Intent i = new Intent(getActivity(), BoardingActivity.class);
	startActivity(i);
	getActivity().finish();
	break;
case R.id.ok:
	i = new Intent();
	if (lang != null) {
		MatchstixApplication.getInstance();
		SettingsFragment.defaultLanguage=lang;
		MatchstixApplication.setLocale(lang);
		i.putExtra(Constants.KEY_LANGUAGE,lang);
		addFragment();
	}
	break;
default:
	break;
}
}
}
