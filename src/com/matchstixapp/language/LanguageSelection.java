package com.matchstixapp.language;

import com.matchstixapp.MatchstixApplication;
import com.matchstixapp.R;
import com.matchstixapp.ui.adapter.SppinerAdapter;
import com.matchstixapp.ui.fragments.SettingsFragment;
import com.matchstixapp.utils.Constants;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

public class LanguageSelection extends Activity implements
		OnItemSelectedListener, OnClickListener {
	private Intent i;
	private String lang;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_language_selection);
		String[] options = getResources()
				.getStringArray(R.array.language_array);
		Spinner staticSpinner = (Spinner) findViewById(R.id.static_spinner);
		ArrayAdapter<String> myAdapter = new SppinerAdapter(this,
				R.layout.listview, options);
		staticSpinner.setAdapter(myAdapter);
		/*
		 * // Create an ArrayAdapter using the string array and a default
		 * spinner ArrayAdapter<String> staticAdapter = new
		 * ArrayAdapter<String>(this,R.array.language_array);
		 * ArrayAdapter<CharSequence> staticAdapter = ArrayAdapter
		 * .createFromResource(this, R.array.language_array,
		 * android.R.layout.simple_spinner_item); // Specify the layout to use
		 * when the list of choices appears staticAdapter
		 * .setDropDownViewResource(R.layout.simple_spinner_dropdown_item); //
		 * Apply the adapter to the spinner
		 * staticSpinner.setAdapter(staticAdapter);
		 */
	}

	@Override
	protected void onResume() {
		((Spinner) findViewById(R.id.static_spinner))
				.setOnItemSelectedListener(this);
		findViewById(R.id.id_action_bar_left_img_view).setOnClickListener(this);
		findViewById(R.id.ok).setOnClickListener(this);
		super.onResume();
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position,
			long id) {
		lang = parent.getItemAtPosition(position).toString();

	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.id_action_bar_left_img_view:
			finish();
			break;
		case R.id.ok:
			i = new Intent();
			if (lang != null) {
				MatchstixApplication.getInstance();
				if(lang!=null)
				SettingsFragment.defaultLanguage=lang;
				if(!getResources().getStringArray(R.array.language_array)[0].equalsIgnoreCase(SettingsFragment.defaultLanguage)){
					MatchstixApplication.setLocale(lang);
					i.putExtra(Constants.KEY_LANGUAGE,lang);
					setResult(Constants.RES_LANGUAGE_SELECTION, i);
				}
				finish();
			}
			break;
		default:
			break;
		}
	}
}
