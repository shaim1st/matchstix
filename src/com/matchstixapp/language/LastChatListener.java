package com.matchstixapp.language;

import com.matchstixapp.modal.Friend;

public interface LastChatListener {
	public void lastChatUpdate(Friend user,int position);
}
